/*
 * IPbusJavaLibrary project
 * 2019 (c) IFIC-CERN
 */
package ipbusjavalibrary.ipbus;

/**
 * RESEND packet constructor. Extends class Packet.
 *
 * @author Juan Valls (2016) Taken from Carlos Solans (CERN)
 * @author Carlos Solans {@literal (solans@cern.ch)}
 */
public class Resend extends Packet {

    /**
     * Constructor.
     */
    public Resend() {
        setType(PacketHeader.RESEND);
        setPacketID(0);
    }

    /**
     * Adds values to the input byte stream array. Overrides method of the
     * Packet abstract class.
     *
     * @param b Input byte array destination where to add data.
     * @param pos Byte array index position where to start to add byte contents.
     * @return the last index position in the input destination byte array.
     */
    @Override
    public Integer addBytes(byte[] b, Integer pos) {
        return pos;
    }

    /**
     * Parse byte stream. Overrides method of the Packet abstract class.
     *
     * @param b Input byte array destination where to set the data.
     * @param pos Byte array index position where to start to set byte contents.
     * @return the last index position in the input destination byte array.
     */
    @Override
    public Integer setBytes(byte[] b, Integer pos) {
        return pos;
    }

    @Override
    public void sent() {
    }
}
