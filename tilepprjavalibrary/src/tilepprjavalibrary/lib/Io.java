/* 
 *  TilePPrJava Library Project
 *  2019 (c) IFIC-CERN
 */
package tilepprjavalibrary.lib;

import java.util.ArrayList;

import ipbusjavalibrary.ipbus.Uhal;
import java.util.concurrent.ThreadPoolExecutor;
import tilepprjavalibrary.reg.PPrReg;

/**
 * Handle IPbus transactions through a client TCP Socket or a UDP
 * DatagramSocket.
 *
 * @author Juan Valls {@literal (valls@cern.ch)}
 * @author Fernando Carrió {@literal (fernando.carrio@cern.ch)}
 * @author Alberto Valero {@literal (alberto.valero@cern.ch)}
 */
public class Io {

    public Uhal client;

    public String protocol;

    /**
     * Constructor.
     */
    public Io() {
        client = null;
    }

    /**
     * Getter for the status flag of the Uhal constructor. The Uhal object is
     * defined upon a ipb_connect() method call.
     *
     * @return a Boolean with the status of Uhal constructor.
     */
    public Boolean getUhalObject() {
        if (client == null) {
            return false;
        } else {
            return client.getUhalReturn();
        }
    }

    /**
     * Getter for the client PPr Uhal object thread pool.
     *
     * @return a ThreadPoolExecutor object with the Uhal PPr thread pool.
     */
    public ThreadPoolExecutor getUhalExecutor() {
        if (client == null) {
            return null;
        } else {
            return client.getUhalExecutor();
        }
    }

    /**
     * Connects a client Socket (TCP) or DatagramSocket (UDP). Defines object of
     * the Uhal class.
     *
     * @param connstr Input string with TCP or UDP server and prot names.
     * @return boolean flag from the Uhal instance object constructor.
     */
    public synchronized Boolean connect(String connstr) {
        // Thread pool name for the ThreadPoolFactory.
        String uhalTF = "Uhal";

        if (PPrLib.getDebugPPr()) {
            System.out.println("Io.connect() -> calling Uhal with: " + connstr);
        }

        protocol = "udp";
        Integer pos1 = connstr.indexOf("://");
        if (pos1 != -1) {
            if (connstr.substring(0, pos1).contains("tcp")) {
                protocol = "tcp";
            }
        }

        client = new Uhal(connstr, uhalTF);

        if (PPrLib.getDebugPPr()) {
            System.out.println("Io.connect() -> Socket/DatagramSocket creation status: "
                    + client.getUhalReturn());
        }

        return client.getUhalReturn();
    }

    /**
     * Disconnects the client Socket (TCP) or DatagramSocket (UDP). Closes the
     * connection.
     *
     * @return
     */
    public synchronized Boolean disconnect() {
        return client.closeConnection();
    }

    /**
     * Synchronizes the IPbus instance connection with the remote host (server).
     * Sends an IPbus STATUS request packet to the server. Waits for a response
     * packet. Sets the server next expected Packet ID into the client Packet
     * ID.
     *
     * @return the synchonization status flag.
     */
    public synchronized Boolean syncConnection() {
        return client.sync();
    }

    /**
     * Tests IPbus instance connection with the PPr hardware by writing into its
     * DUMMY register.
     *
     * @return true upon success
     */
    public synchronized Boolean testConnection() {
        // Checks a DatagramSocket client exists.
        if (client == null) {
            return false;
        }

        // Writes a word on the client DatagramSocket and wait for acknowledge.
        Integer tw = 0x123456;
        Boolean ret = client.write(PPrReg.RO_DUMMY, tw);
        if (!ret) {
            return false;
        }

        // Now reads response from the server.
        Integer tr = client.read(PPrReg.RO_DUMMY);
        return (tw.equals(tr));
    }

    /**
     * Writes at an input address an input data value.
     *
     * @param address Input address word.
     * @param value Input data.
     * @return the IPbus status flag.
     */
    public synchronized Boolean write(Integer address, Integer value) {
        // Checks a DatagramSockect client is present.
        if (client == null) {
            return false;
        }
        return client.write(address, value);
    }

    /**
     * Writes at the input address the value "value1" and at address+1 the value
     * "value2".
     *
     * @param address Input address.
     * @param value1
     * @param value2
     * @return the IPbus status flag.
     */
    public synchronized Boolean write(Integer address, Integer value1, Integer value2) {
        // Checks whether a DatagramSockect client is present.
        if (client == null) {
            return false;
        }

        ArrayList<Integer> v = new ArrayList<>();
        v.add(value1);
        v.add(value2);

        if ("udp".equals(protocol)) {
            if (client.sync()) {
                return client.write(address, v, false);
            } else {
                return false;
            }
        } else if ("tcp".equals(protocol)) {
            return client.write(address, v, false);
        }

        return false;
    }

    /**
     *
     * @param address
     * @return
     */
    public synchronized Integer read(Integer address) {
        // Checks a Socket/DatagramSockect client is present.
        if (client == null) {
            return null;
        }

        return client.read(address);
    }

    /**
     * Reads from an address an ArrayList of required size (words).
     *
     * @param address Input address word.
     * @param size Input number of words to read.
     * @return the IPbus values read.
     */
    public synchronized ArrayList<Integer> read(Integer address, Integer size) {
        // Checks a Socket/DatagramSockect client is present.
        if (client == null) {
            return null;
        }

        return client.read(address, size);
    }

    /**
     * Reads from an address an ArrayList object of specified size (number of
     * words) using a FIFO upon condition. Returns ArrayList with data.
     *
     * @param address Input address word.
     * @param size Input number of words to ipb_read.
     * @param fifo True for non-incrementd READ (READFIFO). False for READ.
     * @return the IPbus values read.
     */
    public synchronized ArrayList<Integer> read(Integer address, Integer size, Boolean fifo) {
        // Checks a Socket/DatagramSockect client is present.
        if (client == null) {
            return null;
        }

        return client.read(address, size, fifo);
    }
}
