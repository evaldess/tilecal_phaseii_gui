/* 
 *  PPrDemo Test Bench Java Client
 *  2019 (c) IFIC-CERN
 */
package tilepprdemo;

import java.awt.Color;
import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import tilepprjavalibrary.lib.PPrLib;

/**
 *
 * @author Juan Valls {@literal (valls@cern.ch)}
 * @author Fernando Carrió {@literal (fernando.carrio@cern.ch)}
 * @author Alberto Valero {@literal (alberto.valero@cern.ch)}
 */
public class IPbusPanel extends javax.swing.JPanel {

    /**
     * Creates new form IPbusPanel
     *
     * @param pprlib
     */
    public IPbusPanel(PPrLib pprlib) {
        System.out.println("IPbusPanel -> constructor");

        // NetBeans components settings.
        initComponents();

        // NetBeans components modifications.
        initModifyComponents();

        // Set PPrLib class object as given by constructor argument.
        this.pprlib = pprlib;

        hostsStrArr = new ArrayList<>();
        portsStrArr = new ArrayList<>();

        hostsPPrStrArr = new ArrayList<>();
        portsPPrStrArr = new ArrayList<>();

        // Initializes label for IPbus connection status.
        LabelConnect.setBackground(Color.RED);

        // Defines Semaphore for PPr access.
        semaphoreConnect = new Semaphore(0);
        semaphoreConnect.release();

        // Initializes generic ArrayList for write/read operations.
        data = new ArrayList<>();

        // Set field values with NetBeans default text widgets.
        // Needed to define and set initial field values.
        get_all_parameters();

        // Define Runnables through lambda expressions.
        initDefineRunnables();

        // Defines instance of ThreadPoolExecutor class to create a thread pool.
        // A thread pool manages a collection of Runnable threads.   
        executor = new ThreadPoolExecutor(
                5,
                5,
                0L,
                TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>(),
                new TBThreadFactory("IPbusPanel"));
    }

    /**
     * Modify some of the Swing components.
     */
    private void initModifyComponents() {
        // Right-aligns text in JComboBoxes.
        ((javax.swing.JLabel) ComboBoxHost.getRenderer()).setHorizontalAlignment(javax.swing.JLabel.RIGHT);
        ((javax.swing.JLabel) ComboBoxPort.getRenderer()).setHorizontalAlignment(javax.swing.JLabel.RIGHT);
    }

    /**
     * Defines Runnables for threads. A Runnable object is an interface designed
     * to provide a common protocol for objects that wish to execute code while
     * they are active
     */
    private void initDefineRunnables() {
        RunnableReadFirmVer = () -> {
            readFirmVer();
        };

        RunnableConnect = () -> {
            connectPPr();
        };

        RunnableDisconnect = () -> {
            disconnectPPr();
        };

        RunnableTestConnection = () -> {
            testPPrConnection();
        };

        RunnableWrite = () -> {
            writePPr();
        };

        RunnableRead = () -> {
            readPPr();
        };
    }

    /**
     * Get hosts/ports as ArrayLists.
     */
    private synchronized void get_hosts() {
        hostsStrArr.clear();
        for (Integer i = 0; i < ComboBoxHost.getItemCount(); i++) {
            hostsStrArr.add(ComboBoxHost.getItemAt(i));
        }

        portsStrArr.clear();
        for (Integer i = 0; i < ComboBoxPort.getItemCount(); i++) {
            portsStrArr.add(ComboBoxPort.getItemAt(i));
        }

        hostsPPrStrArr.clear();
        for (Integer i = 0; i < ComboBoxHostPPr.getItemCount(); i++) {
            hostsPPrStrArr.add(ComboBoxHostPPr.getItemAt(i));
        }

        portsPPrStrArr.clear();
        for (Integer i = 0; i < ComboBoxPortPPr.getItemCount(); i++) {
            portsPPrStrArr.add(ComboBoxPortPPr.getItemAt(i));
        }

        host = ComboBoxHost.getSelectedItem().toString();
        port = ComboBoxPort.getSelectedItem().toString();
        hostPPr = ComboBoxHostPPr.getSelectedItem().toString();
        portPPr = ComboBoxPortPPr.getSelectedItem().toString();
    }

    /**
     * Set hosts/ports JComboBox elements with present field values.
     */
    public static synchronized void set_hosts() {
        ComboBoxHost.removeAllItems();
        for (Integer i = 0; i < hostsStrArr.size(); i++) {
            ComboBoxHost.addItem(hostsStrArr.get(i));
        }

        ComboBoxPort.removeAllItems();
        for (Integer i = 0; i < portsStrArr.size(); i++) {
            ComboBoxPort.addItem(portsStrArr.get(i));
        }

        ComboBoxHostPPr.removeAllItems();
        for (Integer i = 0; i < hostsPPrStrArr.size(); i++) {
            ComboBoxHostPPr.addItem(hostsPPrStrArr.get(i));
        }

        ComboBoxPortPPr.removeAllItems();
        for (Integer i = 0; i < portsPPrStrArr.size(); i++) {
            ComboBoxPortPPr.addItem(portsPPrStrArr.get(i));
        }

        ComboBoxHost.setSelectedItem(host);
        ComboBoxPort.setSelectedItem(port);
        ComboBoxHostPPr.setSelectedItem(hostPPr);
        ComboBoxPortPPr.setSelectedItem(portPPr);
    }

    /**
     * Get protocol connection type (TCP or UDP).
     */
    private synchronized void get_protocol() {
        protocol = ComboBoxProtocol.getSelectedItem().toString();
    }

    /**
     * Set protocol connection type (TCP or UDP).
     */
    public static synchronized void set_protocol() {
        ComboBoxProtocol.setSelectedItem(protocol);
    }

    /**
     * Get field values from text widgets.
     */
    private synchronized void get_all_parameters() {
        get_hosts();
        get_protocol();
    }

    /**
     * Set text widgets with present field values.
     */
    public static synchronized void set_all_parameters() {
        set_hosts();
        set_protocol();
    }

    /**
     * Reads PPr firmware version.
     */
    private synchronized void readFirmVer() {
        Integer ifirm = pprlib.ppr.get_firmware_version();
        if (ifirm == -1) {
            TextFieldPPrFirmVer.setText("Error");
        } else {
            TextFieldPPrFirmVer.setText(Integer.toHexString(ifirm));
            String fs = "%s 0x%08X (%d)";
            Console.print_cr(String.format(fs, "IPbusPanel.readFirmVer() -> ", ifirm, ifirm));
        }
    }

    /**
     * Start an IPbus connection with the PPr. Use a thread to connect with
     * remote host through a socket and test the connection.
     */
    public void connectPPr() {
        String connstr;

        get_hosts();

        get_protocol();

        if (null == protocol) {
            connstr = null;
        } else {
            switch (protocol) {
                case "UDP":
                    connstr = "udp://" + host + ":" + port + "?target=" + hostPPr + ":" + portPPr;
                    break;
                case "TCP":
                    connstr = "tcp://" + host + ":" + port + "?target=" + hostPPr + ":" + portPPr;
                    break;
                default:
                    connstr = null;
                    break;
            }
        }

        Console.print_cr("IPbusPanel.connectPPr() -> trying to create a connection...");
        if (DebugSettings.getVerbose()) {
            Console.print_cr("IPbusPanel.connectPPr() -> " + connstr);
        }

        // Tries to create the IPbus connection
        //if (!ppr.ipb_connect(connstr)) {
        if (!pprlib.io.connect(connstr)) {
            Console.print_cr("IPbusPanel.connectPPr() -> IPbus server PPr connection failed");
            LabelConnect.setBackground(Color.RED);
            LabelConnect.setText("not connected");

            // Releases the connection PPr semaphore.
            semaphoreConnect.release();

            return;
        }
        Console.print_cr("IPbusPanel.connectPPr() -> IPbus server PPr connection Ok");

        // Tries to synchronize the connection.
        // Only for UDP connections.
        if ("UDP".equals(protocol)) {
            Console.print_cr("IPbusPanel.connectPPr() -> trying synch with the PPr hardware...");
            if (!pprlib.io.syncConnection()) {
                LabelConnect.setBackground(Color.RED);
                LabelConnect.setText("not connected");

                // Releases the connection PPr semaphore.
                semaphoreConnect.release();
                Console.print_cr("IPbusPanel.connectPPr() -> sync connection with PPr: failed");
                Console.print_cr("IPbusPanel.connectPPr() -> not connected to "
                        + host + ":" + port);
                return;
            }
            Console.print_cr("IPbusPanel.connectPPr() -> sync connection with PPr: Ok");
        }

        // Tests the connection.
        Console.print_cr("IPbusPanel.connectPPr() -> testing connection with PPr...");
        //if (ppr.ipb_testConnection()) {
        if (pprlib.io.testConnection()) {
            Console.print_cr("IPbusPanel.connectPPr() -> connected to "
                    + host + ":" + port);

            // Updates PPr status label in IPbusServers panel.
            LabelConnect.setBackground(Color.GREEN);
            LabelConnect.setText("connected");

            // Updates PPr IPbus connection status label in MainFrame frame.
            MainFrame.LabelConnectMainFrame.setForeground(Color.GREEN.darker());
            MainFrame.LabelConnectMainFrame.setText("connected");

            // Reads PPr firmware version.
            //Integer ifirm = ppr.ppr_get_firmware_version();
            Integer ifirm = pprlib.ppr.get_firmware_version();
            if (ifirm == -1) {
                TextFieldPPrFirmVer.setText("Error");
            } else {
                TextFieldPPrFirmVer.setText(Integer.toHexString(ifirm));
            }
            // Actions on test connection failure...
        } else {
            Console.print_cr("IPbusPanel.connectPPr() -> not connected to "
                    + host + ":" + port);

            // Updates PPr connection status labels in IPbusServers frame.
            LabelConnect.setBackground(Color.RED);
            LabelConnect.setText("not connected");

            // Updates PPr status label in MainFrame.
            MainFrame.LabelConnectMainFrame.setForeground(Color.red);
            MainFrame.LabelConnectMainFrame.setText("not connected");

            // Releases the connection PPr semaphore.
            semaphoreConnect.release();

            return;
        }

        // Releases the connection PPr semaphore.
        semaphoreConnect.release();

        ConnectFlag = true;

        if (PPrDemoCRCChecks.CheckBoxReadUpdate.isSelected()) {
            PPrDemoCRCChecks.CheckBoxReadUpdate.setSelected(false);
        }

        if (PPrDemoLinksStatus.CheckBoxReadUpdate.isSelected()) {
            PPrDemoLinksStatus.CheckBoxReadUpdate.setSelected(false);
        }
    }

    /**
     * Stops IPbus connection with the PPr hardware. Closes the connection.
     */
    public void disconnectPPr() {
        // Closes the Uhal Socket/DataSocket.
        pprlib.io.disconnect();

        // Updates PPr status label in IPbusServers panel.
        LabelConnect.setBackground(Color.RED);
        LabelConnect.setText("not connected");

        // Updates PPr status label in MainFrame frame.
        MainFrame.LabelConnectMainFrame.setForeground(Color.red);
        MainFrame.LabelConnectMainFrame.setText("not connected");

        ConnectFlag = false;

        Console.print_cr("IPbusPanel.disconnectPPr() -> PPr server connection terminated");

        if (PPrDemoCRCChecks.CheckBoxReadUpdate.isSelected()) {
            PPrDemoCRCChecks.CheckBoxReadUpdate.setSelected(false);
        }

        if (PPrDemoLinksStatus.CheckBoxReadUpdate.isSelected()) {
            PPrDemoLinksStatus.CheckBoxReadUpdate.setSelected(false);
        }
    }

    /**
     * Test IPbus connection with the PPr hardware.
     */
    public void testPPrConnection() {
        Console.print_cr("IPbusPanel.testPPrConnection() -> testing PPr server connection...");

        // Tries to synchronize the connection.
        // Only for UDP connections.
        if ("UDP".equals(protocol)) {
            Console.print_cr("IPbusPanel.testPPrConnection() -> trying synch with the PPr hardware...");
            if (!pprlib.io.syncConnection()) {
                LabelConnect.setBackground(Color.RED);
                LabelConnect.setText("not connected");
                Console.print_cr("IPbusPanel.testPPrConnection() -> sync connection with PPr: failed");
                Console.print_cr("IPbusPanel.testPPrConnection() -> not connected to: "
                        + host + ":" + port);
                return;
            }
            Console.print_cr("IPbusPanel.testPPrConnection() -> sync connection with PPr: Ok");
        }

        //Boolean returnio = ppr.ipb_testConnection();
        Boolean returnio = pprlib.io.testConnection();
        Console.print_cr("IPbusPanel.testPPrConnection() -> PPr hardware connection: "
                + (returnio ? "Ok" : "Fail"));

        if (!returnio) {
            // Updates PPr status label in IPbusServers frame.
            LabelConnect.setBackground(Color.RED);
            LabelConnect.setText("not connected");

            // Updates PPr status label in MainFrame panel.
            MainFrame.LabelConnectMainFrame.setForeground(Color.red);
            MainFrame.LabelConnectMainFrame.setText("not connected");

            ConnectFlag = false;
        }
    }

    /**
     * Writes to a PPr register.
     */
    private synchronized void writePPr() {
        // Gets the address and value to write.
        Integer address = Integer.valueOf(TextFieldWriteAddress.getText(), 16);
        Integer value = Integer.valueOf(TextFieldWriteValue.getText(), 16);

        Console.print_cr(String.format("IPbusPanel.writePPr() -> writing to address: 0X%08X value: 0X%08X (%d)",
                address, value, value));

        if (!pprlib.io.write(address, value)) {
            Console.print_cr("IPbusPanel.writePPr() -> error writing...");
        }
    }

    /**
     * Reads PPr register. Output is sent to the console screen.
     */
    private synchronized void readPPr() {
        // Gets the address to read and the number of words.
        Integer address = Integer.valueOf(TextFieldReadAddress.getText(), 16);
        Integer size = Integer.valueOf(TextFieldReadSize.getText());

        Console.print_cr(String.format("IPbusPanel.readPPr() -> reading %d words from address: 0X%08X",
                size, address));

        // Reads from address the selected size number of words.
        data = pprlib.io.read(address, size, CheckBoxReadFIFO.isSelected());
        if (!(data == null)) {
            dumpReadData();
        } else {
            Console.print_cr("IPbusPanel.readPPr() -> no data to read...");
        }
    }

    /**
     * Updates console panel with available data read from PPr register.
     */
    private void dumpReadData() {
        // If mask checkbox is selected and the mask is not null apply
        // the mask to the data. Default mask is 1.
        Integer mask = 0xFFFFFFFF;
        if (CheckBoxApplyMask.isSelected()
                && !TextFieldReadMask.getText().equals("")) {
            mask = Integer.valueOf(TextFieldReadMask.getText(), 16);
        }

        // Check we have data to show. Otherwise return and do nothing.
        if (data == null) {
            return;
        }

        Console.print_cr("IPbusPanel.dumpReadData() -> dumping data...");

        String f = "=> 0x%08X (%d)";
        for (Integer i = 0; i < data.size(); i++) {
            Integer value = data.get(i) & mask;
            Console.print_cr(String.format(f, value, value));
        }
    }

    /**
     * Getter method for private field ConnectFlag.
     *
     * @return the connection status flag.
     */
    public static Boolean getConnectFlag() {
        return ConnectFlag;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    private void initComponents() {//GEN-BEGIN:initComponents
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel3 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel12 = new javax.swing.JPanel();
        jPanel13 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        ComboBoxHost = new javax.swing.JComboBox<>();
        jPanel14 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        ComboBoxPort = new javax.swing.JComboBox<>();
        jPanel26 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        ComboBoxHostPPr = new javax.swing.JComboBox<>();
        jPanel15 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        ComboBoxProtocol = new javax.swing.JComboBox<>();
        jPanel27 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        ComboBoxPortPPr = new javax.swing.JComboBox<>();
        jPanel16 = new javax.swing.JPanel();
        ButtonConnect = new javax.swing.JButton();
        LabelConnect = new javax.swing.JLabel();
        ButtonTestConnection = new javax.swing.JButton();
        ButtonDisconnect = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        PanelPPrVC707Info = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        TextFieldWriteAddress = new javax.swing.JTextField();
        ButtonWrite = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        TextFieldWriteValue = new javax.swing.JTextField();
        jPanel8 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        TextFieldReadAddress = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        TextFieldReadSize = new javax.swing.JTextField();
        CheckBoxReadFIFO = new javax.swing.JCheckBox();
        ButtonRead = new javax.swing.JButton();
        jPanel24 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        TextFieldReadMask = new javax.swing.JTextField();
        CheckBoxApplyMask = new javax.swing.JCheckBox();
        jPanel21 = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        jTextField5 = new javax.swing.JTextField();
        jCheckBox2 = new javax.swing.JCheckBox();
        jButton3 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jPanel11 = new javax.swing.JPanel();
        jPanel17 = new javax.swing.JPanel();
        jPanel18 = new javax.swing.JPanel();
        jPanel19 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jPanel22 = new javax.swing.JPanel();
        jPanel25 = new javax.swing.JPanel();
        jPanel20 = new javax.swing.JPanel();
        TextFieldPPrFirmVer = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jPanel23 = new javax.swing.JPanel();
        ButtonReadFirmVer = new javax.swing.JButton();
        jPanel28 = new javax.swing.JPanel();
        jPanel29 = new javax.swing.JPanel();
        ButtonSaveAsDefaults = new javax.swing.JButton();
        ButtonLoadDefaults = new javax.swing.JButton();
        ButtonShowDefaults = new javax.swing.JButton();
        jPanel30 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();

        java.awt.GridBagLayout jPanel3Layout = new java.awt.GridBagLayout();
        jPanel3Layout.columnWidths = new int[] {0};
        jPanel3Layout.rowHeights = new int[] {0, 15, 0, 15, 0, 15, 0, 15, 0, 15, 0, 15, 0};
        jPanel3.setLayout(jPanel3Layout);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("IPbus Server"));
        java.awt.GridBagLayout jPanel1Layout = new java.awt.GridBagLayout();
        jPanel1Layout.columnWidths = new int[] {0};
        jPanel1Layout.rowHeights = new int[] {0, 15, 0, 15, 0};
        jPanel1.setLayout(jPanel1Layout);

        jPanel2.setLayout(new java.awt.GridBagLayout());

        java.awt.GridBagLayout jPanel12Layout = new java.awt.GridBagLayout();
        jPanel12Layout.columnWidths = new int[] {0, 18, 0, 18, 0, 18, 0, 18, 0};
        jPanel12Layout.rowHeights = new int[] {0};
        jPanel12.setLayout(jPanel12Layout);

        java.awt.GridBagLayout jPanel13Layout = new java.awt.GridBagLayout();
        jPanel13Layout.columnWidths = new int[] {0, 5, 0};
        jPanel13Layout.rowHeights = new int[] {0};
        jPanel13.setLayout(jPanel13Layout);

        jLabel7.setText("Host:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel13.add(jLabel7, gridBagConstraints);

        ComboBoxHost.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "tical31.ific.uv.es", "tical02.ific.uv.es", "localhost", "192.168.0.1" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel13.add(ComboBoxHost, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel12.add(jPanel13, gridBagConstraints);

        java.awt.GridBagLayout jPanel14Layout = new java.awt.GridBagLayout();
        jPanel14Layout.columnWidths = new int[] {0, 5, 0};
        jPanel14Layout.rowHeights = new int[] {0};
        jPanel14.setLayout(jPanel14Layout);

        jLabel8.setText("Port:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel14.add(jLabel8, gridBagConstraints);

        ComboBoxPort.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "10203", "50001", "50002", "50003" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel14.add(ComboBoxPort, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel12.add(jPanel14, gridBagConstraints);

        java.awt.GridBagLayout jPanel26Layout = new java.awt.GridBagLayout();
        jPanel26Layout.columnWidths = new int[] {0, 5, 0};
        jPanel26Layout.rowHeights = new int[] {0};
        jPanel26.setLayout(jPanel26Layout);

        jLabel2.setText("PPr:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel26.add(jLabel2, gridBagConstraints);

        ComboBoxHostPPr.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "192.168.0.1" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel26.add(ComboBoxHostPPr, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel12.add(jPanel26, gridBagConstraints);

        java.awt.GridBagLayout jPanel15Layout = new java.awt.GridBagLayout();
        jPanel15Layout.columnWidths = new int[] {0, 5, 0};
        jPanel15Layout.rowHeights = new int[] {0};
        jPanel15.setLayout(jPanel15Layout);

        jLabel9.setText("Protocol:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel15.add(jLabel9, gridBagConstraints);

        ComboBoxProtocol.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "UDP", "TCP" }));
        ComboBoxProtocol.setSelectedIndex(1);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel15.add(ComboBoxProtocol, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel12.add(jPanel15, gridBagConstraints);

        java.awt.GridBagLayout jPanel27Layout = new java.awt.GridBagLayout();
        jPanel27Layout.columnWidths = new int[] {0, 5, 0};
        jPanel27Layout.rowHeights = new int[] {0};
        jPanel27.setLayout(jPanel27Layout);

        jLabel6.setText("Port:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel27.add(jLabel6, gridBagConstraints);

        ComboBoxPortPPr.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "50001", "50002" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel27.add(ComboBoxPortPPr, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel12.add(jPanel27, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel2.add(jPanel12, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel1.add(jPanel2, gridBagConstraints);

        java.awt.GridBagLayout jPanel16Layout = new java.awt.GridBagLayout();
        jPanel16Layout.columnWidths = new int[] {0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0};
        jPanel16Layout.rowHeights = new int[] {0};
        jPanel16.setLayout(jPanel16Layout);

        ButtonConnect.setText("Connect");
        ButtonConnect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonConnectActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel16.add(ButtonConnect, gridBagConstraints);

        LabelConnect.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelConnect.setText("Not Connected");
        LabelConnect.setOpaque(true);
        LabelConnect.setPreferredSize(new java.awt.Dimension(106, 29));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel16.add(LabelConnect, gridBagConstraints);

        ButtonTestConnection.setText("Test Connection");
        ButtonTestConnection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonTestConnectionActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel16.add(ButtonTestConnection, gridBagConstraints);

        ButtonDisconnect.setText("Stop Connection");
        ButtonDisconnect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonDisconnectActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 0;
        jPanel16.add(ButtonDisconnect, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        jPanel1.add(jPanel16, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel3.add(jPanel1, gridBagConstraints);

        jPanel4.setLayout(new java.awt.GridBagLayout());

        PanelPPrVC707Info.setBorder(javax.swing.BorderFactory.createTitledBorder("PPr Hardware Access"));
        java.awt.GridBagLayout PanelPPrVC707InfoLayout = new java.awt.GridBagLayout();
        PanelPPrVC707InfoLayout.columnWidths = new int[] {0, 13, 0, 13, 0};
        PanelPPrVC707InfoLayout.rowHeights = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0};
        PanelPPrVC707Info.setLayout(PanelPPrVC707InfoLayout);

        java.awt.GridBagLayout jPanel5Layout = new java.awt.GridBagLayout();
        jPanel5Layout.columnWidths = new int[] {0};
        jPanel5Layout.rowHeights = new int[] {0, 5, 0};
        jPanel5.setLayout(jPanel5Layout);

        jPanel6.setLayout(new java.awt.GridBagLayout());

        jLabel3.setText("<html><b>Write</b></html>");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel6.add(jLabel3, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel5.add(jPanel6, gridBagConstraints);

        java.awt.GridBagLayout jPanel7Layout = new java.awt.GridBagLayout();
        jPanel7Layout.columnWidths = new int[] {0, 13, 0, 13, 0, 13, 0, 13, 0};
        jPanel7Layout.rowHeights = new int[] {0};
        jPanel7.setLayout(jPanel7Layout);

        jLabel4.setText("Address (0x):");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel7.add(jLabel4, gridBagConstraints);

        TextFieldWriteAddress.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldWriteAddress.setText("0");
        TextFieldWriteAddress.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel7.add(TextFieldWriteAddress, gridBagConstraints);

        ButtonWrite.setText("Write");
        ButtonWrite.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonWriteActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel7.add(ButtonWrite, gridBagConstraints);

        jLabel5.setText("Value (0x):");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel7.add(jLabel5, gridBagConstraints);

        TextFieldWriteValue.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldWriteValue.setText("0");
        TextFieldWriteValue.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel7.add(TextFieldWriteValue, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel5.add(jPanel7, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        PanelPPrVC707Info.add(jPanel5, gridBagConstraints);

        java.awt.GridBagLayout jPanel8Layout = new java.awt.GridBagLayout();
        jPanel8Layout.columnWidths = new int[] {0};
        jPanel8Layout.rowHeights = new int[] {0, 5, 0, 5, 0, 5, 0};
        jPanel8.setLayout(jPanel8Layout);

        jPanel9.setLayout(new java.awt.GridBagLayout());

        jLabel13.setText("<html><b>Read</b></html>");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel9.add(jLabel13, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel8.add(jPanel9, gridBagConstraints);

        java.awt.GridBagLayout jPanel10Layout = new java.awt.GridBagLayout();
        jPanel10Layout.columnWidths = new int[] {0, 13, 0, 13, 0, 13, 0, 13, 0, 13, 0, 13, 0, 13, 0};
        jPanel10Layout.rowHeights = new int[] {0};
        jPanel10.setLayout(jPanel10Layout);

        jLabel14.setText("Address (0x):");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel10.add(jLabel14, gridBagConstraints);

        TextFieldReadAddress.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldReadAddress.setText("0");
        TextFieldReadAddress.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel10.add(TextFieldReadAddress, gridBagConstraints);

        jLabel15.setText("Size:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel10.add(jLabel15, gridBagConstraints);

        TextFieldReadSize.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldReadSize.setText("1");
        TextFieldReadSize.setPreferredSize(new java.awt.Dimension(50, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel10.add(TextFieldReadSize, gridBagConstraints);

        CheckBoxReadFIFO.setText("FIFO");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel10.add(CheckBoxReadFIFO, gridBagConstraints);

        ButtonRead.setText("Read");
        ButtonRead.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonReadActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 0;
        jPanel10.add(ButtonRead, gridBagConstraints);

        jPanel24.setLayout(new java.awt.GridBagLayout());

        jLabel1.setText("Mask (0x):");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel24.add(jLabel1, gridBagConstraints);

        TextFieldReadMask.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldReadMask.setText("FF");
        TextFieldReadMask.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel24.add(TextFieldReadMask, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 0;
        jPanel10.add(jPanel24, gridBagConstraints);

        CheckBoxApplyMask.setText("Apply Mask");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 0;
        jPanel10.add(CheckBoxApplyMask, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel8.add(jPanel10, gridBagConstraints);

        java.awt.GridBagLayout jPanel21Layout = new java.awt.GridBagLayout();
        jPanel21Layout.columnWidths = new int[] {0, 13, 0, 13, 0, 13, 0, 13, 0};
        jPanel21Layout.rowHeights = new int[] {0};
        jPanel21.setLayout(jPanel21Layout);

        jLabel16.setText("Read every (ms):");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel21.add(jLabel16, gridBagConstraints);

        jTextField5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField5.setText("0");
        jTextField5.setPreferredSize(new java.awt.Dimension(80, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel21.add(jTextField5, gridBagConstraints);

        jCheckBox2.setText("Update on Change");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel21.add(jCheckBox2, gridBagConstraints);

        jButton3.setText("Start");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel21.add(jButton3, gridBagConstraints);

        jButton7.setText("Stop");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel21.add(jButton7, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel8.add(jPanel21, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        PanelPPrVC707Info.add(jPanel8, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel4.add(PanelPPrVC707Info, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel3.add(jPanel4, gridBagConstraints);

        jPanel11.setLayout(new java.awt.GridBagLayout());

        jPanel17.setLayout(new java.awt.GridBagLayout());

        java.awt.GridBagLayout jPanel18Layout = new java.awt.GridBagLayout();
        jPanel18Layout.columnWidths = new int[] {0};
        jPanel18Layout.rowHeights = new int[] {0, 5, 0, 5, 0};
        jPanel18.setLayout(jPanel18Layout);

        jPanel19.setLayout(new java.awt.GridBagLayout());

        jLabel11.setText("<html><b>PPr Firmware Version</b></html>");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel19.add(jLabel11, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel18.add(jPanel19, gridBagConstraints);

        java.awt.GridBagLayout jPanel22Layout = new java.awt.GridBagLayout();
        jPanel22Layout.columnWidths = new int[] {0, 20, 0};
        jPanel22Layout.rowHeights = new int[] {0};
        jPanel22.setLayout(jPanel22Layout);

        jPanel25.setLayout(new java.awt.GridBagLayout());

        java.awt.GridBagLayout jPanel20Layout = new java.awt.GridBagLayout();
        jPanel20Layout.columnWidths = new int[] {0, 8, 0};
        jPanel20Layout.rowHeights = new int[] {0};
        jPanel20.setLayout(jPanel20Layout);

        TextFieldPPrFirmVer.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPPrFirmVer.setText("0");
        TextFieldPPrFirmVer.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel20.add(TextFieldPPrFirmVer, gridBagConstraints);

        jLabel12.setText("0x");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel20.add(jLabel12, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel25.add(jPanel20, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel22.add(jPanel25, gridBagConstraints);

        jPanel23.setLayout(new java.awt.GridBagLayout());

        ButtonReadFirmVer.setText("Read");
        ButtonReadFirmVer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonReadFirmVerActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel23.add(ButtonReadFirmVer, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel22.add(jPanel23, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        jPanel18.add(jPanel22, gridBagConstraints);

        jPanel17.add(jPanel18, new java.awt.GridBagConstraints());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel11.add(jPanel17, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel3.add(jPanel11, gridBagConstraints);

        java.awt.GridBagLayout jPanel28Layout = new java.awt.GridBagLayout();
        jPanel28Layout.columnWidths = new int[] {0};
        jPanel28Layout.rowHeights = new int[] {0, 10, 0};
        jPanel28.setLayout(jPanel28Layout);

        java.awt.GridBagLayout jPanel29Layout = new java.awt.GridBagLayout();
        jPanel29Layout.columnWidths = new int[] {0, 10, 0, 10, 0};
        jPanel29Layout.rowHeights = new int[] {0};
        jPanel29.setLayout(jPanel29Layout);

        ButtonSaveAsDefaults.setText("<html><div style='text-align: center;'><body>Save As Defaults<br>Settings</body></div></html>");
        ButtonSaveAsDefaults.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonSaveAsDefaultsActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel29.add(ButtonSaveAsDefaults, gridBagConstraints);

        ButtonLoadDefaults.setText("<html><div style='text-align: center;'><body>Load Defaults<br>Settings</body></div></html>");
        ButtonLoadDefaults.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonLoadDefaultsActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel29.add(ButtonLoadDefaults, gridBagConstraints);

        ButtonShowDefaults.setText("<html><div style='text-align: center;'><body>Show Defaults<br>Settings</body></div></html>");
        ButtonShowDefaults.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonShowDefaultsActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel29.add(ButtonShowDefaults, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel28.add(jPanel29, gridBagConstraints);

        jPanel30.setLayout(new java.awt.GridBagLayout());

        jLabel10.setText("<html><div style='text-align: center;'><body>To change default server IPbus hosts/ports and PPr address/port settings<br>edit the <b>tilepprdemo.properties</b> file in the running directory<br>and click on <b>Load  Defaults Settings</b></body></div></html>\n");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel30.add(jLabel10, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel28.add(jPanel30, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 12;
        jPanel3.add(jPanel28, gridBagConstraints);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(51, 51, 51)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(147, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(71, 71, 71)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }//GEN-END:initComponents

    /**
     * Starts a new IPbus connection with the PPr hardware. If no connection
     * tries to set it. Acquires a permit from connection semaphore only if one
     * is available at the time of invocation and returns immediately, with the
     * value true, reducing the number of available permits by one. Then starts
     * a thread.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonConnectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonConnectActionPerformed
        if (!ConnectFlag && semaphoreConnect.tryAcquire()) {
            try {
                executor.execute(RunnableConnect);
            } catch (RejectedExecutionException e) {
                Console.print_cr("ButtonSartIPbusPPrConnectionActionPerformed() -> RejectedExecutionException = "
                        + e.getMessage());

            } catch (NullPointerException e) {
                Console.print_cr("ButtonSartIPbusPPrConnectionActionPerformed() -> NullPointerException = "
                        + e.getMessage());
            }
        }
    }//GEN-LAST:event_ButtonConnectActionPerformed

    /**
     * Tests the IPbus connection with the PPr hardware.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonTestConnectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonTestConnectionActionPerformed
        if (!ConnectFlag) {
            Console.print_cr("IPbusPanel -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnableTestConnection);
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonTestIPbusPPrConnectionActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonTestIPbusPPrConnectionActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonTestConnectionActionPerformed

    /**
     * Disconnects the IPbus connection with the PPr hardware.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonDisconnectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonDisconnectActionPerformed
        if (ConnectFlag) {
            try {
                executor.execute(RunnableDisconnect);
            } catch (RejectedExecutionException e) {
                Console.print_cr("ButtonStopIPbusVMEConnectionActionPerformed() -> RejectedExecutionException = "
                        + e.getMessage());
            } catch (NullPointerException e) {
                Console.print_cr("ButtonStopIPbusVMEConnectionActionPerformed() -> NullPointerException = "
                        + e.getMessage());
            }
        }
    }//GEN-LAST:event_ButtonDisconnectActionPerformed

    /**
     * Writes a PPr register.
     *
     * @param evt ActionEvent object.
     */
    private void ButtonWriteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonWriteActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("IPbusPanel -> no IPbus PPr connection");
            return;
        }

        if ((TextFieldWriteAddress.getText().equals(""))
                || (TextFieldWriteValue.getText().equals(""))) {
            Console.print_cr("IPbusPanel -> missing write address or write value");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnableWrite);
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonWriteActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonWriteActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonWriteActionPerformed

    /**
     * Reads a PPr register.
     *
     * @param evt ActionEvent object.
     */
    private void ButtonReadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonReadActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("IPbusPanel() -> no IPbus PPr connection");
            return;
        }

        if ((TextFieldReadAddress.getText().equals(""))
                || (TextFieldReadSize.getText().equals(""))) {
            Console.print_cr("IPbusPanel() -> missing read address or read size value");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnableRead);
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonReadActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonReadActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonReadActionPerformed

    /**
     * Reads the PPr firmware version.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonReadFirmVerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonReadFirmVerActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("IPbusPanel -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnableReadFirmVer);
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonReadFirmVerActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonReadFirmVerActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonReadFirmVerActionPerformed

    /**
     * First gets all field values from text widgets. Then sets field values in
     * the properties configuration file.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonSaveAsDefaultsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonSaveAsDefaultsActionPerformed
        Console.print_cr("IPbusPanel -> saving field values in properties file: " + Defaults.propsFile);
        get_all_parameters();
        Defaults.saveIPbusProperties();
    }//GEN-LAST:event_ButtonSaveAsDefaultsActionPerformed

    /**
     * Loads default pedestal run parameter values from properties configuration
     * file into field values. Then fills all text widgets with field values.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonLoadDefaultsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonLoadDefaultsActionPerformed
        Console.print_cr("IPbusPanel -> loading default values from properties file: " + Defaults.propsFile);
        Defaults.loadIPbusProperties();
        set_all_parameters();
    }//GEN-LAST:event_ButtonLoadDefaultsActionPerformed

    /**
     * Shows default values from properties configuration file.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonShowDefaultsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonShowDefaultsActionPerformed
        Defaults.showProperties();
    }//GEN-LAST:event_ButtonShowDefaultsActionPerformed

    /**
     * Declares a PPr class object to communicate with PPr IPbus server.
     */
    private PPrLib pprlib;

    /**
     * Declares a ThreadPoolExecutor object. It executes each submitted task
     * using one of possibly several pooled threads, normally configured using
     * Executors factory methods.
     */
    public static ThreadPoolExecutor executor;

    /**
     * Declares Runnable for readFirmVer() method.
     */
    Runnable RunnableReadFirmVer;

    /**
     * Declares Runnable for pprConnect() method.
     */
    Runnable RunnableConnect;

    /**
     * Declares Runnable for pprDisconnect() method.
     */
    Runnable RunnableDisconnect;

    /**
     * Declares Runnable for pprTestConnection() method.
     */
    Runnable RunnableTestConnection;

    /**
     * Declares Runnable for write action.
     */
    Runnable RunnableWrite;

    /**
     * Declares Runnable for read action.
     */
    Runnable RunnableRead;

    /**
     * Selected ArrayList with hosts.
     */
    public static ArrayList<String> hostsStrArr;

    /**
     * Selected ArrayList with ports.
     */
    public static ArrayList<String> portsStrArr;

    /**
     * Selected ArrayList with PPr hosts.
     */
    public static ArrayList<String> hostsPPrStrArr;

    /**
     * Selected ArrayList with PPr ports.
     */
    public static ArrayList<String> portsPPrStrArr;

    /**
     * Selected protocol.
     */
    public static String protocol;

    /**
     * Selected host.
     */
    public static String host;

    /**
     * Selected port.
     */
    public static String port;

    /**
     * Selected PPr host.
     */
    public static String hostPPr;

    /**
     * Selected PPr port.
     */
    public static String portPPr;

    /**
     * Generic Integer ArrayList for read/write operations.
     */
    private ArrayList<Integer> data;

    /**
     * Flag to define the IPbus PPr connection status.
     */
    private static Boolean ConnectFlag = false;

    /**
     * Declares Semaphore for PPr connection.
     */
    private final Semaphore semaphoreConnect;

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ButtonConnect;
    private javax.swing.JButton ButtonDisconnect;
    private javax.swing.JButton ButtonLoadDefaults;
    private javax.swing.JButton ButtonRead;
    private javax.swing.JButton ButtonReadFirmVer;
    private javax.swing.JButton ButtonSaveAsDefaults;
    private javax.swing.JButton ButtonShowDefaults;
    private javax.swing.JButton ButtonTestConnection;
    private javax.swing.JButton ButtonWrite;
    private javax.swing.JCheckBox CheckBoxApplyMask;
    private javax.swing.JCheckBox CheckBoxReadFIFO;
    public static javax.swing.JComboBox<String> ComboBoxHost;
    public static javax.swing.JComboBox<String> ComboBoxHostPPr;
    public static javax.swing.JComboBox<String
    > ComboBoxPort;
    public static javax.swing.JComboBox<String> ComboBoxPortPPr;
    public static javax.swing.JComboBox<String> ComboBoxProtocol;
    private javax.swing.JLabel LabelConnect;
    private javax.swing.JPanel PanelPPrVC707Info;
    private javax.swing.JTextField TextFieldPPrFirmVer;
    private javax.swing.JTextField TextFieldReadAddress;
    private javax.swing.JTextField TextFieldReadMask;
    private javax.swing.JTextField TextFieldReadSize;
    private javax.swing.JTextField TextFieldWriteAddress;
    private javax.swing.JTextField TextFieldWriteValue;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton7;
    private javax.swing.JCheckBox jCheckBox2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel23;
    private javax.swing.JPanel jPanel24;
    private javax.swing.JPanel jPanel25;
    private javax.swing.JPanel jPanel26;
    private javax.swing.JPanel jPanel27;
    private javax.swing.JPanel jPanel28;
    private javax.swing.JPanel jPanel29;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel30;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JTextField jTextField5;
    // End of variables declaration//GEN-END:variables
}
