/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tilepprjavalibrary.lib;

import java.util.ArrayList;

import ipbusjavalibrary.utilities.ByteTools;
import static ipbusjavalibrary.utilities.ByteTools.setBit;

import tilepprjavalibrary.reg.PPrReg;

/**
 *
 * @author Juan Valls {@literal (valls@cern.ch)}
 * @author Fernando Carrió {@literal (fernando.carrio@cern.ch)}
 * @author Alberto Valero {@literal (alberto.valero@cern.ch)}
 */
public class PPr {

    /**
     *
     */
    public static Io io;

    public PPr(Io io) {
        PPr.io = io;
    }

    /**
     * Returns PPr Firmware Readout Format Version register (R only).
     *
     * @return the PPr firmware version. -1 on Io error.
     */
    public synchronized Integer get_firmware_version() {
        return io.read(PPrReg.RO_FIRMWARE_VERSION);
    }

    /**
     * Reads the PPr Global TTC Configuration register (R/W).
     *
     * @return the IPbus read value.
     */
    public synchronized Integer get_global_TTC() {
        return io.read(PPrReg.RO_GLOBAL_TTC);
    }

    /**
     * Reads the PPr Global TTC Internal/External bit.
     *
     * @return he IPbus read value.
     */
    public synchronized Integer get_global_TTC_internal() {
        Integer val = io.read(PPrReg.RO_GLOBAL_TTC);
        if (val == null) {
            return null;
        }

        return ByteTools.getBit(22, val);
    }

    /**
     * Reads the PPr Global TTC Enable max BCID bit.
     *
     * @return he IPbus read value.
     */
    public synchronized Integer get_global_TTC_enable_maxBCID() {
        Integer val = io.read(PPrReg.RO_GLOBAL_TTC);
        if (val == null) {
            return null;
        }

        return ByteTools.getBit(26, val);
    }

    /**
     * Writes the PPr Global TTC Configuration register (R/W).
     *
     * @param val Input value to write.
     * @return the IPbus write status.
     */
    public synchronized Boolean set_global_TTC(Integer val) {
        return io.write(PPrReg.RO_GLOBAL_TTC, val);
    }

    /**
     * Sets Internal TTC bit in the PPr Global TTC Configuration register.
     *
     * @return
     */
    public synchronized Boolean set_global_TTC_internal() {
        Integer val = io.read(PPrReg.RO_GLOBAL_TTC);
        if (val == -1) {
            return false;
        }

        return io.write(PPrReg.RO_GLOBAL_TTC, val & (0 << PPrReg.RO_GLOBAL_TTC_INT_OFFSET));
    }

    /**
     * Sets External TTC bit in the PPr Global TTC Configuration register.
     *
     * @return
     */
    public synchronized Boolean set_global_TTC_external() {
        Integer val = io.read(PPrReg.RO_GLOBAL_TTC);
        if (val == -1) {
            return false;
        }

        return io.write(PPrReg.RO_GLOBAL_TTC, val & (1 << PPrReg.RO_GLOBAL_TTC_INT_OFFSET));
    }

    /**
     * Reads the PPr Global Trigger Configuration register (R/W).
     *
     * @return
     */
    public synchronized Integer get_global_trigger() {
        return io.read(PPrReg.RO_GLOBAL_TRIGGER);
    }

    /**
     * Reads rate (orbit units) from the PPr Global Trigger Configuration
     * register.
     *
     * @return
     */
    public synchronized Integer get_global_trigger_rate_orbits() {
        Integer val = io.read(PPrReg.RO_GLOBAL_TRIGGER);
        return (val >> 25) & 0x000007F;
    }

    /**
     * Reads BCID for L1A from the PPr Global Trigger Configuration register.
     *
     * @return
     */
    public synchronized Integer get_global_trigger_BCID_for_L1A() {
        Integer val = io.read(PPrReg.RO_GLOBAL_TRIGGER);
        return (val >> 13) & 0x00000FFF;
    }

    /**
     * Reads BCID for L1A from the PPr Global Trigger Configuration register.
     *
     * @return
     */
    public synchronized Integer get_global_trigger_L1A_delay() {
        Integer val = io.read(PPrReg.RO_GLOBAL_TRIGGER);
        return (val >> 4) & 0x000001FF;
    }

    /**
     * Reads manual L1A from the PPr Global Trigger Configuration register.
     *
     * @return
     */
    public synchronized Integer get_global_trigger_manual_L1A() {
        Integer val = io.read(PPrReg.RO_GLOBAL_TRIGGER);
        return (val >> 1) & 0x00000001;
    }

    /**
     * Reads enable trigger from the PPr Global Trigger Configuration register.
     *
     * @return
     */
    public synchronized Integer get_global_trigger_enable_trg() {
        Integer val = io.read(PPrReg.RO_GLOBAL_TRIGGER);
        return (val >> 2) & 0x00000001;
    }

    /**
     * Reads enable deadtime from the PPr Global Trigger Configuration register.
     *
     * @return
     */
    public synchronized Integer get_global_trigger_enable_deadtime() {
        Integer val = io.read(PPrReg.RO_GLOBAL_TRIGGER);
        return (val >> 3) & 0x00000001;
    }

    /**
     * Writes the PPr Global Trigger Configuration register (R/W).
     *
     * @param val Input value to write.
     * @return the IPbus write status.
     */
    public synchronized Boolean set_global_trigger(Integer val) {
        return io.write(PPrReg.RO_GLOBAL_TRIGGER, val);
    }

    /**
     * Sets enable deadtime bit in the PPr Global Trigger Configuration
     * register. With this bit set to 1 a sort of busy signal is activated to
     * block writing the data pipeline registers in the PPrDemo. To realese it a
     * last event BCID/L1ID read is needed. This prevents arrivals of triggers
     * while reading the pipelines if IPbus readout is used.
     *
     * @param setbit
     * @return
     */
    public synchronized Boolean set_global_trigger_deadtime(Integer setbit) {
        Integer val = io.read(PPrReg.RO_GLOBAL_TRIGGER);
        if (val == -1) {
            return false;
        }

        val = setBit(3, setbit, val);

        return io.write(PPrReg.RO_GLOBAL_TRIGGER, val);
    }

    /**
     * Reads the PPr Global Pipeline register (R/W).
     *
     * @return the IPbus read value.
     */
    public synchronized Integer get_global_pipeline() {
        return (io.read(PPrReg.RO_GLOBAL_PIPELINE) & 0xFF);
    }

    /**
     * Writes the PPr Global Pipeline register (R/W).
     *
     * @param val Input value to write.
     * @return the IPbus write status.
     */
    public synchronized Boolean set_global_pipeline(Integer val) {
        return io.write(PPrReg.RO_GLOBAL_PIPELINE, val);
    }

    /**
     * Reads the PPr Synchronous Commands Configuration register (R/W).
     *
     * @return The IPbus read value.
     */
    public synchronized Integer get_sync_cmd() {
        return io.read(PPrReg.RO_SYNC_CMD);
    }

    /**
     * Writes the PPr Synchronous Commands Configuration register (R/W).
     *
     * @param val Input value to write.
     * @return the IPbus write status.
     */
    public synchronized Boolean set_sync_cmd(Integer val) {
        return io.write(PPrReg.RO_SYNC_CMD, val);
    }

    /**
     *
     * @param bval
     * @return
     */
    public synchronized Boolean set_sync_cmd_reset(Integer bval) {
        Integer val = io.read(PPrReg.RO_SYNC_CMD);
        if (val == -1) {
            return false;
        }

        return io.write(PPrReg.RO_SYNC_CMD, ByteTools.setBit(0, bval, val));
    }

    /**
     *
     * @param bval
     * @return
     */
    public synchronized Boolean set_sync_cmd_enable(Integer bval) {
        Integer val = io.read(PPrReg.RO_SYNC_CMD);
        if (val == -1) {
            return false;
        }

        return io.write(PPrReg.RO_SYNC_CMD, ByteTools.setBit(1, bval, val));
    }

    /**
     * Reads the PPr Commands Counter register (R/W).
     *
     * @return The IPbus read value.
     */
    public synchronized Integer get_counter_cmd() {
        return io.read(PPrReg.CMD_COUNTER);
    }

    /**
     * Reads the PPr Async Command Counter register (R/W).
     *
     * @return The IPbus read value.
     */
    public synchronized Integer get_counter_async_cmd() {
        return io.read(PPrReg.ASYNC_CMD_COUNTER);
    }

    /**
     * Reads the PPr Sync Command Counter register (R/W).
     *
     * @return The IPbus read value.
     */
    public synchronized Integer get_counter_sync_cmd() {
        return io.read(PPrReg.SYNC_CMD_COUNTER);
    }

    /**
     * Reads the PPr Last Event BCID Counter register (R).
     *
     * @return The IPbus read value.
     */
    public synchronized Integer get_counter_last_event_BCID() {
        return io.read(PPrReg.LAST_EVT_BCID) & 0xFFF;
    }

    /**
     * Reads the PPr Last Event L1ID Counter register (R).
     *
     * @return The IPbus read value.
     */
    public synchronized Integer get_counter_last_event_L1ID() {
        return io.read(PPrReg.LAST_EVT_L1ID);
    }

    /**
     * Reads the PPr L1A Counter register (R).
     *
     * @return The IPbus read value.
     */
    public synchronized Integer get_counter_L1A() {
        return io.read(PPrReg.L1A_COUNTER);
    }

    /**
     * Reads the PPr Events Out Counter register (R).
     *
     * @return The IPbus read value.
     */
    public synchronized Integer get_counter_Events_Out() {
        return io.read(PPrReg.EVTS_OUT_COUNTER);
    }

    /**
     * Reads the PPr Links Control and Global Resets register (R/W)
     *
     * @return
     */
    public synchronized Integer get_links_control_global_resets() {
        return io.read(PPrReg.LINKS_CTRL_GLB_RESETS);
    }

    /**
     * Writes the PPr Links Control and Global Resets register (R/W).
     *
     * @param val Input value to write.
     * @return the IPbus write status.
     */
    public synchronized Boolean set_links_control_global_resets(Integer val) {
        return io.write(PPrReg.LINKS_CTRL_GLB_RESETS, val);
    }

    /**
     * Reads the Link Control select bits from the PPr Link Control and Global
     * Resets register.
     *
     * @param word Input register.
     * @param md Input minidrawer (0 to 3).
     * @param side Input side ("A" or "B").
     * @return the Link select 2-bit value (0, 1 or 2). -1 on error.
     */
    public synchronized Integer get_links_control_select(Integer word, Integer md, String side) {
        Integer retval = -1;

        if (null == word | md < 0 | md > 3 | (!"A".equals(side) & !"B".equals(side))) {
            return retval;
        } else {
            switch (md) {
                case 0:
                    if ("A".equals(side)) {
                        retval = word & 0x3;
                    }
                    if ("B".equals(side)) {
                        retval = (word >> 2) & 0x3;
                    }
                    break;
                case 1:
                    if ("A".equals(side)) {
                        retval = (word) >> 4 & 0x3;
                    }
                    if ("B".equals(side)) {
                        retval = (word >> 6) & 0x3;
                    }
                    break;
                case 2:
                    if ("A".equals(side)) {
                        retval = (word >> 8) & 0x3;
                    }
                    if ("B".equals(side)) {
                        retval = (word >> 10) & 0x3;
                    }
                    break;
                case 3:
                    if ("A".equals(side)) {
                        retval = (word >> 12) & 0x3;
                    }
                    if ("B".equals(side)) {
                        retval = (word >> 14) & 0x3;
                    }
                    break;
                default:
                    break;
            }
        }

        return retval;
    }

    /**
     * Sets the Link Control select bits from the PPr Link Control and Global
     * Resets register.
     *
     * @param word Input word with register value.
     * @param md Input minidrawer (0 to 3).
     * @param side Input side ("A or "B")
     * @param val Input value (0 for automatic,1 for side A or 2 for side B).
     * @return the new register value. -1 on error.
     */
    public synchronized Integer set_links_control_select(Integer word, Integer md, String side, Integer val) {
        Integer retword = -1;

        if (null == word | md < 0 | md > 3 | (!"A".equals(side) & !"B".equals(side)) | val < 0 | val > 2) {
            return retword;
        } else {
            switch (md) {
                case 0:
                    if ("A".equals(side)) {
                        retword = ByteTools.setBits(0, 1, val, word);
                    }
                    if ("B".equals(side)) {
                        retword = ByteTools.setBits(2, 3, val, word);
                    }
                    break;
                case 1:
                    if ("A".equals(side)) {
                        retword = ByteTools.setBits(4, 5, val, word);
                    }
                    if ("B".equals(side)) {
                        retword = ByteTools.setBits(6, 7, val, word);
                    }
                    break;
                case 2:
                    if ("A".equals(side)) {
                        retword = ByteTools.setBits(8, 9, val, word);
                    }
                    if ("B".equals(side)) {
                        retword = ByteTools.setBits(10, 11, val, word);
                    }
                    break;
                case 3:
                    if ("A".equals(side)) {
                        retword = ByteTools.setBits(12, 13, val, word);
                    }
                    if ("B".equals(side)) {
                        retword = ByteTools.setBits(14, 15, val, word);
                    }
                    break;
                default:
                    break;
            }
        }

        return retword;
    }

    /**
     * Reads PPr Link Status register for selected minidrawer and side.
     *
     * @param md Input minidrawer (0 to 3).
     * @param side
     * @return
     */
    public synchronized Integer get_links_status(Integer md, String side) {
        if (md < 0 | md > 3 | (!"A".equals(side) & !"B".equals(side))) {
            return null;
        }

        switch (md) {
            case 0:
                if ("A".equals(side)) {
                    return io.read(PPrReg.LINK_STAT_MD1A);
                }
                if ("B".equals(side)) {
                    return io.read(PPrReg.LINK_STAT_MD1B);
                }
                break;
            case 1:
                if ("A".equals(side)) {
                    return io.read(PPrReg.LINK_STAT_MD2A);
                }
                if ("B".equals(side)) {
                    return io.read(PPrReg.LINK_STAT_MD2B);
                }
                break;
            case 2:
                if ("A".equals(side)) {
                    return io.read(PPrReg.LINK_STAT_MD3A);
                }
                if ("B".equals(side)) {
                    return io.read(PPrReg.LINK_STAT_MD3B);
                }
                break;
            case 3:
                if ("A".equals(side)) {
                    return io.read(PPrReg.LINK_STAT_MD4A);
                }
                if ("B".equals(side)) {
                    return io.read(PPrReg.LINK_STAT_MD4B);
                }
                break;
            default:
                break;
        }

        return null;
    }

    /**
     * Parses PPr Link Status register and returns bit contents.
     *
     * @param word
     * @param link
     * @return
     */
    public synchronized ArrayList<Integer> get_links_status_bits(Integer word, Integer link) {
        ArrayList<Integer> values = new ArrayList<>();

        if (link < 0 | link > 1) {
            return null;
        }

        if (link == 1) {
            word = (word & 0xFFFF0000) >> 16;
        }

        values.add(word & 0x1);
        values.add((word & 0x2) >> 1);
        values.add((word & 0x4) >> 2);
        values.add((word & 0x8) >> 3);
        values.add((word & 0x3F0) >> 4);
        values.add((word & 0x400) >> 10);
        values.add((word & 0x800) >> 11);
        values.add((word & 0x1000) >> 12);

        return values;
    }

    /**
     * Reads the PPr GBT Links Resets register (R/W)
     *
     * @return
     */
    public synchronized Integer get_GBT_links_resets() {
        return io.read(PPrReg.GBT_LINKS_RESETS);
    }

    /**
     * Writes the PPr GBT Links Resets register (R/W).
     *
     * @param val Input value to write.
     * @return the IPbus write status.
     */
    public synchronized Boolean set_GBT_links_resets(Integer val) {
        return io.write(PPrReg.GBT_LINKS_RESETS, val);
    }

    /**
     * Reads CRC errors registers per link and per minidrawer.
     *
     * @param md Input minidrawer (0 to 3).
     * @param link
     * @return
     */
    public synchronized Integer get_CRC_errors(Integer md, String link) {
        if (md < 0 | md > 3
                | (!"A0".equals(link) & !"A1".equals(link)
                & !"B0".equals(link) & !"B1".equals(link))) {
            return null;
        }

        if (md == 0) {
            if ("A0".equals(link)) {
                return io.read(PPrReg.CRC_ERR_LINK_A0_MD1);
            }
            if ("A1".equals(link)) {
                return io.read(PPrReg.CRC_ERR_LINK_A1_MD1);
            }
            if ("B0".equals(link)) {
                return io.read(PPrReg.CRC_ERR_LINK_B0_MD1);
            }
            if ("B1".equals(link)) {
                return io.read(PPrReg.CRC_ERR_LINK_B1_MD1);
            }
        }

        if (md == 1) {
            if ("A0".equals(link)) {
                return io.read(PPrReg.CRC_ERR_LINK_A0_MD2);
            }
            if ("A1".equals(link)) {
                return io.read(PPrReg.CRC_ERR_LINK_A1_MD2);
            }
            if ("B0".equals(link)) {
                return io.read(PPrReg.CRC_ERR_LINK_B0_MD2);
            }
            if ("B1".equals(link)) {
                return io.read(PPrReg.CRC_ERR_LINK_B1_MD2);
            }
        }

        if (md == 2) {
            if ("A0".equals(link)) {
                return io.read(PPrReg.CRC_ERR_LINK_A0_MD3);
            }
            if ("A1".equals(link)) {
                return io.read(PPrReg.CRC_ERR_LINK_A1_MD3);
            }
            if ("B0".equals(link)) {
                return io.read(PPrReg.CRC_ERR_LINK_B0_MD3);
            }
            if ("B1".equals(link)) {
                return io.read(PPrReg.CRC_ERR_LINK_B1_MD3);
            }
        }

        if (md == 3) {
            if ("A0".equals(link)) {
                return io.read(PPrReg.CRC_ERR_LINK_A0_MD4);
            }
            if ("A1".equals(link)) {
                return io.read(PPrReg.CRC_ERR_LINK_A1_MD4);
            }
            if ("B0".equals(link)) {
                return io.read(PPrReg.CRC_ERR_LINK_B0_MD4);
            }
            if ("B1".equals(link)) {
                return io.read(PPrReg.CRC_ERR_LINK_B1_MD4);
            }
        }

        return null;
    }

    /**
     * Reads CRC total errors registers per DB side and per minidrawer.
     *
     * @param md Input minidrawer (0 to 3).
     * @param side Input minidrawewr side ("A" or "B").
     * @return the total number of errors for the minidrawer side.
     */
    public synchronized Integer get_CRC_tot_errors(Integer md, String side) {
        if (md < 0 | md > 3 | (!"A".equals(side) & !"B".equals(side))) {
            return null;
        }

        if (md == 0) {
            if ("A".equals(side)) {
                return io.read(PPrReg.CRC_ERR_TOT_SIDE_A_MD1);
            }
            if ("B".equals(side)) {
                return io.read(PPrReg.CRC_ERR_TOT_SIDE_B_MD1);
            }
        }

        if (md == 1) {
            if ("A".equals(side)) {
                return io.read(PPrReg.CRC_ERR_TOT_SIDE_A_MD2);
            }
            if ("B".equals(side)) {
                return io.read(PPrReg.CRC_ERR_TOT_SIDE_B_MD2);
            }
        }

        if (md == 2) {
            if ("A".equals(side)) {
                return io.read(PPrReg.CRC_ERR_TOT_SIDE_A_MD3);
            }
            if ("B".equals(side)) {
                return io.read(PPrReg.CRC_ERR_TOT_SIDE_B_MD3);
            }
        }

        if (md == 3) {
            if ("A".equals(side)) {
                return io.read(PPrReg.CRC_ERR_TOT_SIDE_A_MD4);
            }
            if ("B".equals(side)) {
                return io.read(PPrReg.CRC_ERR_TOT_SIDE_B_MD4);
            }
        }

        return null;
    }

    /**
     *
     * @param CRCerr
     * @param frames
     * @return
     */
    public synchronized Double get_CRC_BER(Integer CRCerr, Long frames) {
        Double val;

        if (frames != 0) {
            val = (Double.valueOf(CRCerr) / (120 * frames.doubleValue()));
            return val;
        }

        return null;
    }

    /**
     *
     * @param CRCerr
     * @param frames
     * @return
     */
    public synchronized Double get_CRC_fraction_per_million(Integer CRCerr, Long frames) {
        Double val;

        if (frames != 0) {
            val = (Math.pow(10, 6) * CRCerr) / frames;
            return val;
        }

        return null;
    }

    /**
     * Returns the number of frames per minidrawer. Reads CRC minidrawer frame
     * registers.
     *
     * @param md Input minidrawer number (from 0 to 3).
     * @return
     */
    public synchronized Long get_frames(Integer md) {
        Integer frame1, frame2;
        Long lframe1 = 0L, lframe2 = 0L;

        switch (md) {
            case 0:
                frame1 = io.read(PPrReg.CRC_FRAMES1_MD1);
                if (frame1 == null) {
                    return null;
                }
                frame2 = io.read(PPrReg.CRC_FRAMES2_MD1);
                if (frame2 == null) {
                    return null;
                }
                break;
            case 1:
                frame1 = io.read(PPrReg.CRC_FRAMES1_MD2);
                if (frame1 == null) {
                    return null;
                }
                frame2 = io.read(PPrReg.CRC_FRAMES2_MD2);
                if (frame2 == null) {
                    return null;
                }
                break;
            case 2:
                frame1 = io.read(PPrReg.CRC_FRAMES1_MD3);
                if (frame1 == null) {
                    return null;
                }
                frame2 = io.read(PPrReg.CRC_FRAMES2_MD3);
                if (frame2 == null) {
                    return null;
                }
                break;
            case 3:
                frame1 = io.read(PPrReg.CRC_FRAMES1_MD4);
                if (frame1 == null) {
                    return null;
                }
                frame2 = io.read(PPrReg.CRC_FRAMES2_MD4);
                if (frame2 == null) {
                    return null;
                }
                break;
            default:
                return null;
        }

        lframe1 = frame1.longValue() & 0xFFFFFFFFL;
        lframe2 = frame2.longValue() & 0xFFFFFFFFL;

        //System.out.printf("frame1: 0x%016X (%d)%n", lframe1, lframe1);
        //System.out.printf("frame2: 0x%016X (%d)%n", lframe2, lframe2);
        //System.out.printf("total: 0x%016X (%d)%n",
        //        (lframe1 & 0xFFFFFFFFL) | lframe2 << 32,
        //        (lframe1 & 0xFFFFFFFFL) | lframe2 << 32);
        return ((lframe1 & 0xFFFFFFFFL) | lframe2 << 32);
    }

    /**
     *
     * @param md Input minidrawer (from 0 to 3)
     * @return
     */
    public synchronized ArrayList<Integer> get_latencies(Integer md) {
        ArrayList<Integer> latency = new ArrayList<>();
        Integer val;

        switch (md) {
            case 0:
                val = io.read(PPrReg.LAT_MD1_MD2);
                if (val == null) {
                    return null;
                } else {
                    latency.add(val & 0xFF);
                    latency.add((val >> 8) & 0xFF);
                    return latency;
                }
            case 1:
                val = io.read(PPrReg.LAT_MD1_MD2);
                if (val == null) {
                    return null;
                } else {
                    latency.add((val >> 16) & 0xFF);
                    latency.add((val >> 24) & 0xFF);
                    return latency;
                }
            case 2:
                val = io.read(PPrReg.LAT_MD3_MD4);
                if (val == null) {
                    return null;
                } else {
                    latency.add(val & 0xFF);
                    latency.add((val >> 8) & 0xFF);
                    return latency;
                }
            case 3:
                val = io.read(PPrReg.LAT_MD3_MD4);
                if (val == null) {
                    return null;
                } else {
                    latency.add((val >> 16) & 0xFF);
                    latency.add((val >> 24) & 0xFF);
                    return latency;
                }
            default:
                return null;
        }
    }

    /**
     * Resets the PPr CRC counters.
     *
     * @return the IPbus write status value.
     */
    public synchronized Boolean reset_CRC_counters() {
        Boolean ret;

        ret = io.write(PPrReg.RO_RESET_CRC_CNTS, 0);
        if (!ret) {
            System.out.println("PPr.ppr_reset_CRC_counters() -> IPbus write transaction error");
            return false;
        }

        ret = io.write(PPrReg.RO_RESET_CRC_CNTS, 1);
        if (!ret) {
            System.out.println("PPr.ppr_reset_CRC_counters() -> IPbus write transaction error");
            return false;
        }

        ret = io.write(PPrReg.RO_RESET_CRC_CNTS, 0);
        if (!ret) {
            System.out.println("PPr.ppr_reset_CRC_counters() -> IPbus write transaction error");
            return false;
        }

        return true;
    }

    /**
     * Resets Command counters. Cycles from 1 to 0 the Reset Cmd Counter bit of
     * the PPr Synchronous Commands Configuration register.
     *
     * @return the IPbus write status value.
     */
    public synchronized Boolean reset_cmd_counters() {
        Boolean ret;

        Integer val = get_sync_cmd();
        if (val == -1) {
            System.out.println("PPr.ppr_reset_cmd_counters() -> IPbus read sync cmd register error");
            return false;
        }

        val = ByteTools.setBit(16, 1, val);
        ret = set_sync_cmd(val);
        if (!ret) {
            System.out.println("PPr.ppr_reset_cmd_counters() -> IPbus write sync cmd register error");
            return false;
        }

        val = ByteTools.setBit(16, 0, val);
        ret = set_sync_cmd(val);
        if (!ret) {
            System.out.println("PPr.ppr_reset_cmd_counters() -> IPbus write ync cmd register error");
            return false;
        }
        return true;
    }

    /**
     * Resets selected PPrDemo QSFP transceiver. Use the PPr Links Control and
     * Global Resets register.
     *
     * @param qsfp Input PPrDemo QSFP transceiver (0 to 3).
     * @return the reset Boolean status operation.
     */
    public synchronized Boolean reset_QSFP_Link(Integer qsfp) {
        Boolean ret = false;

        if (null != qsfp) {
            Integer val = get_links_control_global_resets();
            if (val != null) {
                switch (qsfp) {
                    case 0:
                        val = ByteTools.setBit(16, 0, val);
                        ret = set_links_control_global_resets(val);
                        if (ret == false) {
                            return ret;
                        }
                        val = ByteTools.setBit(16, 1, val);
                        ret = set_links_control_global_resets(val);
                        if (ret == false) {
                            return ret;
                        }
                        val = ByteTools.setBit(16, 0, val);
                        ret = set_links_control_global_resets(val);
                        if (ret == false) {
                            return ret;
                        }
                        break;
                    case 1:
                        val = ByteTools.setBit(17, 0, val);
                        ret = set_links_control_global_resets(val);
                        if (ret == false) {
                            return ret;
                        }
                        val = ByteTools.setBit(17, 1, val);
                        ret = set_links_control_global_resets(val);
                        if (ret == false) {
                            return ret;
                        }
                        val = ByteTools.setBit(17, 0, val);
                        ret = set_links_control_global_resets(val);
                        if (ret == false) {
                            return ret;
                        }
                        break;
                    case 2:
                        val = ByteTools.setBit(18, 0, val);
                        ret = set_links_control_global_resets(val);
                        if (ret == false) {
                            return ret;
                        }
                        val = ByteTools.setBit(18, 1, val);
                        ret = set_links_control_global_resets(val);
                        if (ret == false) {
                            return ret;
                        }
                        val = ByteTools.setBit(18, 0, val);
                        ret = set_links_control_global_resets(val);
                        if (ret == false) {
                            return ret;
                        }
                        break;
                    case 3:
                        val = ByteTools.setBit(19, 0, val);
                        ret = set_links_control_global_resets(val);
                        if (ret == false) {
                            return ret;
                        }
                        val = ByteTools.setBit(19, 1, val);
                        ret = set_links_control_global_resets(val);
                        if (ret == false) {
                            return ret;
                        }
                        val = ByteTools.setBit(19, 0, val);
                        ret = set_links_control_global_resets(val);
                        if (ret == false) {
                            return ret;
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        return ret;
    }

    /**
     * Resets selected minidrawer DB Side. Use the PPr Links Control and Global
     * Resets register.
     *
     * @param md Input minidrawer (0 to 3).
     * @param side Inopt DB Side (0 to 1)
     * @return the reset Boolean status operation.
     */
    public synchronized Boolean reset_DB_FPGA(Integer md, Integer side) {
        Boolean ret = false;

        if (md < 0 | md > 3 | side < 0 | side > 1) {
            return ret;
        }

        Integer val = get_links_control_global_resets();
        if (val != null) {
            switch (md) {
                case 0:
                    if (side == 0) {
                        val = ByteTools.setBit(20, 0, val);
                        ret = set_links_control_global_resets(val);
                        if (ret == false) {
                            return ret;
                        }
                        val = ByteTools.setBit(20, 1, val);
                        ret = set_links_control_global_resets(val);
                        if (ret == false) {
                            return ret;
                        }
                        val = ByteTools.setBit(20, 0, val);
                        ret = set_links_control_global_resets(val);
                        if (ret == false) {
                            return ret;
                        }
                    } else if (side == 1) {
                        val = ByteTools.setBit(21, 0, val);
                        ret = set_links_control_global_resets(val);
                        if (ret == false) {
                            return ret;
                        }
                        val = ByteTools.setBit(21, 1, val);
                        ret = set_links_control_global_resets(val);
                        if (ret == false) {
                            return ret;
                        }
                        val = ByteTools.setBit(21, 0, val);
                        ret = set_links_control_global_resets(val);
                        if (ret == false) {
                            return ret;
                        }
                    }
                    break;
                case 1:
                    if (side == 0) {
                        val = ByteTools.setBit(22, 0, val);
                        ret = set_links_control_global_resets(val);
                        if (ret == false) {
                            return ret;
                        }
                        val = ByteTools.setBit(22, 1, val);
                        ret = set_links_control_global_resets(val);
                        if (ret == false) {
                            return ret;
                        }
                        val = ByteTools.setBit(22, 0, val);
                        ret = set_links_control_global_resets(val);
                        if (ret == false) {
                            return ret;
                        }
                    } else if (side == 1) {
                        val = ByteTools.setBit(23, 0, val);
                        ret = set_links_control_global_resets(val);
                        if (ret == false) {
                            return ret;
                        }
                        val = ByteTools.setBit(23, 1, val);
                        ret = set_links_control_global_resets(val);
                        if (ret == false) {
                            return ret;
                        }
                        val = ByteTools.setBit(23, 0, val);
                        ret = set_links_control_global_resets(val);
                        if (ret == false) {
                            return ret;
                        }
                    }
                    break;
                case 2:
                    if (side == 0) {
                        val = ByteTools.setBit(24, 0, val);
                        ret = set_links_control_global_resets(val);
                        if (ret == false) {
                            return ret;
                        }
                        val = ByteTools.setBit(24, 1, val);
                        ret = set_links_control_global_resets(val);
                        if (ret == false) {
                            return ret;
                        }
                        val = ByteTools.setBit(24, 0, val);
                        ret = set_links_control_global_resets(val);
                        if (ret == false) {
                            return ret;
                        }
                    } else if (side == 1) {
                        val = ByteTools.setBit(25, 0, val);
                        ret = set_links_control_global_resets(val);
                        if (ret == false) {
                            return ret;
                        }
                        val = ByteTools.setBit(25, 1, val);
                        ret = set_links_control_global_resets(val);
                        if (ret == false) {
                            return ret;
                        }
                        val = ByteTools.setBit(25, 0, val);
                        ret = set_links_control_global_resets(val);
                        if (ret == false) {
                            return ret;
                        }
                    }
                    break;
                case 3:
                    if (side == 0) {
                        val = ByteTools.setBit(26, 0, val);
                        ret = set_links_control_global_resets(val);
                        if (ret == false) {
                            return ret;
                        }
                        val = ByteTools.setBit(26, 1, val);
                        ret = set_links_control_global_resets(val);
                        if (ret == false) {
                            return ret;
                        }
                        val = ByteTools.setBit(26, 0, val);
                        ret = set_links_control_global_resets(val);
                        if (ret == false) {
                            return ret;
                        }
                    } else if (side == 1) {
                        val = ByteTools.setBit(27, 0, val);
                        ret = set_links_control_global_resets(val);
                        if (ret == false) {
                            return ret;
                        }
                        val = ByteTools.setBit(27, 1, val);
                        ret = set_links_control_global_resets(val);
                        if (ret == false) {
                            return ret;
                        }
                        val = ByteTools.setBit(27, 0, val);
                        ret = set_links_control_global_resets(val);
                        if (ret == false) {
                            return ret;
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        return ret;
    }

    /**
     * Resets selected PPrDemo QSFP GBT link RX/TX transceiver. Use the PPr GBT
     * Links Resets register.
     *
     * @param qsfp
     * @param link
     * @param transc
     * @return
     */
    public synchronized Boolean reset_QSFP_GBT_Link(Integer qsfp, Integer link, String transc) {
        Boolean ret = false;

        if (qsfp < 0 | qsfp > 3 | link < 0 | link > 3
                | (!"RX".equals(transc) & !"TX".equals(transc))) {
            return ret;
        }

        Integer val = get_GBT_links_resets();
        if (val != null) {
            switch (qsfp) {
                case 0:
                    switch (link) {
                        case 0:
                            if ("TX".equals(transc)) {
                                val = ByteTools.setBit(0, 0, val);
                                ret = set_GBT_links_resets(val);
                                if (ret == false) {
                                    return ret;
                                }
                                val = ByteTools.setBit(0, 1, val);
                                ret = set_GBT_links_resets(val);
                                if (ret == false) {
                                    return ret;
                                }
                                val = ByteTools.setBit(0, 0, val);
                                ret = set_GBT_links_resets(val);
                                if (ret == false) {
                                    return ret;
                                }
                            }
                            if ("RX".equals(transc)) {
                                val = ByteTools.setBit(1, 0, val);
                                ret = set_GBT_links_resets(val);
                                if (ret == false) {
                                    return ret;
                                }
                                val = ByteTools.setBit(1, 1, val);
                                ret = set_GBT_links_resets(val);
                                if (ret == false) {
                                    return ret;
                                }
                                val = ByteTools.setBit(1, 0, val);
                                ret = set_GBT_links_resets(val);
                                if (ret == false) {
                                    return ret;
                                }
                            }
                            break;
                        case 1:
                            if ("TX".equals(transc)) {
                                val = ByteTools.setBit(2, 0, val);
                                ret = set_GBT_links_resets(val);
                                if (ret == false) {
                                    return ret;
                                }
                                val = ByteTools.setBit(2, 1, val);
                                ret = set_GBT_links_resets(val);
                                if (ret == false) {
                                    return ret;
                                }
                                val = ByteTools.setBit(2, 0, val);
                                ret = set_GBT_links_resets(val);
                                if (ret == false) {
                                    return ret;
                                }
                            }
                            if ("RX".equals(transc)) {
                                val = ByteTools.setBit(3, 0, val);
                                ret = set_GBT_links_resets(val);
                                if (ret == false) {
                                    return ret;
                                }
                                val = ByteTools.setBit(3, 1, val);
                                ret = set_GBT_links_resets(val);
                                if (ret == false) {
                                    return ret;
                                }
                                val = ByteTools.setBit(3, 0, val);
                                ret = set_GBT_links_resets(val);
                                if (ret == false) {
                                    return ret;
                                }
                            }
                            break;
                        case 2:
                            if ("TX".equals(transc)) {
                                val = ByteTools.setBit(4, 0, val);
                                ret = set_GBT_links_resets(val);
                                if (ret == false) {
                                    return ret;
                                }
                                val = ByteTools.setBit(4, 1, val);
                                ret = set_GBT_links_resets(val);
                                if (ret == false) {
                                    return ret;
                                }
                                val = ByteTools.setBit(4, 0, val);
                                ret = set_GBT_links_resets(val);
                                if (ret == false) {
                                    return ret;
                                }
                            }
                            if ("RX".equals(transc)) {
                                val = ByteTools.setBit(5, 0, val);
                                ret = set_GBT_links_resets(val);
                                if (ret == false) {
                                    return ret;
                                }
                                val = ByteTools.setBit(5, 1, val);
                                ret = set_GBT_links_resets(val);
                                if (ret == false) {
                                    return ret;
                                }
                                val = ByteTools.setBit(5, 0, val);
                                ret = set_GBT_links_resets(val);
                                if (ret == false) {
                                    return ret;
                                }
                            }
                            break;
                        case 3:
                            if ("TX".equals(transc)) {
                                val = ByteTools.setBit(6, 0, val);
                                ret = set_GBT_links_resets(val);
                                if (ret == false) {
                                    return ret;
                                }
                                val = ByteTools.setBit(6, 1, val);
                                ret = set_GBT_links_resets(val);
                                if (ret == false) {
                                    return ret;
                                }
                                val = ByteTools.setBit(6, 0, val);
                                ret = set_GBT_links_resets(val);
                                if (ret == false) {
                                    return ret;
                                }
                            }
                            if ("RX".equals(transc)) {
                                val = ByteTools.setBit(7, 0, val);
                                ret = set_GBT_links_resets(val);
                                if (ret == false) {
                                    return ret;
                                }
                                val = ByteTools.setBit(7, 1, val);
                                ret = set_GBT_links_resets(val);
                                if (ret == false) {
                                    return ret;
                                }
                                val = ByteTools.setBit(7, 0, val);
                                ret = set_GBT_links_resets(val);
                                if (ret == false) {
                                    return ret;
                                }
                            }
                            break;
                    }
                    break;
                case 1:
                    break;
                case 2:
                    break;
                case 3:
                    break;
            }
        }

        return ret;
    }

    /**
     *
     * @param md Input minidrawer (0 to 3).
     * @param feb Input FEB (0 to 11).
     * @return
     */
    public synchronized Integer read_feb_switches(Integer md, Integer feb) {
        Integer side;
        Boolean ret;
        String fs;

        if (md < 0 | md > 3 | feb < 0 | feb > 11) {
            return null;
        }

        if (feb % 2 == 0) {
            side = 0;
        } else {
            side = 1;
        }

        Integer ctrlw = (side << 6) | PPrReg.PPR_FEB_SWITCHES[feb >> 1];

        if (PPrLib.getDebugPPr()) {
            fs = "PPr.read_feb_switches -> MD: %d FEB: %d";
            System.out.println(String.format(fs, md, feb));
            fs = "    PPr address write: 0x%08X value: 0x%08X (%s)";
            System.out.println(String.format(fs,
                    PPrReg.PPR_FEB_CTRL_REG[md], ctrlw, ByteTools.intToString(ctrlw, 4)));
        }

        ret = io.write(PPrReg.PPR_FEB_CTRL_REG[md], ctrlw);
        if (!ret) {
            return -1;
        }

        Integer val = io.read(PPrReg.PPR_FEB_DATA_REG[md]);
        val = val & 0xFFF;
        if (PPrLib.getDebugPPr()) {
            fs = "    PPr address read: 0x%08X value: 0x%08X (%d)";
            System.out.println(String.format(fs, PPrReg.PPR_FEB_DATA_REG[md], val, val));
        }

        return val;
    }

    /**
     *
     * @param md Input minidrawer (0 to 3).
     * @param feb Input FEB (0 to 11).
     * @return
     */
    public synchronized Integer read_feb_CIS_DAC(Integer md, Integer feb) {
        Integer side;
        Boolean ret;
        String fs;

        if (md < 0 | md > 3 | feb < 0 | feb > 11) {
            return null;
        }

        if (feb % 2 == 0) {
            side = 0;
        } else {
            side = 1;
        }

        Integer ctrlw = (side << 6) | PPrReg.PPR_FEB_CIS_DAC[feb >> 1];

        if (PPrLib.getDebugPPr()) {
            fs = "PPr.ppr_read_feb_CIS_DAC -> MD: %d FEB: %d";
            System.out.println(String.format(fs, md, feb));
            fs = "    PPr address write: 0x%08X value: 0x%08X (%s)";
            System.out.println(String.format(fs,
                    PPrReg.PPR_FEB_CTRL_REG[md], ctrlw, ByteTools.intToString(ctrlw, 4)));
        }

        ret = io.write(PPrReg.PPR_FEB_CTRL_REG[md], ctrlw);
        if (!ret) {
            return -1;
        }

        Integer val = io.read(PPrReg.PPR_FEB_DATA_REG[md]);
        val = val & 0xFFF;
        if (PPrLib.getDebugPPr()) {
            fs = "    PPr address read: 0x%08X value: 0x%08X (%d)";
            System.out.println(String.format(fs, PPrReg.PPR_FEB_DATA_REG[md], val, val));
        }

        return val;
    }

    /**
     *
     * @param md Input minidrawer (0 to 3).
     * @param feb Input FEB (0 to 11).
     * @return
     */
    public synchronized Integer read_feb_ped_HG_pos(Integer md, Integer feb) {
        Integer side;
        Boolean ret;
        String fs;

        if (md < 0 | md > 3 | feb < 0 | feb > 11) {
            return null;
        }

        if (feb % 2 == 0) {
            side = 0;
        } else {
            side = 1;
        }

        Integer ctrlw = (side << 6) | PPrReg.PPR_FEB_PED_HG_POS[feb >> 1];

        if (PPrLib.getDebugPPr()) {
            fs = "PPr.ppr_read_feb_ped_HG_pos -> MD: %d FEB: %d";
            System.out.println(String.format(fs, md, feb));
            fs = "    PPr address write: 0x%08X value: 0x%08X (%s)";
            System.out.println(String.format(fs,
                    PPrReg.PPR_FEB_CTRL_REG[md], ctrlw, ByteTools.intToString(ctrlw, 4)));
        }

        ret = io.write(PPrReg.PPR_FEB_CTRL_REG[md], ctrlw);
        if (!ret) {
            return null;
        }

        Integer val = io.read(PPrReg.PPR_FEB_DATA_REG[md]);
        val = val & 0xFFF;
        if (PPrLib.getDebugPPr()) {
            fs = "    PPr address read: 0x%08X value: 0x%08X (%d)";
            System.out.println(String.format(fs, PPrReg.PPR_FEB_DATA_REG[md], val, val));
        }

        return val;
    }

    /**
     *
     * @param md Input minidrawer (0 to 3).
     * @param feb Input FEB (0 to 11).
     * @return
     */
    public synchronized Integer read_feb_ped_HG_neg(Integer md, Integer feb) {
        Integer side;
        Boolean ret;
        String fs;

        if (md < 0 | md > 3 | feb < 0 | feb > 11) {
            return null;
        }

        if (feb % 2 == 0) {
            side = 0;
        } else {
            side = 1;
        }

        Integer ctrlw = (side << 6) | PPrReg.PPR_FEB_PED_HG_NEG[feb >> 1];

        if (PPrLib.getDebugPPr()) {
            fs = "PPr.ppr_read_feb_ped_HG_neg -> MD: %d FEB: %d";
            System.out.println(String.format(fs, md, feb));
            fs = "    PPr address write: 0x%08X value: 0x%08X (%s)";
            System.out.println(String.format(fs,
                    PPrReg.PPR_FEB_CTRL_REG[md], ctrlw, ByteTools.intToString(ctrlw, 4)));
        }

        ret = io.write(PPrReg.PPR_FEB_CTRL_REG[md], ctrlw);
        if (!ret) {
            return null;
        }

        Integer val = io.read(PPrReg.PPR_FEB_DATA_REG[md]);
        val = val & 0xFFF;
        if (PPrLib.getDebugPPr()) {
            fs = "    PPr address read: 0x%08X value: 0x%08X (%d)";
            System.out.println(String.format(fs, PPrReg.PPR_FEB_DATA_REG[md], val, val));
        }

        return val;
    }

    /**
     *
     * @param md Input minidrawer (0 to 3).
     * @param feb Input FEB (0 to 11).
     * @return
     */
    public synchronized Integer read_feb_ped_LG_pos(Integer md, Integer feb) {
        Integer side;
        Boolean ret;
        String fs;

        if (md < 0 | md > 3 | feb < 0 | feb > 11) {
            return null;
        }

        if (feb % 2 == 0) {
            side = 0;
        } else {
            side = 1;
        }

        Integer ctrlw = (side << 6) | PPrReg.PPR_FEB_PED_LG_POS[feb >> 1];

        if (PPrLib.getDebugPPr()) {
            fs = "PPr.ppr_read_feb_ped_LG_pos -> MD: %d FEB: %d";
            System.out.println(String.format(fs, md, feb));
            fs = "    PPr address write: 0x%08X value: 0x%08X (%s)";
            System.out.println(String.format(fs,
                    PPrReg.PPR_FEB_CTRL_REG[md], ctrlw, ByteTools.intToString(ctrlw, 4)));
        }

        ret = io.write(PPrReg.PPR_FEB_CTRL_REG[md], ctrlw);
        if (!ret) {
            return null;
        }

        Integer val = io.read(PPrReg.PPR_FEB_DATA_REG[md]);
        val = val & 0xFFF;
        if (PPrLib.getDebugPPr()) {
            fs = "    PPr address read: 0x%08X value: 0x%08X (%d)";
            System.out.println(String.format(fs, PPrReg.PPR_FEB_DATA_REG[md], val, val));
        }

        return val;
    }

    /**
     *
     * @param md Input minidrawer (0 to 3).
     * @param feb Input FEB (0 to 11).
     * @return
     */
    public synchronized Integer read_feb_ped_LG_neg(Integer md, Integer feb) {
        Integer side;
        Boolean ret;
        String fs;

        if (md < 0 | md > 3 | feb < 0 | feb > 11) {
            return null;
        }

        if (feb % 2 == 0) {
            side = 0;
        } else {
            side = 1;
        }

        Integer ctrlw = (side << 6) | PPrReg.PPR_FEB_PED_LG_NEG[feb >> 1];

        if (PPrLib.getDebugPPr()) {
            fs = "PPr.ppr_read_feb_ped_LG_neg -> MD: %d FEB: %d";
            System.out.println(String.format(fs, md, feb));
            fs = "    PPr address write: 0x%08X value: 0x%08X (%s)";
            System.out.println(String.format(fs,
                    PPrReg.PPR_FEB_CTRL_REG[md], ctrlw, ByteTools.intToString(ctrlw, 4)));
        }

        ret = io.write(PPrReg.PPR_FEB_CTRL_REG[md], ctrlw);
        if (!ret) {
            return null;
        }

        Integer val = io.read(PPrReg.PPR_FEB_DATA_REG[md]);
        val = val & 0xFFF;
        if (PPrLib.getDebugPPr()) {
            fs = "    PPr address read: 0x%08X value: 0x%08X (%d)";
            System.out.println(String.format(fs, PPrReg.PPR_FEB_DATA_REG[md], val, val));
        }

        return val;
    }

    /**
     * Reads input number of samples for a particular FEB HG channel of a
     * minidrawer.
     *
     * @param md Input minidrawer (0 to 3).
     * @param adc Input FEB channel (0 to 12).
     * @param samples Input number of samples.
     * @return ArrayList with data.
     */
    public synchronized ArrayList<Integer> get_data_HG(Integer md, Integer adc, Integer samples) {
        ArrayList<Integer> values;

        Integer offset = (adc + md * 12) * 32;

        values = io.read(PPrReg.RAM_MEM_HG + offset, samples);

        if (values == null) {
            return null;
        }

        for (int i = 0; i < values.size(); i++) {
            values.set(i, values.get(i) & 0xFFF);
        }

        return values;
    }

    /**
     * Reads input number of samples for a particular FEB LG channel of a
     * minidrawer.
     *
     * @param md Input minidrawer (0 to 3).
     * @param adc Input FEB channel (0 to 12).
     * @param samples Input number of samples.
     * @return ArrayList with data.
     */
    public synchronized ArrayList<Integer> get_data_LG(Integer md, Integer adc, Integer samples) {
        ArrayList<Integer> values;

        Integer offset = (adc + md * 12) * 32;

        values = io.read(PPrReg.RAM_MEM_LG + offset, samples);

        if (values == null) {
            return null;
        }

        for (int i = 0; i < values.size(); i++) {
            values.set(i, values.get(i) & 0xFFF);
        }

        return values;
    }
}
