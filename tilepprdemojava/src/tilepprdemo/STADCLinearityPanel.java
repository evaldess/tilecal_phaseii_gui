/* 
 *  TilePPrDemoJava Project
 *  2019 (c) IFIC-CERN
 */
package tilepprdemo;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.RejectedExecutionException;
import javax.swing.JPanel;

import histogram.JScan;
import tilepprjavalibrary.lib.PPrLib;
import utilities.DataArrayList;

/**
 * ADC linearity scan panel controls window.
 *
 * @author Juan Valls {@literal (valls@cern.ch)}
 * @author Fernando Carrió {@literal (fernando.carrio@cern.ch)}
 * @author Alberto Valero {@literal (alberto.valero@cern.ch)}
 */
public class STADCLinearityPanel extends javax.swing.JPanel {

    /**
     * Creates new form STADCLinearityPanel
     *
     * @param pprlib
     */
    public STADCLinearityPanel(PPrLib pprlib) {
        System.out.println("STADCLinearityPanel -> constructor");

        this.pprlib = pprlib;

        // NetBeans components settings.
        initComponents();

        // Defines JPanel array with JPanel channels.
        PanelPlots = new JPanel[]{PanelPlot1, PanelPlot2, PanelPlot3,
            PanelPlot4, PanelPlot5, PanelPlot6,
            PanelPlot7, PanelPlot8, PanelPlot9,
            PanelPlot10, PanelPlot11, PanelPlot12};

        // Defines flag to indicate ADC linearity run status through Atomic Integer. 
        ADCLinearityRunStatus = new AtomicInteger(0);

        // Defines Atomic Integer variable with number of processed steps.
        ProcScanPoint = new AtomicInteger(0);

        // Defines Atomic Integer variable with number of processed events per step.
        ProcEventsPerStep = new AtomicInteger(0);

        // Defines ArrayList with selected minidrawers.
        MDsel = new ArrayList<>();

        // Default selected MD to show plots.
        selectedMDPlot = 0;
        ButtonSetMD1Plots.setBackground(Color.GRAY);

        // Default selected gain to show plots.
        selectedGainPlot = "HG";
        ButtonSetHGPlots.setBackground(Color.GRAY);

        LabelADClinearityRunStatus.setForeground(Color.RED);

        LabelScanPointCounter.setForeground(Color.blue);
        LabelDACpCounter.setForeground(Color.blue);
        LabelDACnCounter.setForeground(Color.blue);
        LabelADCCounter.setForeground(Color.blue);
        LabelEventsPerStepCounter.setForeground(Color.blue);

        hxlow_DAC = 1200.f;
        hxhigh_DAC = 2300.f;

        hxlow_ADC = 0.f;
        hxhigh_ADC = 4095.f;

        min_DAC = 1278.f;
        max_DAC = 2261.f;

        min_ADC = 0.f;
        max_ADC = 4095.f;

        // Define Integer 2d arrays of ArrayList with data.
        dataLG = new DataArrayList[4][12];
        dataHG = new DataArrayList[4][12];

        // Initialize the 2d array so that it is filled with Empty ArrayList<>'s'.
        for (Integer md = 0; md < 4; md++) {
            for (Integer adc = 0; adc < 12; adc++) {
                dataHG[md][adc] = new DataArrayList();
                dataLG[md][adc] = new DataArrayList();
            }
        }

        // Set field values with NetBeans default text widgets.
        // Needed to define and set initial field values.
        get_all_parameters();

        // Defines Runnables through lambda expressions.
        initDefineRunnables();

        // Defines instance of ThreadPoolExecutor class to create a thread pool.
        // A thread pool manages a collection of Runnable threads.
        // This class implements Executor and ExecutorService interfaces.
        executor = new ThreadPoolExecutor(
                10,
                10,
                0L,
                TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>(),
                new TBThreadFactory("STADCLinearityPanel")
        );
    }

    /**
     * Gets all field values from text widgets.
     */
    private synchronized void get_all_parameters() {
        get_DB_FPGA_side();
        get_MDs();
        get_BC_to_read();
        get_samples();
        get_steps();
        get_scan_units();
    }

    public static synchronized void set_all_parameters() {
        set_DB_FPGA_side();
        set_MDs();
        set_BC_to_read();
        set_samples();
        set_steps();
        set_scan_units();
    }

    /**
     * Gets DB FPGA side.
     */
    private synchronized void get_DB_FPGA_side() {
        String side_strings[] = new String[]{"Broadcast", "Side A", "Side B", "Unknown"};

        Object selected = ComboBoxDBSide.getSelectedItem();
        String DBSideStr = selected.toString();
        switch (DBSideStr) {
            case "Broadcast":
                dbside = 0;
                dbsidestring = side_strings[0];
                break;
            case "Side A":
                dbside = 1;
                dbsidestring = side_strings[1];
                break;
            case "Side B":
                dbside = 2;
                dbsidestring = side_strings[2];
                break;
            default:
                dbsidestring = side_strings[3];
                break;
        }
    }

    /**
     * Sets DB FPGA side ComboBox with field value.
     */
    public static synchronized void set_DB_FPGA_side() {
        switch (dbsidestring) {
            case "Broadcast":
                dbside = 0;
                break;
            case "Side A":
                dbside = 1;
                break;
            case "Side B":
                dbside = 2;
                break;
        }

        ComboBoxDBSide.setSelectedIndex(dbside);
    }

    /**
     * Gets minidrawers to process and minidrawer broadcast mode.
     */
    private synchronized void get_MDs() {
        MDsel.clear();
        MDsel.add(CheckBoxMD1.isSelected() ? 1 : 0);
        MDsel.add(CheckBoxMD2.isSelected() ? 1 : 0);
        MDsel.add(CheckBoxMD3.isSelected() ? 1 : 0);
        MDsel.add(CheckBoxMD4.isSelected() ? 1 : 0);

        MDtot = 0;
        for (Integer md = 0; md < 4; md++) {
            if (MDsel.get(md) == 1) {
                MDtot++;
            }
        }

        MDbroadcast = (CheckBoxBroadcastMD.isSelected() ? 1 : 0);
    }

    /**
     * Sets minidrawers to process and minidrawer broadcast mode.
     */
    public static synchronized void set_MDs() {
        if (MDsel.get(0) == 0) {
            CheckBoxMD1.setSelected(false);
        } else {
            CheckBoxMD1.setSelected(true);
        }

        if (MDsel.get(1) == 0) {
            CheckBoxMD2.setSelected(false);
        } else {
            CheckBoxMD2.setSelected(true);
        }

        if (MDsel.get(2) == 0) {
            CheckBoxMD3.setSelected(false);
        } else {
            CheckBoxMD3.setSelected(true);
        }

        if (MDsel.get(3) == 0) {
            CheckBoxMD4.setSelected(false);
        } else {
            CheckBoxMD4.setSelected(true);
        }

        if (MDbroadcast == 0) {
            CheckBoxBroadcastMD.setSelected(false);
        } else {
            CheckBoxBroadcastMD.setSelected(true);
        }
    }

    /**
     * Gets samples.
     */
    private synchronized void get_samples() {
        Object selected = ComboBoxSamples.getSelectedItem();
        String strSamp = selected.toString();
        samples = Integer.valueOf(strSamp);
    }

    /**
     * Sets samples.
     */
    public static synchronized void set_samples() {
        ComboBoxSamples.setSelectedIndex(samples - 1);
    }

    /**
     * Gets BCID for L1A.
     */
    private synchronized void get_BC_to_read() {
        BCID = Integer.valueOf(TextFieldBCID.getText(), 10);
        if (BCID > 3564) {
            BCID = 3564;
            TextFieldBCID.setText("3564");
        }
        if (BCID < 0) {
            BCID = 0;
            TextFieldBCID.setText("0");
        }
    }

    /**
     * Sets BCID for L1A.
     */
    public static synchronized void set_BC_to_read() {
        TextFieldBCID.setText(String.valueOf(BCID));
    }

    /**
     * Gets DAC steps.
     */
    private synchronized void get_steps() {
        nbSteps = Integer.valueOf(TextFieldSteps.getText(), 10);
        if (nbSteps > 200) {
            nbSteps = 200;
            TextFieldSteps.setText("200");
        }
        if (nbSteps < 1) {
            nbSteps = 1;
            TextFieldSteps.setText("1");
        }

        step_length_DAC = (max_DAC - min_DAC) / nbSteps;
        step_length_ADC = (max_ADC - min_ADC) / nbSteps;

        stepEvents = Integer.valueOf(TextFieldEvtsStep.getText(), 10);
        if (stepEvents < 1) {
            stepEvents = 1;
            TextFieldEvtsStep.setText("1");
        }
    }

    /**
     * Sets DAC steps.
     */
    public static synchronized void set_steps() {
        TextFieldSteps.setText(String.valueOf(nbSteps));
        TextFieldEvtsStep.setText(String.valueOf(stepEvents));
    }

    /**
     * Get units for ADC linearity scan (DACs or ADCs).
     */
    private synchronized void get_scan_units() {
        Object selected = ComboBoxScanUnits.getSelectedItem();
        scanUnits = selected.toString();
    }

    /**
     * Set units for ADC linearity scan (DACs or ADCs).
     */
    public static synchronized void set_scan_units() {
        ComboBoxScanUnits.setSelectedItem(scanUnits);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    private void initComponents() {//GEN-BEGIN:initComponents
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jPanel19 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        ComboBoxSamples = new javax.swing.JComboBox<>();
        jPanel6 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        TextFieldBCID = new javax.swing.JTextField();
        jPanel12 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        CheckBoxBroadcastMD = new javax.swing.JCheckBox();
        jPanel14 = new javax.swing.JPanel();
        jPanel18 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        ComboBoxDBSide = new javax.swing.JComboBox<>();
        jPanel11 = new javax.swing.JPanel();
        ButtonSetMD1Plots = new javax.swing.JButton();
        ButtonSetMD2Plots = new javax.swing.JButton();
        ButtonSetMD3Plots = new javax.swing.JButton();
        ButtonSetMD4Plots = new javax.swing.JButton();
        CheckBoxMD1 = new javax.swing.JCheckBox();
        CheckBoxMD2 = new javax.swing.JCheckBox();
        CheckBoxMD3 = new javax.swing.JCheckBox();
        CheckBoxMD4 = new javax.swing.JCheckBox();
        jPanel13 = new javax.swing.JPanel();
        ButtonSetLGPlots = new javax.swing.JButton();
        ButtonSetHGPlots = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        jPanel15 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        ComboBoxScanUnits = new javax.swing.JComboBox<>();
        jPanel20 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        TextFieldSteps = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        TextFieldEvtsStep = new javax.swing.JTextField();
        jPanel16 = new javax.swing.JPanel();
        ButtonStartRunADCLinearity = new javax.swing.JButton();
        ButtonStopRunADCLinearity = new javax.swing.JButton();
        LabelADClinearityRunStatus = new javax.swing.JLabel();
        jPanel17 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        LabelEventsPerStepCounter = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        LabelScanPointCounter = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        LabelDACpCounter = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        LabelADCCounter = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        LabelDACnCounter = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        PanelPlot1 = new javax.swing.JPanel();
        PanelPlot2 = new javax.swing.JPanel();
        PanelPlot3 = new javax.swing.JPanel();
        PanelPlot4 = new javax.swing.JPanel();
        PanelPlot5 = new javax.swing.JPanel();
        PanelPlot6 = new javax.swing.JPanel();
        PanelPlot7 = new javax.swing.JPanel();
        PanelPlot8 = new javax.swing.JPanel();
        PanelPlot9 = new javax.swing.JPanel();
        PanelPlot10 = new javax.swing.JPanel();
        PanelPlot11 = new javax.swing.JPanel();
        PanelPlot12 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jPanel21 = new javax.swing.JPanel();
        jPanel25 = new javax.swing.JPanel();
        jPanel26 = new javax.swing.JPanel();
        ButtonSaveAsDefaults = new javax.swing.JButton();
        ButtonLoadDefaults = new javax.swing.JButton();
        ButtonShowDefaults = new javax.swing.JButton();

        java.awt.GridBagLayout jPanel1Layout = new java.awt.GridBagLayout();
        jPanel1Layout.columnWidths = new int[] {0};
        jPanel1Layout.rowHeights = new int[] {0, 15, 0, 15, 0, 15, 0, 15, 0, 15, 0};
        jPanel1.setLayout(jPanel1Layout);

        java.awt.GridBagLayout jPanel2Layout = new java.awt.GridBagLayout();
        jPanel2Layout.columnWidths = new int[] {0, 40, 0, 40, 0};
        jPanel2Layout.rowHeights = new int[] {0};
        jPanel2.setLayout(jPanel2Layout);

        jPanel4.setLayout(new java.awt.GridBagLayout());

        java.awt.GridBagLayout jPanel5Layout1 = new java.awt.GridBagLayout();
        jPanel5Layout1.columnWidths = new int[] {0};
        jPanel5Layout1.rowHeights = new int[] {0, 7, 0};
        jPanel5.setLayout(jPanel5Layout1);

        java.awt.GridBagLayout jPanel19Layout = new java.awt.GridBagLayout();
        jPanel19Layout.columnWidths = new int[] {0, 5, 0};
        jPanel19Layout.rowHeights = new int[] {0};
        jPanel19.setLayout(jPanel19Layout);

        jLabel7.setText("Samples:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel19.add(jLabel7, gridBagConstraints);

        ComboBoxSamples.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16" }));
        ComboBoxSamples.setSelectedIndex(15);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel19.add(ComboBoxSamples, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel5.add(jPanel19, gridBagConstraints);

        java.awt.GridBagLayout jPanel6Layout = new java.awt.GridBagLayout();
        jPanel6Layout.columnWidths = new int[] {0, 5, 0};
        jPanel6Layout.rowHeights = new int[] {0};
        jPanel6.setLayout(jPanel6Layout);

        jLabel6.setText("BC to read:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel6.add(jLabel6, gridBagConstraints);

        TextFieldBCID.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldBCID.setText("3200");
        TextFieldBCID.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel6.add(TextFieldBCID, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel5.add(jPanel6, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel4.add(jPanel5, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel2.add(jPanel4, gridBagConstraints);

        java.awt.GridBagLayout jPanel12Layout = new java.awt.GridBagLayout();
        jPanel12Layout.columnWidths = new int[] {0};
        jPanel12Layout.rowHeights = new int[] {0, 7, 0, 7, 0, 7, 0, 7, 0};
        jPanel12.setLayout(jPanel12Layout);

        java.awt.GridBagLayout jPanel10Layout = new java.awt.GridBagLayout();
        jPanel10Layout.columnWidths = new int[] {0, 10, 0};
        jPanel10Layout.rowHeights = new int[] {0};
        jPanel10.setLayout(jPanel10Layout);

        CheckBoxBroadcastMD.setText("MD Broadcast");
        CheckBoxBroadcastMD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckBoxBroadcastMDActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel10.add(CheckBoxBroadcastMD, gridBagConstraints);

        jPanel14.setLayout(new java.awt.GridBagLayout());

        jPanel18.setLayout(new java.awt.GridBagLayout());

        jLabel4.setText("DB FPGA Side:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel18.add(jLabel4, gridBagConstraints);

        ComboBoxDBSide.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Broadcast", "Side A", "Side B" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel18.add(ComboBoxDBSide, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel14.add(jPanel18, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel10.add(jPanel14, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel12.add(jPanel10, gridBagConstraints);

        java.awt.GridBagLayout jPanel11Layout = new java.awt.GridBagLayout();
        jPanel11Layout.columnWidths = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0};
        jPanel11Layout.rowHeights = new int[] {0, 5, 0, 5, 0};
        jPanel11.setLayout(jPanel11Layout);

        ButtonSetMD1Plots.setText("MD 1");
        ButtonSetMD1Plots.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonSetMD1PlotsActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        jPanel11.add(ButtonSetMD1Plots, gridBagConstraints);

        ButtonSetMD2Plots.setText("MD 2");
        ButtonSetMD2Plots.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonSetMD2PlotsActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 4;
        jPanel11.add(ButtonSetMD2Plots, gridBagConstraints);

        ButtonSetMD3Plots.setText("MD 3");
        ButtonSetMD3Plots.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonSetMD3PlotsActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 4;
        jPanel11.add(ButtonSetMD3Plots, gridBagConstraints);

        ButtonSetMD4Plots.setText("MD 4");
        ButtonSetMD4Plots.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonSetMD4PlotsActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 4;
        jPanel11.add(ButtonSetMD4Plots, gridBagConstraints);

        CheckBoxMD1.setSelected(true);
        CheckBoxMD1.setText("On/Off");
        CheckBoxMD1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckBoxMD1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel11.add(CheckBoxMD1, gridBagConstraints);

        CheckBoxMD2.setText("On/Off");
        CheckBoxMD2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckBoxMD2ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel11.add(CheckBoxMD2, gridBagConstraints);

        CheckBoxMD3.setText("On/Off");
        CheckBoxMD3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckBoxMD3ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel11.add(CheckBoxMD3, gridBagConstraints);

        CheckBoxMD4.setText("On/Off");
        CheckBoxMD4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckBoxMD4ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 0;
        jPanel11.add(CheckBoxMD4, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        jPanel12.add(jPanel11, gridBagConstraints);

        java.awt.GridBagLayout jPanel13Layout = new java.awt.GridBagLayout();
        jPanel13Layout.columnWidths = new int[] {0, 3, 0, 3, 0};
        jPanel13Layout.rowHeights = new int[] {0};
        jPanel13.setLayout(jPanel13Layout);

        ButtonSetLGPlots.setText("Low Gain");
        ButtonSetLGPlots.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonSetLGPlotsActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel13.add(ButtonSetLGPlots, gridBagConstraints);

        ButtonSetHGPlots.setText("High Gain");
        ButtonSetHGPlots.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonSetHGPlotsActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel13.add(ButtonSetHGPlots, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        jPanel12.add(jPanel13, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel2.add(jPanel12, gridBagConstraints);

        jPanel7.setLayout(new java.awt.GridBagLayout());

        java.awt.GridBagLayout jPanel15Layout1 = new java.awt.GridBagLayout();
        jPanel15Layout1.columnWidths = new int[] {0};
        jPanel15Layout1.rowHeights = new int[] {0, 3, 0, 3, 0, 3, 0, 3, 0};
        jPanel15.setLayout(jPanel15Layout1);

        java.awt.GridBagLayout jPanel9Layout = new java.awt.GridBagLayout();
        jPanel9Layout.columnWidths = new int[] {0, 8, 0};
        jPanel9Layout.rowHeights = new int[] {0};
        jPanel9.setLayout(jPanel9Layout);

        jLabel5.setText("Steps units:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel9.add(jLabel5, gridBagConstraints);

        ComboBoxScanUnits.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "DACs bias offsets", "ADC counts" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel9.add(ComboBoxScanUnits, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel15.add(jPanel9, gridBagConstraints);

        java.awt.GridBagLayout jPanel20Layout = new java.awt.GridBagLayout();
        jPanel20Layout.columnWidths = new int[] {0, 8, 0, 8, 0, 8, 0, 8, 0};
        jPanel20Layout.rowHeights = new int[] {0};
        jPanel20.setLayout(jPanel20Layout);

        jLabel12.setText("Steps:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel20.add(jLabel12, gridBagConstraints);

        TextFieldSteps.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldSteps.setText("2");
        TextFieldSteps.setPreferredSize(new java.awt.Dimension(50, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel20.add(TextFieldSteps, gridBagConstraints);

        jLabel13.setText("Evts/step:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel20.add(jLabel13, gridBagConstraints);

        TextFieldEvtsStep.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldEvtsStep.setText("10");
        TextFieldEvtsStep.setPreferredSize(new java.awt.Dimension(50, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel20.add(TextFieldEvtsStep, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel15.add(jPanel20, gridBagConstraints);

        java.awt.GridBagLayout jPanel16Layout1 = new java.awt.GridBagLayout();
        jPanel16Layout1.columnWidths = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0};
        jPanel16Layout1.rowHeights = new int[] {0};
        jPanel16.setLayout(jPanel16Layout1);

        ButtonStartRunADCLinearity.setText("Start Run");
        ButtonStartRunADCLinearity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonStartRunADCLinearityActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel16.add(ButtonStartRunADCLinearity, gridBagConstraints);

        ButtonStopRunADCLinearity.setText("Stop Run");
        ButtonStopRunADCLinearity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonStopRunADCLinearityActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel16.add(ButtonStopRunADCLinearity, gridBagConstraints);

        LabelADClinearityRunStatus.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelADClinearityRunStatus.setText("stopped");
        LabelADClinearityRunStatus.setPreferredSize(new java.awt.Dimension(80, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel16.add(LabelADClinearityRunStatus, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        jPanel15.add(jPanel16, gridBagConstraints);

        java.awt.GridBagLayout jPanel17Layout = new java.awt.GridBagLayout();
        jPanel17Layout.columnWidths = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0};
        jPanel17Layout.rowHeights = new int[] {0, 3, 0, 3, 0};
        jPanel17.setLayout(jPanel17Layout);

        jLabel9.setText("Events/Step:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 0;
        jPanel17.add(jLabel9, gridBagConstraints);

        LabelEventsPerStepCounter.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelEventsPerStepCounter.setText("0");
        LabelEventsPerStepCounter.setPreferredSize(new java.awt.Dimension(70, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 4;
        jPanel17.add(LabelEventsPerStepCounter, gridBagConstraints);

        jLabel3.setText("Scan point:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel17.add(jLabel3, gridBagConstraints);

        LabelScanPointCounter.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelScanPointCounter.setText("0");
        LabelScanPointCounter.setPreferredSize(new java.awt.Dimension(70, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        jPanel17.add(LabelScanPointCounter, gridBagConstraints);

        jLabel14.setText("DACp:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel17.add(jLabel14, gridBagConstraints);

        LabelDACpCounter.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelDACpCounter.setText("0");
        LabelDACpCounter.setPreferredSize(new java.awt.Dimension(70, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 4;
        jPanel17.add(LabelDACpCounter, gridBagConstraints);

        jLabel2.setText("ADC:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 0;
        jPanel17.add(jLabel2, gridBagConstraints);

        LabelADCCounter.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelADCCounter.setText("0");
        LabelADCCounter.setPreferredSize(new java.awt.Dimension(70, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 4;
        jPanel17.add(LabelADCCounter, gridBagConstraints);

        jLabel1.setText("DACn:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel17.add(jLabel1, gridBagConstraints);

        LabelDACnCounter.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelDACnCounter.setText("0");
        LabelDACnCounter.setPreferredSize(new java.awt.Dimension(70, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 4;
        jPanel17.add(LabelDACnCounter, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        jPanel15.add(jPanel17, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel7.add(jPanel15, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel2.add(jPanel7, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel1.add(jPanel2, gridBagConstraints);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Profile Histograms"));
        jPanel3.setLayout(new java.awt.GridBagLayout());

        PanelPlot1.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelPlot1.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel3.add(PanelPlot1, gridBagConstraints);

        PanelPlot2.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelPlot2.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        jPanel3.add(PanelPlot2, gridBagConstraints);

        PanelPlot3.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelPlot3.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel3.add(PanelPlot3, gridBagConstraints);

        PanelPlot4.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelPlot4.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        jPanel3.add(PanelPlot4, gridBagConstraints);

        PanelPlot5.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelPlot5.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel3.add(PanelPlot5, gridBagConstraints);

        PanelPlot6.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelPlot6.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        jPanel3.add(PanelPlot6, gridBagConstraints);

        PanelPlot7.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelPlot7.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        jPanel3.add(PanelPlot7, gridBagConstraints);

        PanelPlot8.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelPlot8.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        jPanel3.add(PanelPlot8, gridBagConstraints);

        PanelPlot9.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelPlot9.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        jPanel3.add(PanelPlot9, gridBagConstraints);

        PanelPlot10.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelPlot10.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        jPanel3.add(PanelPlot10, gridBagConstraints);

        PanelPlot11.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelPlot11.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        jPanel3.add(PanelPlot11, gridBagConstraints);

        PanelPlot12.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelPlot12.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 1;
        jPanel3.add(PanelPlot12, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        jPanel1.add(jPanel3, gridBagConstraints);

        jPanel8.setLayout(new java.awt.GridBagLayout());

        java.awt.GridBagLayout jPanel21Layout = new java.awt.GridBagLayout();
        jPanel21Layout.columnWidths = new int[] {0};
        jPanel21Layout.rowHeights = new int[] {0};
        jPanel21.setLayout(jPanel21Layout);

        jPanel25.setLayout(new java.awt.GridBagLayout());

        java.awt.GridBagLayout jPanel26Layout = new java.awt.GridBagLayout();
        jPanel26Layout.columnWidths = new int[] {0, 10, 0, 10, 0};
        jPanel26Layout.rowHeights = new int[] {0};
        jPanel26.setLayout(jPanel26Layout);

        ButtonSaveAsDefaults.setText("<html><div style='text-align: center;'><body>Save As Defaults<br>Settings</body></div></html>");
        ButtonSaveAsDefaults.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonSaveAsDefaultsActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel26.add(ButtonSaveAsDefaults, gridBagConstraints);

        ButtonLoadDefaults.setText("<html><div style='text-align: center;'><body>Load Defaults<br>Settings</body></div></html>");
        ButtonLoadDefaults.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonLoadDefaultsActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel26.add(ButtonLoadDefaults, gridBagConstraints);

        ButtonShowDefaults.setText("<html><div style='text-align: center;'><body>Show Defaults<br>Settings</body></div></html>");
        ButtonShowDefaults.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonShowDefaultsActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel26.add(ButtonShowDefaults, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel25.add(jPanel26, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel21.add(jPanel25, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel8.add(jPanel21, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        jPanel1.add(jPanel8, gridBagConstraints);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(84, Short.MAX_VALUE))
        );
    }//GEN-END:initComponents

    /**
     * Sets MD1 plots.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonSetMD1PlotsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonSetMD1PlotsActionPerformed
        if (MDsel.get(0) == 0) {
            return;
        }
        selectedMDPlot = 0;
        ButtonSetMD1Plots.setBackground(Color.GRAY);
        ButtonSetMD2Plots.setBackground(null);
        ButtonSetMD3Plots.setBackground(null);
        ButtonSetMD4Plots.setBackground(null);
        plot_RefreshPanelPlots();
    }//GEN-LAST:event_ButtonSetMD1PlotsActionPerformed

    /**
     * Sets MD2 plots.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonSetMD2PlotsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonSetMD2PlotsActionPerformed
        if (MDsel.get(1) == 0) {
            return;
        }
        selectedMDPlot = 1;
        ButtonSetMD1Plots.setBackground(null);
        ButtonSetMD2Plots.setBackground(Color.GRAY);
        ButtonSetMD3Plots.setBackground(null);
        ButtonSetMD4Plots.setBackground(null);
        plot_RefreshPanelPlots();
    }//GEN-LAST:event_ButtonSetMD2PlotsActionPerformed

    /**
     * Sets MD3 plots.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonSetMD3PlotsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonSetMD3PlotsActionPerformed
        if (MDsel.get(2) == 0) {
            return;
        }
        selectedMDPlot = 2;
        ButtonSetMD1Plots.setBackground(null);
        ButtonSetMD2Plots.setBackground(null);
        ButtonSetMD3Plots.setBackground(Color.GRAY);
        ButtonSetMD4Plots.setBackground(null);
        plot_RefreshPanelPlots();
    }//GEN-LAST:event_ButtonSetMD3PlotsActionPerformed

    /**
     * Sets MD4 plots.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonSetMD4PlotsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonSetMD4PlotsActionPerformed
        if (MDsel.get(3) == 0) {
            return;
        }
        selectedMDPlot = 3;
        ButtonSetMD1Plots.setBackground(null);
        ButtonSetMD2Plots.setBackground(null);
        ButtonSetMD3Plots.setBackground(null);
        ButtonSetMD4Plots.setBackground(Color.GRAY);
        plot_RefreshPanelPlots();
    }//GEN-LAST:event_ButtonSetMD4PlotsActionPerformed

    /**
     * Sets LG plots.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonSetLGPlotsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonSetLGPlotsActionPerformed
        selectedGainPlot = "LG";
        ButtonSetLGPlots.setBackground(Color.GRAY);
        ButtonSetHGPlots.setBackground(null);
        plot_RefreshPanelPlots();
    }//GEN-LAST:event_ButtonSetLGPlotsActionPerformed

    /**
     * Sets HG plots.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonSetHGPlotsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonSetHGPlotsActionPerformed
        selectedGainPlot = "HG";
        ButtonSetLGPlots.setBackground(null);
        ButtonSetHGPlots.setBackground(Color.GRAY);
        plot_RefreshPanelPlots();
    }//GEN-LAST:event_ButtonSetHGPlotsActionPerformed

    /**
     * Starts ADC lineartity run.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonStartRunADCLinearityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonStartRunADCLinearityActionPerformed
        // If no IPbus connection. Do nothing.
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("STADCLinearityPanel -> no IPbus PPrconnection");
            return;
        }

        // Run in progress: do nothing...
        if ((ADCLinearityRunStatus.get() == 1)
                || (STCISLinearityPanel.CISLinearityRunStatus.get() == 1)
                || (STCISLinearityPanel.FindBCRunStatus.get() == 1)
                || (STCISShapePanel.CISShapeRunStatus.get() == 1)
                || (STCISShapePanel.FindBCRunStatus.get() == 1)
                || (STPedestalPanel.PedestalRunStatusFlag.get() == 1)) {
            return;
        }

        // Updates the run status flag.
        ADCLinearityRunStatus.set(1);

        // Executes the given task.
        try {
            executor.execute(RunnableStartRun);
        } catch (RejectedExecutionException e) {
            System.out.println("STADCLinearityPanel -> RejectedExecutionException: " + e.getMessage());
        } catch (NullPointerException e) {
            System.out.println("STADCLinearityPanel -> NullPointerException: " + e.getMessage());
        }
    }//GEN-LAST:event_ButtonStartRunADCLinearityActionPerformed

    /**
     * Stops ADC linearity run.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonStopRunADCLinearityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonStopRunADCLinearityActionPerformed
        // If no IPbus PPr connection do nothing.
        if (!IPbusPanel.getConnectFlag()) {
            return;
        }

        // If run in progress, terminates it by setting the corresponding flag.
        // Otherwise do nothing.
        if (ADCLinearityRunStatus.get() == 1) {
            ADCLinearityRunStatus.set(0);

            // Updates label for ADC linearity run status.
            LabelADClinearityRunStatus.setText("terminating...");
            LabelADClinearityRunStatus.setForeground(Color.blue);

            // Updates MainFrame label for ADC linearity run status.
            MainFrame.LabelADCLinearityRunMainFrame.setText("not running");
            MainFrame.LabelCISLinearityRunMainFrame.setForeground(Color.RED);
        }
    }//GEN-LAST:event_ButtonStopRunADCLinearityActionPerformed

    /**
     * First gets all field values from text widgets. Then sets field values as
     * default values in properties configuration file.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonSaveAsDefaultsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonSaveAsDefaultsActionPerformed
        Console.print_cr("STADCLinearityPanel -> saving field values in properties file: " + Defaults.propsFile);
        get_all_parameters();
        Defaults.saveADCLinearityScanProperties();
    }//GEN-LAST:event_ButtonSaveAsDefaultsActionPerformed

    /**
     * Loads default pedestal run parameter values from properties configuration
     * file into field values. Then fills all text widgets with field values.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonLoadDefaultsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonLoadDefaultsActionPerformed
        Console.print_cr("STADCLinearityPanel -> loading default values from properties file: " + Defaults.propsFile);
        Defaults.loadADCLinearityProperties();
        set_all_parameters();
    }//GEN-LAST:event_ButtonLoadDefaultsActionPerformed

    /**
     * Sets minidrawers to process data in broadcast mode.
     *
     * @param evt Input ActionEvent object.
     */
    private void CheckBoxBroadcastMDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckBoxBroadcastMDActionPerformed
        if (CheckBoxBroadcastMD.isSelected()) {
            MDbroadcast = 1;
        } else {
            MDbroadcast = 0;
        }
    }//GEN-LAST:event_CheckBoxBroadcastMDActionPerformed

    /**
     * Sets MD1 to process data.
     *
     * @param evt Input ActionEvent object.
     */
    private void CheckBoxMD1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckBoxMD1ActionPerformed
        MDsel.set(0, CheckBoxMD1.isSelected() ? 1 : 0);
    }//GEN-LAST:event_CheckBoxMD1ActionPerformed

    /**
     * Sets MD2 to process data.
     *
     * @param evt Input ActionEvent object.
     */
    private void CheckBoxMD2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckBoxMD2ActionPerformed
        MDsel.set(1, CheckBoxMD2.isSelected() ? 1 : 0);
    }//GEN-LAST:event_CheckBoxMD2ActionPerformed

    /**
     * Sets MD3 to process data.
     *
     * @param evt Input ActionEvent object.
     */
    private void CheckBoxMD3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckBoxMD3ActionPerformed
        MDsel.set(2, CheckBoxMD3.isSelected() ? 1 : 0);
    }//GEN-LAST:event_CheckBoxMD3ActionPerformed

    /**
     * Sets MD4 to process data.
     *
     * @param evt Input ActionEvent object.
     */
    private void CheckBoxMD4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckBoxMD4ActionPerformed
        MDsel.set(3, CheckBoxMD4.isSelected() ? 1 : 0);
    }//GEN-LAST:event_CheckBoxMD4ActionPerformed

    /**
     * Shows default values from properties configuration file.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonShowDefaultsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonShowDefaultsActionPerformed
        Defaults.showProperties();
    }//GEN-LAST:event_ButtonShowDefaultsActionPerformed

    /**
     * Define Runnables through lambda expressions.
     */
    private void initDefineRunnables() {
        RunnableStartRun = () -> {
            run_start_ADC_linearity();
        };
    }

    /**
     *
     */
    public static void plot_BookHistograms() {
        String mdstr;

        // Defines 2d-array (4 minidrawers, 12 FEBs) of histogram JProfile with HG/LG.
        h_plot_LG = new JScan[4][12];
        h_plot_HG = new JScan[4][12];

        for (int md = 0; md < 4; md++) {
            mdstr = String.format("MD%1d", md + 1);
            for (int adc = 0; adc < 12; adc++) {
                if ("DACs bias offsets".equals(scanUnits)) {
                    htitle = String.format("%s Ch %1d (LG);DAC counts;", mdstr, adc);
                    h_plot_LG[md][adc] = new JScan(htitle, nbSteps, min_DAC, max_DAC);
                    h_plot_LG[md][adc].setYAxisMinMax(0, 4096);
                    h_plot_LG[md][adc].setXAxisMinMax(hxlow_DAC, hxhigh_DAC);
                    h_plot_LG[md][adc].setMarkerSize(4);
                    h_plot_LG[md][adc].setXAxisTickMarkSpacing(75);

                    htitle = String.format("%s Ch %1d (HG);DAC counts;", mdstr, adc);
                    h_plot_HG[md][adc] = new JScan(htitle, nbSteps, min_DAC, max_DAC);
                    h_plot_HG[md][adc].setYAxisMinMax(0, 4096);
                    h_plot_HG[md][adc].setXAxisMinMax(hxlow_DAC, hxhigh_DAC);
                    h_plot_HG[md][adc].setMarkerSize(4);
                    h_plot_HG[md][adc].setXAxisTickMarkSpacing(75);
                } else if ("ADC counts".equals(scanUnits)) {
                    htitle = String.format("%s Ch %1d (LG);ADC counts;", mdstr, adc);
                    h_plot_LG[md][adc] = new JScan(htitle, nbSteps, min_ADC, max_ADC);
                    h_plot_LG[md][adc].setYAxisMinMax(0, 4096);
                    h_plot_LG[md][adc].setXAxisMinMax(hxlow_ADC, hxhigh_ADC);
                    h_plot_LG[md][adc].setMarkerSize(4);
                    h_plot_LG[md][adc].setXAxisTickMarkSpacing(60);

                    htitle = String.format("%s Ch %1d (HG);ADC counts;", mdstr, adc);
                    h_plot_HG[md][adc] = new JScan(htitle, nbSteps, min_ADC, max_ADC);
                    h_plot_HG[md][adc].setYAxisMinMax(0, 4096);
                    h_plot_HG[md][adc].setXAxisMinMax(hxlow_ADC, hxhigh_ADC);
                    h_plot_HG[md][adc].setMarkerSize(4);
                    h_plot_HG[md][adc].setXAxisTickMarkSpacing(60);
                }
            }
        }

        // The JPanel where to place the histogram has to be added the property
        // of Set Layout to Border Layout by right-clicking on the Panel and 
        // select Set Layout.
        for (int adc = 0; adc < 12; adc++) {
            if ("LG".equals(selectedGainPlot)) {
                PanelPlots[adc].add(h_plot_LG[selectedMDPlot][adc], BorderLayout.CENTER);
            }
            if ("HG".equals(selectedGainPlot)) {
                PanelPlots[adc].add(h_plot_HG[selectedMDPlot][adc], BorderLayout.CENTER);
            }
        }

        // validate() method inherited from Java Container class.
        // Validates (lays out its subcomponents) this container and all 
        // of its subcomponents.
        for (int i = 0; i < 12; i++) {
            PanelPlots[i].validate();
        }
    }

    /**
     * Refresh plots.
     */
    private void plot_RefreshPanelPlots() {
        // Removes all present JScan plot components from JPanel plots ArrayList.
        for (int adc = 0; adc < 12; adc++) {
            PanelPlots[adc].removeAll();
        }

        for (int adc = 0; adc < 12; adc++) {
            if ("LG".equals(selectedGainPlot)) {
                PanelPlots[adc].add(h_plot_LG[selectedMDPlot][adc], BorderLayout.CENTER);
            }

            if ("HG".equals(selectedGainPlot)) {
                PanelPlots[adc].add(h_plot_HG[selectedMDPlot][adc], BorderLayout.CENTER);
            }

            PanelPlots[adc].revalidate();
            PanelPlots[adc].repaint();
        }
    }

    /**
     * Resets histogram settings.
     */
    private void plot_ResetHistograms() {
        String fs;
        String mdstr;

        // Removes all present JScan components from JPanel plots ArrayList.
        for (int adc = 0; adc < 12; adc++) {
            PanelPlots[adc].removeAll();
        }

        for (int md = 0; md < 4; md++) {
            for (int adc = 0; adc < 12; adc++) {
                h_plot_LG[md][adc].reset();
                h_plot_HG[md][adc].reset();
            }
        }

        for (int md = 0; md < 4; md++) {
            mdstr = String.format("MD%1d", md + 1);
            for (int adc = 0; adc < 12; adc++) {
                if ("DACs bias offsets".equals(scanUnits)) {
                    htitle = String.format("%s Ch %1d (HG);DAC counts;", mdstr, adc);
                    h_plot_HG[md][adc].setTitle(htitle);
                    h_plot_HG[md][adc].setBins(nbSteps, min_DAC, max_DAC);
                    h_plot_HG[md][adc].setXAxisMinMax(hxlow_DAC, hxhigh_DAC);
                    h_plot_HG[md][adc].setXAxisTickMarkSpacing(75);
                    h_plot_HG[md][adc].draw();

                    htitle = String.format("%s Ch %1d (LG);DAC counts;", mdstr, adc);
                    h_plot_LG[md][adc].setTitle(htitle);
                    h_plot_LG[md][adc].setBins(nbSteps, min_DAC, max_DAC);
                    h_plot_LG[md][adc].setXAxisMinMax(hxlow_DAC, hxhigh_DAC);
                    h_plot_LG[md][adc].setXAxisTickMarkSpacing(75);
                    h_plot_LG[md][adc].draw();

                } else if ("ADC counts".equals(scanUnits)) {
                    htitle = String.format("%s Ch %1d (HG);ADC counts;", mdstr, adc);
                    h_plot_HG[md][adc].setTitle(htitle);
                    h_plot_HG[md][adc].setBins(nbSteps, min_ADC, max_ADC);
                    h_plot_HG[md][adc].setXAxisMinMax(hxlow_ADC, hxhigh_ADC);
                    h_plot_HG[md][adc].setXAxisTickMarkSpacing(60);
                    h_plot_HG[md][adc].draw();

                    htitle = String.format("%s Ch %1d (LG);ADC counts;", mdstr, adc);
                    h_plot_LG[md][adc].setTitle(htitle);
                    h_plot_LG[md][adc].setBins(nbSteps, min_ADC, max_ADC);
                    h_plot_LG[md][adc].setXAxisMinMax(hxlow_ADC, hxhigh_ADC);
                    h_plot_LG[md][adc].setXAxisTickMarkSpacing(60);
                    h_plot_LG[md][adc].draw();
                }
            }
        }

        if (DebugSettings.getVerbose()) {
            fs = "    Total steps (HG): %d ";
            Console.print_cr(String.format(fs, nbSteps));
            fs = "    Total steps (LG): %d ";
            Console.print_cr(String.format(fs, nbSteps));
        }

        for (int adc = 0; adc < 12; adc++) {
            if ("LG".equals(selectedGainPlot)) {
                PanelPlots[adc].add(h_plot_LG[selectedMDPlot][adc], BorderLayout.CENTER);
            }

            if ("HG".equals(selectedGainPlot)) {
                PanelPlots[adc].add(h_plot_HG[selectedMDPlot][adc], BorderLayout.CENTER);
            }

            //PanelPlots[adc].revalidate();
            PanelPlots[adc].repaint();
        }
    }

    /**
     * Configures and starts pedestal run.
     */
    private synchronized void run_start_ADC_linearity() {
        Boolean ret;
        String fs;
        Integer step;
        Integer LastEvtBCID, LastEvtL1ID;

        Console.print_cr("==> ADC linearity run preparing... ");

        ArrayList<Integer> VinDACs = new ArrayList<>();
        VinDACs.clear();

        LabelADClinearityRunStatus.setText("preparing...");
        LabelADClinearityRunStatus.setForeground(Color.blue);

        // Change label run status in MainFrame.
        MainFrame.LabelADCLinearityRunMainFrame.setText("running");
        MainFrame.LabelADCLinearityRunMainFrame.setForeground(Color.GREEN.darker());

        // Initialize field for DAC/ADC counts scan value.
        LabelDACpCounter.setText(Integer.toString(0));
        LabelDACnCounter.setText(Integer.toString(0));
        LabelADCCounter.setText(Integer.toString(0));

        // Initialize field for number of steps.
        ProcScanPoint.set(0);
        LabelScanPointCounter.setText(Integer.toString(ProcScanPoint.get()));

        // Initialize field for events per step.
        ProcEventsPerStep.set(0);
        LabelEventsPerStepCounter.setText(Integer.toString(ProcEventsPerStep.get()));

        Console.print_cr("Getting ADC linearity scan parameters... ");

        // Get selected minidrawers.
        // -------------------------
        get_MDs();
        fs = "  Broadcast MD: %s";
        Console.print_cr(String.format(fs, ((MDbroadcast == 1) ? "True" : "False")));
        if (MDbroadcast == 0) {
            fs = "  Total minidrawers: %d. Active: %d %d %d %d";
            Console.print_cr(String.format(fs,
                    MDtot,
                    MDsel.get(0),
                    MDsel.get(1),
                    MDsel.get(2),
                    MDsel.get(3)));
        }

        if (MDtot == 0) {
            fs = "  No minidrawers selected.";
            Console.print_cr(String.format(fs));
            run_terminate_ADC_linearity();
            return;
        }

        // Get selected DB FPGA side.
        // -------------------------
        get_DB_FPGA_side();
        fs = "  DB FPGA Side: %d (%s)";
        Console.print_cr(String.format(fs, dbside, dbsidestring));

        // Gets number of samples to readout from each data pipeline.
        // ----------------------------------------------------------
        get_samples();
        Console.print_cr("  Samples: " + samples);

        // Get BCID to read pipeline data from.
        // ------------------------------------
        get_BC_to_read();
        fs = "  BC to read: %d";
        Console.print_cr(String.format(fs, BCID));

        // Get charge units (DACs or pC).
        // ------------------------------
        get_scan_units();
        fs = "  Charge units: %s";
        Console.print_cr(String.format(fs, scanUnits));

        // Get steps.
        // ----------
        get_steps();
        fs = "  Steps: %d. Evts/step: %d. Lengths-> %.1f DACs; %.1f ADCs";
        Console.print_cr(String.format(fs,
                nbSteps,
                stepEvents,
                step_length_DAC,
                step_length_ADC));
        for (Integer i = 0; i < nbSteps + 1; i++) {
            DACbiasP = Math.round(min_DAC) + i * Math.round(step_length_DAC);
            DACbiasN = Math.round(max_DAC) - i * Math.round(step_length_DAC);
            ADCped = i * Math.round(step_length_ADC);
            fs = "    Step: %d. DACbiasP: %d. DACbiasP: %d. ADCped: %d";
            Console.print_cr(String.format(fs, (i + 1), DACbiasP, DACbiasN, ADCped));
        }

        // Redefine JScan settings according to selected DAC step
        // or ADC step for the scan (DACs or ADCs).
        // ---------------------------------------------------------
        fs = "  Setting series binning...";
        Console.print_cr(String.format(fs));
        plot_ResetHistograms();

        // Set internal TTC.
        // -----------------
        Console.print_nr("Setting TTC internal... ");
        ret = pprlib.ppr.set_global_TTC_internal();
        fs = "%s";
        Console.print_cr(String.format(fs, (ret ? "Ok" : "Failed")));

        // Reset CRC counters.
        // -------------------
        Console.print_nr("Resetting CRC counters... ");
        ret = pprlib.ppr.reset_CRC_counters();
        fs = "%s";
        Console.print_cr(String.format(fs, (ret ? "Ok" : "Failed")));

        // Enable deadtime bit in global trigger configuration register
        // ------------------------------------------------------------
        Console.print_cr("Setting PPr enable deadtime bit in Global Trigger Conf... ");
        ret = pprlib.ppr.set_global_trigger_deadtime(0);
        fs = "  set bit to 0: %s";
        Console.print_cr(String.format(fs, (ret ? "Ok" : "Fail")));
        ret = pprlib.ppr.set_global_trigger_deadtime(1);
        fs = "  set bit to 1: %s";
        Console.print_cr(String.format(fs, (ret ? "Ok" : "Fail")));

        Console.print_cr("==> Starts loop on ADC linearity scan...");
        LabelADClinearityRunStatus.setText("processing...");
        LabelADClinearityRunStatus.setForeground(Color.blue);

        // Loop while:
        // 1) Run is not stopped.
        // 2) IPbus connection has not dropped.
        // 3) Processed events not reached the selected value.
        step = 0;
        while (true) {
            if (!IPbusPanel.getConnectFlag()
                    || ADCLinearityRunStatus.get() == 0
                    || step > nbSteps) {
                break;
            }

            // Initializes field for proccessed events per step.
            ProcEventsPerStep.set(0);

            if ("DACs bias offsets".equals(scanUnits)) {
                DACbiasP = Math.round(min_DAC) + step * Math.round(step_length_DAC);
                DACbiasN = Math.round(max_DAC) - step * Math.round(step_length_DAC);
                ADCped = pprlib.feb.convert_ped_DACs_to_ADC(DACbiasP, DACbiasN);
                fs = "  DAC Vp: %d. DAC Vn: %d. ADC counts: %d";
                Console.print_cr(String.format(fs, DACbiasP, DACbiasN, ADCped));
            } else if ("ADC counts".equals(scanUnits)) {
                ADCped = step * Math.round(step_length_ADC);
                VinDACs = pprlib.feb.convert_ped_ADC_to_DACs(ADCped);
                DACbiasP = VinDACs.get(0);
                DACbiasN = VinDACs.get(1);
                fs = "  ADC counts: %d. DAC Vp: %d. DAC Vn: %d";
                Console.print_cr(String.format(fs, ADCped, DACbiasP, DACbiasN));
            }

            // Configure FEBs pedestal settings.
            // ----------------------------------
            fs = "Step: %d";
            Console.print_cr(String.format(fs, step));
            Console.print_cr("Setting FEB ADC bias offsets... ");
            ret = set_FEB_ADC_bias_offsets_DACs();
            fs = "  Result: %s";
            Console.print_cr(String.format(fs, (ret ? "Ok" : "Failed")));

            // Configures FEBs to load ADC DACs.
            // ---------------------------------
            Console.print_cr("Loading FEB ADC DACs... ");
            ret = set_FEB_load_ADC_DACs();
            fs = "  Result: %s";
            Console.print_cr(String.format(fs, (ret ? "Ok" : "Failed")));

            // Update step counter.
            ProcScanPoint.addAndGet(1);
            LabelScanPointCounter.setText(Integer.toString(ProcScanPoint.get()));

            // DACs and ADC values counters.
            LabelDACpCounter.setText(Integer.toString(DACbiasP));
            LabelDACnCounter.setText(Integer.toString(DACbiasN));
            LabelADCCounter.setText(Integer.toString(ADCped));

            // Loops over events for a particular ADC scan value.
            // ---------------------------------------------------
            for (Integer event = 0; event < stepEvents; event++) {
                if (!IPbusPanel.getConnectFlag()
                        || ADCLinearityRunStatus.get() == 0) {
                    run_terminate_ADC_linearity();
                    return;
                }

                // Updates events per step counter.
                ProcEventsPerStep.addAndGet(1);
                LabelEventsPerStepCounter.setText(Integer.toString(ProcEventsPerStep.get()));

                // Reads last Event BCID (disables busy to read pipelines).
                LastEvtBCID = pprlib.ppr.get_counter_last_event_BCID();

                // Reads last Event L1ID (disables busy to read pipelines).
                LastEvtL1ID = pprlib.ppr.get_counter_last_event_L1ID();

                // Sends a PPr L1A.
                pprlib.feb.send_L1A(BCID, 3);

                if (DebugSettings.getVerbose()) {
                    fs = "Event: %d";
                    Console.print_cr(String.format(fs, event + 1));
                }

                // Reads pipeline data.
                for (Integer md = 0; md < 4; md++) {
                    if (MDsel.get(md) == 1) {
                        for (Integer adc = 0; adc < 12; adc++) {

                            dataHG[md][adc].getData().clear();
                            dataLG[md][adc].getData().clear();

                            dataHG[md][adc].setData(pprlib.ppr.get_data_HG(md, adc, samples));
                            dataLG[md][adc].setData(pprlib.ppr.get_data_LG(md, adc, samples));

                            if (DebugSettings.getVerbose()) {
                                if (adc == 0) {
                                    fs = "  MD: %d ADC: %d Data HG: %d %d %d %d %d %d %d %d %d %d %d %d ";
                                    Console.print_cr(String.format(fs,
                                            md + 1,
                                            adc,
                                            dataHG[md][adc].getData().get(0),
                                            dataHG[md][adc].getData().get(1),
                                            dataHG[md][adc].getData().get(2),
                                            dataHG[md][adc].getData().get(3),
                                            dataHG[md][adc].getData().get(4),
                                            dataHG[md][adc].getData().get(5),
                                            dataHG[md][adc].getData().get(6),
                                            dataHG[md][adc].getData().get(7),
                                            dataHG[md][adc].getData().get(8),
                                            dataHG[md][adc].getData().get(9),
                                            dataHG[md][adc].getData().get(10),
                                            dataHG[md][adc].getData().get(11)));

                                    fs = "  MD: %d ADC: %d Data LG: %d %d %d %d %d %d %d %d %d %d %d %d ";
                                    Console.print_cr(String.format(fs,
                                            md + 1,
                                            adc,
                                            dataLG[md][adc].getData().get(0),
                                            dataLG[md][adc].getData().get(1),
                                            dataLG[md][adc].getData().get(2),
                                            dataLG[md][adc].getData().get(3),
                                            dataLG[md][adc].getData().get(4),
                                            dataLG[md][adc].getData().get(5),
                                            dataLG[md][adc].getData().get(6),
                                            dataLG[md][adc].getData().get(7),
                                            dataLG[md][adc].getData().get(8),
                                            dataLG[md][adc].getData().get(9),
                                            dataLG[md][adc].getData().get(10),
                                            dataLG[md][adc].getData().get(11)));
                                }
                            }

                            // Loops over samples to fill histograms.
                            for (int samp = 0; samp < samples; samp++) {
                                h_plot_HG[md][adc].fill(step, dataHG[md][adc].getData().get(samp));
                                h_plot_LG[md][adc].fill(step, dataLG[md][adc].getData().get(samp));

                                h_plot_HG[md][adc].draw();
                                h_plot_LG[md][adc].draw();
                            }
                        }
                    }
                }
            }

            step++;
        }

        // Terminate run.
        run_terminate_ADC_linearity();
    }

    /**
     * Terminates run.
     */
    private synchronized void run_terminate_ADC_linearity() {
        // Sets run status flag as run terminated.
        ADCLinearityRunStatus.set(0);

        // Updates CIS linearity run status.
        LabelADClinearityRunStatus.setText("stopped");
        LabelADClinearityRunStatus.setForeground(Color.RED);

        // Updates MainFrame label for ADC linearity run status.
        MainFrame.LabelADCLinearityRunMainFrame.setText("not running");
        MainFrame.LabelADCLinearityRunMainFrame.setForeground(Color.RED);

        Console.print_cr("==> ADC linearity run terminated");
    }

    /**
     * Sets pedestal settings for selected FEBs (HG, LG, positive, negative).
     */
    private synchronized Boolean set_FEB_ADC_bias_offsets_DACs() {
        Boolean ret = true;
        String fs;
        int MDmin, MDmax;

        if (MDbroadcast == 1) {
            MDmin = 0;
            MDmax = 1;
        } else {
            MDmin = 1;
            MDmax = 5;
        }

        int mdidx = 0;
        for (Integer md = MDmin; md < MDmax; md++) {
            if ((MDbroadcast == 1) || ((MDbroadcast == 0) && (MDsel.get(mdidx) == 1))) {
                for (Integer adc = 0; adc < 12; adc++) {
                    ret = pprlib.feb.set_ped_HG_pos(md, dbside, adc, DACbiasP);
                    if (DebugSettings.getVerbose()) {
                        fs = "  MD: %d FEB: %2d set ped HGpos: %d result: %s";
                        Console.print_cr(String.format(fs, md, adc, DACbiasP, (ret ? "Ok" : "Fail")));
                    }

                    ret = pprlib.feb.set_ped_LG_pos(md, dbside, adc, DACbiasP);
                    if (DebugSettings.getVerbose()) {
                        fs = "  MD: %d FEB: %2d set ped LGpos: %d result: %s";
                        Console.print_cr(String.format(fs, md, adc, DACbiasP, (ret ? "Ok" : "Fail")));
                    }

                    ret = pprlib.feb.set_ped_HG_neg(md, dbside, adc, DACbiasN);
                    if (DebugSettings.getVerbose()) {
                        fs = "  MD: %d FEB: %2d set ped HGneg: %d result: %s";
                        Console.print_cr(String.format(fs, md, adc, DACbiasN, (ret ? "Ok" : "Fail")));
                    }

                    ret = pprlib.feb.set_ped_LG_neg(md, dbside, adc, DACbiasN);
                    if (DebugSettings.getVerbose()) {
                        fs = "  MD: %d FEB: %2d set ped LGneg: %d result: %s";
                        Console.print_cr(String.format(fs, md, adc, DACbiasN, (ret ? "Ok" : "Fail")));
                    }
                }
            }
            mdidx++;
        }

        return ret;
    }

    /**
     * Loads ADC DACS (HG and LG) for selected FEBs.
     */
    private synchronized Boolean set_FEB_load_ADC_DACs() {
        Boolean ret = true;
        String fs;
        int MDmin, MDmax;

        if (MDbroadcast == 1) {
            MDmin = 0;
            MDmax = 1;
        } else {
            MDmin = 1;
            MDmax = 5;
        }

        int mdidx = 0;
        for (Integer md = MDmin; md < MDmax; md++) {
            if ((MDbroadcast == 1) || ((MDbroadcast == 0) && (MDsel.get(mdidx) == 1))) {
                for (Integer adc = 0; adc < 12; adc++) {

                    ret = pprlib.feb.load_ped_HG(md, dbside, adc);
                    if (DebugSettings.getVerbose()) {
                        fs = "  MD: %d FEB: %2d load HG result: %s";
                        Console.print_cr(String.format(fs, md, adc, (ret ? "Ok" : "Fail")));
                    }

                    ret = pprlib.feb.load_ped_LG(md, dbside, adc);
                    if (DebugSettings.getVerbose()) {
                        fs = "  MD: %d FEB: %2d load LG result: %s";
                        Console.print_cr(String.format(fs, md, adc, (ret ? "Ok" : "Fail")));
                    }
                }
            }
            mdidx++;
        }

        return ret;
    }

    private PPrLib pprlib;

    /**
     * Declares an ExecutorService interface (extends Executor interface), an
     * asynchronous execution mechanism which is capable of executing tasks
     * concurrently in the background. ExecutorService is created and return
     * from methods of the Executors Class.
     */
    public static ThreadPoolExecutor executor;

    private Runnable RunnableStartRun;

    /**
     * Flag to define the ADC linearity run status. Atomic Java variables are
     * lock-free thread-safe programming variables whose values may be updated
     * atomically. They allow concurrent accesses (used by many threads
     * concurrently).
     */
    public static AtomicInteger ADCLinearityRunStatus;

    /**
     * Selected Daughterboard FPGA side.
     */
    public static Integer dbside;

    /**
     * Selected Daughterboard FPGA side (as String).
     */
    public static String dbsidestring;

    /**
     * Selected ArrayList with minidrawers to be filled with data.
     */
    public static ArrayList<Integer> MDsel;

    /**
     * Selected minidrawers broadcast flag.
     */
    public static Integer MDbroadcast;

    /**
     * Selected BCID for L1A.
     */
    public static Integer BCID;

    /**
     * Selected samples.
     */
    public static Integer samples;

    /**
     * Selected DAC steps.
     */
    public static Integer nbSteps;

    /**
     * Selected units for the scan (DACs or ADCs).
     */
    public static String scanUnits;

    /**
     * Selected events per DAC step.
     */
    public static Integer stepEvents;

    /**
     * Step length in DACs (both for LG and HG).
     */
    private Float step_length_DAC;

    /**
     * Step length in ADC counts (both for LG and HG).
     */
    private Float step_length_ADC;

    /**
     * Selected DAC bias positive pedestal.
     */
    private Integer DACbiasP;

    /**
     * Selected DAC bias negative pedestal.
     */
    private Integer DACbiasN;

    /**
     * Selected ADC pedestal counts.
     */
    private Integer ADCped;

    /**
     * Total selected MDs.
     */
    private Integer MDtot;

    /**
     * Declares AtomicInteger counter for proccessed step.
     */
    AtomicInteger ProcScanPoint;

    /**
     * Declares AtomicInteger counter for proccessed events per step.
     */
    AtomicInteger ProcEventsPerStep;

    private static javax.swing.JPanel[] PanelPlots;

    private static String htitle;

    private static Float hxlow_DAC;
    private static Float hxhigh_DAC;

    private static Float min_DAC;
    private static Float max_DAC;

    private static Float hxlow_ADC;
    private static Float hxhigh_ADC;

    private static Float min_ADC;
    private static Float max_ADC;

    /**
     * Declares 2d-array (4 minidrawers, 12 FEBs) of JScan plots with HG (high
     * gain) ADC linearity data.
     */
    private static JScan[][] h_plot_HG;

    /**
     * Declares 2d-array (4 minidrawers, 12 FEBs) of JScan plots with LG (low
     * gain) ADC linearity data.
     */
    private static JScan[][] h_plot_LG;

    /**
     * Selected minidrawer for plots.
     */
    public static Integer selectedMDPlot;

    /**
     * Selected gain for plots.
     */
    public static String selectedGainPlot;

    /**
     * Declares object with HG data 2d-array of Integer ArrayList with samples.
     */
    private DataArrayList[][] dataHG;

    /**
     * Declares object with LG data 2d-array of Integer ArrayList with samples.
     */
    private DataArrayList[][] dataLG;

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ButtonLoadDefaults;
    private javax.swing.JButton ButtonSaveAsDefaults;
    private javax.swing.JButton ButtonSetHGPlots;
    private javax.swing.JButton ButtonSetLGPlots;
    private javax.swing.JButton ButtonSetMD1Plots;
    private javax.swing.JButton ButtonSetMD2Plots;
    private javax.swing.JButton ButtonSetMD3Plots;
    private javax.swing.JButton ButtonSetMD4Plots;
    private javax.swing.JButton ButtonShowDefaults;
    private javax.swing.JButton ButtonStartRunADCLinearity;
    private javax.swing.JButton ButtonStopRunADCLinearity;
    public static javax.swing.JCheckBox CheckBoxBroadcastMD;
    public static javax.swing.JCheckBox CheckBoxMD1;
    public static javax.swing.JCheckBox CheckBoxMD2;
    public static javax.swing.JCheckBox CheckBoxMD3;
    public static javax.swing.JCheckBox CheckBoxMD4;
    public static javax.swing.JComboBox<String> ComboBoxDBSide;
    public static javax.swing.JComboBox<String> ComboBoxSamples;
    public static javax.swing.JComboBox<String> ComboBoxScanUnits;
    private javax.swing.JLabel LabelADCCounter;
    private javax.swing.JLabel LabelADClinearityRunStatus;
    private javax.swing.JLabel LabelDACnCounter;
    private javax.swing.JLabel LabelDACpCounter;
    private javax.swing.JLabel LabelEventsPerStepCounter;
    private javax.swing.JLabel LabelScanPointCounter;
    private javax.swing.JPanel PanelPlot1;
    private javax.swing.JPanel PanelPlot10;
    private javax.swing.JPanel PanelPlot11;
    private javax.swing.JPanel PanelPlot12;
    private javax.swing.JPanel PanelPlot2;
    private javax.swing.JPanel PanelPlot3;
    private javax.swing.JPanel PanelPlot4;
    private javax.swing.JPanel PanelPlot5;
    private javax.swing.JPanel PanelPlot6;
    private javax.swing.JPanel PanelPlot7;
    private javax.swing.JPanel PanelPlot8;
    private javax.swing.JPanel PanelPlot9;
    public static javax.swing.JTextField TextFieldBCID;
    public static javax.swing.JTextField TextFieldEvtsStep;
    public static javax.swing.JTextField TextFieldSteps;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel25;
    private javax.swing.JPanel jPanel26;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    // End of variables declaration//GEN-END:variables
}
