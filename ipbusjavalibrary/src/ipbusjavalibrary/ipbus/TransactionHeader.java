/*
 * IPbusJavaLibrary project
 * 2019 (c) IFIC-CERN
 */
package ipbusjavalibrary.ipbus;

import ipbusjavalibrary.utilities.ByteTools;

/**
 * CONTROL header constructor.
 *
 * @author Juan Valls {@literal (valls@cern.ch)}
 * @author Carlos Solans {@literal (solans@cern.ch)}
 */
public class TransactionHeader {

    /**
     * Transaction header Type ID for READ
     */
    public static Integer READ = 0;

    /**
     * Trasaction header Type ID for WRITE
     */
    public static Integer WRITE = 1;

    /**
     * Trasaction header Type ID for non-incremented READ FIFO
     */
    public static Integer READFIFO = 2;

    /**
     * Trasaction header Type ID for non-incremented WRITE FIFO
     */
    public static Integer WRITEFIFO = 3;

    /**
     * Trasaction header Type ID for RMWbits
     */
    public static Integer RMWBITS = 4;

    /**
     * Trasaction header Type ID for RMWsum
     */
    public static Integer RMWSUM = 5;

    /**
     * Transaction header Packet Info Code for successful responses
     */
    public static Integer RESPONSE = 0x0;

    /**
     * Transaction header Packet Info Code for requests
     */
    public static Integer REQUEST = 0xF;

    /**
     * Packet Protocol Version value, 4 bits [28-31]
     */
    private Integer pver;

    /**
     * Packet Transaction ID value, 12 bits [16-27]
     */
    private Integer trid;

    /**
     * Packet Words (size) value, 8 bits [8-15]
     */
    private Integer size;

    /**
     * Packet Type ID value, 4 bits [4-7]
     */
    private Integer type;

    /**
     * Packet Info Code value, 4 bits [0-3]
     */
    private Integer info;

    /**
     * Full packet header value, 32 bits
     */
    private Integer head;

    /**
     * Constructor. Set default transaction header bits.
     */
    public TransactionHeader() {
        pver = 2;
        trid = 0;
        size = 0;
        type = 0;
        info = 0xF;
        pack();
    }

    /**
     * Set CONTROL header bits into CONTROL header word.
     */
    private void pack() {
        head = 0;
        head |= (pver & 0xF) << 28;
        head |= (trid & 0xFFF) << 16;
        head |= (size & 0xFF) << 8;
        head |= (type & 0xF) << 4;
        head |= (info & 0xF) << 0;
    }

    /**
     * Convert CONTROL header word into CONTROL header bits.
     */
    private void unpack() {
        pver = (head >> 28) & 0xF;
        trid = (head >> 16) & 0xFFF;
        size = (head >> 8) & 0XFF;
        type = (head >> 4) & 0xF;
        info = (head >> 0) & 0xF;
    }

    /**
     * Getter for CONTROL header Words.
     *
     * @return the CONTROL header Words.
     */
    public Integer getSize() {
        return size;
    }

    /**
     * Setter for the CONTROL header Words.
     *
     * @param size Input CONTROL header Words.
     */
    public void setSize(Integer size) {
        this.size = size;
        pack();
    }

    /**
     * Getter for the CONTROL header Type ID
     *
     * @return the CONTROL header Type ID.
     */
    public Integer getType() {
        return type;
    }

    /**
     * Setter for the CONTROL header Type ID.
     *
     * @param type Input CONTROL header Type ID.
     */
    public void setType(Integer type) {
        this.type = type;
        pack();
    }

    /**
     * Getter for CONTROL header Info Code.
     *
     * @return the CONTROL header Info Code.
     */
    public Integer getInfo() {
        return info;
    }

    /**
     * Setter for the CONTROL header Info Code
     *
     * @param info Input CONTROL header Info Code.
     */
    public void setInfo(Integer info) {
        this.info = info;
        pack();
    }

    /**
     * Getter for the CONTROL header Transaction ID
     *
     * @return the CONTROL header Transaction ID.
     */
    public Integer getTrid() {
        return trid;
    }

    /**
     * Setter for the CONTROL header Transaction ID
     *
     * @param trid Input CONTROL header Transaction ID.
     */
    public void setTrid(Integer trid) {
        this.trid = trid;
        pack();
    }

    /**
     * Getter for the CONTROL header Protocol Version
     *
     * @return the CONTROL header Protocol Version.
     */
    public Integer getPver() {
        return pver;
    }

    /**
     * Setter for the CONTROL header Protocol Version.
     *
     * @param pver Input CONTROL header Protocol Version.
     */
    public void setPver(Integer pver) {
        this.pver = pver;
        pack();
    }

    /**
     * Getter for the CONTROL header.
     *
     * @return the CONTROL header.
     */
    public Integer getHeader() {
        return head;
    }

    /**
     * Setter for the CONTROL header.
     *
     * @param head CONTROL header.
     */
    public void setHeader(Integer head) {
        this.head = head;
        unpack();
    }

    /**
     * Getter for the CONTROL header four bytes.
     *
     * @return the CONTROL header four bytes.
     */
    public byte[] getBytes() {
        pack();
        byte[] ret = new byte[4];
        ByteTools.addBytes(ret, 0, head);
        return ret;
    }

    /**
     * Setter for the CONTROL header from an input byte array stream.
     *
     * @param b Input byte array.
     * @param pos Input index byte array position.
     */
    public void setBytes(byte[] b, Integer pos) {
        head = ByteTools.pack(b, pos);
        unpack();
    }

}
