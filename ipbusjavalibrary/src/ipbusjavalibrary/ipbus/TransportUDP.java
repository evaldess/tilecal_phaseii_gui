/*
 * IPbusJavaLibrary project
 * 2019 (c) IFIC-CERN
 */
package ipbusjavalibrary.ipbus;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.channels.IllegalBlockingModeException;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Abstract class to create a generic IPbus packet.
 *
 * @author Juan Valls {@literal (valls@cern.ch)}
 * @author Carlos Solans {@literal (solans@cern.ch)}
 */
public class TransportUDP extends Transport {

    /**
     * Java DatagramSocket. A DatagramSocket is a socket for sending and
     * receiving datagram packets. It is used as a sending or receiving point
     * for a packet delivery service.
     */
    private DatagramSocket socket;

    /**
     * IP address on the remote host for DatagramPackets.
     */
    private InetAddress host;

    /**
     * Internet port on the remote host for DatagramPackets.
     */
    private Integer port;

    /**
     * Atomic Integer variable for concurrent accesses. It is used for handling
     * received transactions after sending data through a socket.
     */
    private AtomicInteger socketStatus;

    /**
     * Status boolean flag from the TransportUDP instance object constructor.
     */
    private Boolean retTransportUDP = false;

    /**
     * Declares and defines a ThreadPoolExecutor class object to create a thread
     * pool. A thread pool manages a collection of Runnable threads.
     */
    private ThreadPoolExecutor executor;

    // Defines instance of ThreadPoolExecutor class to create a thread pool.
    private Callable<Boolean> CallableRecPackAck;

    /**
     *
     * @param hostname Input hostname String.
     * @param port Input port String.
     * @param threadFactoryName Input name for the ThrteadPoolExecutor thread.
     */
    public TransportUDP(String hostname, Integer port, String threadFactoryName) {
        if (Debug.getDebugIPbus()) {
            System.out.println("TransportUDP() -> host: " + hostname
                    + "  port: " + port + "  ThreadPoolExecutor: "
                    + threadFactoryName);
        }

        // Defines a thread pool through a ThreadPoolExecutor.
        executor = new ThreadPoolExecutor(
                20,
                20,
                0L,
                TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>(),
                new IPbusThreadFactory(threadFactoryName));

        // Defines Callables through lambda expressions.
        initDefineCallables();

        socketStatus = new AtomicInteger(0);

        this.port = port;

        // Gets the IP address for the remote host as an InetAddress object, given 
        // the host's name.
        try {
            host = InetAddress.getByName(hostname);
        } catch (UnknownHostException ex) {
            System.out.println("TransportUDP() -> UnknownHostException on InetAddress.getByName = "
                    + ex.getMessage());
            retTransportUDP = false;
            return;
        } catch (SecurityException ex) {
            System.out.println("TransportUDP() -> SecurityException on InetAddress.getByName = "
                    + ex.getMessage());
            retTransportUDP = false;
            return;
        }

        // Creates a client DatagramSocket to initiate a communication with 
        // a host/server for sending/receiving datagram packets.
        // Binds it to any available port on the client local host (note there are no  
        // arguments in the DatagramSocket constructor). 
        // Throws a SocketException.
        try {
            if (Debug.getDebugIPbus()) {
                System.out.println("TransportUDP() -> trying to create DatagramSocket... ");
            }
            socket = new DatagramSocket();
            if (Debug.getDebugIPbus()) {
                System.out.println("TransportUDP() -> UDP DatagramSocket created. ");
            }
        } catch (SocketException ex) {
            System.out.println("TransportUDP() -> SocketException on creating DatagramSocket() = " + ex);
            retTransportUDP = false;
            return;
        }

        retTransportUDP = true;
    }

    /**
     * Sets the timeout that a read() call to a DatagramSocket will block.
     *
     * @param milis Input time in milliseconds.
     * @return the boolean status to the call to setSoTimeout().
     */
    @Override
    public Boolean setTimeout(Integer milis) {

        // Enables SO_TIMEOUT with the specified timeout (milliseconds).
        // When set to a non-zero timeout, a call to receive() for this 
        // DatagramSocket will block for only this amount of time. If the 
        // timeout expires, a SocketException is raised, though the DatagramSocket 
        // is still valid. The option must be enabled prior to entering the 
        // blocking operation to have effect. The timeout must be > 0. A timeout 
        // of zero is interpreted as an infinite timeout.
        try {
            if (socket != null) {
                if (Debug.getDebugIPbus()) {
                    System.out.println("TransportUDP.setTimeout() -> setting timeout to: " + milis + " milliseconds");
                }
                socket.setSoTimeout(milis);
            }
        } catch (SocketException ex) {
            System.out.println("TransportUDP.setTimeout() Error: " + ex.getMessage());
            return false;
        }

        return true;
    }

    /**
     * Sends an input request IPbus Packet object and returns an IPbus response
     * Packet.
     *
     * @param inputPacket Input IPbus request Packet.
     * @return an IPbus response Packet.
     */
    @Override
    public Packet send(Packet inputPacket) {
        // Packs...
        inputPacket.pack();

        // Creates reference to the the IPbus response Packet object. 
        // Initialized to null.
        Packet response = null;

        if (Debug.getDebugIPbus()) {
            System.out.println("TransportUDP.send() -> Trying to create a request DatagramPacket for host: "
                    + host + " port: " + port);
        }

        // Constructs a request DatagramPacket objet from the input IPbus Packet.
        // The DatagramPacket arguments are a byte array with the data to be 
        // sent, its length, the IP address (InetAddress) of the remote host,  
        // and the port number on the remote host.
        DatagramPacket sP = new DatagramPacket(
                inputPacket.getBytes(),
                inputPacket.getLength(),
                host,
                port);

        if (Debug.getDebugIPbus()) {
            System.out.println("TransportUDP.send() -> request DatagramPacket created from input Packet object of type "
                    + inputPacket.getPacketType(inputPacket.getType()) + ".");
            System.out.println("TransportUDP.send() -> PacketID: " + inputPacket.getPacketID()
                    + String.format(" (0x%04X)", inputPacket.getPacketID()));
            System.out.println("TransportUDP.send() -> Dump packet byte frames to be sent:");
            inputPacket.dumpTotalWords();
            inputPacket.dumpBytes();
        }

        // Sends the request input DatagramPacket to the host through the client 
        // DatagramSocket. Throws exception on failure.
        try {
            socket.send(sP);
        } catch (IOException ex) {
            if (Debug.getDebugIPbus()) {
                System.out.println("TransportUDP.send() -> IOException on send packet data = "
                        + ex.getMessage());
            }
            return response;
        } catch (SecurityException ex) {
            if (Debug.getDebugIPbus()) {
                System.out.println("TransportUDP.send() -> SecurityException on send packet data = "
                        + ex.getMessage());
            }
            return response;
        } catch (IllegalBlockingModeException ex) {
            if (Debug.getDebugIPbus()) {
                System.out.println("TransportUDP.send() -> IllegalBlockingModeException on send packet data = "
                        + ex.getMessage());
            }
            return response;
        } catch (IllegalArgumentException ex) {
            if (Debug.getDebugIPbus()) {
                System.out.println("TransportUDP.send() -> IllegalArgumentException on send packet data = "
                        + ex.getMessage());
            }
            return response;
        }

        if (Debug.getDebugIPbus()) {
            System.out.println("TransportUDP.send() -> request DatagramPacket successfully sent through DatagramSocket.");
        }

        // For input IPbus CONTROL request transaction packets increments 
        // the generic IPbus header Packet ID.
        //inputPacket.sent();
        socketStatus.set(0);

        if (Debug.getDebugIPbus()) {
            System.out.println("TransportUDP.send() -> Thread started. Waiting for response DatagramPacket...");
        }

        // Receive acknowledge. Executes the given task.
        try {
            Future<Boolean> futureReadAll = executor.submit(CallableRecPackAck);
        } catch (RejectedExecutionException e) {
            if (Debug.getDebugIPbus()) {
                System.out.println("TransportUDP.send() -> RejectedExecutionException = "
                        + e.getMessage());
            }
        } catch (NullPointerException e) {
            if (Debug.getDebugIPbus()) {
                System.out.println("TransportUDP.send() -> NullPointerException = "
                        + e.getMessage());
            }
        }

        // Wait for data or timeout
        while (socketStatus.get() == 0) {
        }

        if (socketStatus.get() == 1) {
            // According to header, set IPbus response packet to 
            // either STATUS, CONTROL or RESENT type
            response = responsePacketBuilder.getPacket();

            if (Debug.getDebugIPbus()) {
                System.out.println("TransportUDP.send() -> Dump incoming response packet byte frames:");
                response.dumpTotalWords();
                response.dumpBytes();
            }
        }

        return response;
    }

    /**
     * Getter with a boolean flag from the TransportUDP instance object
     * constructor.
     *
     * @return a Boolean with the status of TransportUDP constructor.
     */
    @Override
    public Boolean getTransportReturn() {
        return retTransportUDP;
    }

    /**
     *
     * @return the executor thread pool.
     */
    @Override
    public ThreadPoolExecutor getTransportExecutor() {
        return executor;
    }

    /**
     * Define Callables through lambda expressions. A Callable object is an
     * interface designed to provide a common protocol for objects that wish to
     * execute code while they are active
     */
    public final void initDefineCallables() {
        CallableRecPackAck = () -> {
            return send_ReceivedPacket();
        };
    }

    /**
     * Receives a DatagramPacket from the client socket. When this method
     * returns, the DatagramPacket's buffer is filled with the data received.
     * The DatagramPacket also contains the sender's IP address, and the port
     * number on the sender's machine. This method blocks until a datagram is
     * received.
     *
     * @return the...
     */
    public Boolean send_ReceivedPacket() {
        // Constructs a response DatagramPacket for receiving packets of 
        // size length.
        DatagramPacket rP = new DatagramPacket(
                responsePacketBuilder.getBytes(),
                responsePacketBuilder.getBytes().length);

        // Receives a DatagramPacket. When this method returns, 
        // the DatagramPacket's buffer is filled with the data received.
        try {
            socket.receive(rP);
        } catch (IOException ex) {
            if (Debug.getDebugIPbus()) {
                System.out.println("TransportUDP::send_ReceivedPacket() -> IOException on receive acknowledge = "
                        + ex.getMessage());
            }
            socketStatus.set(-1);
            return false;
        } catch (IllegalBlockingModeException ex) {
            if (Debug.getDebugIPbus()) {
                System.out.println("TransportUDP.send_ReceivedPacket() -> IllegalBlockingModeException on receive acknowledge = "
                        + ex.getMessage());
            }
            socketStatus.set(-1);
            return false;
        }

        // Sets the response PacketBuilder length.
        responsePacketBuilder.setLength(rP.getLength());

        // Unpacks the response PacketBuilder IPbus header.
        responsePacketBuilder.unpack();

        socketStatus.set(1);

        return true;
    }

    /**
     *
     * @return the close connection return Boolean status.
     */
    @Override
    public Boolean closeConnection() {
        return true;
    }
}
