/*
 * IPbusJavaLibrary project
 * 2019 (c) IFIC-CERN
 */
package ipbusjavalibrary.ipbus;

import java.util.Vector;
import ipbusjavalibrary.utilities.ByteTools;

/**
 * STATUS packet constructor. Extends class Packet. A generic IPbus header is
 * built (done through the Packet base class constructor). The IPbus header
 * Packet Type is set to STATUS and the Packet ID initialized to zero. A new
 * Vector with the packet data is also constructed.
 *
 * @author Juan Valls {@literal (valls@cern.ch)}
 * @author Carlos Solans {@literal (solans@cern.ch)}
 */
public class Status extends Packet {

    /**
     * Default size for STATUS request/response data stream packets.
     */
    private final Integer SIZE = 15;

    /**
     * Wrapper class Vector variable (Integers) with STATUS packet values.
     */
    private Vector<Integer> values;

    /**
     * Constructor. Creates an IPbus STATUS packet. This is a generic IPbus
     * header of STATUS type followed with 15 data words. The IPbus header
     * Packet ID is initialized to zero. A Vector with packet data is also
     * created.
     */
    public Status() {
        // Executes Packet base class constructor.
        super();

        // Sets the generic IPbus header Packet Type ID to STATUS (4-bits).
        super.setType(PacketHeader.STATUS);

        // Sets the generic IPbus header Packet ID to zero.
        super.getHeader().setPkid(0);

        // Initializes data values for the STATUS packet.
        values = new Vector<>();
        for (int i = 0; i < SIZE; i++) {
            values.add(0);
        }
    }

    /**
     *
     * @return the
     */
    public Integer getBufferSize() {
        return values.get(1);
    }

    /**
     *
     * @param v Input
     */
    public void setBufferSize(Integer v) {
        values.set(1, v);
    }

    /**
     * For STATUS response packets, gets the next expected IPbus packet header.
     * This value is stored in the 32-bit third word of the response data
     * stream. The data is logically shift right 8 bits and then everything
     * masked with 16 bits.
     *
     * @return Packet ID (16-bit word) value.
     */
    public Integer getNextExpectedPacketID() {
        return (values.get(2) >>> 8) & 0xFFFF;
    }

    /**
     * For STATUS response packets, sets the next expected IPbus packet header.
     * This is the value of the 32-bit third word after the header response
     * packet.
     *
     * @param v the next expected IPbus packet header.
     */
    public void setNextExpectedPacketID(Integer v) {
        values.set(2, (v & 0xFFFF) << 8);
    }

    /**
     * Adds byte values to the input byte stream array. Overrides method pf the
     * Packet abstract class.
     *
     * @param b Input byte array destination where to add data.
     * @param pos Byte index position where to start to add byte contents.
     * @return Last index position in the input destination byte array.
     */
    @Override
    public Integer addBytes(byte[] b, Integer pos) {
        for (int i = 0; i < SIZE; i++) {
            pos = ByteTools.addBytes(b, pos, values.get(i));
        }
        return pos;
    }

    /**
     * Parse byte stream. Overrides method of the Packet abstract class.
     *
     * @param b Input byte array destination where to set the data.
     * @param pos Byte array index position where to start to set byte contents.
     * @return the last index position in the input destination byte array.
     */
    @Override
    public Integer setBytes(byte[] b, Integer pos) {
        values.clear();
        for (int i = 0; i < SIZE; i++) {
            values.add(ByteTools.pack(b, pos));
            pos += 4;
        }
        return pos;
    }

}
