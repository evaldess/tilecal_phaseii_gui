/*
 * IPbusJavaLibrary project
 * 2019 (c) IFIC-CERN
 */
package ipbusjavalibrary.ipbus;

import java.util.ArrayList;
import java.util.Objects;

import ipbusjavalibrary.utilities.ByteTools;

/**
 * CONTROL transaction packet constructor. Extends class Packet. In its
 * constructor, a generic IPbus header is built where its Packet ID is set to
 * the next expected Packet ID (all this done through the Packet base class
 * constructor). The next expected Packet ID is obtained from a response STATUS
 * packet received from a host remote (server). The IPbus header Packet Type is
 * set to CONTROL. A new CONTROL transaction header is built. And a new
 * ArrayList with the packet data is also constructed.
 *
 * @author Juan Valls {@literal (valls@cern.ch)}
 * @author Carlos Solans {@literal (solans@cern.ch)}
 */
public class Transaction extends Packet {

    /**
     * TransactionHeader object type theader.
     */
    private final TransactionHeader theader;

    /**
     * IPbus transaction BASE_ADDRESS word.
     */
    private Integer address;

    /**
     * ArrayList with Integer values for CONTROL WRITE/READ transactions.
     */
    private final ArrayList<Integer> values;

    /**
     * Transaction header Packet Type ID READ.
     */
    public static Integer READ = 0;

    /**
     * Transaction header Packet Type ID WRITE.
     */
    public static Integer WRITE = 1;

    /**
     * Transaction header Packet Type ID READFIFO (non incrementing READ).
     */
    public static Integer READFIFO = 2;

    /**
     * Transaction header Packet Type ID WRITEFIFO (non-incrementing WRITE).
     */
    public static Integer WRITEFIFO = 3;

    /**
     * Transaction header Info Code REQUEST.
     */
    public static Integer REQUEST = 0xF;

    /**
     * Transaction header Info Code RESPONSE.
     */
    public static Integer RESPONSE = 0x0;

    /**
     * Constructor. Creates a transaction CONTROL packet. A generic IPbus header
     * is built where its Packet ID is set to the next expected Packet ID (all
     * this done through the Packet base class constructor). The IPbus header
     * Packet Type is set to CONTROL. A new CONTROL transaction header is built.
     * And a new ArrayList with the packet data is also constructed.
     */
    public Transaction() {
        // Execute Packet base class constructor
        super();

        // Sets a generic IPbus header Packet Type to CONTROL (4-bits)
        super.setType(PacketHeader.CONTROL);

        // Creates a transaction CONTROL header.
        theader = new TransactionHeader();

        // Sets default transaction CONTROL header Packet Type ID (to READ).
        theader.setType(TransactionHeader.READ);

        // Initializes packet BASE_ADDRESS and data words.
        address = 0;
        values = new ArrayList<>();
    }

    /**
     * Setter for the packet BASE_ADDRESS.
     *
     * @param address Input packet BASE_ADDRESS word.
     */
    public void setAddress(Integer address) {
        this.address = address;
    }

    /**
     * Getter for the packet BASE_ADDRESS in CONTROL write/read transactions.
     *
     * @return the packet BASE_ADDRESS word.
     */
    public Integer getAddress() {
        return address;
    }

    /**
     * Getter for CONTROL header Type ID.
     *
     * @return the CONTROL header Type ID.
     */
    public Integer getTransactionType() {
        return theader.getType();
    }

    /**
     * Getter for the CONTROL header Words.
     *
     * @return the CONTROL header Words.
     */
    public Integer getSize() {
        return theader.getSize();
    }

    /**
     * Getter for the CONTROL header. A TransactionHeader object type.
     *
     * @return the CONTROL header TransactionHeader object type.
     */
    public TransactionHeader getTransactionHeader() {
        return theader;
    }

    /**
     * Gats an ArrayList with CONTROL transaction packet data words.
     *
     * @return the ArrayList with the CONTROL transaction packet data words.
     */
    public ArrayList<Integer> getData() {
        return values;
    }

    /**
     * Gets a single CONTROL transaction packet data word.
     *
     * @param i Input index in the ArrayList CONTROL transaction packet stream
     * from where to extract the data word.
     * @return the value of the CONTROL transaction packet data word.
     */
    public Integer getData(Integer i) {
        return values.get(i);
    }

    /**
     * Sets an IPbus transaction header Packet Type to READ. Sets transaction
     * header size. Sets packet BASE_ADDRESS word.
     *
     * @param address Input packet BASE_ADDRESS word.
     * @param size Packet size (transaction header Words).
     */
    public void setRead(Integer address, Integer size) {
        setRead(address, size, false);
    }

    /**
     * Sets an IPbus transaction header Packet Type to READ/READFIFO. Sets
     * transaction header size. Sets packet BASE_ADDRESS word.
     *
     * @param address Input packet BASE_ADDRESS word.
     * @param size Packet size (transaction header Words).
     * @param fifo True for non-incremented READ (READFIFO). False for READ.
     */
    public void setRead(Integer address, Integer size, Boolean fifo) {
        theader.setType((fifo ? READFIFO : READ));
        theader.setSize(size);
        this.address = address;
    }

    /**
     * Sets an IPbus transaction header Packet Type to WRITE. Sets transaction
     * header size (Words) to a single word. Sets packet BASE_ADDRESS word. Sets
     * packet transaction data stream to input value.
     *
     * @param address Input packet BASE_ADDRESS word.
     * @param value Input data to be written in packet.
     */
    public void setWrite(Integer address, Integer value) {
        getHeader().setPkid(getNextPacketID());
        theader.setType(WRITE);
        theader.setSize(1);
        this.address = address;
        this.values.clear();
        this.values.add(value);
    }

    /**
     * Sets an IPbus transaction header Packet Type to WRITE. Sets transaction
     * header size to the input data size. Sets packet BASE_ADDRESS word. Sets
     * packet data to input values.
     *
     * @param address Input packet BASE_ADDRESS word.
     * @param values Input data to be written in packet.
     */
    public void setWrite(Integer address, ArrayList<Integer> values) {
        setWrite(address, values, false);
    }

    /**
     * Sets an IPbus transaction header Packet Type to WRITE/WRITEFIFO. Sets
     * transaction header size to the input data size. Sets packet BASE_ADDRESS
     * word. Sets packet data to input values.
     *
     * @param address Input packet BASE_ADDRESS word.
     * @param values Input data to be written in packet.
     * @param fifo True for non-incremented WRITE (WRITEFIFO). False for WRITE.
     */
    public void setWrite(Integer address, ArrayList<Integer> values, Boolean fifo) {
        theader.setType((fifo ? WRITEFIFO : WRITE));
        theader.setSize(values.size());
        this.address = address;
        this.values.clear();
        for (Integer i = 0; i < values.size(); i++) {
            this.values.add(values.get(i));
        }
    }

    // Increments the generic IPbus header Packet ID.
    @Override
    public void sent() {
        incrementNextPacketID();
    }

    /**
     * Adds values to the input byte stream array. Overrides method of the
     * Packet abstract class.
     *
     * @param b Input byte array destination where to add data.
     * @param pos Byte array index position where to start to add byte contents.
     * @return the last index position in the input destination byte array.
     */
    @Override
    public Integer addBytes(byte[] b, Integer pos) {
        // Encode header
        Integer newpos = ByteTools.addBytes(b, pos, theader.getHeader());

        // On a CONTROL header READ/READFIFO request packet adds 
        // BASE_ADDRESS word.
        if (Objects.equals(theader.getType(), READ)
                || Objects.equals(theader.getType(), READFIFO)) {
            if (Objects.equals(theader.getInfo(), REQUEST)) {
                newpos = ByteTools.addBytes(b, newpos, address);
                // On a CONTROL header READ/READFIFO response packet adds 
                // as many words as given by the Words bits of the CONTROL 
                // header.
            } else if (Objects.equals(theader.getInfo(), RESPONSE)) {
                for (int i = 0; i < theader.getSize(); i++) {
                    newpos = ByteTools.addBytes(b, newpos, values.get(i));
                }
            }
            // On a CONTROL header WRITE/WRITEFIFO request packet adds 
            // BASE_ADDRESS word followed by as many words as given by the 
            // Words bits of the CONTROL header.
        } else if (Objects.equals(theader.getType(), WRITE)
                || Objects.equals(theader.getType(), WRITEFIFO)) {
            if (Objects.equals(theader.getInfo(), REQUEST)) {
                newpos = ByteTools.addBytes(b, newpos, address);

                for (int i = 0; i < theader.getSize(); i++) {
                    newpos = ByteTools.addBytes(b, newpos, values.get(i));
                }

                // On a CONTROL header WRITE/WRITEFIFO reponse packet do nothing.
            } else if (Objects.equals(theader.getInfo(), RESPONSE)) {
            }
        }
        return newpos;
    }

    /**
     * Parse packet byte stream. Overrides method of the Packet abstract class.
     *
     * @param b Input byte array destination where to set the data.
     * @param pos Byte array index position where to start to set byte contents.
     * @return the last index position in the input destination byte array.
     */
    @Override
    public Integer setBytes(byte[] b, Integer pos) {

        // Parse IPbus CONTROL header.
        theader.setHeader(ByteTools.pack(b, pos));
        pos += 4;

        // Interpret the packet according to the header	
        if (Objects.equals(theader.getType(), READ)
                || Objects.equals(theader.getType(), READFIFO)) {
            if (Objects.equals(theader.getInfo(), REQUEST)) {
                address = ByteTools.pack(b, pos);
                pos += 4;
            } else if (Objects.equals(theader.getInfo(), RESPONSE)) {
                values.clear();
                for (int i = 0; i < theader.getSize(); i++) {
                    values.add(ByteTools.pack(b, pos));
                    pos += 4;
                }
            }
        } else if (Objects.equals(theader.getType(), WRITE)
                || Objects.equals(theader.getType(), WRITEFIFO)) {
            if (Objects.equals(theader.getInfo(), REQUEST)) {
                address = ByteTools.pack(b, pos);
                pos += 4;
                for (int i = 0; i < theader.getSize(); i++) {
                    values.add(ByteTools.pack(b, pos));
                    pos += 4;
                }
            } else if (Objects.equals(theader.getInfo(), RESPONSE)) {
                //Nothing to do
            }
        }
        return pos;
    }
}
