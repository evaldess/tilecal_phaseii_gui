#
# Sergio Landete
# Fernando Carrio Argos
# University of Valencia
# fernando.carrio@cern.ch
#
from __future__ import print_function
import ttk
import Tkinter as tk
import matplotlib
matplotlib.use("TkAgg")
matplotlib.use("Agg")

from matplotlib.figure import Figure
import numpy as np
import sys
sys.path.append('./tabs')
## Import all tabs

from MainPanelTab import MainPanelTab
from ADClinTab import ADClinTab
from CISlinTab import CISlinTab
from CISshapeTab import CISshapeTab
from PedNoiseTab import PedNoiseTab

LARGE_FONT=("Verdana", 12)        
        
class TILECAL(tk.Tk):
    def __init__(self, *args, **kwargs):
        self._running_anim = None
        tk.Tk.__init__(self, *args, **kwargs)

        style = ttk.Style()                     
        current_theme =style.theme_use()
        
        style.theme_settings(current_theme, {"TNotebook.Tab": {"configure": {"padding": [30, 10]}}})
        style.configure('TNotebook.Tab', font=('URW Gothic L','11','bold') )    
        
        tabControl = ttk.Notebook(self)
        tab1 = tk.Frame(tabControl)
        tab1.grid_rowconfigure(0, weight=1)
        tab1.grid_columnconfigure(0, weight=1)
        tab1.pack(side="top", fill="both", expand= True)
        tabControl.add(tab1, text='Main')

        tab2 = tk.Frame(tabControl)
        tab2.grid_rowconfigure(0, weight=1)
        tab2.grid_columnconfigure(0, weight=1)
        tab2.pack(side="top", fill="both", expand= True)
        tabControl.add(tab2, text='ADC Linearity')      
                 
        tab3 = tk.Frame(tabControl)
        tab3.grid_rowconfigure(0, weight=1)
        tab3.grid_columnconfigure(0, weight=1)
        tab3.pack(side="top", fill="both", expand= True)
        tabControl.add(tab3, text='Cis Linearity')      
                 
        tab4 = tk.Frame(tabControl)
        tab4.grid_rowconfigure(0, weight=1)
        tab4.grid_columnconfigure(0, weight=1)
        tab4.pack(side="top", fill="both", expand= True)
        tabControl.add(tab4, text='Cis Shape')      
                  
        tab5 = tk.Frame(tabControl)
        tab5.grid_rowconfigure(0, weight=1)
        tab5.grid_columnconfigure(0, weight=1)
        tab5.pack(side="top", fill="both", expand= True)
        tabControl.add(tab5, text='Pedestal Noise')      
        tabControl.pack(expand=1, fill="both")

       # self.attributes('-fullscreen', True)####pantalla completa
        # w = self.winfo_screenwidth()
        # h = self.winfo_screenheight()
        
        self.title("Prometeo Interface 2019")
        self.iconbitmap('CERN.ico')

        self.geometry(("%dx%d")%(1200,600))
        self.resizable(False, False)
        
        # Main Panel 
        MainP= MainPanelTab(tab1, self)
        MainP.grid(row=0, column=0, sticky="nsew")  

        ADClinP= ADClinTab(tab2, self)
        ADClinP.grid(row=0, column=0, sticky="nsew")                        
        CISlinP= CISlinTab(tab3, self)
        CISlinP.grid(row=0, column=0, sticky="nsew")                        
        CISshapeP= CISshapeTab(tab4, self)
        CISshapeP.grid(row=0, column=0, sticky="nsew")                        
        PedNoiseP= PedNoiseTab(tab5, self)
        PedNoiseP.grid(row=0, column=0, sticky="nsew")                        

app=TILECAL()
app.mainloop()