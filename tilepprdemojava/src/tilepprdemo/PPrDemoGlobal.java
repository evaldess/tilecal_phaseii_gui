/* 
 *  PPrDemo Test Bench Java Client
 *  2019 (c) IFIC-CERN
 */
package tilepprdemo;

import java.awt.Color;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import ipbusjavalibrary.utilities.ByteTools;

import static ipbusjavalibrary.utilities.ByteTools.setBit;

import tilepprjavalibrary.lib.PPrLib;

/**
 * @author Juan Valls {@literal (valls@cern.ch)}
 * @author Fernando Carrió {@literal (fernando.carrio@cern.ch)}
 * @author Alberto Valero {@literal (alberto.valero@cern.ch)}
 */
public class PPrDemoGlobal extends javax.swing.JPanel {

    /**
     * Creates new form PPrDemoGlobal class.
     *
     * @param pprlib
     */
    public PPrDemoGlobal(PPrLib pprlib) {
        System.out.println("PPrDemoGlobal -> constructor");

        // NetBeans components settings.
        initComponents();

        // Set PPrLib class object as given by constructor argument.
        this.pprlib = pprlib;

        LabelReadGlobalTTC.setText("");
        LabelReadGlobalTrigger.setText("");
        LabelReadSyncComm.setText("");

        // Define Runnables through lambda expressions.
        initDefineRunnables();

        // Defines instance of ThreadPoolExecutor class to create a thread pool.
        // A thread pool manages a collection of Runnable threads.   
        executor = new ThreadPoolExecutor(
                5,
                5,
                0L,
                TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>(),
                new TBThreadFactory("PPrDemoGlobal"));
    }

    /**
     * Defines Runnables for threads. A Runnable object is an interface designed
     * to provide a common protocol for objects that wish to execute code while
     * they are active
     */
    private void initDefineRunnables() {
        RunnableReadGlobalTTC = () -> {
            readGlobalTTC();
        };

        RunnableReadGlobalTrigger = () -> {
            readGlobalTrigger();
        };

        RunnableReadSyncCmdConf = () -> {
            readSyncCmdConf();
        };

        RunnableReadCmdCounters = () -> {
            readCmdCounters();
        };

        RunnableReadLastEventCounters = () -> {
            readLastEventCounters();
        };

        RunnableReadGlobalPipeline = () -> {
            readGlobalPipeline();
        };

        RunnableReadMDPipelines = () -> {
            readMDPipelines();
        };

        RunnableResetCmdCounters = () -> {
            resetCmdCounters();
        };

        RunnableWriteGlobalTTC = () -> {
            writeGlobalTTC();
        };

        RunnableWriteGlobalTrigger = () -> {
            writeGlobalTrigger();
        };

        RunnableWriteSyncCmd = () -> {
            writeSyncCmdConf();
        };

        RunnableWriteGlobalPipeline = () -> {
            writeGlobalPipeline();
        };

        RunnableWriteMDPipelines = () -> {
            writeMDPipelines();
        };
    }

    /**
     * Reads PPr TTC Global Configuration register and bit contents.
     */
    private synchronized void readGlobalTTC() {
        Integer val;
        String fs;

        // Reads the TTC Global Configuration register.
        val = pprlib.ppr.get_global_TTC();
        if (val == null) {
            LabelReadGlobalTTC.setText("Error");
            LabelReadGlobalTTC.setForeground(Color.RED);
            return;
        } else {
            LabelReadGlobalTTC.setText(String.format("0x%08X", val));
            LabelReadGlobalTTC.setForeground(Color.blue);
        }

        fs = "%s 0x%08X -> %s";
        Console.print_cr(String.format(fs, "PPrDemoGlobal.readGlobalTTC -> ",
                val, ByteTools.intToString(val, 4)));

        // Parses TTCrx address.
        Integer ttcRxAddr = val & 0x3FFF;
        fs = "%s %d";
        Console.print_cr(String.format(fs, " TTCrx address: ", ttcRxAddr));
        TextFieldTTCrxAddress.setText(String.format("%d", ttcRxAddr));

        // Parses CIS to DAC conversion.
        Integer CISDACconv = (val & 0x1C000) >> 14;
        fs = "%s %d";
        Console.print_cr(String.format(fs, " CIS to DAC conversion: ", CISDACconv));
        TextFieldCISDACconv.setText(String.format("%d", CISDACconv));

        // Parses Internal/External bit.
        Integer TTCint = ByteTools.getBit(22, val);
        fs = "%s %d -> %s";
        Console.print_cr(String.format(fs, " Internal TTC bit: ", TTCint, (TTCint == 0 ? "Internal" : "External")));
        ComboBoxTTCIntExt.setSelectedIndex(TTCint);

        // Parses enable max. BCID bit.
        Integer enMaxBCID = ByteTools.getBit(26, val);
        fs = "%s %d";
        Console.print_cr(String.format(fs, " Enable max. BCID bit: ", enMaxBCID));
        ComboBoxEnableMaxBCID.setSelectedIndex(enMaxBCID);
    }

    /**
     * Reads PPr Trigger Global Configuration register.
     */
    private synchronized void readGlobalTrigger() {
        String fs;

        Integer val = pprlib.ppr.get_global_trigger();
        if (val == null) {
            LabelReadGlobalTrigger.setText("Error");
            LabelReadGlobalTrigger.setForeground(Color.RED);
            return;
        } else {
            LabelReadGlobalTrigger.setText(String.format("0x%08X", val));
            LabelReadGlobalTrigger.setForeground(Color.blue);
        }

        fs = "%s 0x%08X -> %s";
        Console.print_cr(String.format(fs, "PPrDemoGlobal.readGlobalTrigger -> ",
                val, ByteTools.intToString(val, 4)));

        // Reads manual L1A bit.
        Integer manL1A = ByteTools.getBit(1, val);
        fs = "%s %d";
        Console.print_cr(String.format(fs, " Manual L1A bit: ", manL1A));
        ComboBoxManualL1A.setSelectedIndex(manL1A);

        // Reads enable trigger bit.
        Integer enableTrig = ByteTools.getBit(2, val);
        fs = "%s %d";
        Console.print_cr(String.format(fs, " Enable trigger bit: ", enableTrig));
        ComboBoxEnableTrigger.setSelectedIndex(enableTrig);

        // Reads enable deadtime bit:
        Integer enableDT = ByteTools.getBit(3, val);
        fs = "%s %d";
        Console.print_cr(String.format(fs, " Enable deadtime bit: ", enableDT));
        ComboBoxEnableDeadtime.setSelectedIndex(enableDT);

        // Reads L1A delay.
        Integer l1del = (val & 0x1FF0) >> 4;
        fs = "%s %d";
        Console.print_cr(String.format(fs, " L1A delay: ", l1del));
        TextFieldL1Adel.setText(String.format("%d", l1del));

        // Reads BCID for L1A.
        Integer bcidforl1a = (val & 0x1FFE000) >> 13;
        fs = "%s %d";
        Console.print_cr(String.format(fs, " BCID for L1A: ", bcidforl1a));
        TextFieldBCIDforL1A.setText(String.format("%d", bcidforl1a));

        // Reads orbits (in BC units).
        Integer orbits = (val & 0xFE000000) >> 25;
        fs = "%s %d";
        Console.print_cr(String.format(fs, " Rate (orbit units): ", orbits));
        TextFieldOrbits.setText(String.format("%d", orbits));
    }

    /**
     * Reads PPr Sync Command Configuration register.
     */
    private synchronized void readSyncCmdConf() {
        String fs;

        Integer val = pprlib.ppr.get_sync_cmd();
        if (val == null) {
            LabelReadSyncComm.setText("Error");
            LabelReadSyncComm.setForeground(Color.RED);
            return;
        } else {
            LabelReadSyncComm.setText(String.format("0x%08X", val));
            LabelReadSyncComm.setForeground(Color.blue);
        }

        fs = "%s 0x%08X -> %s";
        Console.print_cr(String.format(fs, "PPrDemoGlobal.readSyncCmdConf -> ",
                val, ByteTools.intToString(val, 4)));

        // Reads Reset bit.
        Integer reset = ByteTools.getBit(0, val);
        fs = "%s %d";
        Console.print_cr(String.format(fs, " Reset bit: ", reset));
        ComboBoxReset.setSelectedIndex(reset);

        // Reads Enable bit.
        Integer enable = ByteTools.getBit(1, val);
        fs = "%s %d";
        Console.print_cr(String.format(fs, " Enable bit: ", enable));
        ComboBoxEnable.setSelectedIndex(enable);

        // Reads number of iterations
        Integer nbiter = (val & 0xFFFC) >> 2;
        fs = "%s %d";
        Console.print_cr(String.format(fs, " Nb of iterations: ", nbiter));
        TextFieldNbIter.setText(String.format("%d", nbiter));

        // Reads reset command counter
        Integer rstcmdcnt = ByteTools.getBit(16, val);
        fs = "%s %d";
        Console.print_cr(String.format(fs, " Reset cmd counter bit: ", rstcmdcnt));
        ComboBoxResetCmdCnt.setSelectedIndex(rstcmdcnt);

        // Reads Disable DCS bit.
        Integer disDCS = ByteTools.getBit(17, val);
        fs = "%s %d";
        Console.print_cr(String.format(fs, " Disable DCS bit: ", disDCS));
        ComboBoxDisableDCS.setSelectedIndex(disDCS);
    }

    /**
     * Reads PPr Commands Counter register.
     */
    private synchronized void readCmdCounters() {
        String fs;
        Integer val;

        Console.print_cr("PPrDemoGlobal.readCmdCounters() -> ");

        // Reads Command Counter register.
        val = pprlib.ppr.get_counter_cmd();
        if (val == null) {
            TextFieldCmdCnt.setText("Error");
            TextFieldCmdCnt.setForeground(Color.RED);
        } else {
            TextFieldCmdCnt.setText(String.format("%d", val));
            fs = "%s 0x%08X (%d)";
            Console.print_cr(String.format(fs, " cmd counter: ",
                    val, val));
        }

        val = pprlib.ppr.get_counter_async_cmd();
        if (val == null) {
            TextFieldAsyncCmdCnt.setText("Error");
            TextFieldAsyncCmdCnt.setForeground(Color.RED);
        } else {
            TextFieldAsyncCmdCnt.setText(String.format("%d", val));
            fs = "%s 0x%08X (%d)";
            Console.print_cr(String.format(fs, " async cmd counter: ",
                    val, val));
        }

        val = pprlib.ppr.get_counter_sync_cmd();
        if (val == null) {
            TextFieldSyncCmdCnt.setText("Error");
            TextFieldSyncCmdCnt.setForeground(Color.RED);
        } else {
            TextFieldSyncCmdCnt.setText(String.format("%d", val));
            fs = "%s 0x%08X (%d)";
            Console.print_cr(String.format(fs, " sync cmd counter: ",
                    val, val));
        }
    }

    /**
     * Resets Command counters. Cycles from 1 to 0 the Reset Cmd Counter bit of
     * the PPr Synchronous Commands Configuration register.
     */
    private synchronized void resetCmdCounters() {
        Boolean ret = pprlib.ppr.reset_cmd_counters();

        String fs = "%s %s";
        Console.print_cr(String.format(fs, "PPrDemoGlobal.resetCmdCounters() -> ",
                (ret ? "Ok" : "Fail")));
    }

    /**
     * Reads Last Event registers.
     */
    private synchronized void readLastEventCounters() {
        Integer val;
        String fs;

        Console.print_cr("PPrDemoGlobal.readLastEventCounters() -> ");

        val = pprlib.ppr.get_counter_last_event_BCID();
        if (val == null) {
            TextFieldLastEvtBCIDCnt.setText("Error");
            TextFieldLastEvtBCIDCnt.setForeground(Color.RED);
            return;
        } else {
            TextFieldLastEvtBCIDCnt.setText(String.format("%d", val));
        }

        fs = " Last Event BCID counter: 0x%08X (%d)";
        Console.print_cr(String.format(fs, val, val));

        val = pprlib.ppr.get_counter_last_event_L1ID();
        if (val == null) {
            TextFieldLastEvtL1IDCnt.setText("Error");
            TextFieldLastEvtL1IDCnt.setForeground(Color.RED);
        } else {
            TextFieldLastEvtL1IDCnt.setText(String.format("%d", val));
        }

        fs = " Last Event L1ID counter: 0x%08X (%d)";
        Console.print_cr(String.format(fs, val, val));

        val = pprlib.ppr.get_counter_L1A();
        if (val == null) {
            TextFieldL1ACnt.setText("Error");
            TextFieldL1ACnt.setForeground(Color.RED);
        } else {
            TextFieldL1ACnt.setText(String.format("%d", val));
        }

        fs = " L1A counter: 0x%08X (%d)";
        Console.print_cr(String.format(fs, val, val));

        val = pprlib.ppr.get_counter_Events_Out();
        if (val == null) {
            TextFieldEvtsOutCnt.setText("Error");
            TextFieldEvtsOutCnt.setForeground(Color.RED);
        } else {
            TextFieldEvtsOutCnt.setText(String.format("%d", val));
        }

        fs = " Events Out counter: 0x%08X (%d)";
        Console.print_cr(String.format(fs, val, val));
    }

    /**
     *
     */
    private synchronized void readGlobalPipeline() {
        Integer val = pprlib.ppr.get_global_pipeline();
        if (val == null) {
            TextFieldGlobalPipeline.setText("Error");
            TextFieldGlobalPipeline.setForeground(Color.RED);
            return;
        } else {
            TextFieldGlobalPipeline.setText(String.format("%d", val));
        }

        String fs = "%s 0x%08X";
        Console.print_cr(String.format(fs, "PPrDemoGlobal.readGlobalPipeline -> ",
                val));
    }

    /**
     *
     */
    private synchronized void readMDPipelines() {

    }

    /**
     * Writes PPr TTC Global Configuration register.
     */
    private synchronized void writeGlobalTTC() {
        String fs;

        Console.print_cr("PPrDemoGlobal.writeGlobalTTC() -> writing TTC Global register...");

        // Builds initial register value.
        Integer SyncCmdConf = 0;
        fs = "%s 0x%08X -> %s";
        Console.print_cr(String.format(fs, "  Init register val: ",
                SyncCmdConf, ByteTools.intToString(SyncCmdConf, 4)));

        // Gets TTCrx address.
        Integer TTCrxAdd = Integer.valueOf(TextFieldTTCrxAddress.getText(), 10);
        if (TTCrxAdd > 16383) {
            TTCrxAdd = 16383;
            TextFieldTTCrxAddress.setText("16383");
        }
        if (TTCrxAdd < 0) {
            TTCrxAdd = 0;
            TextFieldTTCrxAddress.setText("0");
        }

        fs = "%s 0x%08X (%d) -> %s";
        Console.print_cr(String.format(fs, "  TTCrx address set to: ",
                TTCrxAdd, TTCrxAdd, ByteTools.intToString(TTCrxAdd, 4)));

        SyncCmdConf = ByteTools.setBits(0, 13, TTCrxAdd, SyncCmdConf);
        if (SyncCmdConf == -1) {
            Console.print_cr("PPrDemoGlobal -> Error from ByteTools.setBits(): " + SyncCmdConf);
        }

        // Gets CIS DAC conversion.
        Integer CisDacconv = Integer.valueOf(TextFieldCISDACconv.getText(), 10);
        if (CisDacconv > 7) {
            CisDacconv = 7;
            TextFieldCISDACconv.setText("7");
        }
        if (CisDacconv < 0) {
            CisDacconv = 0;
            TextFieldCISDACconv.setText("0");
        }

        fs = "%s 0x%08X (%d) -> %s";
        Console.print_cr(String.format(fs, "  CIS to DAC conversion set to: ",
                CisDacconv, CisDacconv, ByteTools.intToString(CisDacconv, 4)));

        SyncCmdConf = ByteTools.setBits(14, 16, CisDacconv, SyncCmdConf);
        if (SyncCmdConf == -1) {
            Console.print_cr("PPrDemoGlobal -> Error from ByteTools.setBits(): " + SyncCmdConf);
        }

        // Gets TTC Internal/External bit.        
        Integer TTCint = ComboBoxTTCIntExt.getSelectedIndex();
        fs = "%s %d -> %s";
        Console.print_cr(String.format(fs, "  Internal TTC bit set to: ",
                TTCint, (TTCint == 0 ? "Internal" : "External")));
        SyncCmdConf = setBit(22, TTCint, SyncCmdConf);

        // Gets enable max. BCID bit.        
        Integer enMaxBCID = Integer.parseInt((String) ComboBoxEnableMaxBCID.getSelectedItem());
        fs = "%s %d";
        Console.print_cr(String.format(fs, "  Enable max. BCID bit set to: ",
                enMaxBCID));
        SyncCmdConf = setBit(26, enMaxBCID, SyncCmdConf);

        // Final register value.
        fs = "%s 0x%08X -> %s";
        Console.print_cr(String.format(fs, "  Final register val: ",
                SyncCmdConf, ByteTools.intToString(SyncCmdConf, 4)));

        // Writes register.
        Boolean ret = pprlib.ppr.set_global_TTC(SyncCmdConf);

        fs = "%s %s";
        Console.print_cr(String.format(fs, "  Write status: ",
                (ret ? "Ok" : "Fail")));
    }

    /**
     * Writes PPr Trigger Global Configuration register.
     */
    private synchronized void writeGlobalTrigger() {
        String fs;

        Console.print_cr("PPrDemoGlobal.writeGlobalTrigger() -> writing Trigger Global register...");

        // Builds initial register.
        Integer SyncCmdConf = 0;
        fs = "%s 0x%08X -> %s";
        Console.print_cr(String.format(fs, "  Init register val: ",
                SyncCmdConf, ByteTools.intToString(SyncCmdConf, 4)));

        // Gets manual L1A bit.        
        Integer manL1A = Integer.parseInt((String) ComboBoxManualL1A.getSelectedItem());
        fs = "%s %d";
        Console.print_cr(String.format(fs, "  Manual L1A bit set to: ",
                manL1A));
        SyncCmdConf = setBit(1, manL1A, SyncCmdConf);

        // Gets enable trigger bit.        
        Integer enTrg = Integer.parseInt((String) ComboBoxEnableTrigger.getSelectedItem());
        fs = "%s %d";
        Console.print_cr(String.format(fs, "  Enable trigger bit set to: ",
                enTrg));
        SyncCmdConf = setBit(2, enTrg, SyncCmdConf);

        // Gets enable deadtime bit.        
        Integer enDT = Integer.parseInt((String) ComboBoxEnableDeadtime.getSelectedItem());
        fs = "%s %d";
        Console.print_cr(String.format(fs, "  Enable deadtime bit set to: ",
                enDT));
        SyncCmdConf = setBit(3, enDT, SyncCmdConf);

        // Gets L1A delay.
        Integer l1aDel = Integer.valueOf(TextFieldL1Adel.getText(), 10);
        if (l1aDel > 511) {
            l1aDel = 511;
            TextFieldL1Adel.setText("511");
        }
        if (l1aDel < 0) {
            l1aDel = 0;
            TextFieldL1Adel.setText("0");
        }

        fs = "%s 0x%08X (%d) -> %s";
        Console.print_cr(String.format(fs, "  L1A delay set to: ",
                l1aDel, l1aDel, ByteTools.intToString(l1aDel, 4)));

        SyncCmdConf = ByteTools.setBits(4, 12, l1aDel, SyncCmdConf);
        if (SyncCmdConf == -1) {
            Console.print_cr("PPrDemoGlobal -> Error from ByteTools.setBits(): " + SyncCmdConf);
        }

        // Gets BCID for L1A.
        Integer bcid = Integer.valueOf(TextFieldBCIDforL1A.getText(), 10);
        if (bcid > 4095) {
            bcid = 4095;
            TextFieldBCIDforL1A.setText("4095");
        }
        if (bcid < 0) {
            bcid = 0;
            TextFieldBCIDforL1A.setText("0");
        }

        fs = "%s 0x%08X (%d) -> %s";
        Console.print_cr(String.format(fs, "  BCID for L1A set to: ",
                bcid, bcid, ByteTools.intToString(bcid, 4)));

        SyncCmdConf = ByteTools.setBits(13, 24, bcid, SyncCmdConf);
        if (SyncCmdConf == -1) {
            Console.print_cr("PPrDemoGlobal -> Error from ByteTools.setBits(): " + SyncCmdConf);
        }

        // Gets rate (in orbits).
        Integer rate = Integer.valueOf(TextFieldOrbits.getText(), 10);
        if (rate > 127) {
            rate = 127;
            TextFieldOrbits.setText("127");
        }
        if (rate < 0) {
            rate = 0;
            TextFieldOrbits.setText("0");
        }

        fs = "%s 0x%08X (%d) -> %s";
        Console.print_cr(String.format(fs, "  Rate (orbit units) set to: ",
                rate, rate, ByteTools.intToString(bcid, 4)));

        SyncCmdConf = ByteTools.setBits(25, 31, rate, SyncCmdConf);
        if (SyncCmdConf == -1) {
            Console.print_cr("PPrDemoGlobal -> Error from ByteTools.setBits(): " + SyncCmdConf);
        }

        // Final register value.
        fs = "%s 0x%08X -> %s";
        Console.print_cr(String.format(fs, "  Final register val: ",
                SyncCmdConf, ByteTools.intToString(SyncCmdConf, 4)));

        // Writes register.
        Boolean ret = pprlib.ppr.set_global_trigger(SyncCmdConf);

        fs = "%s %s";
        Console.print_cr(String.format(fs, "  Write status: ",
                (ret ? "Ok" : "Fail")));
    }

    /**
     * Writes PPr SyncCommand Configuration register.
     */
    private synchronized void writeSyncCmdConf() {
        String fs;

        Console.print_cr("PPrDemoGlobal.writeSyncCmdConf() -> writing Sync Cmd conf register...");

        // Builds initial word.
        Integer SyncCmdConf = 0;
        fs = "%s 0x%08X -> %s";
        Console.print_cr(String.format(fs, "  Init register val: ",
                SyncCmdConf, ByteTools.intToString(SyncCmdConf, 4)));

        // Gets Reset bit.
        Integer reset = Integer.parseInt((String) ComboBoxReset.getSelectedItem());
        fs = "%s %d";
        Console.print_cr(String.format(fs, "  Reset bit set to: ",
                reset));
        SyncCmdConf = setBit(0, reset, SyncCmdConf);

        // Gets Enable bit.
        Integer enable = Integer.parseInt((String) ComboBoxEnable.getSelectedItem());
        fs = "%s %d";
        Console.print_cr(String.format(fs, "  Enable bit set to: ",
                enable));
        SyncCmdConf = setBit(1, enable, SyncCmdConf);

        // Gets number of iterations.
        Integer nbIter = Integer.valueOf(TextFieldNbIter.getText(), 10);
        if (nbIter > 16383) {
            nbIter = 16383;
            TextFieldNbIter.setText("16383");
        }
        if (nbIter < 0) {
            nbIter = 0;
            TextFieldNbIter.setText("0");
        }

        fs = "%s 0x%08X (%d) -> %s";
        Console.print_cr(String.format(fs, "  Nb of iterations set to: ",
                nbIter, nbIter, ByteTools.intToString(nbIter, 4)));

        SyncCmdConf = ByteTools.setBits(2, 15, nbIter, SyncCmdConf);
        if (SyncCmdConf == -1) {
            Console.print_cr("PPrDemoGlobal -> Error from ByteTools.setBits(): " + SyncCmdConf);
        }

        // Gets reset command counter bit.
        Integer resetCmdCnt = Integer.parseInt((String) ComboBoxResetCmdCnt.getSelectedItem());
        fs = "%s %d";
        Console.print_cr(String.format(fs, "  Reset cmd counter bit set to: ",
                resetCmdCnt));
        SyncCmdConf = setBit(16, resetCmdCnt, SyncCmdConf);

        // Gets disable DCS bit.        
        Integer disDCS = Integer.parseInt((String) ComboBoxDisableDCS.getSelectedItem());
        fs = "%s %d";
        Console.print_cr(String.format(fs, "  Disable DSC bit set to: ",
                disDCS));
        SyncCmdConf = setBit(17, disDCS, SyncCmdConf);

        // Final register value.
        fs = "%s 0x%08X -> %s";
        Console.print_cr(String.format(fs, "  Final register val: ",
                SyncCmdConf, ByteTools.intToString(SyncCmdConf, 4)));

        // Writes register.
        Boolean ret = pprlib.ppr.set_sync_cmd(SyncCmdConf);

        fs = "%s %s";
        Console.print_cr(String.format(fs, "  Write status: ",
                (ret ? "Ok" : "Fail")));
    }

    private synchronized void writeGlobalPipeline() {
        Integer val = Integer.valueOf(TextFieldGlobalPipeline.getText(), 10);
        if (val > 255) {
            val = 255;
            TextFieldGlobalPipeline.setText("255");
        }
        if (val < 0) {
            val = 0;
            TextFieldGlobalPipeline.setText("0");
        }

        String fs = "%s 0x%08X (%d) -> %s";
        Console.print_cr(String.format(fs, "PPrDemoGlobal.writeGlobalPipeline() -> ",
                val, val, ByteTools.intToString(val, 4)));

        Boolean ret = pprlib.ppr.set_global_pipeline(val);

        fs = "%s %s";
        Console.print_cr(String.format(fs, "Write status: ",
                (ret ? "Ok" : "Fail")));
    }

    private synchronized void writeMDPipelines() {

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    private void initComponents() {//GEN-BEGIN:initComponents
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        ComboBoxTTCIntExt = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        TextFieldTTCrxAddress = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        TextFieldCISDACconv = new javax.swing.JTextField();
        ComboBoxEnableMaxBCID = new javax.swing.JComboBox<>();
        filler12 = new javax.swing.Box.Filler(new java.awt.Dimension(15, 0), new java.awt.Dimension(15, 0), new java.awt.Dimension(15, 32767));
        filler13 = new javax.swing.Box.Filler(new java.awt.Dimension(15, 0), new java.awt.Dimension(15, 0), new java.awt.Dimension(15, 32767));
        filler14 = new javax.swing.Box.Filler(new java.awt.Dimension(15, 0), new java.awt.Dimension(15, 0), new java.awt.Dimension(15, 32767));
        jPanel30 = new javax.swing.JPanel();
        ButtonWriteGlobalTTC = new javax.swing.JButton();
        ButtonReadGlobalTTC = new javax.swing.JButton();
        LabelReadGlobalTTC = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        TextFieldL1Adel = new javax.swing.JTextField();
        TextFieldOrbits = new javax.swing.JTextField();
        TextFieldBCIDforL1A = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        ComboBoxManualL1A = new javax.swing.JComboBox<>();
        ComboBoxEnableTrigger = new javax.swing.JComboBox<>();
        ComboBoxEnableDeadtime = new javax.swing.JComboBox<>();
        filler3 = new javax.swing.Box.Filler(new java.awt.Dimension(10, 0), new java.awt.Dimension(15, 0), new java.awt.Dimension(10, 32767));
        filler4 = new javax.swing.Box.Filler(new java.awt.Dimension(15, 0), new java.awt.Dimension(15, 0), new java.awt.Dimension(15, 32767));
        filler5 = new javax.swing.Box.Filler(new java.awt.Dimension(15, 0), new java.awt.Dimension(15, 0), new java.awt.Dimension(15, 32767));
        filler6 = new javax.swing.Box.Filler(new java.awt.Dimension(15, 0), new java.awt.Dimension(15, 0), new java.awt.Dimension(15, 32767));
        filler7 = new javax.swing.Box.Filler(new java.awt.Dimension(15, 0), new java.awt.Dimension(15, 0), new java.awt.Dimension(15, 32767));
        jPanel31 = new javax.swing.JPanel();
        ButtonWriteGlobalTrigger = new javax.swing.JButton();
        ButtonReadGlobalTrigger = new javax.swing.JButton();
        LabelReadGlobalTrigger = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        jPanel11 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jPanel12 = new javax.swing.JPanel();
        jPanel13 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        TextFieldNbIter = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        ComboBoxDisableDCS = new javax.swing.JComboBox<>();
        ComboBoxResetCmdCnt = new javax.swing.JComboBox<>();
        ComboBoxEnable = new javax.swing.JComboBox<>();
        ComboBoxReset = new javax.swing.JComboBox<>();
        filler8 = new javax.swing.Box.Filler(new java.awt.Dimension(15, 0), new java.awt.Dimension(15, 0), new java.awt.Dimension(15, 32767));
        filler9 = new javax.swing.Box.Filler(new java.awt.Dimension(15, 0), new java.awt.Dimension(15, 0), new java.awt.Dimension(15, 32767));
        filler10 = new javax.swing.Box.Filler(new java.awt.Dimension(15, 0), new java.awt.Dimension(15, 0), new java.awt.Dimension(15, 32767));
        filler11 = new javax.swing.Box.Filler(new java.awt.Dimension(15, 0), new java.awt.Dimension(15, 0), new java.awt.Dimension(15, 32767));
        jPanel32 = new javax.swing.JPanel();
        ButtonWriteSyncCmd = new javax.swing.JButton();
        ButtonReadSyncCmd = new javax.swing.JButton();
        LabelReadSyncComm = new javax.swing.JLabel();
        jPanel14 = new javax.swing.JPanel();
        jPanel15 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jPanel16 = new javax.swing.JPanel();
        jPanel19 = new javax.swing.JPanel();
        jLabel27 = new javax.swing.JLabel();
        TextFieldCmdCnt = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        TextFieldAsyncCmdCnt = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        TextFieldSyncCmdCnt = new javax.swing.JTextField();
        ButtonReadCmdCounters = new javax.swing.JButton();
        ButtonResetCounters = new javax.swing.JButton();
        jPanel17 = new javax.swing.JPanel();
        jPanel18 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jPanel33 = new javax.swing.JPanel();
        jLabel31 = new javax.swing.JLabel();
        TextFieldLastEvtBCIDCnt = new javax.swing.JTextField();
        jLabel32 = new javax.swing.JLabel();
        TextFieldLastEvtL1IDCnt = new javax.swing.JTextField();
        ButtonReadLastEventCounters = new javax.swing.JButton();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        TextFieldL1ACnt = new javax.swing.JTextField();
        TextFieldEvtsOutCnt = new javax.swing.JTextField();
        jPanel24 = new javax.swing.JPanel();
        jPanel25 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jPanel26 = new javax.swing.JPanel();
        jPanel27 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        TextFieldGlobalPipeline = new javax.swing.JTextField();
        ButtonWriteGlobalPipeline = new javax.swing.JButton();
        ButtonReadGlobalPipeline = new javax.swing.JButton();
        filler2 = new javax.swing.Box.Filler(new java.awt.Dimension(15, 0), new java.awt.Dimension(15, 0), new java.awt.Dimension(15, 32767));
        jPanel28 = new javax.swing.JPanel();
        jPanel29 = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        jTextField5 = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        jTextField6 = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jTextField7 = new javax.swing.JTextField();
        ButtonWriteMDPipelines = new javax.swing.JButton();
        ButtonReadMDPipelines = new javax.swing.JButton();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(10, 0), new java.awt.Dimension(15, 0), new java.awt.Dimension(10, 32767));

        java.awt.GridBagLayout jPanel1Layout = new java.awt.GridBagLayout();
        jPanel1Layout.columnWidths = new int[] {0};
        jPanel1Layout.rowHeights = new int[] {0, 13, 0, 13, 0, 13, 0, 13, 0, 13, 0, 13, 0, 13, 0, 13, 0, 13, 0, 13, 0, 13, 0};
        jPanel1.setLayout(jPanel1Layout);

        java.awt.GridBagLayout jPanel2Layout = new java.awt.GridBagLayout();
        jPanel2Layout.columnWidths = new int[] {0};
        jPanel2Layout.rowHeights = new int[] {0, 10, 0, 10, 0};
        jPanel2.setLayout(jPanel2Layout);

        jPanel3.setLayout(new java.awt.GridBagLayout());

        jLabel1.setText("<html><b>Global TTC Configuration Register</b></html>");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel3.add(jLabel1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel2.add(jPanel3, gridBagConstraints);

        jPanel4.setLayout(new java.awt.GridBagLayout());

        java.awt.GridBagLayout jPanel7Layout = new java.awt.GridBagLayout();
        jPanel7Layout.columnWidths = new int[] {0, 7, 0, 7, 0, 7, 0, 7, 0, 7, 0, 7, 0, 7, 0, 7, 0, 7, 0, 7, 0};
        jPanel7Layout.rowHeights = new int[] {0};
        jPanel7.setLayout(jPanel7Layout);

        ComboBoxTTCIntExt.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Internal", "External" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 0;
        jPanel7.add(ComboBoxTTCIntExt, gridBagConstraints);

        jLabel2.setText("TTC:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 0;
        jPanel7.add(jLabel2, gridBagConstraints);

        jLabel22.setText("Enable Max. BCID:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 0;
        jPanel7.add(jLabel22, gridBagConstraints);

        jLabel23.setText("TTCrx address:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel7.add(jLabel23, gridBagConstraints);

        TextFieldTTCrxAddress.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldTTCrxAddress.setText("0");
        TextFieldTTCrxAddress.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel7.add(TextFieldTTCrxAddress, gridBagConstraints);

        jLabel24.setText("CIS DAC Conv:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel7.add(jLabel24, gridBagConstraints);

        TextFieldCISDACconv.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldCISDACconv.setText("0");
        TextFieldCISDACconv.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel7.add(TextFieldCISDACconv, gridBagConstraints);

        ComboBoxEnableMaxBCID.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "0", "1" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 20;
        gridBagConstraints.gridy = 0;
        jPanel7.add(ComboBoxEnableMaxBCID, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel7.add(filler12, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 0;
        jPanel7.add(filler13, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 0;
        jPanel7.add(filler14, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel4.add(jPanel7, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel2.add(jPanel4, gridBagConstraints);

        java.awt.GridBagLayout jPanel30Layout = new java.awt.GridBagLayout();
        jPanel30Layout.columnWidths = new int[] {0, 15, 0, 15, 0};
        jPanel30Layout.rowHeights = new int[] {0};
        jPanel30.setLayout(jPanel30Layout);

        ButtonWriteGlobalTTC.setText("Write");
        ButtonWriteGlobalTTC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonWriteGlobalTTCActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel30.add(ButtonWriteGlobalTTC, gridBagConstraints);

        ButtonReadGlobalTTC.setText("Read");
        ButtonReadGlobalTTC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonReadGlobalTTCActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel30.add(ButtonReadGlobalTTC, gridBagConstraints);

        LabelReadGlobalTTC.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelReadGlobalTTC.setText("status");
        LabelReadGlobalTTC.setPreferredSize(new java.awt.Dimension(90, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel30.add(LabelReadGlobalTTC, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel2.add(jPanel30, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel1.add(jPanel2, gridBagConstraints);

        java.awt.GridBagLayout jPanel5Layout = new java.awt.GridBagLayout();
        jPanel5Layout.columnWidths = new int[] {0};
        jPanel5Layout.rowHeights = new int[] {0, 5, 0};
        jPanel5.setLayout(jPanel5Layout);

        jPanel6.setLayout(new java.awt.GridBagLayout());

        jLabel3.setText("<html><b>Global Trigger Configuration Register</b></html>");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel6.add(jLabel3, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel5.add(jPanel6, gridBagConstraints);

        java.awt.GridBagLayout jPanel8Layout = new java.awt.GridBagLayout();
        jPanel8Layout.columnWidths = new int[] {0};
        jPanel8Layout.rowHeights = new int[] {0, 10, 0};
        jPanel8.setLayout(jPanel8Layout);

        java.awt.GridBagLayout jPanel9Layout = new java.awt.GridBagLayout();
        jPanel9Layout.columnWidths = new int[] {0, 7, 0, 7, 0, 7, 0, 7, 0, 7, 0, 7, 0, 7, 0, 7, 0, 7, 0, 7, 0, 7, 0, 7, 0, 7, 0, 7, 0, 7, 0, 7, 0};
        jPanel9Layout.rowHeights = new int[] {0};
        jPanel9.setLayout(jPanel9Layout);

        jLabel4.setText("Rate (orbits):");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 30;
        gridBagConstraints.gridy = 0;
        jPanel9.add(jLabel4, gridBagConstraints);

        jLabel9.setText("BCID for L1A:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 24;
        gridBagConstraints.gridy = 0;
        jPanel9.add(jLabel9, gridBagConstraints);

        TextFieldL1Adel.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldL1Adel.setText("0");
        TextFieldL1Adel.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 20;
        gridBagConstraints.gridy = 0;
        jPanel9.add(TextFieldL1Adel, gridBagConstraints);

        TextFieldOrbits.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldOrbits.setText("0");
        TextFieldOrbits.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 32;
        gridBagConstraints.gridy = 0;
        jPanel9.add(TextFieldOrbits, gridBagConstraints);

        TextFieldBCIDforL1A.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldBCIDforL1A.setText("0");
        TextFieldBCIDforL1A.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 26;
        gridBagConstraints.gridy = 0;
        jPanel9.add(TextFieldBCIDforL1A, gridBagConstraints);

        jLabel17.setText("L1A Delay (BCs):");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 0;
        jPanel9.add(jLabel17, gridBagConstraints);

        jLabel18.setText("Enable Deadtime:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 0;
        jPanel9.add(jLabel18, gridBagConstraints);

        jLabel19.setText("Enable Trg:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel9.add(jLabel19, gridBagConstraints);

        jLabel20.setText("Manual L1A:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel9.add(jLabel20, gridBagConstraints);

        ComboBoxManualL1A.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "0", "1" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel9.add(ComboBoxManualL1A, gridBagConstraints);

        ComboBoxEnableTrigger.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "0", "1" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel9.add(ComboBoxEnableTrigger, gridBagConstraints);

        ComboBoxEnableDeadtime.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "0", "1" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 0;
        jPanel9.add(ComboBoxEnableDeadtime, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel9.add(filler3, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 0;
        jPanel9.add(filler4, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 0;
        jPanel9.add(filler5, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 22;
        gridBagConstraints.gridy = 0;
        jPanel9.add(filler6, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 28;
        gridBagConstraints.gridy = 0;
        jPanel9.add(filler7, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel8.add(jPanel9, gridBagConstraints);

        java.awt.GridBagLayout jPanel31Layout = new java.awt.GridBagLayout();
        jPanel31Layout.columnWidths = new int[] {0, 13, 0, 13, 0};
        jPanel31Layout.rowHeights = new int[] {0};
        jPanel31.setLayout(jPanel31Layout);

        ButtonWriteGlobalTrigger.setText("Write");
        ButtonWriteGlobalTrigger.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonWriteGlobalTriggerActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel31.add(ButtonWriteGlobalTrigger, gridBagConstraints);

        ButtonReadGlobalTrigger.setText("Read");
        ButtonReadGlobalTrigger.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonReadGlobalTriggerActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel31.add(ButtonReadGlobalTrigger, gridBagConstraints);

        LabelReadGlobalTrigger.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelReadGlobalTrigger.setText("status");
        LabelReadGlobalTrigger.setPreferredSize(new java.awt.Dimension(90, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel31.add(LabelReadGlobalTrigger, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel8.add(jPanel31, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel5.add(jPanel8, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel1.add(jPanel5, gridBagConstraints);

        java.awt.GridBagLayout jPanel10Layout = new java.awt.GridBagLayout();
        jPanel10Layout.columnWidths = new int[] {0};
        jPanel10Layout.rowHeights = new int[] {0, 10, 0, 10, 0};
        jPanel10.setLayout(jPanel10Layout);

        jPanel11.setLayout(new java.awt.GridBagLayout());

        jLabel5.setText("<html><b>Synchronous Command Configuration Register</b></html>");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel11.add(jLabel5, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel10.add(jPanel11, gridBagConstraints);

        java.awt.GridBagLayout jPanel12Layout = new java.awt.GridBagLayout();
        jPanel12Layout.columnWidths = new int[] {0};
        jPanel12Layout.rowHeights = new int[] {0};
        jPanel12.setLayout(jPanel12Layout);

        java.awt.GridBagLayout jPanel13Layout = new java.awt.GridBagLayout();
        jPanel13Layout.columnWidths = new int[] {0, 7, 0, 7, 0, 7, 0, 7, 0, 7, 0, 7, 0, 7, 0, 7, 0, 7, 0, 7, 0, 7, 0, 7, 0, 7, 0};
        jPanel13Layout.rowHeights = new int[] {0};
        jPanel13.setLayout(jPanel13Layout);

        jLabel10.setText("Disable DCS:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 24;
        gridBagConstraints.gridy = 0;
        jPanel13.add(jLabel10, gridBagConstraints);

        jLabel11.setText("Reset Cmd Counter:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 0;
        jPanel13.add(jLabel11, gridBagConstraints);

        jLabel12.setText("Nb. Iterations:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 0;
        jPanel13.add(jLabel12, gridBagConstraints);

        TextFieldNbIter.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldNbIter.setText("0");
        TextFieldNbIter.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 0;
        jPanel13.add(TextFieldNbIter, gridBagConstraints);

        jLabel13.setText("Enable:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel13.add(jLabel13, gridBagConstraints);

        jLabel15.setText("Reset:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel13.add(jLabel15, gridBagConstraints);

        ComboBoxDisableDCS.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "0", "1" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 26;
        gridBagConstraints.gridy = 0;
        jPanel13.add(ComboBoxDisableDCS, gridBagConstraints);

        ComboBoxResetCmdCnt.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "0", "1" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 20;
        gridBagConstraints.gridy = 0;
        jPanel13.add(ComboBoxResetCmdCnt, gridBagConstraints);

        ComboBoxEnable.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "0", "1" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel13.add(ComboBoxEnable, gridBagConstraints);

        ComboBoxReset.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "0", "1" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel13.add(ComboBoxReset, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel13.add(filler8, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 0;
        jPanel13.add(filler9, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 0;
        jPanel13.add(filler10, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 22;
        gridBagConstraints.gridy = 0;
        jPanel13.add(filler11, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel12.add(jPanel13, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel10.add(jPanel12, gridBagConstraints);

        java.awt.GridBagLayout jPanel32Layout = new java.awt.GridBagLayout();
        jPanel32Layout.columnWidths = new int[] {0, 15, 0, 15, 0};
        jPanel32Layout.rowHeights = new int[] {0};
        jPanel32.setLayout(jPanel32Layout);

        ButtonWriteSyncCmd.setText("Write");
        ButtonWriteSyncCmd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonWriteSyncCmdActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel32.add(ButtonWriteSyncCmd, gridBagConstraints);

        ButtonReadSyncCmd.setText("Read");
        ButtonReadSyncCmd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonReadSyncCmdActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel32.add(ButtonReadSyncCmd, gridBagConstraints);

        LabelReadSyncComm.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelReadSyncComm.setText("status");
        LabelReadSyncComm.setPreferredSize(new java.awt.Dimension(90, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel32.add(LabelReadSyncComm, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel10.add(jPanel32, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel1.add(jPanel10, gridBagConstraints);

        java.awt.GridBagLayout jPanel14Layout = new java.awt.GridBagLayout();
        jPanel14Layout.columnWidths = new int[] {0};
        jPanel14Layout.rowHeights = new int[] {0, 5, 0};
        jPanel14.setLayout(jPanel14Layout);

        jPanel15.setLayout(new java.awt.GridBagLayout());

        jLabel6.setText("<html><b>Command Counters</b></html>");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel15.add(jLabel6, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel14.add(jPanel15, gridBagConstraints);

        jPanel16.setLayout(new java.awt.GridBagLayout());

        java.awt.GridBagLayout jPanel19Layout = new java.awt.GridBagLayout();
        jPanel19Layout.columnWidths = new int[] {0, 13, 0, 13, 0, 13, 0, 13, 0, 13, 0, 13, 0, 13, 0};
        jPanel19Layout.rowHeights = new int[] {0, 5, 0};
        jPanel19.setLayout(jPanel19Layout);

        jLabel27.setText("Commands");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel19.add(jLabel27, gridBagConstraints);

        TextFieldCmdCnt.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldCmdCnt.setText("0");
        TextFieldCmdCnt.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel19.add(TextFieldCmdCnt, gridBagConstraints);

        jLabel28.setText("Async Commands");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel19.add(jLabel28, gridBagConstraints);

        TextFieldAsyncCmdCnt.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldAsyncCmdCnt.setText("0");
        TextFieldAsyncCmdCnt.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 2;
        jPanel19.add(TextFieldAsyncCmdCnt, gridBagConstraints);

        jLabel29.setText("Sync Commands");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel19.add(jLabel29, gridBagConstraints);

        TextFieldSyncCmdCnt.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldSyncCmdCnt.setText("0");
        TextFieldSyncCmdCnt.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 2;
        jPanel19.add(TextFieldSyncCmdCnt, gridBagConstraints);

        ButtonReadCmdCounters.setText("Read");
        ButtonReadCmdCounters.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonReadCmdCountersActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 2;
        jPanel19.add(ButtonReadCmdCounters, gridBagConstraints);

        ButtonResetCounters.setText("Reset Counters");
        ButtonResetCounters.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonResetCountersActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 2;
        jPanel19.add(ButtonResetCounters, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel16.add(jPanel19, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel14.add(jPanel16, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 14;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel1.add(jPanel14, gridBagConstraints);

        java.awt.GridBagLayout jPanel17Layout = new java.awt.GridBagLayout();
        jPanel17Layout.columnWidths = new int[] {0};
        jPanel17Layout.rowHeights = new int[] {0, 5, 0};
        jPanel17.setLayout(jPanel17Layout);

        jPanel18.setLayout(new java.awt.GridBagLayout());

        jLabel7.setText("<html><b>Counters</b></html>");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel18.add(jLabel7, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel17.add(jPanel18, gridBagConstraints);

        java.awt.GridBagLayout jPanel33Layout = new java.awt.GridBagLayout();
        jPanel33Layout.columnWidths = new int[] {0, 13, 0, 13, 0, 13, 0, 13, 0};
        jPanel33Layout.rowHeights = new int[] {0, 5, 0};
        jPanel33.setLayout(jPanel33Layout);

        jLabel31.setText("Last Event BCID");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel33.add(jLabel31, gridBagConstraints);

        TextFieldLastEvtBCIDCnt.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldLastEvtBCIDCnt.setText("0");
        TextFieldLastEvtBCIDCnt.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel33.add(TextFieldLastEvtBCIDCnt, gridBagConstraints);

        jLabel32.setText("Last Event L1ID");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel33.add(jLabel32, gridBagConstraints);

        TextFieldLastEvtL1IDCnt.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldLastEvtL1IDCnt.setText("0");
        TextFieldLastEvtL1IDCnt.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        jPanel33.add(TextFieldLastEvtL1IDCnt, gridBagConstraints);

        ButtonReadLastEventCounters.setText("Read");
        ButtonReadLastEventCounters.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonReadLastEventCountersActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 2;
        jPanel33.add(ButtonReadLastEventCounters, gridBagConstraints);

        jLabel33.setText("L1A");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel33.add(jLabel33, gridBagConstraints);

        jLabel34.setText("Events Out");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel33.add(jLabel34, gridBagConstraints);

        TextFieldL1ACnt.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldL1ACnt.setText("0");
        TextFieldL1ACnt.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 2;
        jPanel33.add(TextFieldL1ACnt, gridBagConstraints);

        TextFieldEvtsOutCnt.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldEvtsOutCnt.setText("0");
        TextFieldEvtsOutCnt.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 2;
        jPanel33.add(TextFieldEvtsOutCnt, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel17.add(jPanel33, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 18;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel1.add(jPanel17, gridBagConstraints);

        java.awt.GridBagLayout jPanel24Layout = new java.awt.GridBagLayout();
        jPanel24Layout.columnWidths = new int[] {0};
        jPanel24Layout.rowHeights = new int[] {0, 5, 0, 5, 0, 5, 0};
        jPanel24.setLayout(jPanel24Layout);

        jPanel25.setLayout(new java.awt.GridBagLayout());

        jLabel8.setText("<html><b>Pipeline Delays</b></html>");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel25.add(jLabel8, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel24.add(jPanel25, gridBagConstraints);

        jPanel26.setLayout(new java.awt.GridBagLayout());

        java.awt.GridBagLayout jPanel27Layout = new java.awt.GridBagLayout();
        jPanel27Layout.columnWidths = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0};
        jPanel27Layout.rowHeights = new int[] {0};
        jPanel27.setLayout(jPanel27Layout);

        jLabel14.setText("Global Pipeline (BC units):");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel27.add(jLabel14, gridBagConstraints);

        TextFieldGlobalPipeline.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldGlobalPipeline.setText("0");
        TextFieldGlobalPipeline.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel27.add(TextFieldGlobalPipeline, gridBagConstraints);

        ButtonWriteGlobalPipeline.setText("Write");
        ButtonWriteGlobalPipeline.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonWriteGlobalPipelineActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel27.add(ButtonWriteGlobalPipeline, gridBagConstraints);

        ButtonReadGlobalPipeline.setText("Read");
        ButtonReadGlobalPipeline.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonReadGlobalPipelineActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel27.add(ButtonReadGlobalPipeline, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel27.add(filler2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel26.add(jPanel27, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel24.add(jPanel26, gridBagConstraints);

        jPanel28.setLayout(new java.awt.GridBagLayout());

        java.awt.GridBagLayout jPanel29Layout = new java.awt.GridBagLayout();
        jPanel29Layout.columnWidths = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0};
        jPanel29Layout.rowHeights = new int[] {0};
        jPanel29.setLayout(jPanel29Layout);

        jLabel16.setText("Minidrawer Pipelines (BC units)");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel29.add(jLabel16, gridBagConstraints);

        jTextField4.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField4.setText("0");
        jTextField4.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel29.add(jTextField4, gridBagConstraints);

        jLabel21.setText("MD2:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 0;
        jPanel29.add(jLabel21, gridBagConstraints);

        jTextField5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField5.setText("0");
        jTextField5.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 0;
        jPanel29.add(jTextField5, gridBagConstraints);

        jLabel25.setText("MD3:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 0;
        jPanel29.add(jLabel25, gridBagConstraints);

        jTextField6.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField6.setText("0");
        jTextField6.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 0;
        jPanel29.add(jTextField6, gridBagConstraints);

        jLabel26.setText("MD1:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel29.add(jLabel26, gridBagConstraints);

        jLabel30.setText("MD4:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 22;
        gridBagConstraints.gridy = 0;
        jPanel29.add(jLabel30, gridBagConstraints);

        jTextField7.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField7.setText("0");
        jTextField7.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 24;
        gridBagConstraints.gridy = 0;
        jPanel29.add(jTextField7, gridBagConstraints);

        ButtonWriteMDPipelines.setText("Write");
        ButtonWriteMDPipelines.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonWriteMDPipelinesActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 28;
        gridBagConstraints.gridy = 0;
        jPanel29.add(ButtonWriteMDPipelines, gridBagConstraints);

        ButtonReadMDPipelines.setText("Read");
        ButtonReadMDPipelines.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonReadMDPipelinesActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 30;
        gridBagConstraints.gridy = 0;
        jPanel29.add(ButtonReadMDPipelines, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 26;
        gridBagConstraints.gridy = 0;
        jPanel29.add(filler1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel28.add(jPanel29, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel24.add(jPanel28, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 22;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel1.add(jPanel24, gridBagConstraints);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(66, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }//GEN-END:initComponents

    /**
     * Reads PPr Global TTC register.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonReadGlobalTTCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonReadGlobalTTCActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoGlobal -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnableReadGlobalTTC);
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonTestIPbusPPrConnectionActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonTestIPbusPPrConnectionActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonReadGlobalTTCActionPerformed

    /**
     * Reads PPr Global Trigger register.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonReadGlobalTriggerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonReadGlobalTriggerActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoGlobal -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnableReadGlobalTrigger);
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonTestIPbusPPrConnectionActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonTestIPbusPPrConnectionActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonReadGlobalTriggerActionPerformed

    /**
     * Writes PPr Global TTC register.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonWriteGlobalTTCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonWriteGlobalTTCActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoGlobal -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnableWriteGlobalTTC);
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonWriteGlobalTTCActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonWriteGlobalTTCActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonWriteGlobalTTCActionPerformed

    /**
     * Reads PPr Sync Command Configuration register.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonReadSyncCmdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonReadSyncCmdActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoGlobal -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnableReadSyncCmdConf);
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonReadSyncCommConfActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonReadSyncCommConfActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonReadSyncCmdActionPerformed

    /**
     * Reads PPr Commands Counters register.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonReadCmdCountersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonReadCmdCountersActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoGlobal -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnableReadCmdCounters);
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonReadCmdCountersActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonReadCmdCountersActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonReadCmdCountersActionPerformed

    /**
     * Reads PPr Last Event Counters register.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonReadLastEventCountersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonReadLastEventCountersActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoGlobal -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnableReadLastEventCounters);
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonReadLastEventCountersActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonReadLastEventCountersActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonReadLastEventCountersActionPerformed

    /**
     * Writes PPr Global Trigger register.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonWriteGlobalTriggerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonWriteGlobalTriggerActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoGlobal -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnableWriteGlobalTrigger);
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonWriteGlobalTriggerActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonWriteGlobalTriggerActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonWriteGlobalTriggerActionPerformed

    /**
     * Writes PPr Sync Command Configuration register.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonWriteSyncCmdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonWriteSyncCmdActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoGlobal -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnableWriteSyncCmd);
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonWriteSyncCmdActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonWriteSyncCmdActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonWriteSyncCmdActionPerformed

    /**
     * Resets Command counters. Cycles from 1 to 0 the Reset Cmd Counter bit of
     * the PPr Synchronous Commands Configuration register.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonResetCountersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonResetCountersActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoGlobal -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnableResetCmdCounters);
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonResetCountersActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonResetCountersActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonResetCountersActionPerformed

    /**
     * Writes Global Pipeline delay register.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonWriteGlobalPipelineActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonWriteGlobalPipelineActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoGlobal -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnableWriteGlobalPipeline);
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonWriteGlobalPipelineActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonWriteGlobalPipelineActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonWriteGlobalPipelineActionPerformed

    /**
     * Reads Global Pipeline delay register.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonReadGlobalPipelineActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonReadGlobalPipelineActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoGlobal -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnableReadGlobalPipeline);
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonReadGlobalPipelineActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonReadGlobalPipelineActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonReadGlobalPipelineActionPerformed

    /**
     * Reads Minidrawer Pipeline delay registers.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonReadMDPipelinesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonReadMDPipelinesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ButtonReadMDPipelinesActionPerformed

    /**
     * Writes Minidrawer Pipeline delay registers.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonWriteMDPipelinesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonWriteMDPipelinesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ButtonWriteMDPipelinesActionPerformed

    /**
     * Declares a PPr class object to communicate with PPr IPbus server.
     */
    private PPrLib pprlib;

    /**
     * Declares a ThreadPoolExecutor object. It executes each submitted task
     * using one of possibly several pooled threads, normally configured using
     * Executors factory methods.
     */
    public static ThreadPoolExecutor executor;

    /**
     * Declares Runnable for readGlobalTTC() method.
     */
    Runnable RunnableReadGlobalTTC;

    /**
     * Declares Runnable for readGlobalTrigger() method.
     */
    Runnable RunnableReadGlobalTrigger;

    /**
     * Declares Runnable for readGlobalTrigger() method.
     */
    Runnable RunnableReadSyncCmdConf;

    /**
     * Declares Runnable for readCmdCounters() method.
     */
    Runnable RunnableReadCmdCounters;

    /**
     * Declares Runnable for readLastEvents() method.
     */
    Runnable RunnableReadLastEventCounters;

    /**
     * Declares Runnable for readGlobalPipeline() method.
     */
    Runnable RunnableReadGlobalPipeline;

    /**
     * Declares Runnable for readMDPipelines() method.
     */
    Runnable RunnableReadMDPipelines;

    /**
     * Declares Runnable for resetCmdCounters() method.
     */
    Runnable RunnableResetCmdCounters;

    /**
     * Declares Runnable for writeGlobalTTC() method.
     */
    Runnable RunnableWriteGlobalTTC;

    /**
     * Declares Runnable for writeGlobalTrigger() method.
     */
    Runnable RunnableWriteGlobalTrigger;

    /**
     * Declares Runnable for writeSyncCmdConf() method.
     */
    Runnable RunnableWriteSyncCmd;

    /**
     * Declares Runnable for writeGlobalPipeline() method.
     */
    Runnable RunnableWriteGlobalPipeline;

    /**
     * Declares Runnable for writeMDPipelines() method.
     */
    Runnable RunnableWriteMDPipelines;

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ButtonReadCmdCounters;
    private javax.swing.JButton ButtonReadGlobalPipeline;
    private javax.swing.JButton ButtonReadGlobalTTC;
    private javax.swing.JButton ButtonReadGlobalTrigger;
    private javax.swing.JButton ButtonReadLastEventCounters;
    private javax.swing.JButton ButtonReadMDPipelines;
    private javax.swing.JButton ButtonReadSyncCmd;
    private javax.swing.JButton ButtonResetCounters;
    private javax.swing.JButton ButtonWriteGlobalPipeline;
    private javax.swing.JButton ButtonWriteGlobalTTC;
    private javax.swing.JButton ButtonWriteGlobalTrigger;
    private javax.swing.JButton ButtonWriteMDPipelines;
    private javax.swing.JButton ButtonWriteSyncCmd;
    private javax.swing.JComboBox<String> ComboBoxDisableDCS;
    private javax.swing.JComboBox<String> ComboBoxEnable;
    private javax.swing.JComboBox<String> ComboBoxEnableDeadtime;
    private javax.swing.JComboBox<String> ComboBoxEnableMaxBCID;
    private javax.swing.JComboBox<String> ComboBoxEnableTrigger;
    private javax.swing.JComboBox<String> ComboBoxManualL1A;
    private javax.swing.JComboBox<String> ComboBoxReset;
    private javax.swing.JComboBox<String> ComboBoxResetCmdCnt;
    private javax.swing.JComboBox<String> ComboBoxTTCIntExt;
    private javax.swing.JLabel LabelReadGlobalTTC;
    private javax.swing.JLabel LabelReadGlobalTrigger;
    private javax.swing.JLabel LabelReadSyncComm;
    private javax.swing.JTextField TextFieldAsyncCmdCnt;
    private javax.swing.JTextField TextFieldBCIDforL1A;
    private javax.swing.JTextField TextFieldCISDACconv;
    private javax.swing.JTextField TextFieldCmdCnt;
    private javax.swing.JTextField TextFieldEvtsOutCnt;
    private javax.swing.JTextField TextFieldGlobalPipeline;
    private javax.swing.JTextField TextFieldL1ACnt;
    private javax.swing.JTextField TextFieldL1Adel;
    private javax.swing.JTextField TextFieldLastEvtBCIDCnt;
    private javax.swing.JTextField TextFieldLastEvtL1IDCnt;
    private javax.swing.JTextField TextFieldNbIter;
    private javax.swing.JTextField TextFieldOrbits;
    private javax.swing.JTextField TextFieldSyncCmdCnt;
    private javax.swing.JTextField TextFieldTTCrxAddress;
    private javax.swing.Box.Filler filler1;
    private javax.swing.Box.Filler filler10;
    private javax.swing.Box.Filler filler11;
    private javax.swing.Box.Filler filler12;
    private javax.swing.Box.Filler filler13;
    private javax.swing.Box.Filler filler14;
    private javax.swing.Box.Filler filler2;
    private javax.swing.Box.Filler filler3;
    private javax.swing.Box.Filler filler4;
    private javax.swing.Box.Filler filler5;
    private javax.swing.Box.Filler filler6;
    private javax.swing.Box.Filler filler7;
    private javax.swing.Box.Filler filler8;
    private javax.swing.Box.Filler filler9;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel24;
    private javax.swing.JPanel jPanel25;
    private javax.swing.JPanel jPanel26;
    private javax.swing.JPanel jPanel27;
    private javax.swing.JPanel jPanel28;
    private javax.swing.JPanel jPanel29;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel30;
    private javax.swing.JPanel jPanel31;
    private javax.swing.JPanel jPanel32;
    private javax.swing.JPanel jPanel33;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextField6;
    private javax.swing.JTextField jTextField7;
    // End of variables declaration//GEN-END:variables
}
