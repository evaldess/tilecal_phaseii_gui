/* 
 *  TilePPrDemoJava Project
 *  2019 (c) IFIC-CERN
 */
package tilepprdemo;

import java.awt.Color;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.swing.JSlider;
import javax.swing.JTextField;

import ipbusjavalibrary.utilities.ByteTools;
import tilepprjavalibrary.lib.PPrLib;

/**
 *
 * @author Juan Valls {@literal (valls@cern.ch)}
 * @author Fernando Carrió {@literal (fernando.carrio@cern.ch)}
 * @author Alberto Valero {@literal (alberto.valero@cern.ch)}
 */
public class PPrDemoLinksStatus extends javax.swing.JPanel {

    /**
     * Constructor. Creates new form PPrDemoLinksStatus
     *
     * @param pprlib
     */
    public PPrDemoLinksStatus(PPrLib pprlib) {
        System.out.println("PPrDemoLinksStatus -> constructor");

        // NetBeans components settings.
        initComponents();

        // Set PPrLib class object as given by constructor argument.
        this.pprlib = pprlib;

        link_status_info = new String[]{
            "TX PLL", "RX FRAMECLK", "RX WORDCLK", "MGT RDY",
            "BITSLIPNb", "GBTRX RDY", "GBTTC RDY LOST", "DATA ERROR"};

        TextFieldMD1A0 = new JTextField[]{
            TextFieldMD1A0s1, TextFieldMD1A0s2, TextFieldMD1A0s3,
            TextFieldMD1A0s4, TextFieldMD1A0s5, TextFieldMD1A0s6,
            TextFieldMD1A0s7, TextFieldMD1A0s8};

        TextFieldMD1A1 = new JTextField[]{
            TextFieldMD1A1s1, TextFieldMD1A1s2, TextFieldMD1A1s3,
            TextFieldMD1A1s4, TextFieldMD1A1s5, TextFieldMD1A1s6,
            TextFieldMD1A1s7, TextFieldMD1A1s8};

        TextFieldMD1B0 = new JTextField[]{
            TextFieldMD1B0s1, TextFieldMD1B0s2, TextFieldMD1B0s3,
            TextFieldMD1B0s4, TextFieldMD1B0s5, TextFieldMD1B0s6,
            TextFieldMD1B0s7, TextFieldMD1B0s8};

        TextFieldMD1B1 = new JTextField[]{
            TextFieldMD1B1s1, TextFieldMD1B1s2, TextFieldMD1B1s3,
            TextFieldMD1B1s4, TextFieldMD1B1s5, TextFieldMD1B1s6,
            TextFieldMD1B1s7, TextFieldMD1B1s8};

        TextFieldMD2A0 = new JTextField[]{
            TextFieldMD2A0s1, TextFieldMD2A0s2, TextFieldMD2A0s3,
            TextFieldMD2A0s4, TextFieldMD2A0s5, TextFieldMD2A0s6,
            TextFieldMD2A0s7, TextFieldMD2A0s8};

        TextFieldMD2A1 = new JTextField[]{
            TextFieldMD2A1s1, TextFieldMD2A1s2, TextFieldMD2A1s3,
            TextFieldMD2A1s4, TextFieldMD2A1s5, TextFieldMD2A1s6,
            TextFieldMD2A1s7, TextFieldMD2A1s8};

        TextFieldMD2B0 = new JTextField[]{
            TextFieldMD2B0s1, TextFieldMD2B0s2, TextFieldMD2B0s3,
            TextFieldMD2B0s4, TextFieldMD2B0s5, TextFieldMD2B0s6,
            TextFieldMD2B0s7, TextFieldMD2B0s8};

        TextFieldMD2B1 = new JTextField[]{
            TextFieldMD2B1s1, TextFieldMD2B1s2, TextFieldMD2B1s3,
            TextFieldMD2B1s4, TextFieldMD2B1s5, TextFieldMD2B1s6,
            TextFieldMD2B1s7, TextFieldMD2B1s8};

        TextFieldMD3A0 = new JTextField[]{
            TextFieldMD3A0s1, TextFieldMD3A0s2, TextFieldMD3A0s3,
            TextFieldMD3A0s4, TextFieldMD3A0s5, TextFieldMD3A0s6,
            TextFieldMD3A0s7, TextFieldMD3A0s8};

        TextFieldMD3A1 = new JTextField[]{
            TextFieldMD3A1s1, TextFieldMD3A1s2, TextFieldMD3A1s3,
            TextFieldMD3A1s4, TextFieldMD3A1s5, TextFieldMD3A1s6,
            TextFieldMD3A1s7, TextFieldMD3A1s8};

        TextFieldMD3B0 = new JTextField[]{
            TextFieldMD3B0s1, TextFieldMD3B0s2, TextFieldMD3B0s3,
            TextFieldMD3B0s4, TextFieldMD3B0s5, TextFieldMD3B0s6,
            TextFieldMD3B0s7, TextFieldMD3B0s8};

        TextFieldMD3B1 = new JTextField[]{
            TextFieldMD3B1s1, TextFieldMD3B1s2, TextFieldMD3B1s3,
            TextFieldMD3B1s4, TextFieldMD3B1s5, TextFieldMD3B1s6,
            TextFieldMD3B1s7, TextFieldMD3B1s8};

        TextFieldMD4A0 = new JTextField[]{
            TextFieldMD4A0s1, TextFieldMD4A0s2, TextFieldMD4A0s3,
            TextFieldMD4A0s4, TextFieldMD4A0s5, TextFieldMD4A0s6,
            TextFieldMD4A0s7, TextFieldMD4A0s8};

        TextFieldMD4A1 = new JTextField[]{
            TextFieldMD4A1s1, TextFieldMD4A1s2, TextFieldMD4A1s3,
            TextFieldMD4A1s4, TextFieldMD4A1s5, TextFieldMD4A1s6,
            TextFieldMD4A1s7, TextFieldMD4A1s8};

        TextFieldMD4B0 = new JTextField[]{
            TextFieldMD4B0s1, TextFieldMD4B0s2, TextFieldMD4B0s3,
            TextFieldMD4B0s4, TextFieldMD4B0s5, TextFieldMD4B0s6,
            TextFieldMD4B0s7, TextFieldMD4B0s8};

        TextFieldMD4B1 = new JTextField[]{
            TextFieldMD4B1s1, TextFieldMD4B1s2, TextFieldMD4B1s3,
            TextFieldMD4B1s4, TextFieldMD4B1s5, TextFieldMD4B1s6,
            TextFieldMD4B1s7, TextFieldMD4B1s8};

        // First dimension is minidrawer. Second is link (A0,A1,B0,B1) and third is data.
        TextFieldMD = new JTextField[4][4][8];

        for (int i = 0; i < 8; i++) {
            TextFieldMD[0][0][i] = TextFieldMD1A0[i];
            TextFieldMD[0][1][i] = TextFieldMD1A1[i];
            TextFieldMD[0][2][i] = TextFieldMD1B0[i];
            TextFieldMD[0][3][i] = TextFieldMD1B1[i];

            TextFieldMD[1][0][i] = TextFieldMD2A0[i];
            TextFieldMD[1][1][i] = TextFieldMD2A1[i];
            TextFieldMD[1][2][i] = TextFieldMD2B0[i];
            TextFieldMD[1][3][i] = TextFieldMD2B1[i];

            TextFieldMD[2][0][i] = TextFieldMD3A0[i];
            TextFieldMD[2][1][i] = TextFieldMD3A1[i];
            TextFieldMD[2][2][i] = TextFieldMD3B0[i];
            TextFieldMD[2][3][i] = TextFieldMD3B1[i];

            TextFieldMD[3][0][i] = TextFieldMD4A0[i];
            TextFieldMD[3][1][i] = TextFieldMD4A1[i];
            TextFieldMD[3][2][i] = TextFieldMD4B0[i];
            TextFieldMD[3][3][i] = TextFieldMD4B1[i];
        }

        selected_MD = new ArrayList<>();

        link_status_data = new ArrayList<>();

        // Defines Runnable ArrayList for QSFP Resets.
        RunnablesResetQSFP = new ArrayList<>();

        // Defines Runnable ArrayList for MD DB Resets.
        RunnablesResetDB = new ArrayList<>();

        // Defines Runnable ArrayList for QSFP GBT links Resets.
        RunnablesResetGBTLinks = new ArrayList<>();

        // Define Runnables through lambda expressions.
        initDefineRunnables();

        // Defines instance of ThreadPoolExecutor class to create a thread pool.
        // A thread pool manages a collection of Runnable threads.   
        executor = new ThreadPoolExecutor(
                5,
                5,
                0L,
                TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>(),
                new TBThreadFactory("PPrDemoLinksStatus"));

        // Defines an instance of the ScheduledThreadPoolExecutor class, a subclass of the ThreadPoolExecutor 
        // that can additionally schedule commands to run after a given delay, 
        // or to execute periodically.
        executorScheduled = new ScheduledThreadPoolExecutor(
                5,
                new TBThreadFactory("PPrDemoLinksStatusMonitor"));
    }

    /**
     * Defines Runnables for threads. A Runnable object is an interface designed
     * to provide a common protocol for objects that wish to execute code while
     * they are active
     */
    private void initDefineRunnables() {
        String[] transceiver;
        transceiver = new String[]{"TX", "RX"};

        RunnableReadLinkStatus = () -> {
            readLinkStatus();
        };

        RunnableUpdateScheduler = () -> {
            read_update_scheduler();
        };

        for (int i = 0; i < 4; i++) {
            final int iFinal = i;
            RunnablesResetQSFP.add((Runnable) () -> {
                resetQSFP(iFinal);
            });
        }

        for (int i = 0; i < 4; i++) {
            final int iMD = i;
            for (int j = 0; j < 2; j++) {
                final int jSide = j;
                RunnablesResetDB.add((Runnable) () -> {
                    resetDB(iMD, jSide);
                });
            }
        }

        for (int i = 0; i < 4; i++) {
            final int iQSFP = i;
            for (int j = 0; j < 4; j++) {
                final int jLink = j;
                for (int k = 0; k < 2; k++) {
                    final int kTrans = k;
                    RunnablesResetGBTLinks.add((Runnable) () -> {
                        resetGBTLinks(iQSFP, jLink, transceiver[kTrans]);
                    });
                }
            }
        }
    }

    /**
     * Resets selected PPrDemo QSFP transceiver.
     *
     * @param qsfp Input QSFP transceiver to be rest (0 to 3).
     */
    private synchronized void resetQSFP(Integer qsfp) {
        Boolean ret = pprlib.ppr.reset_QSFP_Link(qsfp);
        String fs = "PPrDemoLinksStatus.resetQSFP(): reset QSFP %d -> %s";
        Console.print_cr(String.format(fs, qsfp + 1, (ret != null ? "Ok" : "Fail")));
    }

    /**
     * Resets selected minidrawer DB Side.
     *
     * @param md Input minidrawer (0 to 3).
     * @param side Input DB side (0 for Side A, 1 for side B).
     */
    private synchronized void resetDB(Integer md, Integer side) {
        String[] strside = {"A", "B"};

        Boolean ret = pprlib.ppr.reset_DB_FPGA(md, side);
        String fs = "PPrDemoLinksStatus.resetDB(): reset MD %d DB Side %s -> %s";
        Console.print_cr(String.format(fs, md + 1, strside[side], (ret != null ? "Ok" : "Fail")));
    }

    /**
     *
     * @param qsfp
     * @param link
     * @param trans
     */
    private synchronized void resetGBTLinks(Integer qsfp, Integer link, String trans) {
        String[] strlink = {"A0", "A1", "B0", "B1"};

        Boolean ret = pprlib.ppr.reset_QSFP_GBT_Link(qsfp, link, trans);
        String fs = "PPrDemoLinksStatus.resetGBTLinks(): QSFP %d link: %s %s -> %s";
        Console.print_cr(String.format(fs, qsfp + 1, strlink[link], trans, (ret != null ? "Ok" : "Fail")));
    }

    /**
     * Reads Link Status registers.
     */
    private synchronized void readLinkStatus() {
        Integer val;
        String fs;

        get_selected_MDs();

        if (DebugSettings.getVerbose()) {
            fs = "%s %d %d %d %d";
            Console.print_cr(String.format(fs, "PPrDemoLinksStatus.readLinkStatus() -> minidrawers: ",
                    selected_MD.get(0), selected_MD.get(1), selected_MD.get(2), selected_MD.get(3)));
        }

        // Loop on active minidrawers.
        for (int md = 0; md < 4; md++) {
            if (selected_MD.get(md) == 1) {

                // Reads Link Status register for Side A.
                val = pprlib.ppr.get_links_status(md, "A");
                if (DebugSettings.getVerbose()) {
                    fs = "  MD: %d Link A read: %s";
                    Console.print_cr(String.format(fs, md + 1, (val != null ? "Ok" : "Fail")));
                }

                if (val != null) {
                    if (DebugSettings.getVerbose()) {
                        fs = "  data: 0x%08X -> %s";
                        Console.print_cr(String.format(fs, val, ByteTools.intToString(val, 4)));
                    }

                    link_status_data.clear();
                    link_status_data = pprlib.ppr.get_links_status_bits(val, 0);
                    for (int i = 0; i < link_status_data.size(); i++) {
                        if (DebugSettings.getVerbose()) {
                            fs = "  Link A0 %s: %d";
                            Console.print_cr(String.format(fs, link_status_info[i], link_status_data.get(i)));
                        }
                        TextFieldMD[md][0][i].setText(String.format("%d", link_status_data.get(i)));
                        readLinkStatus_SetColorInfo(md, 0, i, link_status_data.get(i));
                    }

                    link_status_data.clear();
                    link_status_data = pprlib.ppr.get_links_status_bits(val, 1);
                    for (int i = 0; i < link_status_data.size(); i++) {
                        if (DebugSettings.getVerbose()) {
                            fs = "  Link A1 %s: %d";
                            Console.print_cr(String.format(fs, link_status_info[i], link_status_data.get(i)));
                        }
                        TextFieldMD[md][1][i].setText(String.format("%d", link_status_data.get(i)));
                        readLinkStatus_SetColorInfo(md, 1, i, link_status_data.get(i));
                    }
                } else {
                    for (int i = 0; i < 8; i++) {
                        TextFieldMD[md][0][i].setText("?");
                        TextFieldMD[md][0][i].setForeground(Color.blue);
                        TextFieldMD[md][1][i].setText("?");
                        TextFieldMD[md][1][i].setForeground(Color.blue);
                    }
                }

                // Reads Link Status register for Side B.
                val = pprlib.ppr.get_links_status(md, "B");
                if (DebugSettings.getVerbose()) {
                    fs = "  MD: %d Link B read: %s ";
                    Console.print_cr(String.format(fs, md + 1, (val != null ? "Ok" : "Fail")));
                }

                if (val != null) {
                    if (DebugSettings.getVerbose()) {
                        fs = "  data: 0x%08X -> %s";
                        Console.print_cr(String.format(fs, val, ByteTools.intToString(val, 4)));
                    }

                    link_status_data.clear();
                    link_status_data = pprlib.ppr.get_links_status_bits(val, 0);
                    for (int i = 0; i < link_status_data.size(); i++) {
                        if (DebugSettings.getVerbose()) {
                            fs = "  Link B0 %s: %d";
                            Console.print_cr(String.format(fs, link_status_info[i], link_status_data.get(i)));
                        }
                        TextFieldMD[md][2][i].setText(String.format("%d", link_status_data.get(i)));
                        readLinkStatus_SetColorInfo(md, 2, i, link_status_data.get(i));
                    }

                    link_status_data.clear();
                    link_status_data = pprlib.ppr.get_links_status_bits(val, 1);
                    for (int i = 0; i < link_status_data.size(); i++) {
                        if (DebugSettings.getVerbose()) {
                            fs = "  Link B1 %s: %d";
                            Console.print_cr(String.format(fs, link_status_info[i], link_status_data.get(i)));
                        }
                        TextFieldMD[md][3][i].setText(String.format("%d", link_status_data.get(i)));
                        readLinkStatus_SetColorInfo(md, 3, i, link_status_data.get(i));
                    }
                } else {
                    for (int i = 0; i < 8; i++) {
                        TextFieldMD[md][2][i].setText("?");
                        TextFieldMD[md][2][i].setForeground(Color.blue);
                        TextFieldMD[md][3][i].setText("?");
                        TextFieldMD[md][3][i].setForeground(Color.blue);
                    }
                }
            }
        }
    }

    /**
     *
     * @param md
     * @param side
     * @param info
     * @param value
     */
    private synchronized void readLinkStatus_SetColorInfo(Integer md, Integer side, Integer info, Integer value) {
        switch (info) {
            case 0:
                if (value == 0) {
                    TextFieldMD[md][side][info].setForeground(Color.GREEN.darker());
                } else {
                    TextFieldMD[md][side][info].setForeground(Color.red);
                }
                break;
            case 1:
                if (value == 1) {
                    TextFieldMD[md][side][info].setForeground(Color.GREEN.darker());
                } else {
                    TextFieldMD[md][side][info].setForeground(Color.red);
                }
                break;
            case 2:
                if (value == 1) {
                    TextFieldMD[md][side][info].setForeground(Color.GREEN.darker());
                } else {
                    TextFieldMD[md][side][info].setForeground(Color.red);
                }
                break;
            case 3:
                if (value == 1) {
                    TextFieldMD[md][side][info].setForeground(Color.GREEN.darker());
                } else {
                    TextFieldMD[md][side][info].setForeground(Color.red);
                }
                break;
            case 4:
                if (value == 0) {
                    TextFieldMD[md][side][info].setForeground(Color.GREEN.darker());
                } else {
                    TextFieldMD[md][side][info].setForeground(Color.red);
                }
                break;
            case 5:
                if (value == 1) {
                    TextFieldMD[md][side][info].setForeground(Color.GREEN.darker());
                } else {
                    TextFieldMD[md][side][info].setForeground(Color.red);
                }
                break;
            case 6:
                if (value == 0) {
                    TextFieldMD[md][side][info].setForeground(Color.GREEN.darker());
                } else {
                    TextFieldMD[md][side][info].setForeground(Color.red);
                }
                break;
            case 7:
                if (value == 0) {
                    TextFieldMD[md][side][info].setForeground(Color.GREEN.darker());
                } else {
                    TextFieldMD[md][side][info].setForeground(Color.red);
                }
                break;
            default:
                break;
        }
    }

    /**
     * Gets minidrawers to process.
     */
    private synchronized void get_selected_MDs() {
        selected_MD.clear();
        selected_MD.add(CheckBoxMD1.isSelected() ? 1 : 0);
        selected_MD.add(CheckBoxMD2.isSelected() ? 1 : 0);
        selected_MD.add(CheckBoxMD3.isSelected() ? 1 : 0);
        selected_MD.add(CheckBoxMD4.isSelected() ? 1 : 0);
    }

    /**
     * Runnable task used by the ScheduledThreadPoolExecutor. It reads all V6533
     * VME HV registers. Then set corresponding fields and updates panel with
     * field values.
     */
    private synchronized void read_update_scheduler() {
        if (!IPbusPanel.getConnectFlag()) {
            // Attempts to cancel execution of this task. This attempt will fail
            // if the task has already completed, has already been cancelled,
            // or could not be cancelled for some other reason.
            Boolean ret = scheduledFuture.cancel(true);

            String fs = "%s %s";
            Console.print_cr(String.format(fs, "PPrDemoLinksStatus -> scheduler cancelled (no IPbus connection)... ",
                    (ret ? "Ok." : "Failed.")));

            return;
        }

        if (DebugSettings.getVerbose()) {
            String sl = String.format("read_update_scheduler() -> refresh interval %d sec",
                    SliderReadUpdate.getValue());
            Console.print_cr(sl);
            Console.print_cr("read_update_scheduler() -> elapsed time: "
                    + LocalDateTime.now());
        }

        readLinkStatus();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    private void initComponents() {//GEN-BEGIN:initComponents
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        jPanel22 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jPanel34 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        TextFieldMD1A0s1 = new javax.swing.JTextField();
        TextFieldMD1A0s2 = new javax.swing.JTextField();
        TextFieldMD1A0s3 = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        TextFieldMD1A1s1 = new javax.swing.JTextField();
        TextFieldMD1B0s1 = new javax.swing.JTextField();
        TextFieldMD1B1s1 = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        TextFieldMD1A0s4 = new javax.swing.JTextField();
        TextFieldMD1A0s5 = new javax.swing.JTextField();
        TextFieldMD1A0s6 = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        TextFieldMD1A0s7 = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        TextFieldMD1A0s8 = new javax.swing.JTextField();
        TextFieldMD1A1s2 = new javax.swing.JTextField();
        TextFieldMD1B0s2 = new javax.swing.JTextField();
        TextFieldMD1B1s2 = new javax.swing.JTextField();
        TextFieldMD1A1s3 = new javax.swing.JTextField();
        TextFieldMD1B0s3 = new javax.swing.JTextField();
        TextFieldMD1B1s3 = new javax.swing.JTextField();
        TextFieldMD1A1s4 = new javax.swing.JTextField();
        TextFieldMD1B0s5 = new javax.swing.JTextField();
        TextFieldMD1B1s6 = new javax.swing.JTextField();
        TextFieldMD1B1s8 = new javax.swing.JTextField();
        TextFieldMD1A1s6 = new javax.swing.JTextField();
        TextFieldMD1A1s5 = new javax.swing.JTextField();
        TextFieldMD1A1s7 = new javax.swing.JTextField();
        TextFieldMD1B0s7 = new javax.swing.JTextField();
        TextFieldMD1B1s5 = new javax.swing.JTextField();
        TextFieldMD1B0s4 = new javax.swing.JTextField();
        TextFieldMD1B1s4 = new javax.swing.JTextField();
        TextFieldMD1B0s6 = new javax.swing.JTextField();
        TextFieldMD1B1s7 = new javax.swing.JTextField();
        TextFieldMD1A1s8 = new javax.swing.JTextField();
        TextFieldMD1B0s8 = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        ButtonQ1A0TXReset = new javax.swing.JButton();
        ButtonQ1A1TXReset = new javax.swing.JButton();
        ButtonQ1B0TXReset = new javax.swing.JButton();
        ButtonQ1B1TXReset = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        ButtonQ1A0RXReset = new javax.swing.JButton();
        ButtonQ1A1RXReset = new javax.swing.JButton();
        ButtonQ1B0RXReset = new javax.swing.JButton();
        ButtonQ1B1RXReset = new javax.swing.JButton();
        jPanel16 = new javax.swing.JPanel();
        ButtonResetMD1DBA = new javax.swing.JButton();
        ButtonResetMD1DBB = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jPanel35 = new javax.swing.JPanel();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        TextFieldMD2A0s1 = new javax.swing.JTextField();
        TextFieldMD2A0s2 = new javax.swing.JTextField();
        TextFieldMD2A0s3 = new javax.swing.JTextField();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        TextFieldMD2A1s1 = new javax.swing.JTextField();
        TextFieldMD2B0s1 = new javax.swing.JTextField();
        TextFieldMD2B1s1 = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        jLabel37 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        TextFieldMD2A0s4 = new javax.swing.JTextField();
        TextFieldMD2A0s5 = new javax.swing.JTextField();
        TextFieldMD2A0s6 = new javax.swing.JTextField();
        jLabel39 = new javax.swing.JLabel();
        TextFieldMD2A0s7 = new javax.swing.JTextField();
        jLabel40 = new javax.swing.JLabel();
        TextFieldMD2A0s8 = new javax.swing.JTextField();
        TextFieldMD2A1s2 = new javax.swing.JTextField();
        TextFieldMD2B0s2 = new javax.swing.JTextField();
        TextFieldMD2B1s2 = new javax.swing.JTextField();
        TextFieldMD2A1s3 = new javax.swing.JTextField();
        TextFieldMD2B0s3 = new javax.swing.JTextField();
        TextFieldMD2B1s3 = new javax.swing.JTextField();
        TextFieldMD2A1s4 = new javax.swing.JTextField();
        TextFieldMD2B0s5 = new javax.swing.JTextField();
        TextFieldMD2B1s6 = new javax.swing.JTextField();
        TextFieldMD2B1s8 = new javax.swing.JTextField();
        TextFieldMD2A1s6 = new javax.swing.JTextField();
        TextFieldMD2A1s5 = new javax.swing.JTextField();
        TextFieldMD2A1s7 = new javax.swing.JTextField();
        TextFieldMD2B0s7 = new javax.swing.JTextField();
        TextFieldMD2B1s5 = new javax.swing.JTextField();
        TextFieldMD2B0s4 = new javax.swing.JTextField();
        TextFieldMD2B1s4 = new javax.swing.JTextField();
        TextFieldMD2B0s6 = new javax.swing.JTextField();
        TextFieldMD2B1s7 = new javax.swing.JTextField();
        TextFieldMD2A1s8 = new javax.swing.JTextField();
        TextFieldMD2B0s8 = new javax.swing.JTextField();
        jPanel11 = new javax.swing.JPanel();
        ButtonQ2A0TXReset = new javax.swing.JButton();
        ButtonQ2A1TXReset = new javax.swing.JButton();
        ButtonQ2B0TXReset = new javax.swing.JButton();
        ButtonQ2B1TXReset = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        ButtonQ2A0RXReset = new javax.swing.JButton();
        ButtonQ2A1RXReset = new javax.swing.JButton();
        ButtonQ2B0RXReset = new javax.swing.JButton();
        ButtonQ2B1RXReset = new javax.swing.JButton();
        jPanel17 = new javax.swing.JPanel();
        ButtonResetMD2DBA = new javax.swing.JButton();
        ButtonResetMD2DBB = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        jPanel36 = new javax.swing.JPanel();
        jLabel41 = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        TextFieldMD3A0s1 = new javax.swing.JTextField();
        TextFieldMD3A0s2 = new javax.swing.JTextField();
        TextFieldMD3A0s3 = new javax.swing.JTextField();
        jLabel45 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        TextFieldMD3A1s1 = new javax.swing.JTextField();
        TextFieldMD3B0s1 = new javax.swing.JTextField();
        TextFieldMD3B1s1 = new javax.swing.JTextField();
        jLabel48 = new javax.swing.JLabel();
        jLabel49 = new javax.swing.JLabel();
        jLabel50 = new javax.swing.JLabel();
        TextFieldMD3A0s4 = new javax.swing.JTextField();
        TextFieldMD3A0s5 = new javax.swing.JTextField();
        TextFieldMD3A0s6 = new javax.swing.JTextField();
        jLabel51 = new javax.swing.JLabel();
        TextFieldMD3A0s7 = new javax.swing.JTextField();
        jLabel52 = new javax.swing.JLabel();
        TextFieldMD3A0s8 = new javax.swing.JTextField();
        TextFieldMD3A1s2 = new javax.swing.JTextField();
        TextFieldMD3B0s2 = new javax.swing.JTextField();
        TextFieldMD3B1s2 = new javax.swing.JTextField();
        TextFieldMD3A1s3 = new javax.swing.JTextField();
        TextFieldMD3B0s3 = new javax.swing.JTextField();
        TextFieldMD3B1s3 = new javax.swing.JTextField();
        TextFieldMD3A1s4 = new javax.swing.JTextField();
        TextFieldMD3B0s5 = new javax.swing.JTextField();
        TextFieldMD3B1s6 = new javax.swing.JTextField();
        TextFieldMD3B1s8 = new javax.swing.JTextField();
        TextFieldMD3A1s6 = new javax.swing.JTextField();
        TextFieldMD3A1s5 = new javax.swing.JTextField();
        TextFieldMD3A1s7 = new javax.swing.JTextField();
        TextFieldMD3B0s7 = new javax.swing.JTextField();
        TextFieldMD3B1s5 = new javax.swing.JTextField();
        TextFieldMD3B0s4 = new javax.swing.JTextField();
        TextFieldMD3B1s4 = new javax.swing.JTextField();
        TextFieldMD3B0s6 = new javax.swing.JTextField();
        TextFieldMD3B1s7 = new javax.swing.JTextField();
        TextFieldMD3A1s8 = new javax.swing.JTextField();
        TextFieldMD3B0s8 = new javax.swing.JTextField();
        jPanel18 = new javax.swing.JPanel();
        ButtonQ3A0TXReset = new javax.swing.JButton();
        ButtonQ3A1TXReset = new javax.swing.JButton();
        ButtonQ3B0TXReset = new javax.swing.JButton();
        ButtonQ3B1TXReset = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        ButtonQ3A0RXReset = new javax.swing.JButton();
        ButtonQ3A1RXReset = new javax.swing.JButton();
        ButtonQ3B0RXReset = new javax.swing.JButton();
        ButtonQ3B1RXReset = new javax.swing.JButton();
        jPanel19 = new javax.swing.JPanel();
        ButtonResetMD3DBA = new javax.swing.JButton();
        ButtonResetMD3DBB = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        jPanel12 = new javax.swing.JPanel();
        jPanel37 = new javax.swing.JPanel();
        jLabel53 = new javax.swing.JLabel();
        jLabel54 = new javax.swing.JLabel();
        jLabel55 = new javax.swing.JLabel();
        jLabel56 = new javax.swing.JLabel();
        TextFieldMD4A0s1 = new javax.swing.JTextField();
        TextFieldMD4A0s2 = new javax.swing.JTextField();
        TextFieldMD4A0s3 = new javax.swing.JTextField();
        jLabel57 = new javax.swing.JLabel();
        jLabel58 = new javax.swing.JLabel();
        jLabel59 = new javax.swing.JLabel();
        TextFieldMD4A1s1 = new javax.swing.JTextField();
        TextFieldMD4B0s1 = new javax.swing.JTextField();
        TextFieldMD4B1s1 = new javax.swing.JTextField();
        jLabel60 = new javax.swing.JLabel();
        jLabel61 = new javax.swing.JLabel();
        jLabel62 = new javax.swing.JLabel();
        TextFieldMD4A0s4 = new javax.swing.JTextField();
        TextFieldMD4A0s5 = new javax.swing.JTextField();
        TextFieldMD4A0s6 = new javax.swing.JTextField();
        jLabel63 = new javax.swing.JLabel();
        TextFieldMD4A0s7 = new javax.swing.JTextField();
        jLabel64 = new javax.swing.JLabel();
        TextFieldMD4A0s8 = new javax.swing.JTextField();
        TextFieldMD4A1s2 = new javax.swing.JTextField();
        TextFieldMD4B0s2 = new javax.swing.JTextField();
        TextFieldMD4B1s2 = new javax.swing.JTextField();
        TextFieldMD4A1s3 = new javax.swing.JTextField();
        TextFieldMD4B0s3 = new javax.swing.JTextField();
        TextFieldMD4B1s3 = new javax.swing.JTextField();
        TextFieldMD4A1s4 = new javax.swing.JTextField();
        TextFieldMD4B0s5 = new javax.swing.JTextField();
        TextFieldMD4B1s6 = new javax.swing.JTextField();
        TextFieldMD4B1s8 = new javax.swing.JTextField();
        TextFieldMD4A1s6 = new javax.swing.JTextField();
        TextFieldMD4A1s5 = new javax.swing.JTextField();
        TextFieldMD4A1s7 = new javax.swing.JTextField();
        TextFieldMD4B0s7 = new javax.swing.JTextField();
        TextFieldMD4B1s5 = new javax.swing.JTextField();
        TextFieldMD4B0s4 = new javax.swing.JTextField();
        TextFieldMD4B1s4 = new javax.swing.JTextField();
        TextFieldMD4B0s6 = new javax.swing.JTextField();
        TextFieldMD4B1s7 = new javax.swing.JTextField();
        TextFieldMD4A1s8 = new javax.swing.JTextField();
        TextFieldMD4B0s8 = new javax.swing.JTextField();
        jPanel20 = new javax.swing.JPanel();
        ButtonQ4A0TXReset = new javax.swing.JButton();
        ButtonQ4A1TXReset = new javax.swing.JButton();
        ButtonQ4B0TXReset = new javax.swing.JButton();
        ButtonQ4B1TXReset = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        ButtonQ4A0RXReset = new javax.swing.JButton();
        ButtonQ4A1RXReset = new javax.swing.JButton();
        ButtonQ4B0RXReset = new javax.swing.JButton();
        ButtonQ4B1RXReset = new javax.swing.JButton();
        jPanel21 = new javax.swing.JPanel();
        ButtonResetMD4DBA = new javax.swing.JButton();
        ButtonResetMD4DBB = new javax.swing.JButton();
        jLabel12 = new javax.swing.JLabel();
        jPanel13 = new javax.swing.JPanel();
        jPanel14 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        CheckBoxMD1 = new javax.swing.JCheckBox();
        CheckBoxMD2 = new javax.swing.JCheckBox();
        CheckBoxMD3 = new javax.swing.JCheckBox();
        CheckBoxMD4 = new javax.swing.JCheckBox();
        jPanel5 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        CheckBoxReadUpdate = new javax.swing.JCheckBox();
        SliderReadUpdate = new javax.swing.JSlider();
        jPanel4 = new javax.swing.JPanel();
        ButtonRead = new javax.swing.JButton();
        jPanel15 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        ButtonResetQ1 = new javax.swing.JButton();
        ButtonResetQ2 = new javax.swing.JButton();
        ButtonResetQ3 = new javax.swing.JButton();
        ButtonResetQ4 = new javax.swing.JButton();

        java.awt.GridBagLayout jPanel1Layout = new java.awt.GridBagLayout();
        jPanel1Layout.columnWidths = new int[] {0};
        jPanel1Layout.rowHeights = new int[] {0};
        jPanel1.setLayout(jPanel1Layout);

        java.awt.GridBagLayout jPanel22Layout = new java.awt.GridBagLayout();
        jPanel22Layout.columnWidths = new int[] {0};
        jPanel22Layout.rowHeights = new int[] {0, 15, 0, 15, 0, 15, 0, 15, 0};
        jPanel22.setLayout(jPanel22Layout);

        java.awt.GridBagLayout jPanel9Layout = new java.awt.GridBagLayout();
        jPanel9Layout.columnWidths = new int[] {0, 25, 0, 25, 0};
        jPanel9Layout.rowHeights = new int[] {0};
        jPanel9.setLayout(jPanel9Layout);

        jPanel34.setBorder(javax.swing.BorderFactory.createTitledBorder("MD1"));
        java.awt.GridBagLayout jPanel34Layout = new java.awt.GridBagLayout();
        jPanel34Layout.columnWidths = new int[] {0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0};
        jPanel34Layout.rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0};
        jPanel34.setLayout(jPanel34Layout);

        jLabel17.setText("TX PLL\n");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel34.add(jLabel17, gridBagConstraints);

        jLabel18.setText("RX FRAMECLK");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel34.add(jLabel18, gridBagConstraints);

        jLabel19.setText("RX WORDCLK");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 0;
        jPanel34.add(jLabel19, gridBagConstraints);

        jLabel20.setText("Side A0");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel34.add(jLabel20, gridBagConstraints);

        TextFieldMD1A0s1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD1A0s1.setText("0");
        TextFieldMD1A0s1.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 2;
        jPanel34.add(TextFieldMD1A0s1, gridBagConstraints);

        TextFieldMD1A0s2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD1A0s2.setText("0");
        TextFieldMD1A0s2.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 2;
        jPanel34.add(TextFieldMD1A0s2, gridBagConstraints);

        TextFieldMD1A0s3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD1A0s3.setText("0");
        TextFieldMD1A0s3.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 2;
        jPanel34.add(TextFieldMD1A0s3, gridBagConstraints);

        jLabel21.setText("Side A1");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel34.add(jLabel21, gridBagConstraints);

        jLabel22.setText("Side B0");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel34.add(jLabel22, gridBagConstraints);

        jLabel23.setText("Side B1");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel34.add(jLabel23, gridBagConstraints);

        TextFieldMD1A1s1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD1A1s1.setText("0");
        TextFieldMD1A1s1.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 4;
        jPanel34.add(TextFieldMD1A1s1, gridBagConstraints);

        TextFieldMD1B0s1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD1B0s1.setText("0");
        TextFieldMD1B0s1.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 6;
        jPanel34.add(TextFieldMD1B0s1, gridBagConstraints);

        TextFieldMD1B1s1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD1B1s1.setText("0");
        TextFieldMD1B1s1.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 8;
        jPanel34.add(TextFieldMD1B1s1, gridBagConstraints);

        jLabel24.setText("MGT RDY");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 0;
        jPanel34.add(jLabel24, gridBagConstraints);

        jLabel25.setText("BITSLIPNb");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 0;
        jPanel34.add(jLabel25, gridBagConstraints);

        jLabel26.setText("GBTRX RDY");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 0;
        jPanel34.add(jLabel26, gridBagConstraints);

        TextFieldMD1A0s4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD1A0s4.setText("0");
        TextFieldMD1A0s4.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 2;
        jPanel34.add(TextFieldMD1A0s4, gridBagConstraints);

        TextFieldMD1A0s5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD1A0s5.setText("0");
        TextFieldMD1A0s5.setPreferredSize(new java.awt.Dimension(70, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 2;
        jPanel34.add(TextFieldMD1A0s5, gridBagConstraints);

        TextFieldMD1A0s6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD1A0s6.setText("0");
        TextFieldMD1A0s6.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 2;
        jPanel34.add(TextFieldMD1A0s6, gridBagConstraints);

        jLabel27.setText("GBTTX RDY LOST");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 0;
        jPanel34.add(jLabel27, gridBagConstraints);

        TextFieldMD1A0s7.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD1A0s7.setText("0");
        TextFieldMD1A0s7.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 2;
        jPanel34.add(TextFieldMD1A0s7, gridBagConstraints);

        jLabel28.setText("DATA ERR");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 20;
        gridBagConstraints.gridy = 0;
        jPanel34.add(jLabel28, gridBagConstraints);

        TextFieldMD1A0s8.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD1A0s8.setText("0");
        TextFieldMD1A0s8.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 20;
        gridBagConstraints.gridy = 2;
        jPanel34.add(TextFieldMD1A0s8, gridBagConstraints);

        TextFieldMD1A1s2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD1A1s2.setText("0");
        TextFieldMD1A1s2.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 4;
        jPanel34.add(TextFieldMD1A1s2, gridBagConstraints);

        TextFieldMD1B0s2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD1B0s2.setText("0");
        TextFieldMD1B0s2.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 6;
        jPanel34.add(TextFieldMD1B0s2, gridBagConstraints);

        TextFieldMD1B1s2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD1B1s2.setText("0");
        TextFieldMD1B1s2.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 8;
        jPanel34.add(TextFieldMD1B1s2, gridBagConstraints);

        TextFieldMD1A1s3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD1A1s3.setText("0");
        TextFieldMD1A1s3.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 4;
        jPanel34.add(TextFieldMD1A1s3, gridBagConstraints);

        TextFieldMD1B0s3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD1B0s3.setText("0");
        TextFieldMD1B0s3.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 6;
        jPanel34.add(TextFieldMD1B0s3, gridBagConstraints);

        TextFieldMD1B1s3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD1B1s3.setText("0");
        TextFieldMD1B1s3.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 8;
        jPanel34.add(TextFieldMD1B1s3, gridBagConstraints);

        TextFieldMD1A1s4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD1A1s4.setText("0");
        TextFieldMD1A1s4.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 4;
        jPanel34.add(TextFieldMD1A1s4, gridBagConstraints);

        TextFieldMD1B0s5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD1B0s5.setText("0");
        TextFieldMD1B0s5.setPreferredSize(new java.awt.Dimension(70, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 6;
        jPanel34.add(TextFieldMD1B0s5, gridBagConstraints);

        TextFieldMD1B1s6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD1B1s6.setText("0");
        TextFieldMD1B1s6.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 8;
        jPanel34.add(TextFieldMD1B1s6, gridBagConstraints);

        TextFieldMD1B1s8.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD1B1s8.setText("0");
        TextFieldMD1B1s8.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 20;
        gridBagConstraints.gridy = 8;
        jPanel34.add(TextFieldMD1B1s8, gridBagConstraints);

        TextFieldMD1A1s6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD1A1s6.setText("0");
        TextFieldMD1A1s6.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 4;
        jPanel34.add(TextFieldMD1A1s6, gridBagConstraints);

        TextFieldMD1A1s5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD1A1s5.setText("0");
        TextFieldMD1A1s5.setPreferredSize(new java.awt.Dimension(70, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 4;
        jPanel34.add(TextFieldMD1A1s5, gridBagConstraints);

        TextFieldMD1A1s7.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD1A1s7.setText("0");
        TextFieldMD1A1s7.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 4;
        jPanel34.add(TextFieldMD1A1s7, gridBagConstraints);

        TextFieldMD1B0s7.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD1B0s7.setText("0");
        TextFieldMD1B0s7.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 6;
        jPanel34.add(TextFieldMD1B0s7, gridBagConstraints);

        TextFieldMD1B1s5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD1B1s5.setText("0");
        TextFieldMD1B1s5.setPreferredSize(new java.awt.Dimension(70, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 8;
        jPanel34.add(TextFieldMD1B1s5, gridBagConstraints);

        TextFieldMD1B0s4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD1B0s4.setText("0");
        TextFieldMD1B0s4.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 6;
        jPanel34.add(TextFieldMD1B0s4, gridBagConstraints);

        TextFieldMD1B1s4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD1B1s4.setText("0");
        TextFieldMD1B1s4.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 8;
        jPanel34.add(TextFieldMD1B1s4, gridBagConstraints);

        TextFieldMD1B0s6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD1B0s6.setText("0");
        TextFieldMD1B0s6.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 6;
        jPanel34.add(TextFieldMD1B0s6, gridBagConstraints);

        TextFieldMD1B1s7.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD1B1s7.setText("0");
        TextFieldMD1B1s7.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 8;
        jPanel34.add(TextFieldMD1B1s7, gridBagConstraints);

        TextFieldMD1A1s8.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD1A1s8.setText("0");
        TextFieldMD1A1s8.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 20;
        gridBagConstraints.gridy = 4;
        jPanel34.add(TextFieldMD1A1s8, gridBagConstraints);

        TextFieldMD1B0s8.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD1B0s8.setText("0");
        TextFieldMD1B0s8.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 20;
        gridBagConstraints.gridy = 6;
        jPanel34.add(TextFieldMD1B0s8, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel9.add(jPanel34, gridBagConstraints);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("QSFP 1 Resets"));
        java.awt.GridBagLayout jPanel3Layout1 = new java.awt.GridBagLayout();
        jPanel3Layout1.columnWidths = new int[] {0, 10, 0};
        jPanel3Layout1.rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0};
        jPanel3.setLayout(jPanel3Layout1);

        ButtonQ1A0TXReset.setText("A0");
        ButtonQ1A0TXReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonQ1A0TXResetActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel3.add(ButtonQ1A0TXReset, gridBagConstraints);

        ButtonQ1A1TXReset.setText("A1");
        ButtonQ1A1TXReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonQ1A1TXResetActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        jPanel3.add(ButtonQ1A1TXReset, gridBagConstraints);

        ButtonQ1B0TXReset.setText("B0");
        ButtonQ1B0TXReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonQ1B0TXResetActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        jPanel3.add(ButtonQ1B0TXReset, gridBagConstraints);

        ButtonQ1B1TXReset.setText("B1");
        ButtonQ1B1TXReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonQ1B1TXResetActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        jPanel3.add(ButtonQ1B1TXReset, gridBagConstraints);

        jLabel1.setText("Tx");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel3.add(jLabel1, gridBagConstraints);

        jLabel2.setText("Rx");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel3.add(jLabel2, gridBagConstraints);

        ButtonQ1A0RXReset.setText("A0");
        ButtonQ1A0RXReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonQ1A0RXResetActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        jPanel3.add(ButtonQ1A0RXReset, gridBagConstraints);

        ButtonQ1A1RXReset.setText("A1");
        ButtonQ1A1RXReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonQ1A1RXResetActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        jPanel3.add(ButtonQ1A1RXReset, gridBagConstraints);

        ButtonQ1B0RXReset.setText("B0");
        ButtonQ1B0RXReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonQ1B0RXResetActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        jPanel3.add(ButtonQ1B0RXReset, gridBagConstraints);

        ButtonQ1B1RXReset.setText("B1");
        ButtonQ1B1RXReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonQ1B1RXResetActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 8;
        jPanel3.add(ButtonQ1B1RXReset, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel9.add(jPanel3, gridBagConstraints);

        java.awt.GridBagLayout jPanel16Layout = new java.awt.GridBagLayout();
        jPanel16Layout.columnWidths = new int[] {0};
        jPanel16Layout.rowHeights = new int[] {0, 1, 0, 1, 0, 1, 0};
        jPanel16.setLayout(jPanel16Layout);

        ButtonResetMD1DBA.setText("Side A");
        ButtonResetMD1DBA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonResetMD1DBAActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        jPanel16.add(ButtonResetMD1DBA, gridBagConstraints);

        ButtonResetMD1DBB.setText("Side B");
        ButtonResetMD1DBB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonResetMD1DBBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        jPanel16.add(ButtonResetMD1DBB, gridBagConstraints);

        jLabel5.setText("MD1 DB Reset");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel16.add(jLabel5, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel9.add(jPanel16, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel22.add(jPanel9, gridBagConstraints);

        java.awt.GridBagLayout jPanel8Layout = new java.awt.GridBagLayout();
        jPanel8Layout.columnWidths = new int[] {0, 25, 0, 25, 0};
        jPanel8Layout.rowHeights = new int[] {0};
        jPanel8.setLayout(jPanel8Layout);

        jPanel35.setBorder(javax.swing.BorderFactory.createTitledBorder("MD2"));
        java.awt.GridBagLayout jPanel35Layout = new java.awt.GridBagLayout();
        jPanel35Layout.columnWidths = new int[] {0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0};
        jPanel35Layout.rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0};
        jPanel35.setLayout(jPanel35Layout);

        jLabel29.setText("TX PLL");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel35.add(jLabel29, gridBagConstraints);

        jLabel30.setText("RX FRAMECLK");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel35.add(jLabel30, gridBagConstraints);

        jLabel31.setText("RX WORDCLK");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 0;
        jPanel35.add(jLabel31, gridBagConstraints);

        jLabel32.setText("Side A0");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel35.add(jLabel32, gridBagConstraints);

        TextFieldMD2A0s1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD2A0s1.setText("0");
        TextFieldMD2A0s1.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 2;
        jPanel35.add(TextFieldMD2A0s1, gridBagConstraints);

        TextFieldMD2A0s2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD2A0s2.setText("0");
        TextFieldMD2A0s2.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 2;
        jPanel35.add(TextFieldMD2A0s2, gridBagConstraints);

        TextFieldMD2A0s3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD2A0s3.setText("0");
        TextFieldMD2A0s3.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 2;
        jPanel35.add(TextFieldMD2A0s3, gridBagConstraints);

        jLabel33.setText("Side A1");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel35.add(jLabel33, gridBagConstraints);

        jLabel34.setText("Side B0");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel35.add(jLabel34, gridBagConstraints);

        jLabel35.setText("Side B1");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel35.add(jLabel35, gridBagConstraints);

        TextFieldMD2A1s1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD2A1s1.setText("0");
        TextFieldMD2A1s1.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 4;
        jPanel35.add(TextFieldMD2A1s1, gridBagConstraints);

        TextFieldMD2B0s1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD2B0s1.setText("0");
        TextFieldMD2B0s1.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 6;
        jPanel35.add(TextFieldMD2B0s1, gridBagConstraints);

        TextFieldMD2B1s1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD2B1s1.setText("0");
        TextFieldMD2B1s1.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 8;
        jPanel35.add(TextFieldMD2B1s1, gridBagConstraints);

        jLabel36.setText("MGT RDY");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 0;
        jPanel35.add(jLabel36, gridBagConstraints);

        jLabel37.setText("BITSLIPNb");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 0;
        jPanel35.add(jLabel37, gridBagConstraints);

        jLabel38.setText("GBTRX RDY");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 0;
        jPanel35.add(jLabel38, gridBagConstraints);

        TextFieldMD2A0s4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD2A0s4.setText("0");
        TextFieldMD2A0s4.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 2;
        jPanel35.add(TextFieldMD2A0s4, gridBagConstraints);

        TextFieldMD2A0s5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD2A0s5.setText("0");
        TextFieldMD2A0s5.setPreferredSize(new java.awt.Dimension(70, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 2;
        jPanel35.add(TextFieldMD2A0s5, gridBagConstraints);

        TextFieldMD2A0s6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD2A0s6.setText("0");
        TextFieldMD2A0s6.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 2;
        jPanel35.add(TextFieldMD2A0s6, gridBagConstraints);

        jLabel39.setText("GBTTX RDY LOST");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 0;
        jPanel35.add(jLabel39, gridBagConstraints);

        TextFieldMD2A0s7.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD2A0s7.setText("0");
        TextFieldMD2A0s7.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 2;
        jPanel35.add(TextFieldMD2A0s7, gridBagConstraints);

        jLabel40.setText("DATA ERR");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 20;
        gridBagConstraints.gridy = 0;
        jPanel35.add(jLabel40, gridBagConstraints);

        TextFieldMD2A0s8.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD2A0s8.setText("0");
        TextFieldMD2A0s8.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 20;
        gridBagConstraints.gridy = 2;
        jPanel35.add(TextFieldMD2A0s8, gridBagConstraints);

        TextFieldMD2A1s2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD2A1s2.setText("0");
        TextFieldMD2A1s2.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 4;
        jPanel35.add(TextFieldMD2A1s2, gridBagConstraints);

        TextFieldMD2B0s2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD2B0s2.setText("0");
        TextFieldMD2B0s2.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 6;
        jPanel35.add(TextFieldMD2B0s2, gridBagConstraints);

        TextFieldMD2B1s2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD2B1s2.setText("0");
        TextFieldMD2B1s2.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 8;
        jPanel35.add(TextFieldMD2B1s2, gridBagConstraints);

        TextFieldMD2A1s3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD2A1s3.setText("0");
        TextFieldMD2A1s3.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 4;
        jPanel35.add(TextFieldMD2A1s3, gridBagConstraints);

        TextFieldMD2B0s3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD2B0s3.setText("0");
        TextFieldMD2B0s3.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 6;
        jPanel35.add(TextFieldMD2B0s3, gridBagConstraints);

        TextFieldMD2B1s3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD2B1s3.setText("0");
        TextFieldMD2B1s3.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 8;
        jPanel35.add(TextFieldMD2B1s3, gridBagConstraints);

        TextFieldMD2A1s4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD2A1s4.setText("0");
        TextFieldMD2A1s4.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 4;
        jPanel35.add(TextFieldMD2A1s4, gridBagConstraints);

        TextFieldMD2B0s5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD2B0s5.setText("0");
        TextFieldMD2B0s5.setPreferredSize(new java.awt.Dimension(70, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 6;
        jPanel35.add(TextFieldMD2B0s5, gridBagConstraints);

        TextFieldMD2B1s6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD2B1s6.setText("0");
        TextFieldMD2B1s6.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 8;
        jPanel35.add(TextFieldMD2B1s6, gridBagConstraints);

        TextFieldMD2B1s8.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD2B1s8.setText("0");
        TextFieldMD2B1s8.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 20;
        gridBagConstraints.gridy = 8;
        jPanel35.add(TextFieldMD2B1s8, gridBagConstraints);

        TextFieldMD2A1s6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD2A1s6.setText("0");
        TextFieldMD2A1s6.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 4;
        jPanel35.add(TextFieldMD2A1s6, gridBagConstraints);

        TextFieldMD2A1s5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD2A1s5.setText("0");
        TextFieldMD2A1s5.setPreferredSize(new java.awt.Dimension(70, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 4;
        jPanel35.add(TextFieldMD2A1s5, gridBagConstraints);

        TextFieldMD2A1s7.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD2A1s7.setText("0");
        TextFieldMD2A1s7.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 4;
        jPanel35.add(TextFieldMD2A1s7, gridBagConstraints);

        TextFieldMD2B0s7.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD2B0s7.setText("0");
        TextFieldMD2B0s7.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 6;
        jPanel35.add(TextFieldMD2B0s7, gridBagConstraints);

        TextFieldMD2B1s5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD2B1s5.setText("0");
        TextFieldMD2B1s5.setPreferredSize(new java.awt.Dimension(70, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 8;
        jPanel35.add(TextFieldMD2B1s5, gridBagConstraints);

        TextFieldMD2B0s4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD2B0s4.setText("0");
        TextFieldMD2B0s4.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 6;
        jPanel35.add(TextFieldMD2B0s4, gridBagConstraints);

        TextFieldMD2B1s4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD2B1s4.setText("0");
        TextFieldMD2B1s4.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 8;
        jPanel35.add(TextFieldMD2B1s4, gridBagConstraints);

        TextFieldMD2B0s6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD2B0s6.setText("0");
        TextFieldMD2B0s6.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 6;
        jPanel35.add(TextFieldMD2B0s6, gridBagConstraints);

        TextFieldMD2B1s7.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD2B1s7.setText("0");
        TextFieldMD2B1s7.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 8;
        jPanel35.add(TextFieldMD2B1s7, gridBagConstraints);

        TextFieldMD2A1s8.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD2A1s8.setText("0");
        TextFieldMD2A1s8.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 20;
        gridBagConstraints.gridy = 4;
        jPanel35.add(TextFieldMD2A1s8, gridBagConstraints);

        TextFieldMD2B0s8.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD2B0s8.setText("0");
        TextFieldMD2B0s8.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 20;
        gridBagConstraints.gridy = 6;
        jPanel35.add(TextFieldMD2B0s8, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel8.add(jPanel35, gridBagConstraints);

        jPanel11.setBorder(javax.swing.BorderFactory.createTitledBorder("QSFP 2 Resets"));
        java.awt.GridBagLayout jPanel11Layout = new java.awt.GridBagLayout();
        jPanel11Layout.columnWidths = new int[] {0, 10, 0};
        jPanel11Layout.rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0};
        jPanel11.setLayout(jPanel11Layout);

        ButtonQ2A0TXReset.setText("A0");
        ButtonQ2A0TXReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonQ2A0TXResetActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel11.add(ButtonQ2A0TXReset, gridBagConstraints);

        ButtonQ2A1TXReset.setText("A1");
        ButtonQ2A1TXReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonQ2A1TXResetActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        jPanel11.add(ButtonQ2A1TXReset, gridBagConstraints);

        ButtonQ2B0TXReset.setText("B0");
        ButtonQ2B0TXReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonQ2B0TXResetActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        jPanel11.add(ButtonQ2B0TXReset, gridBagConstraints);

        ButtonQ2B1TXReset.setText("B1");
        ButtonQ2B1TXReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonQ2B1TXResetActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        jPanel11.add(ButtonQ2B1TXReset, gridBagConstraints);

        jLabel3.setText("Tx");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel11.add(jLabel3, gridBagConstraints);

        jLabel4.setText("Rx");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel11.add(jLabel4, gridBagConstraints);

        ButtonQ2A0RXReset.setText("A0");
        ButtonQ2A0RXReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonQ2A0RXResetActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        jPanel11.add(ButtonQ2A0RXReset, gridBagConstraints);

        ButtonQ2A1RXReset.setText("A1");
        ButtonQ2A1RXReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonQ2A1RXResetActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        jPanel11.add(ButtonQ2A1RXReset, gridBagConstraints);

        ButtonQ2B0RXReset.setText("B0");
        ButtonQ2B0RXReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonQ2B0RXResetActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        jPanel11.add(ButtonQ2B0RXReset, gridBagConstraints);

        ButtonQ2B1RXReset.setText("B1");
        ButtonQ2B1RXReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonQ2B1RXResetActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 8;
        jPanel11.add(ButtonQ2B1RXReset, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel8.add(jPanel11, gridBagConstraints);

        java.awt.GridBagLayout jPanel17Layout = new java.awt.GridBagLayout();
        jPanel17Layout.columnWidths = new int[] {0};
        jPanel17Layout.rowHeights = new int[] {0, 1, 0, 1, 0, 1, 0};
        jPanel17.setLayout(jPanel17Layout);

        ButtonResetMD2DBA.setText("Side A");
        ButtonResetMD2DBA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonResetMD2DBAActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        jPanel17.add(ButtonResetMD2DBA, gridBagConstraints);

        ButtonResetMD2DBB.setText("Side B");
        ButtonResetMD2DBB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonResetMD2DBBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        jPanel17.add(ButtonResetMD2DBB, gridBagConstraints);

        jLabel6.setText("MD2 DB Reset");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel17.add(jLabel6, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel8.add(jPanel17, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel22.add(jPanel8, gridBagConstraints);

        java.awt.GridBagLayout jPanel10Layout = new java.awt.GridBagLayout();
        jPanel10Layout.columnWidths = new int[] {0, 25, 0, 25, 0};
        jPanel10Layout.rowHeights = new int[] {0};
        jPanel10.setLayout(jPanel10Layout);

        jPanel36.setBorder(javax.swing.BorderFactory.createTitledBorder("MD3"));
        java.awt.GridBagLayout jPanel36Layout = new java.awt.GridBagLayout();
        jPanel36Layout.columnWidths = new int[] {0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0};
        jPanel36Layout.rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0};
        jPanel36.setLayout(jPanel36Layout);

        jLabel41.setText("TX PLL");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel36.add(jLabel41, gridBagConstraints);

        jLabel42.setText("RX FRAMECLK");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel36.add(jLabel42, gridBagConstraints);

        jLabel43.setText("RX WORDCLK");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 0;
        jPanel36.add(jLabel43, gridBagConstraints);

        jLabel44.setText("Side A0");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel36.add(jLabel44, gridBagConstraints);

        TextFieldMD3A0s1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD3A0s1.setText("0");
        TextFieldMD3A0s1.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 2;
        jPanel36.add(TextFieldMD3A0s1, gridBagConstraints);

        TextFieldMD3A0s2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD3A0s2.setText("0");
        TextFieldMD3A0s2.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 2;
        jPanel36.add(TextFieldMD3A0s2, gridBagConstraints);

        TextFieldMD3A0s3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD3A0s3.setText("0");
        TextFieldMD3A0s3.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 2;
        jPanel36.add(TextFieldMD3A0s3, gridBagConstraints);

        jLabel45.setText("Side A1");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel36.add(jLabel45, gridBagConstraints);

        jLabel46.setText("Side B0");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel36.add(jLabel46, gridBagConstraints);

        jLabel47.setText("Side B1");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel36.add(jLabel47, gridBagConstraints);

        TextFieldMD3A1s1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD3A1s1.setText("0");
        TextFieldMD3A1s1.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 4;
        jPanel36.add(TextFieldMD3A1s1, gridBagConstraints);

        TextFieldMD3B0s1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD3B0s1.setText("0");
        TextFieldMD3B0s1.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 6;
        jPanel36.add(TextFieldMD3B0s1, gridBagConstraints);

        TextFieldMD3B1s1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD3B1s1.setText("0");
        TextFieldMD3B1s1.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 8;
        jPanel36.add(TextFieldMD3B1s1, gridBagConstraints);

        jLabel48.setText("MGT RDY");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 0;
        jPanel36.add(jLabel48, gridBagConstraints);

        jLabel49.setText("BITSLIPNb");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 0;
        jPanel36.add(jLabel49, gridBagConstraints);

        jLabel50.setText("GBTRX RDY");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 0;
        jPanel36.add(jLabel50, gridBagConstraints);

        TextFieldMD3A0s4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD3A0s4.setText("0");
        TextFieldMD3A0s4.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 2;
        jPanel36.add(TextFieldMD3A0s4, gridBagConstraints);

        TextFieldMD3A0s5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD3A0s5.setText("0");
        TextFieldMD3A0s5.setPreferredSize(new java.awt.Dimension(70, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 2;
        jPanel36.add(TextFieldMD3A0s5, gridBagConstraints);

        TextFieldMD3A0s6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD3A0s6.setText("0");
        TextFieldMD3A0s6.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 2;
        jPanel36.add(TextFieldMD3A0s6, gridBagConstraints);

        jLabel51.setText("GBTTX RDY LOST");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 0;
        jPanel36.add(jLabel51, gridBagConstraints);

        TextFieldMD3A0s7.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD3A0s7.setText("0");
        TextFieldMD3A0s7.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 2;
        jPanel36.add(TextFieldMD3A0s7, gridBagConstraints);

        jLabel52.setText("DATA ERR");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 20;
        gridBagConstraints.gridy = 0;
        jPanel36.add(jLabel52, gridBagConstraints);

        TextFieldMD3A0s8.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD3A0s8.setText("0");
        TextFieldMD3A0s8.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 20;
        gridBagConstraints.gridy = 2;
        jPanel36.add(TextFieldMD3A0s8, gridBagConstraints);

        TextFieldMD3A1s2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD3A1s2.setText("0");
        TextFieldMD3A1s2.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 4;
        jPanel36.add(TextFieldMD3A1s2, gridBagConstraints);

        TextFieldMD3B0s2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD3B0s2.setText("0");
        TextFieldMD3B0s2.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 6;
        jPanel36.add(TextFieldMD3B0s2, gridBagConstraints);

        TextFieldMD3B1s2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD3B1s2.setText("0");
        TextFieldMD3B1s2.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 8;
        jPanel36.add(TextFieldMD3B1s2, gridBagConstraints);

        TextFieldMD3A1s3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD3A1s3.setText("0");
        TextFieldMD3A1s3.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 4;
        jPanel36.add(TextFieldMD3A1s3, gridBagConstraints);

        TextFieldMD3B0s3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD3B0s3.setText("0");
        TextFieldMD3B0s3.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 6;
        jPanel36.add(TextFieldMD3B0s3, gridBagConstraints);

        TextFieldMD3B1s3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD3B1s3.setText("0");
        TextFieldMD3B1s3.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 8;
        jPanel36.add(TextFieldMD3B1s3, gridBagConstraints);

        TextFieldMD3A1s4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD3A1s4.setText("0");
        TextFieldMD3A1s4.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 4;
        jPanel36.add(TextFieldMD3A1s4, gridBagConstraints);

        TextFieldMD3B0s5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD3B0s5.setText("0");
        TextFieldMD3B0s5.setPreferredSize(new java.awt.Dimension(70, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 6;
        jPanel36.add(TextFieldMD3B0s5, gridBagConstraints);

        TextFieldMD3B1s6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD3B1s6.setText("0");
        TextFieldMD3B1s6.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 8;
        jPanel36.add(TextFieldMD3B1s6, gridBagConstraints);

        TextFieldMD3B1s8.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD3B1s8.setText("0");
        TextFieldMD3B1s8.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 20;
        gridBagConstraints.gridy = 8;
        jPanel36.add(TextFieldMD3B1s8, gridBagConstraints);

        TextFieldMD3A1s6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD3A1s6.setText("0");
        TextFieldMD3A1s6.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 4;
        jPanel36.add(TextFieldMD3A1s6, gridBagConstraints);

        TextFieldMD3A1s5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD3A1s5.setText("0");
        TextFieldMD3A1s5.setPreferredSize(new java.awt.Dimension(70, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 4;
        jPanel36.add(TextFieldMD3A1s5, gridBagConstraints);

        TextFieldMD3A1s7.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD3A1s7.setText("0");
        TextFieldMD3A1s7.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 4;
        jPanel36.add(TextFieldMD3A1s7, gridBagConstraints);

        TextFieldMD3B0s7.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD3B0s7.setText("0");
        TextFieldMD3B0s7.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 6;
        jPanel36.add(TextFieldMD3B0s7, gridBagConstraints);

        TextFieldMD3B1s5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD3B1s5.setText("0");
        TextFieldMD3B1s5.setPreferredSize(new java.awt.Dimension(70, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 8;
        jPanel36.add(TextFieldMD3B1s5, gridBagConstraints);

        TextFieldMD3B0s4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD3B0s4.setText("0");
        TextFieldMD3B0s4.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 6;
        jPanel36.add(TextFieldMD3B0s4, gridBagConstraints);

        TextFieldMD3B1s4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD3B1s4.setText("0");
        TextFieldMD3B1s4.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 8;
        jPanel36.add(TextFieldMD3B1s4, gridBagConstraints);

        TextFieldMD3B0s6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD3B0s6.setText("0");
        TextFieldMD3B0s6.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 6;
        jPanel36.add(TextFieldMD3B0s6, gridBagConstraints);

        TextFieldMD3B1s7.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD3B1s7.setText("0");
        TextFieldMD3B1s7.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 8;
        jPanel36.add(TextFieldMD3B1s7, gridBagConstraints);

        TextFieldMD3A1s8.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD3A1s8.setText("0");
        TextFieldMD3A1s8.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 20;
        gridBagConstraints.gridy = 4;
        jPanel36.add(TextFieldMD3A1s8, gridBagConstraints);

        TextFieldMD3B0s8.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD3B0s8.setText("0");
        TextFieldMD3B0s8.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 20;
        gridBagConstraints.gridy = 6;
        jPanel36.add(TextFieldMD3B0s8, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel10.add(jPanel36, gridBagConstraints);

        jPanel18.setBorder(javax.swing.BorderFactory.createTitledBorder("QSFP 3 Resets"));
        java.awt.GridBagLayout jPanel18Layout = new java.awt.GridBagLayout();
        jPanel18Layout.columnWidths = new int[] {0, 10, 0};
        jPanel18Layout.rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0};
        jPanel18.setLayout(jPanel18Layout);

        ButtonQ3A0TXReset.setText("A0");
        ButtonQ3A0TXReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonQ3A0TXResetActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel18.add(ButtonQ3A0TXReset, gridBagConstraints);

        ButtonQ3A1TXReset.setText("A1");
        ButtonQ3A1TXReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonQ3A1TXResetActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        jPanel18.add(ButtonQ3A1TXReset, gridBagConstraints);

        ButtonQ3B0TXReset.setText("B0");
        ButtonQ3B0TXReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonQ3B0TXResetActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        jPanel18.add(ButtonQ3B0TXReset, gridBagConstraints);

        ButtonQ3B1TXReset.setText("B1");
        ButtonQ3B1TXReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonQ3B1TXResetActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        jPanel18.add(ButtonQ3B1TXReset, gridBagConstraints);

        jLabel7.setText("Tx");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel18.add(jLabel7, gridBagConstraints);

        jLabel8.setText("Rx");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel18.add(jLabel8, gridBagConstraints);

        ButtonQ3A0RXReset.setText("A0");
        ButtonQ3A0RXReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonQ3A0RXResetActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        jPanel18.add(ButtonQ3A0RXReset, gridBagConstraints);

        ButtonQ3A1RXReset.setText("A1");
        ButtonQ3A1RXReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonQ3A1RXResetActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        jPanel18.add(ButtonQ3A1RXReset, gridBagConstraints);

        ButtonQ3B0RXReset.setText("B0");
        ButtonQ3B0RXReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonQ3B0RXResetActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        jPanel18.add(ButtonQ3B0RXReset, gridBagConstraints);

        ButtonQ3B1RXReset.setText("B1");
        ButtonQ3B1RXReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonQ3B1RXResetActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 8;
        jPanel18.add(ButtonQ3B1RXReset, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel10.add(jPanel18, gridBagConstraints);

        java.awt.GridBagLayout jPanel19Layout = new java.awt.GridBagLayout();
        jPanel19Layout.columnWidths = new int[] {0};
        jPanel19Layout.rowHeights = new int[] {0, 1, 0, 1, 0, 1, 0};
        jPanel19.setLayout(jPanel19Layout);

        ButtonResetMD3DBA.setText("Side A");
        ButtonResetMD3DBA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonResetMD3DBAActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        jPanel19.add(ButtonResetMD3DBA, gridBagConstraints);

        ButtonResetMD3DBB.setText("Side B");
        ButtonResetMD3DBB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonResetMD3DBBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        jPanel19.add(ButtonResetMD3DBB, gridBagConstraints);

        jLabel9.setText("MD3 DB Reset");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel19.add(jLabel9, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel10.add(jPanel19, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel22.add(jPanel10, gridBagConstraints);

        java.awt.GridBagLayout jPanel12Layout = new java.awt.GridBagLayout();
        jPanel12Layout.columnWidths = new int[] {0, 25, 0, 25, 0};
        jPanel12Layout.rowHeights = new int[] {0};
        jPanel12.setLayout(jPanel12Layout);

        jPanel37.setBorder(javax.swing.BorderFactory.createTitledBorder("MD4"));
        java.awt.GridBagLayout jPanel37Layout = new java.awt.GridBagLayout();
        jPanel37Layout.columnWidths = new int[] {0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0};
        jPanel37Layout.rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0};
        jPanel37.setLayout(jPanel37Layout);

        jLabel53.setText("TX PLL");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel37.add(jLabel53, gridBagConstraints);

        jLabel54.setText("RX FRAMECLK");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel37.add(jLabel54, gridBagConstraints);

        jLabel55.setText("RX WORDCLK");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 0;
        jPanel37.add(jLabel55, gridBagConstraints);

        jLabel56.setText("Side A0");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel37.add(jLabel56, gridBagConstraints);

        TextFieldMD4A0s1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD4A0s1.setText("0");
        TextFieldMD4A0s1.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 2;
        jPanel37.add(TextFieldMD4A0s1, gridBagConstraints);

        TextFieldMD4A0s2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD4A0s2.setText("0");
        TextFieldMD4A0s2.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 2;
        jPanel37.add(TextFieldMD4A0s2, gridBagConstraints);

        TextFieldMD4A0s3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD4A0s3.setText("0");
        TextFieldMD4A0s3.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 2;
        jPanel37.add(TextFieldMD4A0s3, gridBagConstraints);

        jLabel57.setText("Side A1");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel37.add(jLabel57, gridBagConstraints);

        jLabel58.setText("Side B0");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel37.add(jLabel58, gridBagConstraints);

        jLabel59.setText("Side B1");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel37.add(jLabel59, gridBagConstraints);

        TextFieldMD4A1s1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD4A1s1.setText("0");
        TextFieldMD4A1s1.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 4;
        jPanel37.add(TextFieldMD4A1s1, gridBagConstraints);

        TextFieldMD4B0s1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD4B0s1.setText("0");
        TextFieldMD4B0s1.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 6;
        jPanel37.add(TextFieldMD4B0s1, gridBagConstraints);

        TextFieldMD4B1s1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD4B1s1.setText("0");
        TextFieldMD4B1s1.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 8;
        jPanel37.add(TextFieldMD4B1s1, gridBagConstraints);

        jLabel60.setText("MGT RDY");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 0;
        jPanel37.add(jLabel60, gridBagConstraints);

        jLabel61.setText("BITSLIPNb");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 0;
        jPanel37.add(jLabel61, gridBagConstraints);

        jLabel62.setText("GBTRX RDY");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 0;
        jPanel37.add(jLabel62, gridBagConstraints);

        TextFieldMD4A0s4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD4A0s4.setText("0");
        TextFieldMD4A0s4.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 2;
        jPanel37.add(TextFieldMD4A0s4, gridBagConstraints);

        TextFieldMD4A0s5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD4A0s5.setText("0");
        TextFieldMD4A0s5.setPreferredSize(new java.awt.Dimension(70, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 2;
        jPanel37.add(TextFieldMD4A0s5, gridBagConstraints);

        TextFieldMD4A0s6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD4A0s6.setText("0");
        TextFieldMD4A0s6.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 2;
        jPanel37.add(TextFieldMD4A0s6, gridBagConstraints);

        jLabel63.setText("GBTTX RDY LOST");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 0;
        jPanel37.add(jLabel63, gridBagConstraints);

        TextFieldMD4A0s7.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD4A0s7.setText("0");
        TextFieldMD4A0s7.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 2;
        jPanel37.add(TextFieldMD4A0s7, gridBagConstraints);

        jLabel64.setText("DATA ERR");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 20;
        gridBagConstraints.gridy = 0;
        jPanel37.add(jLabel64, gridBagConstraints);

        TextFieldMD4A0s8.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD4A0s8.setText("0");
        TextFieldMD4A0s8.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 20;
        gridBagConstraints.gridy = 2;
        jPanel37.add(TextFieldMD4A0s8, gridBagConstraints);

        TextFieldMD4A1s2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD4A1s2.setText("0");
        TextFieldMD4A1s2.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 4;
        jPanel37.add(TextFieldMD4A1s2, gridBagConstraints);

        TextFieldMD4B0s2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD4B0s2.setText("0");
        TextFieldMD4B0s2.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 6;
        jPanel37.add(TextFieldMD4B0s2, gridBagConstraints);

        TextFieldMD4B1s2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD4B1s2.setText("0");
        TextFieldMD4B1s2.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 8;
        jPanel37.add(TextFieldMD4B1s2, gridBagConstraints);

        TextFieldMD4A1s3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD4A1s3.setText("0");
        TextFieldMD4A1s3.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 4;
        jPanel37.add(TextFieldMD4A1s3, gridBagConstraints);

        TextFieldMD4B0s3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD4B0s3.setText("0");
        TextFieldMD4B0s3.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 6;
        jPanel37.add(TextFieldMD4B0s3, gridBagConstraints);

        TextFieldMD4B1s3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD4B1s3.setText("0");
        TextFieldMD4B1s3.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 8;
        jPanel37.add(TextFieldMD4B1s3, gridBagConstraints);

        TextFieldMD4A1s4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD4A1s4.setText("0");
        TextFieldMD4A1s4.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 4;
        jPanel37.add(TextFieldMD4A1s4, gridBagConstraints);

        TextFieldMD4B0s5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD4B0s5.setText("0");
        TextFieldMD4B0s5.setPreferredSize(new java.awt.Dimension(70, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 6;
        jPanel37.add(TextFieldMD4B0s5, gridBagConstraints);

        TextFieldMD4B1s6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD4B1s6.setText("0");
        TextFieldMD4B1s6.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 8;
        jPanel37.add(TextFieldMD4B1s6, gridBagConstraints);

        TextFieldMD4B1s8.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD4B1s8.setText("0");
        TextFieldMD4B1s8.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 20;
        gridBagConstraints.gridy = 8;
        jPanel37.add(TextFieldMD4B1s8, gridBagConstraints);

        TextFieldMD4A1s6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD4A1s6.setText("0");
        TextFieldMD4A1s6.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 4;
        jPanel37.add(TextFieldMD4A1s6, gridBagConstraints);

        TextFieldMD4A1s5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD4A1s5.setText("0");
        TextFieldMD4A1s5.setPreferredSize(new java.awt.Dimension(70, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 4;
        jPanel37.add(TextFieldMD4A1s5, gridBagConstraints);

        TextFieldMD4A1s7.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD4A1s7.setText("0");
        TextFieldMD4A1s7.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 4;
        jPanel37.add(TextFieldMD4A1s7, gridBagConstraints);

        TextFieldMD4B0s7.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD4B0s7.setText("0");
        TextFieldMD4B0s7.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 6;
        jPanel37.add(TextFieldMD4B0s7, gridBagConstraints);

        TextFieldMD4B1s5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD4B1s5.setText("0");
        TextFieldMD4B1s5.setPreferredSize(new java.awt.Dimension(70, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 8;
        jPanel37.add(TextFieldMD4B1s5, gridBagConstraints);

        TextFieldMD4B0s4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD4B0s4.setText("0");
        TextFieldMD4B0s4.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 6;
        jPanel37.add(TextFieldMD4B0s4, gridBagConstraints);

        TextFieldMD4B1s4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD4B1s4.setText("0");
        TextFieldMD4B1s4.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 8;
        jPanel37.add(TextFieldMD4B1s4, gridBagConstraints);

        TextFieldMD4B0s6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD4B0s6.setText("0");
        TextFieldMD4B0s6.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 6;
        jPanel37.add(TextFieldMD4B0s6, gridBagConstraints);

        TextFieldMD4B1s7.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD4B1s7.setText("0");
        TextFieldMD4B1s7.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 8;
        jPanel37.add(TextFieldMD4B1s7, gridBagConstraints);

        TextFieldMD4A1s8.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD4A1s8.setText("0");
        TextFieldMD4A1s8.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 20;
        gridBagConstraints.gridy = 4;
        jPanel37.add(TextFieldMD4A1s8, gridBagConstraints);

        TextFieldMD4B0s8.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD4B0s8.setText("0");
        TextFieldMD4B0s8.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 20;
        gridBagConstraints.gridy = 6;
        jPanel37.add(TextFieldMD4B0s8, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel12.add(jPanel37, gridBagConstraints);

        jPanel20.setBorder(javax.swing.BorderFactory.createTitledBorder("QSFP 4 Resets"));
        java.awt.GridBagLayout jPanel20Layout = new java.awt.GridBagLayout();
        jPanel20Layout.columnWidths = new int[] {0, 10, 0};
        jPanel20Layout.rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0};
        jPanel20.setLayout(jPanel20Layout);

        ButtonQ4A0TXReset.setText("A0");
        ButtonQ4A0TXReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonQ4A0TXResetActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel20.add(ButtonQ4A0TXReset, gridBagConstraints);

        ButtonQ4A1TXReset.setText("A1");
        ButtonQ4A1TXReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonQ4A1TXResetActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        jPanel20.add(ButtonQ4A1TXReset, gridBagConstraints);

        ButtonQ4B0TXReset.setText("B0");
        ButtonQ4B0TXReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonQ4B0TXResetActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        jPanel20.add(ButtonQ4B0TXReset, gridBagConstraints);

        ButtonQ4B1TXReset.setText("B1");
        ButtonQ4B1TXReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonQ4B1TXResetActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        jPanel20.add(ButtonQ4B1TXReset, gridBagConstraints);

        jLabel10.setText("Tx");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel20.add(jLabel10, gridBagConstraints);

        jLabel11.setText("Rx");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel20.add(jLabel11, gridBagConstraints);

        ButtonQ4A0RXReset.setText("A0");
        ButtonQ4A0RXReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonQ4A0RXResetActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        jPanel20.add(ButtonQ4A0RXReset, gridBagConstraints);

        ButtonQ4A1RXReset.setText("A1");
        ButtonQ4A1RXReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonQ4A1RXResetActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        jPanel20.add(ButtonQ4A1RXReset, gridBagConstraints);

        ButtonQ4B0RXReset.setText("B0");
        ButtonQ4B0RXReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonQ4B0RXResetActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        jPanel20.add(ButtonQ4B0RXReset, gridBagConstraints);

        ButtonQ4B1RXReset.setText("B1");
        ButtonQ4B1RXReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonQ4B1RXResetActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 8;
        jPanel20.add(ButtonQ4B1RXReset, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel12.add(jPanel20, gridBagConstraints);

        java.awt.GridBagLayout jPanel21Layout = new java.awt.GridBagLayout();
        jPanel21Layout.columnWidths = new int[] {0};
        jPanel21Layout.rowHeights = new int[] {0, 1, 0, 1, 0, 1, 0};
        jPanel21.setLayout(jPanel21Layout);

        ButtonResetMD4DBA.setText("Side A");
        ButtonResetMD4DBA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonResetMD4DBAActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        jPanel21.add(ButtonResetMD4DBA, gridBagConstraints);

        ButtonResetMD4DBB.setText("Side B");
        ButtonResetMD4DBB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonResetMD4DBBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        jPanel21.add(ButtonResetMD4DBB, gridBagConstraints);

        jLabel12.setText("MD4 DB Reset");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel21.add(jLabel12, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel12.add(jPanel21, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel22.add(jPanel12, gridBagConstraints);

        java.awt.GridBagLayout jPanel13Layout = new java.awt.GridBagLayout();
        jPanel13Layout.columnWidths = new int[] {0, 20, 0, 20, 0, 20, 0};
        jPanel13Layout.rowHeights = new int[] {0};
        jPanel13.setLayout(jPanel13Layout);

        jPanel14.setLayout(new java.awt.GridBagLayout());

        java.awt.GridBagLayout jPanel2Layout = new java.awt.GridBagLayout();
        jPanel2Layout.columnWidths = new int[] {0, 10, 0, 10, 0, 10, 0};
        jPanel2Layout.rowHeights = new int[] {0};
        jPanel2.setLayout(jPanel2Layout);

        CheckBoxMD1.setSelected(true);
        CheckBoxMD1.setText("MD1");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel2.add(CheckBoxMD1, gridBagConstraints);

        CheckBoxMD2.setText("MD2");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel2.add(CheckBoxMD2, gridBagConstraints);

        CheckBoxMD3.setText("MD3");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel2.add(CheckBoxMD3, gridBagConstraints);

        CheckBoxMD4.setText("MD4");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel2.add(CheckBoxMD4, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel14.add(jPanel2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel13.add(jPanel14, gridBagConstraints);

        jPanel5.setLayout(new java.awt.GridBagLayout());

        jPanel6.setLayout(new java.awt.GridBagLayout());

        CheckBoxReadUpdate.setText("Read Update (seconds)");
        CheckBoxReadUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckBoxReadUpdateActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel6.add(CheckBoxReadUpdate, gridBagConstraints);

        SliderReadUpdate.setMajorTickSpacing(4);
        SliderReadUpdate.setMaximum(30);
        SliderReadUpdate.setMinimum(1);
        SliderReadUpdate.setMinorTickSpacing(1);
        SliderReadUpdate.setPaintLabels(true);
        SliderReadUpdate.setPaintTicks(true);
        SliderReadUpdate.setSnapToTicks(true);
        SliderReadUpdate.setValue(2);
        SliderReadUpdate.setPreferredSize(new java.awt.Dimension(230, 52));
        SliderReadUpdate.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                SliderReadUpdateStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        jPanel6.add(SliderReadUpdate, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel5.add(jPanel6, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel13.add(jPanel5, gridBagConstraints);

        jPanel4.setLayout(new java.awt.GridBagLayout());

        ButtonRead.setText("Read");
        ButtonRead.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonReadActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel4.add(ButtonRead, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel13.add(jPanel4, gridBagConstraints);

        jPanel15.setLayout(new java.awt.GridBagLayout());

        java.awt.GridBagLayout jPanel7Layout = new java.awt.GridBagLayout();
        jPanel7Layout.columnWidths = new int[] {0};
        jPanel7Layout.rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0};
        jPanel7.setLayout(jPanel7Layout);

        ButtonResetQ1.setText("Reset QSFP 1");
        ButtonResetQ1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonResetQ1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel7.add(ButtonResetQ1, gridBagConstraints);

        ButtonResetQ2.setText("Reset QSFP 2");
        ButtonResetQ2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonResetQ2ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel7.add(ButtonResetQ2, gridBagConstraints);

        ButtonResetQ3.setText("Reset QSFP 3");
        ButtonResetQ3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonResetQ3ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        jPanel7.add(ButtonResetQ3, gridBagConstraints);

        ButtonResetQ4.setText("Reset QSFP 4");
        ButtonResetQ4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonResetQ4ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        jPanel7.add(ButtonResetQ4, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        jPanel15.add(jPanel7, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel13.add(jPanel15, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        jPanel22.add(jPanel13, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel1.add(jPanel22, gridBagConstraints);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(11, Short.MAX_VALUE))
        );
    }//GEN-END:initComponents

    /**
     * Starts an executor scheduling Runnable task through a
     * ScheduledThreadPoolExecutor. Only if connected and if the automatic
     * update checkbox is set. Otherwise do nothing.
     *
     * @param evt Input ActionEvent object.
     */
    private void CheckBoxReadUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckBoxReadUpdateActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            return;
        }

        if (CheckBoxReadUpdate.isSelected()) {
            Console.print_cr("PPrDemoLinkStatus -> read continuously each "
                    + SliderReadUpdate.getValue() + " seconds...");

            // Changes label Links Status in MainFrame.
            MainFrame.LabelLinksStatusMainFrame.setText("running");
            MainFrame.LabelLinksStatusMainFrame.setForeground(Color.GREEN.darker());

            // Creates and executes a periodic action that becomes enabled first
            // after the given initial delay, and subsequently with the given
            // period;
            scheduledFuture = executorScheduled.scheduleAtFixedRate(
                    RunnableUpdateScheduler,
                    0,
                    SliderReadUpdate.getValue(),
                    TimeUnit.SECONDS);
        } else {
            // Attempts to cancel execution of this task. This attempt will fail
            // if the task has already completed, has already been cancelled,
            // or could not be cancelled for some other reason.
            Boolean ret = scheduledFuture.cancel(true);

            Console.print_cr("PPrDemoLinkStatus -> scheduler cancel status: "
                    + (ret ? "Ok" : "Failed"));

            // Changes label Links Status in MainFrame.
            MainFrame.LabelLinksStatusMainFrame.setText("not running");
            MainFrame.LabelLinksStatusMainFrame.setForeground(Color.RED);
        }
    }//GEN-LAST:event_CheckBoxReadUpdateActionPerformed

    /**
     * Action to be performed on changing the Slider update time. The present
     * scheduled executor is cancelled and a new one is executed with the new
     * update time. Works only if automatic update check box is active and there
     * is an IPbus connection.
     *
     * @param evt Input ActionEvent object.
     */
    private void SliderReadUpdateStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_SliderReadUpdateStateChanged
        // Needed in order not to call this method several times
        // upon a change of a state of the slider
        JSlider source = (JSlider) evt.getSource();
        if (!source.getValueIsAdjusting()) {

            // Perform action only if connected and in the automatic
            // read update box is set. Otherwise do nothing.
            if ((CheckBoxReadUpdate.isSelected())
                    && IPbusPanel.getConnectFlag()) {

                // Attempts to cancel execution of this task. This attempt will fail
                // if the task has already completed, has already been cancelled,
                // or could not be cancelled for some other reason.
                Boolean ret = scheduledFuture.cancel(true);

                Console.print_cr("PPrDemoLinkStatus -> scheduler cancelled: "
                        + (ret ? "Ok." : "Failed.") + " New update time: "
                        + SliderReadUpdate.getValue() + " seconds..."
                );

                scheduledFuture = executorScheduled.scheduleAtFixedRate(
                        RunnableUpdateScheduler,
                        0,
                        SliderReadUpdate.getValue(),
                        TimeUnit.SECONDS);
            }
        }
    }//GEN-LAST:event_SliderReadUpdateStateChanged

    /**
     * Reads Link Status registers.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonReadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonReadActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinkStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnableReadLinkStatus);
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonReadActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonReadActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonReadActionPerformed

    /**
     * Sends a global reset to PPr QSFP transceiver 1.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonResetQ1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonResetQ1ActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksCtrl -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetQSFP.get(0));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonResetQ1ActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonResetQ1ActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonResetQ1ActionPerformed

    /**
     * Sends a global reset to PPr QSFP transceiver 2.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonResetQ2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonResetQ2ActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksCtrl -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetQSFP.get(1));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonResetQ2ActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonResetQ2ActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonResetQ2ActionPerformed

    /**
     * Sends a global reset to PPr QSFP transceiver 3.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonResetQ3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonResetQ3ActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksCtrl -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetQSFP.get(2));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonResetQ2ActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonResetQ2ActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonResetQ3ActionPerformed

    /**
     * Sends a global reset to PPr QSFP transceiver 4.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonResetQ4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonResetQ4ActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksCtrl -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetQSFP.get(3));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonResetQ2ActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonResetQ2ActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonResetQ4ActionPerformed

    /**
     * Resets QSFP 1 Link A0 TX.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonQ1A0TXResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonQ1A0TXResetActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetGBTLinks.get(0));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonQ1A0TXResetActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonQ1A0TXResetActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonQ1A0TXResetActionPerformed

    /**
     * Resets QSFP 1 Link A0 RX.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonQ1A0RXResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonQ1A0RXResetActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetGBTLinks.get(1));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonQ1A0RXResetActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonQ1A0RXResetActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonQ1A0RXResetActionPerformed

    /**
     * Resets QSFP 1 Link A1 TX.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonQ1A1TXResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonQ1A1TXResetActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetGBTLinks.get(2));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonQ1A1TXResetActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonQ1A1TXResetActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonQ1A1TXResetActionPerformed

    /**
     * Resets QSFP 1 Link A1 RX.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonQ1A1RXResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonQ1A1RXResetActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetGBTLinks.get(3));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonQ1A1RXResetActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonQ1A1RXResetActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonQ1A1RXResetActionPerformed

    /**
     * Resets QSFP 1 Link B0 TX.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonQ1B0TXResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonQ1B0TXResetActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetGBTLinks.get(4));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonQ1B0TXResetActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonQ1B0TXResetActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonQ1B0TXResetActionPerformed

    /**
     * Resets QSFP 1 Link B0 RX.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonQ1B0RXResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonQ1B0RXResetActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetGBTLinks.get(5));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonQ1B0RXResetActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonQ1B0RXResetActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonQ1B0RXResetActionPerformed

    /**
     * Resets QSFP 1 Link B1 TX.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonQ1B1TXResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonQ1B1TXResetActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetGBTLinks.get(6));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonQ1B1TXResetActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonQ1B1TXResetActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonQ1B1TXResetActionPerformed

    /**
     * Resets QSFP 1 Link B1 RX.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonQ1B1RXResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonQ1B1RXResetActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetGBTLinks.get(7));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonQ1B1RXResetActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonQ1B1RXResetActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonQ1B1RXResetActionPerformed

    /**
     * Resets DB FPGA Side A for MD 1.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonResetMD1DBAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonResetMD1DBAActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetDB.get(0));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonResetMD1DBAActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonResetMD1DBAActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonResetMD1DBAActionPerformed

    /**
     * Resets DB FPGA Side B for MD 1.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonResetMD1DBBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonResetMD1DBBActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetDB.get(1));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonResetMD1DBBActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonResetMD1DBBActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonResetMD1DBBActionPerformed

    /**
     * Resets QSFP 2 Link A0 TX.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonQ2A0TXResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonQ2A0TXResetActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetGBTLinks.get(8));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonQ2A0TXResetActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonQ2A0TXResetActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonQ2A0TXResetActionPerformed

    /**
     * Resets QSFP 2 Link A0 RX.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonQ2A0RXResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonQ2A0RXResetActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetGBTLinks.get(9));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonQ2A0RXResetActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonQ2A0RXResetActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonQ2A0RXResetActionPerformed

    /**
     * Resets QSFP 2 Link A1 TX.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonQ2A1TXResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonQ2A1TXResetActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetGBTLinks.get(10));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonQ2A1TXResetActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonQ2A1TXResetActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonQ2A1TXResetActionPerformed

    /**
     * Resets QSFP 2 Link A1 RX.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonQ2A1RXResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonQ2A1RXResetActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetGBTLinks.get(11));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonQ2A1RXResetActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonQ2A1RXResetActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonQ2A1RXResetActionPerformed

    /**
     * Resets QSFP 2 Link B0 TX.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonQ2B0TXResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonQ2B0TXResetActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetGBTLinks.get(12));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonQ2B0TXResetActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonQ2B0TXResetActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonQ2B0TXResetActionPerformed

    /**
     * Resets QSFP 2 Link B1 TX.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonQ2B1TXResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonQ2B1TXResetActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetGBTLinks.get(14));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonQ2B1TXResetActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonQ2B1TXResetActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonQ2B1TXResetActionPerformed

    /**
     * Resets DB FPGA Side A for MD 2.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonResetMD2DBAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonResetMD2DBAActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetDB.get(2));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonResetMD2DBAActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonResetMD2DBAActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonResetMD2DBAActionPerformed

    /**
     * Resets DB FPGA Side B for MD 2.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonResetMD2DBBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonResetMD2DBBActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetDB.get(3));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonResetMD2DBBActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonResetMD2DBBActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonResetMD2DBBActionPerformed

    /**
     * Resets DB FPGA Side A for MD 3.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonResetMD3DBAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonResetMD3DBAActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetDB.get(4));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonResetMD3DBAActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonResetMD3DBAActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonResetMD3DBAActionPerformed

    /**
     * Resets DB FPGA Side B for MD 3.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonResetMD3DBBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonResetMD3DBBActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetDB.get(5));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonResetMD3DBBActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonResetMD3DBBActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonResetMD3DBBActionPerformed

    /**
     * Resets DB FPGA Side A for MD 4.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonResetMD4DBAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonResetMD4DBAActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetDB.get(6));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonResetMD4DBAActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonResetMD4DBAActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonResetMD4DBAActionPerformed

    /**
     * Resets DB FPGA Side B for MD 4.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonResetMD4DBBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonResetMD4DBBActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetDB.get(7));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonResetMD4DBBActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonResetMD4DBBActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonResetMD4DBBActionPerformed

    /**
     * Resets QSFP 2 Link B1 RX.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonQ2B1RXResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonQ2B1RXResetActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetGBTLinks.get(15));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonQ2B1RXResetActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonQ2B1RXResetActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonQ2B1RXResetActionPerformed

    /**
     * Resets QSFP 2 Link B0 RX.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonQ2B0RXResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonQ2B0RXResetActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetGBTLinks.get(13));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonQ2B0RXResetActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonQ2B0RXResetActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonQ2B0RXResetActionPerformed

    /**
     * Resets QSFP 3 Link A0 TX.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonQ3A0TXResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonQ3A0TXResetActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetGBTLinks.get(16));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonQ3A0TXResetActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonQ3A0TXResetActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonQ3A0TXResetActionPerformed

    /**
     * Resets QSFP 3 Link A0 RX.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonQ3A0RXResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonQ3A0RXResetActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetGBTLinks.get(17));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonQ3A0TXResetActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonQ3A0TXResetActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonQ3A0RXResetActionPerformed

    /**
     * Resets QSFP 3 Link A1 RX.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonQ3A1RXResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonQ3A1RXResetActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetGBTLinks.get(19));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonQ3A0TXResetActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonQ3A0TXResetActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonQ3A1RXResetActionPerformed

    /**
     * Resets QSFP 3 Link B0 TX.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonQ3B0TXResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonQ3B0TXResetActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetGBTLinks.get(20));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonQ3A0TXResetActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonQ3A0TXResetActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonQ3B0TXResetActionPerformed

    /**
     * Resets QSFP 3 Link B0 RX.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonQ3B0RXResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonQ3B0RXResetActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetGBTLinks.get(21));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonQ3B0RXResetActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonQ3B0RXResetActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonQ3B0RXResetActionPerformed

    /**
     * Resets QSFP 3 Link B1 TX.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonQ3B1TXResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonQ3B1TXResetActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetGBTLinks.get(22));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonQ3B1TXResetActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonQ3B1TXResetActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonQ3B1TXResetActionPerformed

    /**
     * Resets QSFP 4 Link A0 TX.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonQ4A0TXResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonQ4A0TXResetActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetGBTLinks.get(24));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonQ3A0TXResetActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonQ3A0TXResetActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonQ4A0TXResetActionPerformed

    /**
     * Resets QSFP 4 Link A0 RX.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonQ4A0RXResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonQ4A0RXResetActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetGBTLinks.get(25));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonQ3A0TXResetActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonQ3A0TXResetActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonQ4A0RXResetActionPerformed

    /**
     * Resets QSFP 4 Link A1 TX.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonQ4A1TXResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonQ4A1TXResetActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetGBTLinks.get(26));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonQ4A1TXResetActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonQ4A1TXResetActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonQ4A1TXResetActionPerformed

    /**
     * Resets QSFP 4 Link A1 RX.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonQ4A1RXResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonQ4A1RXResetActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetGBTLinks.get(27));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonQ4A1RXResetActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonQ4A1RXResetActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonQ4A1RXResetActionPerformed

    /**
     * Resets QSFP 4 Link B0 TX.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonQ4B0TXResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonQ4B0TXResetActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetGBTLinks.get(28));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonQ4B0TXResetActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonQ4B0TXResetActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonQ4B0TXResetActionPerformed

    /**
     * Resets QSFP 4 Link B1 TX.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonQ4B1TXResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonQ4B1TXResetActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetGBTLinks.get(30));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonQ4B1TXResetActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonQ4B1TXResetActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonQ4B1TXResetActionPerformed

    /**
     * Resets QSFP 4 Link B1 RX.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonQ4B1RXResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonQ4B1RXResetActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetGBTLinks.get(31));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonQ3A0TXResetActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonQ3A0TXResetActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonQ4B1RXResetActionPerformed

    /**
     * Resets QSFP 3 Link A1 TX.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonQ3A1TXResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonQ3A1TXResetActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetGBTLinks.get(18));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonQ3A0TXResetActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonQ3A0TXResetActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonQ3A1TXResetActionPerformed

    /**
     * Resets QSFP 3 Link B1 RX.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonQ3B1RXResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonQ3B1RXResetActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetGBTLinks.get(23));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonQ3B1RXResetActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonQ3B1RXResetActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonQ3B1RXResetActionPerformed

    /**
     * Resets QSFP 4 Link B1 RX.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonQ4B0RXResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonQ4B0RXResetActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksStatus -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetGBTLinks.get(29));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonQ4B0RXResetActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonQ4B0RXResetActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonQ4B0RXResetActionPerformed

    /**
     * Declares a PPr class object to communicate with PPr IPbus server.
     */
    private PPrLib pprlib;

    /**
     * Declares a ThreadPoolExecutor object. It executes each submitted task
     * using one of possibly several pooled threads, normally configured using
     * Executors factory methods.
     */
    public static ThreadPoolExecutor executor;

    /**
     * Declares a ScheduledThreadPoolExecutor object.
     */
    public static ScheduledThreadPoolExecutor executorScheduled;

    /**
     * Declares Runnable for readLinkStatus() method.
     */
    private Runnable RunnableReadLinkStatus;

    /**
     * Declares Runnable ArrayList for QSFP Resets.
     */
    private final ArrayList<Runnable> RunnablesResetQSFP;

    /**
     * Declares Runnable ArrayList for MD DB Resets.
     */
    private final ArrayList<Runnable> RunnablesResetDB;

    /**
     * Declares Runnable ArrayList for QSFP Links TX/RX Resets.
     */
    private final ArrayList<Runnable> RunnablesResetGBTLinks;

    /**
     *
     */
    private Runnable RunnableUpdateScheduler;

    // Declares ScheduleFuture<V> interface (extends Future<V>). This is
    // a return from the ScheduledThreadPoolExecutor object method
    // scheduleAtFixedRate().
    private ScheduledFuture<?> scheduledFuture;

    /**
     * ArrayList with selected minidrawers to be filled with data.
     */
    private final ArrayList<Integer> selected_MD;

    private ArrayList<Integer> link_status_data;

    String[] link_status_info;

    private static javax.swing.JTextField[] TextFieldMD1A0;
    private static javax.swing.JTextField[] TextFieldMD1A1;
    private static javax.swing.JTextField[] TextFieldMD1B0;
    private static javax.swing.JTextField[] TextFieldMD1B1;

    private static javax.swing.JTextField[] TextFieldMD2A0;
    private static javax.swing.JTextField[] TextFieldMD2A1;
    private static javax.swing.JTextField[] TextFieldMD2B0;
    private static javax.swing.JTextField[] TextFieldMD2B1;

    private static javax.swing.JTextField[] TextFieldMD3A0;
    private static javax.swing.JTextField[] TextFieldMD3A1;
    private static javax.swing.JTextField[] TextFieldMD3B0;
    private static javax.swing.JTextField[] TextFieldMD3B1;

    private static javax.swing.JTextField[] TextFieldMD4A0;
    private static javax.swing.JTextField[] TextFieldMD4A1;
    private static javax.swing.JTextField[] TextFieldMD4B0;
    private static javax.swing.JTextField[] TextFieldMD4B1;

    private static javax.swing.JTextField[][][] TextFieldMD;

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ButtonQ1A0RXReset;
    private javax.swing.JButton ButtonQ1A0TXReset;
    private javax.swing.JButton ButtonQ1A1RXReset;
    private javax.swing.JButton ButtonQ1A1TXReset;
    private javax.swing.JButton ButtonQ1B0RXReset;
    private javax.swing.JButton ButtonQ1B0TXReset;
    private javax.swing.JButton ButtonQ1B1RXReset;
    private javax.swing.JButton ButtonQ1B1TXReset;
    private javax.swing.JButton ButtonQ2A0RXReset;
    private javax.swing.JButton ButtonQ2A0TXReset;
    private javax.swing.JButton ButtonQ2A1RXReset;
    private javax.swing.JButton ButtonQ2A1TXReset;
    private javax.swing.JButton ButtonQ2B0RXReset;
    private javax.swing.JButton ButtonQ2B0TXReset;
    private javax.swing.JButton ButtonQ2B1RXReset;
    private javax.swing.JButton ButtonQ2B1TXReset;
    private javax.swing.JButton ButtonQ3A0RXReset;
    private javax.swing.JButton ButtonQ3A0TXReset;
    private javax.swing.JButton ButtonQ3A1RXReset;
    private javax.swing.JButton ButtonQ3A1TXReset;
    private javax.swing.JButton ButtonQ3B0RXReset;
    private javax.swing.JButton ButtonQ3B0TXReset;
    private javax.swing.JButton ButtonQ3B1RXReset;
    private javax.swing.JButton ButtonQ3B1TXReset;
    private javax.swing.JButton ButtonQ4A0RXReset;
    private javax.swing.JButton ButtonQ4A0TXReset;
    private javax.swing.JButton ButtonQ4A1RXReset;
    private javax.swing.JButton ButtonQ4A1TXReset;
    private javax.swing.JButton ButtonQ4B0RXReset;
    private javax.swing.JButton ButtonQ4B0TXReset;
    private javax.swing.JButton ButtonQ4B1RXReset;
    private javax.swing.JButton ButtonQ4B1TXReset;
    private javax.swing.JButton ButtonRead;
    private javax.swing.JButton ButtonResetMD1DBA;
    private javax.swing.JButton ButtonResetMD1DBB;
    private javax.swing.JButton ButtonResetMD2DBA;
    private javax.swing.JButton ButtonResetMD2DBB;
    private javax.swing.JButton ButtonResetMD3DBA;
    private javax.swing.JButton ButtonResetMD3DBB;
    private javax.swing.JButton ButtonResetMD4DBA;
    private javax.swing.JButton ButtonResetMD4DBB;
    private javax.swing.JButton ButtonResetQ1;
    private javax.swing.JButton ButtonResetQ2;
    private javax.swing.JButton ButtonResetQ3;
    private javax.swing.JButton ButtonResetQ4;
    private javax.swing.JCheckBox CheckBoxMD1;
    private javax.swing.JCheckBox CheckBoxMD2;
    private javax.swing.JCheckBox CheckBoxMD3;
    private javax.swing.JCheckBox CheckBoxMD4;
    public static javax.swing.JCheckBox CheckBoxReadUpdate;
    private javax.swing.JSlider SliderReadUpdate;
    private javax.swing.JTextField TextFieldMD1A0s1;
    private javax.swing.JTextField TextFieldMD1A0s2;
    private javax.swing.JTextField TextFieldMD1A0s3;
    private javax.swing.JTextField TextFieldMD1A0s4;
    private javax.swing.JTextField TextFieldMD1A0s5;
    private javax.swing.JTextField TextFieldMD1A0s6;
    private javax.swing.JTextField TextFieldMD1A0s7;
    private javax.swing.JTextField TextFieldMD1A0s8;
    private javax.swing.JTextField TextFieldMD1A1s1;
    private javax.swing.JTextField TextFieldMD1A1s2;
    private javax.swing.JTextField TextFieldMD1A1s3;
    private javax.swing.JTextField TextFieldMD1A1s4;
    private javax.swing.JTextField TextFieldMD1A1s5;
    private javax.swing.JTextField TextFieldMD1A1s6;
    private javax.swing.JTextField TextFieldMD1A1s7;
    private javax.swing.JTextField TextFieldMD1A1s8;
    private javax.swing.JTextField TextFieldMD1B0s1;
    private javax.swing.JTextField TextFieldMD1B0s2;
    private javax.swing.JTextField TextFieldMD1B0s3;
    private javax.swing.JTextField TextFieldMD1B0s4;
    private javax.swing.JTextField TextFieldMD1B0s5;
    private javax.swing.JTextField TextFieldMD1B0s6;
    private javax.swing.JTextField TextFieldMD1B0s7;
    private javax.swing.JTextField TextFieldMD1B0s8;
    private javax.swing.JTextField TextFieldMD1B1s1;
    private javax.swing.JTextField TextFieldMD1B1s2;
    private javax.swing.JTextField TextFieldMD1B1s3;
    private javax.swing.JTextField TextFieldMD1B1s4;
    private javax.swing.JTextField TextFieldMD1B1s5;
    private javax.swing.JTextField TextFieldMD1B1s6;
    private javax.swing.JTextField TextFieldMD1B1s7;
    private javax.swing.JTextField TextFieldMD1B1s8;
    private javax.swing.JTextField TextFieldMD2A0s1;
    private javax.swing.JTextField TextFieldMD2A0s2;
    private javax.swing.JTextField TextFieldMD2A0s3;
    private javax.swing.JTextField TextFieldMD2A0s4;
    private javax.swing.JTextField TextFieldMD2A0s5;
    private javax.swing.JTextField TextFieldMD2A0s6;
    private javax.swing.JTextField TextFieldMD2A0s7;
    private javax.swing.JTextField TextFieldMD2A0s8;
    private javax.swing.JTextField TextFieldMD2A1s1;
    private javax.swing.JTextField TextFieldMD2A1s2;
    private javax.swing.JTextField TextFieldMD2A1s3;
    private javax.swing.JTextField TextFieldMD2A1s4;
    private javax.swing.JTextField TextFieldMD2A1s5;
    private javax.swing.JTextField TextFieldMD2A1s6;
    private javax.swing.JTextField TextFieldMD2A1s7;
    private javax.swing.JTextField TextFieldMD2A1s8;
    private javax.swing.JTextField TextFieldMD2B0s1;
    private javax.swing.JTextField TextFieldMD2B0s2;
    private javax.swing.JTextField TextFieldMD2B0s3;
    private javax.swing.JTextField TextFieldMD2B0s4;
    private javax.swing.JTextField TextFieldMD2B0s5;
    private javax.swing.JTextField TextFieldMD2B0s6;
    private javax.swing.JTextField TextFieldMD2B0s7;
    private javax.swing.JTextField TextFieldMD2B0s8;
    private javax.swing.JTextField TextFieldMD2B1s1;
    private javax.swing.JTextField TextFieldMD2B1s2;
    private javax.swing.JTextField TextFieldMD2B1s3;
    private javax.swing.JTextField TextFieldMD2B1s4;
    private javax.swing.JTextField TextFieldMD2B1s5;
    private javax.swing.JTextField TextFieldMD2B1s6;
    private javax.swing.JTextField TextFieldMD2B1s7;
    private javax.swing.JTextField TextFieldMD2B1s8;
    private javax.swing.JTextField TextFieldMD3A0s1;
    private javax.swing.JTextField TextFieldMD3A0s2;
    private javax.swing.JTextField TextFieldMD3A0s3;
    private javax.swing.JTextField TextFieldMD3A0s4;
    private javax.swing.JTextField TextFieldMD3A0s5;
    private javax.swing.JTextField TextFieldMD3A0s6;
    private javax.swing.JTextField TextFieldMD3A0s7;
    private javax.swing.JTextField TextFieldMD3A0s8;
    private javax.swing.JTextField TextFieldMD3A1s1;
    private javax.swing.JTextField TextFieldMD3A1s2;
    private javax.swing.JTextField TextFieldMD3A1s3;
    private javax.swing.JTextField TextFieldMD3A1s4;
    private javax.swing.JTextField TextFieldMD3A1s5;
    private javax.swing.JTextField TextFieldMD3A1s6;
    private javax.swing.JTextField TextFieldMD3A1s7;
    private javax.swing.JTextField TextFieldMD3A1s8;
    private javax.swing.JTextField TextFieldMD3B0s1;
    private javax.swing.JTextField TextFieldMD3B0s2;
    private javax.swing.JTextField TextFieldMD3B0s3;
    private javax.swing.JTextField TextFieldMD3B0s4;
    private javax.swing.JTextField TextFieldMD3B0s5;
    private javax.swing.JTextField TextFieldMD3B0s6;
    private javax.swing.JTextField TextFieldMD3B0s7;
    private javax.swing.JTextField TextFieldMD3B0s8;
    private javax.swing.JTextField TextFieldMD3B1s1;
    private javax.swing.JTextField TextFieldMD3B1s2;
    private javax.swing.JTextField TextFieldMD3B1s3;
    private javax.swing.JTextField TextFieldMD3B1s4;
    private javax.swing.JTextField TextFieldMD3B1s5;
    private javax.swing.JTextField TextFieldMD3B1s6;
    private javax.swing.JTextField TextFieldMD3B1s7;
    private javax.swing.JTextField TextFieldMD3B1s8;
    private javax.swing.JTextField TextFieldMD4A0s1;
    private javax.swing.JTextField TextFieldMD4A0s2;
    private javax.swing.JTextField TextFieldMD4A0s3;
    private javax.swing.JTextField TextFieldMD4A0s4;
    private javax.swing.JTextField TextFieldMD4A0s5;
    private javax.swing.JTextField TextFieldMD4A0s6;
    private javax.swing.JTextField TextFieldMD4A0s7;
    private javax.swing.JTextField TextFieldMD4A0s8;
    private javax.swing.JTextField TextFieldMD4A1s1;
    private javax.swing.JTextField TextFieldMD4A1s2;
    private javax.swing.JTextField TextFieldMD4A1s3;
    private javax.swing.JTextField TextFieldMD4A1s4;
    private javax.swing.JTextField TextFieldMD4A1s5;
    private javax.swing.JTextField TextFieldMD4A1s6;
    private javax.swing.JTextField TextFieldMD4A1s7;
    private javax.swing.JTextField TextFieldMD4A1s8;
    private javax.swing.JTextField TextFieldMD4B0s1;
    private javax.swing.JTextField TextFieldMD4B0s2;
    private javax.swing.JTextField TextFieldMD4B0s3;
    private javax.swing.JTextField TextFieldMD4B0s4;
    private javax.swing.JTextField TextFieldMD4B0s5;
    private javax.swing.JTextField TextFieldMD4B0s6;
    private javax.swing.JTextField TextFieldMD4B0s7;
    private javax.swing.JTextField TextFieldMD4B0s8;
    private javax.swing.JTextField TextFieldMD4B1s1;
    private javax.swing.JTextField TextFieldMD4B1s2;
    private javax.swing.JTextField TextFieldMD4B1s3;
    private javax.swing.JTextField TextFieldMD4B1s4;
    private javax.swing.JTextField TextFieldMD4B1s5;
    private javax.swing.JTextField TextFieldMD4B1s6;
    private javax.swing.JTextField TextFieldMD4B1s7;
    private javax.swing.JTextField TextFieldMD4B1s8;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel62;
    private javax.swing.JLabel jLabel63;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel34;
    private javax.swing.JPanel jPanel35;
    private javax.swing.JPanel jPanel36;
    private javax.swing.JPanel jPanel37;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    // End of variables declaration//GEN-END:variables
}
