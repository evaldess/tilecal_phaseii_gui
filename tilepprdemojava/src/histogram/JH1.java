/* 
 *  TestBench Project
 *  Cosmic Hodoscope to Test Silicon Photodiode Detectors
 *  2019 (c) IFIC-CERN
 */
package histogram;

import com.xeiam.xchart.Chart;
import com.xeiam.xchart.XChartPanel;
import com.xeiam.xchart.ChartBuilder;
import com.xeiam.xchart.StyleManager.ChartType;
import com.xeiam.xchart.StyleManager.ChartTheme;

import javax.swing.JPanel;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.awt.Color;
import java.awt.BorderLayout;
import static java.awt.Color.blue;

/**
 * JH1 extends JPanel to create instances of histograms. Taken from original
 * code of Prometeo software written by Carlos Solans (CERN)
 *
 * @author Juan Valls {@literal (valls@cern.ch)}
 * @author Carlos Solans {@literal (solans@cern.ch)}
 */
public class JH1 extends JPanel {

    /**
     * Declares ArrayList of JBin objects.
     */
    protected ArrayList<JBin> bins;

    protected JBin lastBin;
    protected Integer lastIdx;
    private AtomicInteger semaphore;

    /**
     * Declares instance of class Chart. Thisn is an XChart Chart.
     */
    protected Chart chart;

    /**
     * Declares object of class XChartPanel. This is a Swing JPanel that
     * contains a Chart.
     */
    protected XChartPanel panel;

    protected ArrayList<Double> x;
    protected ArrayList<Double> y;
    protected ArrayList<Double> e;

    /**
     *
     */
    protected JH1() {
        // Defines new AtomicInteger object.
        semaphore = new AtomicInteger(0);
    }

    /**
     * Constructor.
     *
     * @param stitle Input histogram title.
     * @param nbins Input number of bins.
     * @param xlo Input low range.
     * @param xhi Input high range.
     */
    public JH1(String stitle, Integer nbins, Integer xlo, Integer xhi) {
        bins = new ArrayList<>();
        x = new ArrayList<>();
        y = new ArrayList<>();
        e = new ArrayList<>();

        // Defines new AtomicInteger object.
        semaphore = new AtomicInteger(0);

        // Defines the Chart class object with the data.
        initJH1Histo(stitle, nbins, xlo, xhi);
    }

    /**
     * This method is made private as called within constructor and want to
     * avoid overridable methods called within constructor.
     *
     * @param stitle Input String with the title of the histogram (encoded).
     * @param nbins Input number of bins.
     * @param xlo Input minimum bin value.
     * @param xhi Input maximum bin value.
     */
    private void initJH1Histo(String stitle, Integer nbins, Integer xlo, Integer xhi) {
        // Sets JPanel layout.
        setLayout(new BorderLayout());

        // Defines Chart class object, an instance of class ChartBuilder.
        chart = new ChartBuilder()
                .chartType(ChartType.Bar)
                .theme(ChartTheme.Matlab)
                .width(400).height(300).build();

        // Manages all things related to styling of the Chart components.
        // Sets the chart legend visibility.
        chart.getStyleManager().setLegendVisible(false);

        // Sets the visibility of the gridlines on the plot area
        chart.getStyleManager().setPlotGridLinesVisible(true);

        // Set the plot area's grid lines color
        chart.getStyleManager().setPlotGridLinesColor(blue);

        // Sets the visibility of the ticks marks inside the plot area
        chart.getStyleManager().setAxisTicksMarksVisible(true);

        // Sets the visibility of the tick marks
        chart.getStyleManager().setAxisTicksVisible(true);

        // Fills ArrayList of JBin objects (bins).
        setBins(nbins, xlo, xhi);

        //chart.getStyleManager().setYAxisLogarithmic(true);
        chart.addSeries("data", x, y);

        // Sets title and labels of the histogram on the Chart class object.
        setTitle(stitle);

        // Defines instance of class XChartPanel.
        panel = new XChartPanel(chart);

        // Adds XChartPanel object (uses JPanel method).
        add(panel, BorderLayout.CENTER);
    }

    /**
     * Fills ArrayList of JBin objects (bins). Sets number of bins and the low
     * and high x range for each bin.
     *
     * @param nbins Input number of bins.
     * @param xlo Input minimum bin value.
     * @param xhi Input maximum bin value.
     */
    public void setBins(Integer nbins, Integer xlo, Integer xhi) {
        bins.clear();
        x.clear();
        y.clear();
        e.clear();

        // Fills ArrayList bins of JBin fields.
        bins.add(new JBin(Double.MIN_VALUE, xlo));
        double pos = (double) xlo;
        double stp = (double) (xhi - xlo) / nbins;

        for (Integer i = 0; i < nbins; i++) {
            bins.add(new JBin(pos, pos + stp));
            pos = pos + stp;
        }

        bins.add(new JBin(xhi, Double.MAX_VALUE));
        lastIdx = 1;
        lastBin = bins.get(lastIdx);

        // Fills ArrayLists with x and y values.
        for (Integer i = 1; i < bins.size() - 1; i++) {
            JBin bin = bins.get(i);
            x.add(bin.getX());
            y.add(bin.getY());
            e.add(bin.getStdErr());
        }
    }

    /**
     * Sets title and X and Y labels of the histogram on the Chart class object.
     *
     * @param stitle Input String title.
     */
    public void setTitle(String stitle) {
        String[] t = stitle.replace(";", ":").split(":");

        String title = t[0];

        // Sets x-label if available.
        String xlabel = "";
        if (t.length > 1) {
            xlabel = t[1];
        }

        String ylabel = "";
        if (t.length > 2) {
            ylabel = t[2];
        }

        chart.setChartTitle(title);
        chart.setXAxisTitle(xlabel);
        chart.setYAxisTitle(ylabel);
    }

    /**
     *
     * @param value
     * @return
     */
    public JBin findBin(Number value) {
        while (semaphore.get() != 0) {
        }

        semaphore.set(1);

        double dval = value.doubleValue();
        //Option 1. Dummy loop
        /*
	  for(Integer i=0;i<bins.size();i++){
	    if(pos>=bins.get(i).getLow()&&pos<bins.get(i).getHigh()){
		return bins.get(i);
	    }
	} 
         */
        //Option 2. upwards/downwards iterator
        //Find out if is above or below lastBin
        //Iterators
        /*if(dval>=lastBin.current().getLow()&&dval<lastBin.current().getHigh()){
	    //Is the same bin
	    return lastBin.current().getValue();
	}else if(dval<lastBin.getValue().getLow()){
	    //iterate downwards
	    while(lastBin.hasPrevious()){
		lastBin.previous();
		if(dval>=lastBin.getValue().getLow()&&dval<lastBin.getValue().getHigh()){
		    return lastBin.getValue();
		}
	    }
	}else{
	    //Iterate upwards
	    while(lastBin.hastNext()){
		lastBin.next();
		if(dval>=lastBin.getValue().getLow()&&dval<lastBin.getValue().getHigh()){
		    return lastBin.getValue();
		}
	    }
	}	
         */
 /*
	//Pointers
	if(dval>=lastBin.getLow()&&dval<lastBin.getHigh()){
	    //Is the same bin
	}else if(dval<lastBin.getLow()){
	    //iterate downwards
	    for(Integer i=lastIdx;i>=0;i--){
		lastBin=bins.get(i);
		if(dval>=lastBin.getLow()&&dval<lastBin.getHigh()){
		    lastIdx=i;
		    break;
		}
	    }
	}else{
	    //Iterate upwards
	    for(Integer i=lastIdx;i<bins.size();i++){
		lastBin=bins.get(i);
		if(dval>=lastBin.getLow()&&dval<lastBin.getHigh()){
		    lastIdx=i;
		    break;
		}
	    }
	}
         */
        //Option 3. Goes directly to the bin.
        JBin b = bins.get(1);
        Integer pos = (int) Math.floor((dval - b.getLow()) / (b.getHigh() - b.getLow()));
        if (pos < 1) {
            lastBin = bins.get(0);
        } else if (pos > bins.size() - 1) {
            lastBin = bins.get(bins.size() - 1);
        } else {
            lastBin = bins.get(pos);
        }

        // Removes the semaphore and return.
        semaphore.set(0);

        return lastBin;
    }

    /**
     * Puts an ArrayList of Integers (data) into the bins of a histogram and
     * updates it.
     *
     * @param data
     * @return
     */
    public Boolean putVector(ArrayList<Integer> data) {
        // Check number of bins are equal to ArrayList size
        if (!(data.size() == (bins.size() - 2))) {
            return false;
        }

        // Loops over bins and dump data vector on them
        for (Integer i = 1; i < bins.size() - 1; i++) {
            y.set(i - 1, data.get(i - 1).doubleValue());
        }

        // Update panel with the series
        //panel.updateSeries("data", x, y);
        return true;
    }

    /**
     * Fills with no weight the histogram value.
     *
     * @param value Input value with data to be filled.
     */
    public void fill(Number value) {
        fill(value, 1.);
    }

    /**
     * Fills with a weight the histogram value.
     *
     * @param value Input value with data to be filled.
     * @param weight Input histogram weight.
     */
    public void fill(Number value, Number weight) {
        fill(value, weight, false);
    }

    /**
     *
     * @param value Input value with data to be filled.
     * @param weight Input histogram weight.
     * @param notify
     */
    public void fill(Number value, Number weight, Boolean notify) {
        JBin bin = findBin(value);

        bin.count(weight);

        if (notify) {
            update();
        }
    }

    /**
     *
     */
    public void draw() {
        update();
    }

    /**
     *
     */
    public void update() {
        if (bins.size() < 1) {
            return;
        }

        for (Integer i = 1; i < bins.size() - 1; i++) {
            JBin bin = bins.get(i);
            y.set(i - 1, bin.getY());
        }

        panel.updateSeries("data", x, y);
    }

    /**
     * Returns a JBin object corresponding to the input bin.
     *
     * @param i Input bin (must be above 1 and below total number of bins).
     * @return the JBin object for the input bin.
     */
    public JBin getBin(Integer i) {
        if (i < 1 || i >= bins.size()) {
            return null;
        }
        JBin bin = bins.get(i);
        return bin;
    }

    /**
     * Returns for each input bin its x dimensions in a Double ArrayList: low,
     * middle and end values.
     *
     * @param i Input bin (must be above 1 and below total number of bins).
     * @return the Double ArrayList with bin dimensions for the input bin.
     */
    public ArrayList<Double> getBinXvalue(Integer i) {
        ArrayList<Double> values = new ArrayList<>();

        if (i < 1 || i >= bins.size()) {
            return null;
        }

        JBin bin = bins.get(i);

        values.add(bin.getX());
        values.add(bin.getLow());
        values.add(bin.getHigh());

        return values;
    }

    /**
     *
     */
    public void reset() {
        for (Integer i = 0; i < bins.size(); i++) {
            JBin bin = bins.get(i);
            bin.clear();
        }
        update();
    }

    /**
     *
     * @param color
     */
    public void setLineColor(Color color) {
    }

    public Double getMean() {
        JBin bin;
        Double mean, num = 0.0d, den = 0.0d;

        if (bins.size() - 1 < 0) {
            return 0.0d;
        }

        for (Integer i = 1; i < bins.size() - 1; i++) {
            bin = bins.get(i);
            num = num + bin.getY() * bin.getX();
            den = den + bin.getY();
        }

        mean = num / den;

        if (mean.isNaN()) {
            return 0d;
        }

        return mean;
    }

    public Double getRMS() {
        JBin bin;
        Double mean, rms, num = 0.0d, den = 0.0d;

        if (bins.size() - 1 < 0) {
            return 0.0d;
        }

        mean = getMean();

        for (Integer i = 1; i < bins.size() - 1; i++) {
            bin = bins.get(i);
            num = num + bin.getY() * Math.pow((bin.getX() - mean), 2);
            den = den + bin.getY();
        }

        rms = Math.sqrt(num / den);

        if (rms.isNaN()) {
            return 0d;
        }

        return rms;
    }
}
