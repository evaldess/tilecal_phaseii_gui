/*
 * IPbusJavaLibrary project
 * 2019 (c) IFIC-CERN
 */
package ipbusjavalibrary.ipbus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * A class to handle the IPbus protocol. Its constructor creates a
 * DatagramSocket for IPbus communication with a host (PPr device or VME).
 * Contains write/read methods to send CONTROL transaction request packets and
 * handle the response packets.
 *
 * @author Juan Valls {@literal (valls@cern.ch)}
 * @author Carlos Solans {@literal (solans@cern.ch)}
 */
public class Uhal {

    /**
     * Defines a Transport object.
     */
    private Transport client;

    private Integer m_pkid;

    /**
     * Status boolean flag from the Uhal instance object constructor.
     */
    private Boolean retUhal = false;

    /**
     * CONTROL transaction IPbus request packet used in write/read transactions.
     */
    private Transaction requestControlPacket;

    /**
     * Generic IPbus Packet object return from send transactions. To be cast
     * into STATUS or CONTROL IPbus response packets.
     */
    private Packet dummyPacket;

    /**
     * STATUS IPbus request packet used in sync transactions.
     */
    private Status requestStatusPacket;

    /**
     * STATUS IPbus response packet used in sync transactions.
     */
    private Status responseStatusPacket;

    /**
     * RESEND IPbus packet.
     */
    private Resend resendPacket;

    /**
     * IPbus CONTROL transaction response packet used for write/read
     * transactions.
     */
    private Transaction responsePacket;

    private char[] charArray = new char[50];
    String ndl1;

    private static final String RELEASE_NB = "1.0";

    /**
     *
     * @param connstr Connection string of the form:
     * protocol://device:port?target=device1:port1.
     * @param threadFactoryName Input name for the ThrteadPoolExecutor thread.
     */
    public Uhal(String connstr, String threadFactoryName) {
        Integer port = 0, port2 = 0;

        if (Debug.getDebugIPbus()) {
            System.out.println("Uhal() -> creating a client socket: " + connstr);
        }

        Arrays.fill(charArray, '-');
        ndl1 = String.valueOf(charArray);

        m_pkid = 0;

        requestControlPacket = new Transaction();
        requestStatusPacket = new Status();

        String proto = "udp";
        Integer pos1 = connstr.indexOf("://");
        if (pos1 != -1) {
            if (connstr.substring(0, pos1).contains("tcp")) {
                proto = "tcp";
            }
            pos1 += 3;
        } else {
            pos1 = 0;
        }

        Integer pos2 = connstr.indexOf(":", pos1);
        String host = connstr.substring(pos1, pos2);

        pos1 = pos2;
        pos2 = connstr.indexOf("?", pos1);
        if (pos2 != -1) {
            port = Integer.valueOf(connstr.substring(pos1 + 1, pos2));
        }

        pos1 = pos2;
        pos2 = connstr.indexOf("target=", pos1);
        if (pos2 == -1) {
            System.out.println("Connection string error");
            return;
        }

        pos1 = pos2 + 7;
        pos2 = connstr.indexOf(":", pos1);
        String host2 = connstr.substring(pos1, pos2);

        if (pos2 != -1) {
            port2 = Integer.valueOf(connstr.substring(pos2 + 1));
        }

        if (Debug.getDebugIPbus()) {
            System.out.println("Uhal() -> parsed input string: host = "
                    + host + "  port = " + port
                    + "  host2 = " + host2 + "  port2 = " + port2);
        }

        if (proto.equals("udp")) {
            // Creates Java UDP DatagramSocket.
            client = new TransportUDP(host, port, threadFactoryName);
            retUhal = client.getTransportReturn();
            if (Debug.getDebugIPbus()) {
                System.out.println("Uhal() -> UDP connection status: "
                        + retUhal);
            }

        } else if (proto.equals("tcp")) {
            // Creates Java TCP Socket.
            client = new TransportTCP(host, port, host2, port2, threadFactoryName);
            retUhal = client.getTransportReturn();
            if (Debug.getDebugIPbus()) {
                System.out.println("Uhal() -> TCP connection status: "
                        + retUhal);
            }
        }

        if (retUhal) {
            retUhal = client.setTimeout(5000);
        }
    }

    /**
     * Getter with the Uhal version release.
     *
     * @return the Uhal version release.
     */
    public static String getUhalRelease() {
        return RELEASE_NB;
    }

    /**
     * Getter with the boolean flag from the Uhal instance object constructor.
     *
     * @return a Boolean with the status of Uhal constructor.
     */
    public Boolean getUhalReturn() {
        return retUhal;
    }

    /**
     *
     * @return the Transport thread pool executor.
     */
    public ThreadPoolExecutor getUhalExecutor() {
        return client.getTransportExecutor();
    }

    /**
     * Synchronizes the expected Packet ID with remote host device. Sends an
     * IPbus STATUS request packet to the server. Waits for a response packet.
     * Sets the server next expected Packet ID into the client Packet ID.
     *
     * @return the synchronize status flag.
     */
    public Boolean sync() {
        if (Debug.getDebugIPbus()) {
            String ndl2 = String.format("Uhal.sync() -> synchronization started with the server...");
            System.out.println("\n" + ndl1 + "\n" + ndl2);
        }

        // Creates a STATUS IPbus request packet and sends it 
        // through a client socket. Returns a Packet as response.
        dummyPacket = send(requestStatusPacket);

        // Protects against a possible ClassCastException.
        if (dummyPacket instanceof Status) {
            responseStatusPacket = (Status) dummyPacket;
        } else {
            if (Debug.getDebugIPbus()) {
                System.out.println("Uhal.sync() -> ClassCastException on send STATUS");
            }
            responseStatusPacket = null;
        }

        if (responseStatusPacket == null) {
            if (Debug.getDebugIPbus()) {
                System.out.println("Uhal.sync() -> return null STATUS packet");
            }
            return false;
        } else {
            // Takes the value of the next expected IPbus packet header from the
            // remote host (the STATUS response packet) and sets it.
            if (Debug.getDebugIPbus()) {
                System.out.println("Uhal.sync() -> Sets next expected PacketID from STATUS response packet.");
            }
            m_pkid = responseStatusPacket.getNextExpectedPacketID();
            Packet.setNextPacketID(m_pkid);
            return true;
        }
    }

    /**
     *
     * @param req Input
     * @return the
     */
    public Transaction reSync(Transaction req) {
        if (Debug.getDebugIPbus()) {
            System.out.println("Uhal.reSync() -> trying to send a STATUS packet...");
        }

        // Creates a STATUS IPbus request packet and sends it 
        // through a client socket. Returns a Packet as response.
        dummyPacket = send(requestStatusPacket);

        System.out.println("Uhal.reSync() -> STATUS packet sent.");

        // Protects against a possible ClassCastException.
        if (dummyPacket instanceof Status) {
            responseStatusPacket = (Status) dummyPacket;
        } else {
            if (Debug.getDebugIPbus()) {
                System.out.println("Uhal.reSync() -> ClassCastException on send STATUS");
            }
            responseStatusPacket = null;
        }

        if (responseStatusPacket == null) {
            if (Debug.getDebugIPbus()) {
                System.out.println("Uhal.reSync() -> return null STATUS packet");
            }
            return null;
        } else {
            System.out.println("Uhal.reSync() -> STATUS packet is not null");
            if (responseStatusPacket.getNextExpectedPacketID() > req.getPacketID()
                    + responseStatusPacket.getBufferSize()) {
                System.out.println("Uhal.reSync() -> status PacketID > ... return null");
                return null;
            }

            System.out.println("Uhal.reSync() -> request PacketID: " + req.getPacketID());
            resendPacket = new Resend();
            resendPacket.setPacketID(req.getPacketID());
            System.out.println("Uhal.reSync() -> resendPacket PacketID set to " + req.getPacketID());

            dummyPacket = send(resendPacket);
            if (dummyPacket instanceof Transaction) {
                responsePacket = (Transaction) dummyPacket;
            } else {
                if (Debug.getDebugIPbus()) {
                    System.out.println("Uhal.reSync() -> ClassCastException on send RESEND");
                }
                responsePacket = null;
            }
            return responsePacket;
        }
    }

    /**
     * Sends an input request IPbus Packet object and returns an IPbus response
     * Packet. Constructs a Java DatagramPacket from the input IPbus request
     * Packet object. Then sends it through a client DatagramSocket. Waits for
     * response and constructs an IPbus response Packet on return.
     *
     * @param inputPacket Input IPbus request Packet.
     * @return IPbus response Packet.
     */
    public Packet send(Packet inputPacket) {
        if (Debug.getDebugIPbus()) {
            System.out.println("Uhal.send() -> send packet (set Packet ID = "
                    + m_pkid + ")");
        }

        // Sets the input request PacketID
        inputPacket.setPacketID(m_pkid);

        Packet ret = client.send(inputPacket);

        // Increments next expected PacketID counter.
        if (Objects.equals(inputPacket.getType(), PacketHeader.CONTROL) && ret != null) {
            m_pkid++;
            if (m_pkid > 0xFFFF) {
                m_pkid = 0;
            }
        }

        return ret;
    }

    /**
     * Writes an address with a single data word. Sends a CONTROL WRITE request
     * IPbus transaction packet with a single data word. A request CONTROL WRITE
     * transaction packet is first constructed and then sent through a
     * DatagramSocket. A response CONTROL WRITE packet is then received. Returns
     * a boolean with write status.
     *
     * @param address Input packet BASE_ADDRESS word.
     * @param value Input data to be written in packet.
     * @return a boolean flag with the transaction status.
     */
    public Boolean write(Integer address, Integer value) {
        if (Debug.getDebugIPbus()) {
            String ndl2 = String.format("Uhal.write() -> write at address: 0x%08X  Single value: 0x%08X",
                    address, value);
            System.out.println("\n" + ndl1 + "\n" + ndl2);
        }

        // Sets input data for the CONTROL write transaction IPbus request packet. 
        // Input data is given by the BASE_ADDRESS and a value to be written.
        requestControlPacket.setWrite(address, value);

        // Sends the request packet through the client DatagramSocket.
        // Receives a CONTROL WRITE response packet.
        // Protect against a possible ClassCastException.          
        dummyPacket = send(requestControlPacket);
        if (dummyPacket instanceof Transaction) {
            responsePacket = (Transaction) dummyPacket;
        } else {
            if (Debug.getDebugIPbus()) {
                System.out.println("Uhal.write() -> ClassCastException on send TRANSACTION...");
            }
            responsePacket = null;
            //sync();
        }
        return responsePacket != null;
    }

    /**
     * Writes an address with several data words. Sends a CONTROL WRITE request
     * IPbus transaction packet with several data words. Address value is
     * incremented by a unit for succesive data words. The packet is first
     * constructed and then sent through a DatagramSocket. A response CONTROL
     * WRITE packet is then received. Returns a boolean with write status.
     *
     * @param address Input packet BASE_ADDRESS word.
     * @param values Input data to be written in packet.
     * @return true If response packet is non-NULL.
     */
    public Boolean write(Integer address, ArrayList<Integer> values) {
        if (Debug.getDebugIPbus()) {
            System.out.println(String.format("Uhal.write(): write at address: 0x%08X ArrayList Integer values",
                    address));
        }

        // Sets input data for the CONTROL write transaction IPbus request packet. 
        // Input data is given by the BASE_ADDRESS and a value to be written.
        requestControlPacket.setWrite(address, values);

        // Sends the request packet through the client DatagramSocket.
        // Receives a CONTROL WRITE response packet.
        // Protect against a possible ClassCastException.          
        dummyPacket = send(requestControlPacket);
        if (dummyPacket instanceof Transaction) {
            responsePacket = (Transaction) dummyPacket;
        } else {
            if (Debug.getDebugIPbus()) {
                System.out.println("Uhal.write() -> ClassCastException on send TRANSACTION... while write ArrayList");
            }
            responsePacket = null;
            sync();
        }
        return responsePacket != null;
    }

    /**
     * Writes an address with several data words. Sends a CONTROL
     * WRITE/WRITEFIFO request IPbus transaction packet with several data words.
     * Address value is incremented by a unit for succesive data words. The
     * packet is first constructed and then sent through a DatagramSocket. A
     * response CONTROL WRITE/WRITEFIFO packet is then received. Returns a
     * boolean with write status.
     *
     * @param address Input packet BASE_ADDRESS word.
     * @param values Input data to be written in packet.
     * @param fifo True for non-incremented WRITE (WRITEFIFO). False for WRITE.
     * @return true If response packet is non-NULL.
     */
    public Boolean write(Integer address, ArrayList<Integer> values, Boolean fifo) {
        if (Debug.getDebugIPbus()) {
            System.out.println(String.format("Uhal.write(): write at address: 0x%08X ArrayList Integer values with fifo",
                    address));
        }

        // Sets input data for the CONTROL write transaction IPbus request packet. 
        // Input data is given by the BASE_ADDRESS and a value to be written.
        requestControlPacket.setWrite(address, values, fifo);

        // Sends the request packet through the client DatagramSocket.
        // Receives a CONTROL WRITE/WRITEFIFO response packet.
        // Protect against a possible ClassCastException.          
        dummyPacket = send(requestControlPacket);
        if (dummyPacket instanceof Transaction) {
            responsePacket = (Transaction) dummyPacket;
        } else {
            if (Debug.getDebugIPbus()) {
                System.out.println("Uhal.write() -> ClassCastException on send TRANSACTION... while write ArrayList with fifo");
            }
            responsePacket = null;
            sync();
        }
        return responsePacket != null;
    }

    /**
     * Reads a single data word from an address. Sends a CONTROL READ request
     * IPbus transaction packet from an input BASE_ADDRESS. A request CONTROL
     * READ transaction packet is first constructed and then sent through a
     * DatagramSocket. A response CONTROL READ packet is then received. Returns
     * data stored in the response packet.
     *
     * @param address Input packet BASE_ADDRESS word.
     * @return Data stored in the response packet (Integer). -1 if no data.
     */
    public Integer read(Integer address) {
        if (Debug.getDebugIPbus()) {
            String ndl2 = String.format("Uhal.read(): read from address: 0x%08X",
                    address);
            System.out.println("\n" + ndl1 + "\n" + ndl2);
        }

        // Sets the CONTROL read transaction IPbus request packet. 
        requestControlPacket.setRead(address, 1);

        // Sends the request packet through the client DatagramSocket.
        // Receives a CONTROL READ response packet.
        // Protects against a possible ClassCastException.      
        dummyPacket = send(requestControlPacket);
        if (dummyPacket instanceof Transaction) {
            responsePacket = (Transaction) dummyPacket;
        } else {
            if (Debug.getDebugIPbus()) {
                System.out.println("Uhal.read() -> ClassCastException on send TRANSACTION... while read Integer");
            }
            responsePacket = null;
            sync();
            return null;
        }

        if (responsePacket == null) {
            System.out.println("Uhal.read() -> single read failed. Calling Resync");
            responsePacket = reSync(requestControlPacket);
        }

        if (responsePacket == null) {
            return null;
        }

        if (responsePacket.getData().size() > 0) {
            return responsePacket.getData().get(0);
        }

        return null;
    }

    /**
     * Reads several data words from an address. Sends a CONTROL READ request
     * IPbus transaction packet from an input BASE_ADDRESS. A request CONTROL
     * READ transaction packet is first constructed and then sent through a
     * socket. A response CONTROL READ packet is then received. Returns data
     * stored in the response packet.
     *
     * @param address Input packet BASE_ADDRESS word.
     * @param size Input packet data size.
     * @return Data stored in the response packet (ArrayList). Null on failure.
     */
    public ArrayList<Integer> read(Integer address, Integer size) {
        return read(address, size, false);
    }

    /**
     * Reads several data words from an address. Sends a CONTROL READ/READFIFO
     * request IPbus transaction packet from an input BASE_ADDRESS. A request
     * CONTROL READ/READFIFO transaction packet is first constructed and then
     * sent through a DatagramSocket. A response CONTROL READ/READFIFO packet is
     * then received. Returns data stored in the response packet.
     *
     * @param address Input packet BASE_ADDRESS word.
     * @param size Input packet data size.
     * @param fifo True for non-incrementd READ (READFIFO). False for READ.
     * @return Data stored in the response packet (ArrayList). Null on failure.
     */
    public ArrayList<Integer> read(
            Integer address,
            Integer size,
            Boolean fifo) {

        if (Debug.getDebugIPbus()) {
            String ndl2 = String.format("Uhal.read(): read from address: 0x%08X",
                    address);
            System.out.println("\n" + ndl1 + "\n" + ndl2);
        }

        // Sets the CONTROL read transaction IPbus request packet. 
        requestControlPacket.setRead(address, size, fifo);

        // Sends the request packet through the client DatagramSocket.
        // Receives a CONTROL READ/READFIFO response packet.
        // Protect against a possible ClassCastException.        
        dummyPacket = send(requestControlPacket);
        if (dummyPacket instanceof Transaction) {
            responsePacket = (Transaction) dummyPacket;
        } else {
            if (Debug.getDebugIPbus()) {
                System.out.println("Uhal.read() -> ClassCastException on send TRANSACTION... while read ArrayList");
            }
            responsePacket = null;
            sync();
            return null;
        }

        if (responsePacket == null) {
            System.out.println("Uhal.read() -> ArrayList read failed. Calling reSync");
            responsePacket = reSync(requestControlPacket);
        }

        if (responsePacket == null) {
            System.out.println("Uhal.read() -> ArrayList read failed again. return null Packet");
            return null;
        }

        if (responsePacket.getData().size() > 0) {
            return responsePacket.getData();
        }
        return null;
    }

    /**
     *
     * @return the close connection return Boolean status.
     */
    public Boolean closeConnection() {
        return client.closeConnection();
    }
}
