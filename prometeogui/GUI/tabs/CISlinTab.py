#
# Sergio Landete
# Fernando Carrio Argos
# University of Valencia
# fernando.carrio@cern.ch
#
import Tkinter as tk
from matplotlib import style
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import numpy as np
from scipy import stats
import sys
sys.path.append('./scripts')
from CISlinearity import CISlinearity
sys.path.append('./common')
from UpdatePlot import UpdatePlot 

class CISlinTab(tk.Frame): # CIS LINEARITY
    def __init__(self, parent, controller):
        tk.Frame.__init__(self,parent)
        self.configure(background="snow3")
        botonFrame=tk.Frame(self)
        botonFrame.configure(background="snow3")
        botonFrame.pack(side=tk.TOP)
        self.nsamp=20
        self.md=tk.IntVar()
        self.stateHGLG=tk.IntVar()

        buttonMD1 = tk.Radiobutton(botonFrame, text="MD1", indicatoron = 0, width = 2, padx = 10, variable=self.md, command=lambda:self.animate(), value=0)
        buttonMD2 = tk.Radiobutton(botonFrame, text="MD2", indicatoron = 0, width = 2, padx = 10, variable=self.md, command=lambda:self.animate(), value=1)
        buttonMD3 = tk.Radiobutton(botonFrame, text="MD3", indicatoron = 0, width = 2, padx = 10, variable=self.md, command=lambda:self.animate(), value=2)
        buttonMD4 = tk.Radiobutton(botonFrame, text="MD4", indicatoron = 0, width = 2, padx = 10, variable=self.md, command=lambda:self.animate(), value=3)
        buttonMD1.pack(padx=20, pady=10, ipadx=50, side=tk.LEFT)
        buttonMD2.pack(padx=20, pady=10, ipadx=50, side=tk.LEFT)
        buttonMD3.pack(padx=20, pady=10, ipadx=50, side=tk.LEFT)
        buttonMD4.pack(padx=20, pady=10, ipadx=50, side=tk.LEFT)

        buttonHG = tk.Radiobutton(botonFrame, text="HG", indicatoron = 0, width = 2, padx = 10, variable=self.stateHGLG, command=lambda:self.animate(), value=1)
        buttonLG = tk.Radiobutton(botonFrame, text="LG", indicatoron = 0, width = 2, padx = 10, variable=self.stateHGLG, command=lambda:self.animate(), value=0)
        buttonHG.config(activebackground='dark green', bg='dark green', fg='black')
        buttonLG.config(activebackground='dark red', bg='dark red', fg='black')
        buttonHG.pack(padx=5, pady=10, ipadx=10, side=tk.LEFT)
        buttonLG.pack(padx=5, pady=10, ipadx=10, side=tk.LEFT)

        buttonRun=tk.Button(botonFrame, text="Run", command=lambda:self.runtest())
        buttonRun.pack(padx=20, pady=10, ipadx=20, side=tk.LEFT)                

        style.use("ggplot")
        self.figure=Figure(figsize=(5,5), dpi=100)

        self.samplesLG1 = np.zeros((4,12,self.nsamp))
        self.samplesHG1 = np.zeros((4,12,self.nsamp))
        self.samplesLG2 = np.zeros((4,12,self.nsamp))
        self.samplesHG2 = np.zeros((4,12,self.nsamp)) 
        self.DAC_values = np.zeros((self.nsamp)) 

        self.ax1 = 12*[0]
        xaxes= [0, 4095]                                
        yaxes= [0, 4095]
        for i in range (12):
                self.ax1[i] = self.figure.add_subplot(2, 6, i+1)
                self.ax1[i].xaxis.set_ticks_position("bottom")

                self.ax1[i].set_xlim(xaxes)
                self.ax1[i].set_ylim(yaxes)
                self.ax1[i].xaxis.set_ticks_position("bottom")
                self.ax1[i].set_xlabel('Q injected',fontsize=10)                              
                self.ax1[i].xaxis.label.set_color('black')

                for label in self.ax1[i].xaxis.get_ticklabels():
                        label.set_fontsize(8)
                for label in self.ax1[i].yaxis.get_ticklabels():
                        label.set_fontsize(8)
        self.ax1[0].set_ylabel('Q Reconstructed',fontsize=10)                               
        self.ax1[0].yaxis.label.set_color('black')      
        self.ax1[6].set_ylabel('Q Reconstructed',fontsize=10)                               
        self.ax1[6].yaxis.label.set_color('black')      
        
        
        self.figure.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.4, hspace=0.4)
        self.canvas=FigureCanvasTkAgg(self.figure, self)
        self.canvas.show()
        self.canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH ,expand=True)
        self.canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=True)
        self.animate()
    def runtest(self):
        self.DAC_values, self.samplesLG1, self.samplesHG1=CISlinearity(self.stateHGLG.get(),self.nsamp)
        self.animate()
        
    def animate(self):
        md = self.md.get()
        xaxes= [0, 4095]                                
        yaxes= [0, 4095]

        for i in range (12):
            self.ax1[i].clear()
            self.ax1[i].set_xlim(xaxes)
            self.ax1[i].set_ylim(yaxes)
            self.ax1[i].xaxis.set_ticks_position("bottom")
            self.ax1[i].set_xlabel('Q injected',fontsize=10)                              
            self.ax1[i].xaxis.label.set_color('black')
            for label in self.ax1[i].xaxis.get_ticklabels():
                label.set_fontsize(8)
            for label in self.ax1[i].yaxis.get_ticklabels():
                label.set_fontsize(8)
        self.ax1[0].set_ylabel('Q Reconstructed',fontsize=10)                               
        self.ax1[0].yaxis.label.set_color('black')      
        self.ax1[6].set_ylabel('Q Reconstructed',fontsize=10)                               
        self.ax1[6].yaxis.label.set_color('black')      
        
        if self.stateHGLG.get() == 1:
                samplesaux1 = self.samplesHG1[md]
                samplesaux2 = self.DAC_values
                color = 'g'
        else:
                samplesaux1 = self.samplesLG1[md]
                samplesaux2 = self.DAC_values
                color = 'r'
        self.figure.suptitle('MD'+str(md+1))
        for i in range (12):
                if self.stateHGLG.get() == 1:
                    self.ax1[i].set_title('PMT HG ' + str(i + md*12 + 1),fontsize=10)
                else:
                    self.ax1[i].set_title('PMT LG ' + str(i + md*12 + 1),fontsize=10)                    
                _, _, r_value, _, _ = stats.linregress(samplesaux2,(samplesaux1[i]))
                try:
                    line_fit = np.polyfit(samplesaux2,samplesaux1[i], 1)
                    self.ax1[i].plot(np.unique(samplesaux1[i]), np.poly1d(line_fit)(np.unique(samplesaux1[i])), 'k', linewidth=2)
                    txt =  r'$R^2 = $' + str("%.2f" % r_value)
                except np.linalg.LinAlgError as error:
                    print(error)
                    txt =  r'$R^2 = NaN' # Not a Number
                self.ax1[i].text(200, 3500, txt, bbox=dict(facecolor=color, alpha=0.5))
                self.ax1[i].scatter(samplesaux2,samplesaux1[i], c=color, alpha=0.5, marker='o')
        self.figure.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.4, hspace=0.4)
        self.canvas.draw()