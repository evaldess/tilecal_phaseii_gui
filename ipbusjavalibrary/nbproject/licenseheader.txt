<#if licenseFirst??>
${licenseFirst}
</#if>
${licensePrefix}IPbusJavaLibrary project
${licensePrefix}2019 (c) IFIC-CERN
<#if licenseLast??>
${licenseLast}
</#if>