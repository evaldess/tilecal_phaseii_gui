/*
 * IPbusJavaLibrary project
 * 2019 (c) IFIC-CERN
 */
package ipbusjavalibrary.utilities;

import static java.lang.Math.pow;

/**
 * Byte handling tools abstract class for the IPbusJavaLibrary.
 *
 * @author Juan Valls {@literal (valls@cern.ch)}
 * @author Carlos Solans {@literal (solans@cern.ch)}
 */
public abstract class ByteTools {

    /**
     * Constructor. Do nothing.
     */
    public ByteTools() {
    }

    /**
     * Add data bytes to an input destination byte array.
     *
     * @param dest Input destination byte array where to add data.
     * @param pos Starting index position in the byte array where to add data.
     * @param src Data to be added.
     * @return Last index position in the input destination byte array.
     */
    public static Integer addBytes(byte[] dest, Integer pos, Integer src) {
        for (int i = 0; i < 4; i++) {
            dest[pos + 3 - i] = (byte) ((src >>> 8 * i) & 0xFF);
        }
        return pos + 4;
    }

    /**
     * Copy a set of bytes between two input byte arrays.
     *
     * @param src Input source byte array.
     * @param srcPos Input source index position in the source byte array.
     * @param dest Input destination byte array.
     * @param destPos Input destination index position in the destination byte
     * array.
     * @param length Length of bytes to be copied.
     * @param endianChange endian...
     */
    public static void copyBytes(byte[] src, int srcPos,
            byte[] dest, int destPos,
            int length, boolean endianChange) {
        if (endianChange) {
            for (Integer i = 0; i < length; i += 4) {
                for (Integer j = 0; j < 4; j++) {
                    dest[destPos + i + j] = src[srcPos + i + 3 - j];
                }
            }
        } else {
            for (Integer i = 0; i < length; i += 4) {
                for (Integer j = 0; j < 4; j++) {
                    dest[destPos + i + j] = src[srcPos + i + j];
                }
            }
        }
    }

    /**
     * Returns an Integer word with 4 bytes from an input byte array starting at
     * a specified index byte array position.
     *
     * @param b Input byte array from where to extract the data.
     * @param pos Byte index position where to start to pack byte contents.
     * @return des Destination Integer word.
     */
    public static Integer pack(byte[] b, Integer pos) {
        Integer des = 0;
        Integer len = (pos + b.length > 4 ? 4 : pos + b.length);
        for (int i = 0; i < len; i++) {
            des |= (int) ((0x00FF & b[pos + len - 1 - i]) << (8 * i));
        }
        return des;
    }

    /**
     * Returns a formatted byte String in hexadecimal format from the data in an
     * input byte array.
     *
     * @param b Input byte array.
     * @param pos Input starting index position in the byte array.
     * @return the hexadecimal formatted String.
     */
    public static String formatBytes(byte[] b, Integer pos) {
        String s = "";
        for (Integer i = pos; i < pos + 4; i++) {
            s += String.format("%02X", b[i]) + " ";
        }
        return s;
    }

    /**
     * Dumps an input byte array in formatted 32-bit hexadecimal words.
     *
     * @param b Input byte array.
     * @param length Input length (in bytes).
     */
    public static void dumpBytesHex(byte[] b, Integer length) {
        System.out.println("  ByteTools.dumpBytes():");
        for (int i = 0; i < length; i += 4) {
            System.out.println("  0x " + formatBytes(b, i));
        }
    }

    /**
     * Dumps total number of 32-bit words from an input byte array.
     *
     * @param b Input byte array.
     * @param length Input length (in bytes).
     */
    public static void dumpTotalWords(byte[] b, Integer length) {
        int tot_words = 0;
        for (int i = 0; i < length; i += 4) {
            tot_words++;
        }
        System.out.println("  ByteTools.dumpTotalWords(): bytes = "
                + length + " / words = " + tot_words);
    }

    // Check value of a bit in an Integer word
    public static Boolean isZero(int pos, int val) {
        return ((val &= (1 << pos)) == 0);
    }

    /**
     * Sets value of a particular bit in an Integer 32-bit word.
     *
     * @param pos Input bit position (starts at bit 0).
     * @param val Input bit value (= 0, 1).
     * @param word Input word to be modified.
     * @return the modified input word.
     */
    public static Integer setBit(int pos, Integer val, Integer word) {
        if ((val != 0) && (val != 1)) {
            System.out.println("ByteTools.setBit() failed: improper bit value... no action");
            return word;
        }
        if ((pos < 0) || (pos > 31)) {
            System.out.println("ByteTools.setBit() failed: improper bit range... no action");
            return word;
        }

        if (val == 1) {
            word = word | (1 << pos);
        } else {
            word = word & ~(1 << pos);
        }

        return word;
    }

    /**
     * Sets a group of bits in a word to a particular value.
     *
     * @param lsb Input less significant bit (starts at 0).
     * @param msb Input most significant bit (starts at 0);
     * @param val Input value to be written.
     * @param word Input original word.
     * @return the new word with bits set.
     */
    public static Integer setBits(Integer lsb, Integer msb, Integer val, Integer word) {
        if (msb < lsb) {
            return -1;
        }

        Integer nbits = (msb - lsb) + 1;

        if (val > pow(2, nbits) - 1) {
            return -1;
        }

        val = val << lsb;

        Integer mask = 0;
        for (int i = 0; i < nbits; i++) {
            mask = setBit(i, 1, mask);
        }
        mask = mask << lsb;

        return (word & ~mask) | val;
    }

    /**
     * Gets value of a particular bit in an Integer 32-bit word.
     *
     * @param pos Input bit position (from 0 to 31).
     * @param value Input 32-bit Integer word.
     * @return the bit value.
     */
    public static Integer getBit(Integer pos, Integer value) {
        Boolean isSet = (value & (1 << pos)) != 0;

        if (isSet) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * Converts an integer to a 32-bit binary string.
     *
     * @param number the number to convert.
     * @param groupSize the number of bits in a group.
     * @return the 32-bit long bit string.
     */
    public static String intToString(int number, int groupSize) {
        StringBuilder result = new StringBuilder();

        for (int i = 31; i >= 0; i--) {
            int mask = 1 << i;
            result.append((number & mask) != 0 ? "1" : "0");

            if (i % groupSize == 0) {
                result.append(" ");
            }
        }

        result.replace(result.length() - 1, result.length(), "");

        return result.toString();
    }

}
