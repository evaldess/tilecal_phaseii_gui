#
# Sergio Landete
# Fernando Carrio Argos
# University of Valencia
# fernando.carrio@cern.ch
#
import Tkinter as tk
import tkMessageBox
import ttk
import sys
sys.path.append('./common')
from ipbus import ipbus
sys.path.append('./PyChips/PyChips_1_4_1/src')
from PyChipsUser import *

class MainPanelTab(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self,parent)
        self.configure(background="khaki")
        global text
        text=tk.StringVar()
        text.set('')

        conectarLabel=tk.Label(self, text="Version: ", width=10)
        pantallaVersion=tk.Label(self, textvariable=text, width=10)
        conectarBoton=tk.Button(self, text="Connect", command=lambda:self.funcionConexion(), width=20)
        reportesBoton=tk.Button(self, text="Reports", command=lambda:self.funcionReporte(), width=20)
        salirBoton=tk.Button(self, text="Exit", command=lambda:self.funcionSalir(), width=20)
        comboBarrel = ttk.Combobox(self, values=["EBA", "EBC","LBA","LBC"], width=9,state="readonly")
        comboBarrel.current(0)
        spinbox = tk.Spinbox(self, from_=1, to=64, width=10)

        conectarLabel.grid(row=3, column=0, sticky="nsew", padx=10, pady=10, columnspan=1)
        pantallaVersion.grid(row=3, column=1, sticky="nsew", padx=10, pady=10, columnspan=1)
        conectarBoton.grid(row=4, column=0, sticky="nsew", padx=10, pady=10, columnspan=2)
        reportesBoton.grid(row=7, column=0, sticky="nsew", padx=10, pady=10, columnspan=2)
        salirBoton.grid(row=8, column=0, sticky="nsew", padx=10, pady=10, columnspan=2)
        comboBarrel.grid(row=1, column=0, padx=0, pady=10, columnspan=1)
        spinbox.grid(row=1, column=1, padx=0, pady=10, columnspan=1)

    def funcionConexion(self):
        global text   
        ppr = ipbus()
        reg1 = ppr.ipbus.read("sys_id")                
        version=uInt32HexStr(reg1)
        print (version)
        txt = version[0:2] +' / '+version[2:4]+" / "+version[4:8]
        text.set(txt)

    def funcionSalir(self):
        valor=tkMessageBox.askquestion("Exit", "Are you sure?")
        if valor=="yes":
            self.destroy()