package tilepprjavalibrary.lib;

import ipbusjavalibrary.utilities.ByteTools;
import java.util.ArrayList;

import tilepprjavalibrary.reg.DBReg;
import tilepprjavalibrary.reg.PPrReg;

/**
 *
 * @author juano
 */
public class FEB {

    public Io io;

    public FEB(Io io) {
        this.io = io;
    }

    /**
     * Sends a PPr asynchronous command to the Front-End Boards. The method
     * first builds the bit contents of an asynchronous FE command. Then sends
     * through IPbus the async command address, its contents and the contents of
     * the async command argument.
     *
     * @param md Input minidrawer (=0: for broadcast. Otherwise, 1 to 4).
     * @param dbside Input DB FPGA side (=0: broadcast; =1: side A; =2: side B).
     * @param command Input asynchronous command argument.
     * @return The IPbus write status.
     */
    public synchronized Boolean send_asyncFEcommand(Integer md, Integer dbside, Integer command) {
        Integer async_addr_val;
        String fs;

        // Builds bit contents of the asynchronous command address register.
        async_addr_val = (md << 28) & PPrReg.ASYNC_ADDRESS_MD_MASK;

        // Sets DB side bits.
        switch (dbside) {
            case 0:
                async_addr_val = ByteTools.setBit(13, 0, async_addr_val);
                break;
            case 1:
                async_addr_val = ByteTools.setBit(13, 1, async_addr_val);
                async_addr_val = ByteTools.setBit(12, 0, async_addr_val);
                break;
            case 2:
                async_addr_val = ByteTools.setBit(13, 1, async_addr_val);
                async_addr_val = ByteTools.setBit(12, 1, async_addr_val);
                break;
            default:
                return false;
        }

        // Sets DB address bits.
        async_addr_val = async_addr_val | DBReg.DB_FEB_COMMAND;

        if (PPrLib.getDebugPPr()) {
            fs = "    FEB.send_asyncFECommand -> MD: %d, DB side: %d";
            System.out.println(String.format(fs, md, dbside));
            fs = "      PPr async address command register: 0x%08X";
            System.out.println(String.format(fs, PPrReg.ASYNC_ADDRESS));
            fs = "      PPr async address command register value: 0x%08X (%s)";
            System.out.println(String.format(fs, async_addr_val, ByteTools.intToString(async_addr_val, 4)));
            fs = "      PPr async address argument register argument value: 0x%08X (%s)";
            System.out.println(String.format(fs, command, ByteTools.intToString(command, 4)));
        }

        return io.write(PPrReg.ASYNC_ADDRESS, async_addr_val, command);
    }

    /**
     * Sends a PPr asynchronous command for CIS mainboard configuration. The
     * method first builds the bit contents of an asynchronous CIS mainboard
     * command. Then sends through IPbus the async command address, its contents
     * and the contents of the async command argument.
     *
     * @param md Input minidrawer (=0: for broadcast. Otherwise, 1 to 4).
     * @param dbside Input DB FPGA side (=0: broadcast; =1: side A; =2: side B).
     * @param command Input asynchronous command argument.
     * @return The IPbus write status.
     */
    public synchronized Boolean send_asyncCIScommand(Integer md, Integer dbside, Integer command) {
        Integer async_addr_val;
        String fs;

        // Builds bit contents of the asynchronous command address register.
        async_addr_val = (md << 28) & PPrReg.ASYNC_ADDRESS_MD_MASK;

        // Sets DB side bits.
        switch (dbside) {
            case 0:
                async_addr_val = ByteTools.setBit(13, 0, async_addr_val);
                break;
            case 1:
                async_addr_val = ByteTools.setBit(13, 1, async_addr_val);
                async_addr_val = ByteTools.setBit(12, 0, async_addr_val);
                break;
            case 2:
                async_addr_val = ByteTools.setBit(13, 1, async_addr_val);
                async_addr_val = ByteTools.setBit(12, 1, async_addr_val);
                break;
            default:
                return false;
        }

        // Sets DB address bits.
        async_addr_val = async_addr_val | DBReg.DB_CIS;

        if (PPrLib.getDebugPPr()) {
            fs = "    FEB.send_asyncCIScommand -> MD: %d, DB side: %d";
            System.out.println(String.format(fs, md, dbside));
            fs = "      PPr async address command register: 0x%08X";
            System.out.println(String.format(fs, PPrReg.ASYNC_ADDRESS));
            fs = "      PPr async address command register value: 0x%08X (%s)";
            System.out.println(String.format(fs, async_addr_val, ByteTools.intToString(async_addr_val, 4)));
            fs = "      PPr async address command register argument value: 0x%08X (%s)";
            System.out.println(String.format(fs, command, ByteTools.intToString(command, 4)));
        }

        return io.write(PPrReg.ASYNC_ADDRESS, async_addr_val, command);
    }

    /**
     * Sends a PPr synchronous command to the Front-End Boards through the
     * Daughterboard. The method first builds the bit contents of an synchronous
     * FE command. Then sends through IPbus the sync command address, its
     * contents and the contents of the sync command argument.
     *
     * @param md Input minidrawer (=0: for broadcast. Otherwise, 1 to 4).
     * @param dbside Input DB FPGA side (=0: broadcast; =1: side A; =2: side B).
     * @param bcid Input BCID.
     * @param command Input synchronous command argument.
     * @return The IPbus write status.
     */
    public synchronized Boolean send_syncFEcommand(Integer md, Integer dbside, Integer bcid, Integer command) {
        Integer sync_addr_val;
        String fs;

        // Builds bit contents of the synchronous command address register.
        sync_addr_val = (md << 28) & PPrReg.SYNC_ADDRESS_MD_MASK;

        // Sets DB side bits.
        switch (dbside) {
            case 0:
                sync_addr_val = ByteTools.setBit(13, 0, sync_addr_val);
                break;
            case 1:
                sync_addr_val = ByteTools.setBit(13, 1, sync_addr_val);
                sync_addr_val = ByteTools.setBit(12, 0, sync_addr_val);
                break;
            case 2:
                sync_addr_val = ByteTools.setBit(13, 1, sync_addr_val);
                sync_addr_val = ByteTools.setBit(12, 1, sync_addr_val);

                break;
            default:
                return false;
        }

        // Sets BCID bits.
        sync_addr_val = ByteTools.setBits(16, 27, bcid, sync_addr_val);

        // Sets DB address bits.
        sync_addr_val = sync_addr_val | DBReg.DB_FEB_COMMAND;

        if (PPrLib.getDebugPPr()) {
            fs = "    FEB.send_syncFECommand -> MD: %d, DB side: %d";
            System.out.println(String.format(fs, md, dbside));
            fs = "      PPr sync address command register: 0x%08X";
            System.out.println(String.format(fs, PPrReg.SYNC_ADDRESS));
            fs = "      PPr sync address command register value: 0x%08X (%s)";
            System.out.println(String.format(fs, sync_addr_val, ByteTools.intToString(sync_addr_val, 4)));
            fs = "      PPr sync address argument register argument value: 0x%08X (%s)";
            System.out.println(String.format(fs, command, ByteTools.intToString(command, 4)));
        }

        return io.write(PPrReg.SYNC_ADDRESS, sync_addr_val, command);
    }

    /**
     * Sends a PPr synchronous command to the PPr to set a BCID counter with a
     * L1A trigger to be readout from the PPr data pipelines. The method first
     * builds the bit contents of a synchronous command. Then sends through
     * IPbus the sync command address, its contents and the contents of the sync
     * command argument.
     *
     * @param bcid Input BCID.
     * @param command Input synchronous command argument.
     * @return The IPbus write status.
     */
    public synchronized Boolean send_syncPPrcommand(Integer bcid, Integer command) {
        Integer sync_ppr_addr_val = 0;
        String fs;

        // Sets BCID bits.
        sync_ppr_addr_val = ByteTools.setBits(16, 27, bcid & 0XFFF, sync_ppr_addr_val);

        if (PPrLib.getDebugPPr()) {
            fs = "    FEB.send_syncPPrcommand -> BCID: %d";
            System.out.println(String.format(fs, bcid));
            fs = "      PPr sync address command register: 0x%08X";
            System.out.println(String.format(fs, PPrReg.SYNC_PPR_ADDRESS));
            fs = "      PPr sync address command register value: 0x%08X (%s)";
            System.out.println(String.format(fs, sync_ppr_addr_val, ByteTools.intToString(sync_ppr_addr_val, 4)));
            fs = "      PPr sync address argument register argument value: 0x%08X (%s)";
            System.out.println(String.format(fs, command, ByteTools.intToString(command, 4)));
        }

        return io.write(PPrReg.SYNC_PPR_ADDRESS, sync_ppr_addr_val, command);
    }

    /**
     * Returns bits contents for the Mainboard Control FPGA fields of the TTC
     * FEB Command register.
     *
     * @param feb Input FEB number (from 0 to 11).
     * @return the bit contents.
     */
    public synchronized Integer feb_to_FPGA(Integer feb) {
        String fs;

        Integer fpga = (DBReg.FPGA[feb] << DBReg.CMD_FPGA_OFFSET) & DBReg.CMD_FPGA_MASK;

        if (PPrLib.getDebugPPr()) {
            fs = "    FEB.feb_to_FPGA -> FEB: %d FPGA: %d FPGA bits: 0x%01X (%d)";
            System.out.println(String.format(fs, feb, DBReg.FPGA[feb],
                    (fpga >> 18), (fpga >> 18)));
        }

        return fpga;
    }

    /**
     * Returns bits contents for the Mainboard Control FPGA and Channel fields
     * of the TTC FEB Command register.
     *
     * @param feb Input FEB number (from 0 to 11).
     * @return the bit contents.
     */
    public synchronized Integer feb_to_card(Integer feb) {
        String fs;

        Integer card = (DBReg.FPGA_CHANNEL[feb] << DBReg.CMD_FPGA_CARD_OFFSET)
                & DBReg.CMD_FPGA_CHANNEL_MASK;

        if (PPrLib.getDebugPPr()) {
            fs = "    FEB.feb_to_card -> FEB: %d card: %d card bits: 0x%01X (%d)";
            System.out.println(String.format(fs, feb, DBReg.FPGA_CHANNEL[feb],
                    (card >> 16), (card >> 16)));
        }

        return card;
    }

    /**
     *
     * @param md
     * @param dbside
     * @param feb
     * @param val
     * @return
     */
    public synchronized Boolean transmit_CIS_DAC(Integer md, Integer dbside, Integer feb, Integer val) {
        Integer command;
        Boolean ret;
        String fs;

        if (PPrLib.getDebugPPr()) {
            fs = "FEB.transmit_CIS_DAC -> MD: %d DB side: %d FEB: %d";
            System.out.println(String.format(fs, md, dbside, feb));
        }

        command = feb_to_FPGA(feb)
                | feb_to_card(feb)
                | ((DBReg.CMD_SET_CIS_DAC << DBReg.CMD_OFFSET) & DBReg.CMD_MASK)
                | (val & DBReg.CMD_DATA_MASK);

        ret = send_asyncFEcommand(md, dbside, command);
        if (!ret) {
            return false;
        }

        command = DBReg.CMD_BIT_E_MASK
                | feb_to_FPGA(feb)
                | feb_to_card(feb)
                | (DBReg.CMD_SET_CIS_DAC << DBReg.CMD_OFFSET) & DBReg.CMD_MASK
                | (val & DBReg.CMD_DATA_MASK);

        ret = send_asyncFEcommand(md, dbside, command);

        return ret;
    }

    /**
     * Sends first a PPr async FE command (ped HG positive) with bit E=0 and
     * then with bit E=1. The FE command is then transmitted from the DB to the
     * mainboard.
     *
     * @param md
     * @param dbside
     * @param feb
     * @param val
     * @return
     */
    public synchronized Boolean transmit_ped_HG_pos(Integer md, Integer dbside, Integer feb, Integer val) {
        Integer command;
        Boolean ret;
        String fs;

        if (PPrLib.getDebugPPr()) {
            fs = "FEB.transmit_ped_LG_pos -> MD: %d DB side: %d FEB: %d";
            System.out.println(String.format(fs, md, dbside, feb));
        }

        command = feb_to_FPGA(feb)
                | feb_to_card(feb)
                | (DBReg.CMD_SET_PED_HG_POS << DBReg.CMD_OFFSET) & DBReg.CMD_MASK
                | (val & DBReg.CMD_DATA_MASK);

        ret = send_asyncFEcommand(md, dbside, command);
        if (!ret) {
            return false;
        }

        command = DBReg.CMD_BIT_E_MASK
                | feb_to_FPGA(feb)
                | feb_to_card(feb)
                | (DBReg.CMD_SET_PED_HG_POS << DBReg.CMD_OFFSET) & DBReg.CMD_MASK
                | (val & DBReg.CMD_DATA_MASK);

        ret = send_asyncFEcommand(md, dbside, command);

        return ret;
    }

    /**
     * Sends first a PPr async FE command (ped HG negative) with bit E=0 and
     * then with bit E=1. The FE command is then transmitted from the DB to the
     * mainboard.
     *
     * @param md Input minidrawer (0:broadcast; 1 to 4 for selected minidrawer).
     * @param dbside
     * @param feb
     * @param val
     * @return
     */
    public synchronized Boolean transmit_ped_HG_neg(Integer md, Integer dbside, Integer feb, Integer val) {
        Integer command;
        Boolean ret;
        String fs;

        if (PPrLib.getDebugPPr()) {
            fs = "FEB.transmit_ped_HG_neg -> MD: %d DB side: %d FEB: %d";
            System.out.println(String.format(fs, md, dbside, feb));
        }

        command = feb_to_FPGA(feb)
                | feb_to_card(feb)
                | (DBReg.CMD_SET_PED_HG_NEG << DBReg.CMD_OFFSET) & DBReg.CMD_MASK
                | (val & DBReg.CMD_DATA_MASK);

        ret = send_asyncFEcommand(md, dbside, command);
        if (!ret) {
            return false;
        }

        command = DBReg.CMD_BIT_E_MASK
                | feb_to_FPGA(feb)
                | feb_to_card(feb)
                | (DBReg.CMD_SET_PED_HG_NEG << DBReg.CMD_OFFSET) & DBReg.CMD_MASK
                | (val & DBReg.CMD_DATA_MASK);

        return send_asyncFEcommand(md, dbside, command);
    }

    /**
     * Sends first a PPr async FE command (ped LG positive) with bit E=0 and
     * then with bit E=1. The FE command is then transmitted from the DB to the
     * mainboard.
     *
     * @param md
     * @param dbside
     * @param feb
     * @param val
     * @return
     */
    public synchronized Boolean transmit_ped_LG_pos(Integer md, Integer dbside, Integer feb, Integer val) {
        Integer command;
        Boolean ret;
        String fs;

        if (PPrLib.getDebugPPr()) {
            fs = "FEB.transmit_ped_LG_pos -> MD: %d DB side: %d FEB: %d";
            System.out.println(String.format(fs, md, dbside, feb));
        }

        command = feb_to_FPGA(feb)
                | feb_to_card(feb)
                | (DBReg.CMD_SET_PED_LG_POS << DBReg.CMD_OFFSET) & DBReg.CMD_MASK
                | (val & DBReg.CMD_DATA_MASK);

        ret = send_asyncFEcommand(md, dbside, command);
        if (!ret) {
            return false;
        }

        command = DBReg.CMD_BIT_E_MASK
                | feb_to_FPGA(feb)
                | feb_to_card(feb)
                | (DBReg.CMD_SET_PED_LG_POS << DBReg.CMD_OFFSET) & DBReg.CMD_MASK
                | (val & DBReg.CMD_DATA_MASK);

        ret = send_asyncFEcommand(md, dbside, command);

        return ret;
    }

    /**
     * Sends first a PPr async FE command (ped LG negative) with bit E=0 and
     * then with bit E=1. The FE command is then transmitted from the DB to the
     * mainboard.
     *
     * @param md
     * @param dbside
     * @param feb
     * @param val
     * @return
     */
    public synchronized Boolean transmit_ped_LG_neg(Integer md, Integer dbside, Integer feb, Integer val) {
        Integer command;
        Boolean ret;
        String fs;

        if (PPrLib.getDebugPPr()) {
            fs = "FEB.transmit_ped_LG_neg -> MD: %d DB side: %d FEB: %d";
            System.out.println(String.format(fs, md, dbside, feb));
        }

        command = feb_to_FPGA(feb)
                | feb_to_card(feb)
                | (DBReg.CMD_SET_PED_LG_NEG << DBReg.CMD_OFFSET) & DBReg.CMD_MASK
                | (val & DBReg.CMD_DATA_MASK);

        ret = send_asyncFEcommand(md, dbside, command);
        if (!ret) {
            return false;
        }

        command = DBReg.CMD_BIT_E_MASK
                | feb_to_FPGA(feb)
                | feb_to_card(feb)
                | (DBReg.CMD_SET_PED_LG_NEG << DBReg.CMD_OFFSET) & DBReg.CMD_MASK
                | (val & DBReg.CMD_DATA_MASK);

        ret = send_asyncFEcommand(md, dbside, command);

        return ret;
    }

    /**
     *
     * @param md
     * @param dbside
     * @param feb
     * @param val
     * @return
     */
    public synchronized Boolean transmit_switches(Integer md, Integer dbside, Integer feb, Integer val) {
        Integer command;
        Boolean ret;
        String fs;

        if (PPrLib.getDebugPPr()) {
            fs = "FEB.transmit_switches -> MD: %d DB side: %d FEB: %d";
            System.out.println(String.format(fs, md, dbside, feb));
        }

        command = feb_to_FPGA(feb)
                | feb_to_card(feb)
                | ((DBReg.CMD_SET_SWITCHES << DBReg.CMD_OFFSET) & DBReg.CMD_MASK)
                | (val & DBReg.CMD_DATA_MASK);

        ret = send_asyncFEcommand(md, dbside, command);
        if (!ret) {
            return false;
        }

        command = DBReg.CMD_BIT_E_MASK
                | feb_to_FPGA(feb)
                | feb_to_card(feb)
                | ((DBReg.CMD_SET_SWITCHES << DBReg.CMD_OFFSET) & DBReg.CMD_MASK)
                | (val & DBReg.CMD_DATA_MASK);

        ret = send_asyncFEcommand(md, dbside, command);

        return ret;
    }

    /**
     * Sends a PPr asynchronous command to configure a FEB CIS DAC value.
     *
     * @param md Input minidrawer (0 for broadcast. Otherwise, 1 to 4).
     * @param dbside
     * @param feb
     * @param val
     * @return
     */
    public synchronized Boolean set_CIS_DAC(Integer md, Integer dbside, Integer feb, Integer val) {
        String fs;

        if (PPrLib.getDebugPPr()) {
            fs = "FEB.set_CIS_DAC -> MD: %d DB side: %d FEB: %d";
            System.out.println(String.format(fs, md, dbside, feb));
        }
        Integer command = DBReg.CMD_BIT_E_MASK
                | feb_to_FPGA(feb)
                | feb_to_card(feb)
                | ((DBReg.CMD_SET_CIS_DAC << DBReg.CMD_OFFSET) & DBReg.CMD_MASK)
                | (val & DBReg.CMD_DATA_MASK);

        return send_asyncFEcommand(md, dbside, command);
    }

    /**
     * Sends a PPr asynchronous command to configure FEB switches.
     *
     * @param md
     * @param dbside
     * @param feb
     * @param val
     * @return
     */
    public synchronized Boolean set_switches(Integer md, Integer dbside, Integer feb, Integer val) {
        String fs;

        if (PPrLib.getDebugPPr()) {
            fs = "FEB.set_switches -> MD: %d DB side: %d FEB: %d";
            System.out.println(String.format(fs, md, dbside, feb));
        }

        Integer command = DBReg.CMD_BIT_E_MASK
                | feb_to_FPGA(feb)
                | feb_to_card(feb)
                | ((DBReg.CMD_SET_SWITCHES << DBReg.CMD_OFFSET) & DBReg.CMD_MASK)
                | (val & DBReg.CMD_DATA_MASK);

        return send_asyncFEcommand(md, dbside, command);
    }

    /**
     * Sends a PPr asynchronous command to configure switches of a particular
     * FEB for a noise run.
     *
     * @param md
     * @param dbside
     * @param feb
     * @return
     */
    public synchronized Boolean set_switches_noise(Integer md, Integer dbside, Integer feb) {
        Integer val = 0;
        String fs;

        if (PPrLib.getDebugPPr()) {
            fs = "FEB.set_switches_noise -> MD: %d DB side: %d FEB: %d";
            System.out.println(String.format(fs, md, dbside, feb));
        }

        // Builds data field switches for noise.
        // TPH=0; TPL=0;ITG=1;S1=S2=S3=S4=0;TRG=1
        val = val | (1 << 7) | (1 << 2);

        Integer command = DBReg.CMD_BIT_E_MASK
                | feb_to_FPGA(feb)
                | feb_to_card(feb)
                | ((DBReg.CMD_SET_SWITCHES << DBReg.CMD_OFFSET) & DBReg.CMD_MASK)
                | (val & DBReg.CMD_DATA_MASK);

        return send_asyncFEcommand(md, dbside, command);
    }

    /**
     * Sends a PPr asynchronous command to configure a FEB ADC offset positive
     * High Gain (pedestal).
     *
     * @param md Input minidrawer (0 for broadcast. Otherwise, 1 to 4).
     * @param dbside Input DB FPGA side (0: broadcast, 1: side A, 2: side B)
     * @param feb
     * @param val
     * @return
     */
    public synchronized Boolean set_ped_HG_pos(Integer md, Integer dbside, Integer feb, Integer val) {
        String fs;

        if (PPrLib.getDebugPPr()) {
            fs = "FEB.set_ped_HG_pos -> MD: %d DB side: %d FEB: %d";
            System.out.println(String.format(fs, md, dbside, feb));
        }

        Integer command = DBReg.CMD_BIT_E_MASK
                | feb_to_FPGA(feb)
                | feb_to_card(feb)
                | ((DBReg.CMD_SET_PED_HG_POS << DBReg.CMD_OFFSET) & DBReg.CMD_MASK)
                | (val & DBReg.CMD_DATA_MASK);

        return send_asyncFEcommand(md, dbside, command);
    }

    /**
     * Sends a PPr asynchronous command to configure a FEB ADC offset negative
     * High Gain (pedestal).
     *
     * @param md Input minidrawer (0 for broadcast. Otherwise, 1 to 4).
     * @param dbside Input DB FPGA side (0: broadcast; 1: side A; 2: side B).
     * @param feb
     * @param val
     * @return
     */
    public synchronized Boolean set_ped_HG_neg(Integer md, Integer dbside, Integer feb, Integer val) {
        String fs;

        if (PPrLib.getDebugPPr()) {
            fs = "FEB.set_ped_HG_neg -> MD: %d DB side: %d FEB: %d";
            System.out.println(String.format(fs, md, dbside, feb));
        }

        Integer command = DBReg.CMD_BIT_E_MASK
                | feb_to_FPGA(feb)
                | feb_to_card(feb)
                | ((DBReg.CMD_SET_PED_HG_NEG << DBReg.CMD_OFFSET) & DBReg.CMD_MASK)
                | (val & DBReg.CMD_DATA_MASK);

        return send_asyncFEcommand(md, dbside, command);
    }

    /**
     * Sends a PPr asynchronous command to configure a FEB ADC offset positive
     * Low Gain (pedestal).
     *
     * @param md Input minidrawer (0 for broadcast. Otherwise, 1 to 4).
     * @param dbside Input DB FPGA side (0: broadcast, 1: side A, 2: side B).
     * @param feb
     * @param val
     * @return
     */
    public synchronized Boolean set_ped_LG_pos(Integer md, Integer dbside, Integer feb, Integer val) {
        String fs;

        if (PPrLib.getDebugPPr()) {
            fs = "FEB.set_ped_LG_pos -> MD: %d DB side: %d FEB: %d";
            System.out.println(String.format(fs, md, dbside, feb));
        }

        Integer command = DBReg.CMD_BIT_E_MASK
                | feb_to_FPGA(feb)
                | feb_to_card(feb)
                | ((DBReg.CMD_SET_PED_LG_POS << DBReg.CMD_OFFSET) & DBReg.CMD_MASK)
                | (val & DBReg.CMD_DATA_MASK);

        return send_asyncFEcommand(md, dbside, command);
    }

    /**
     * Sends a PPr asynchronous command to configure a FEB ADC offset negative
     * Low Gain (pedestal).
     *
     * @param md Input minidrawer (0 for broadcast. Otherwise, 1 to 4).
     * @param dbside Input DB FPGA side (0: broadcast, 1: side A, 2: side B)
     * @param feb
     * @param val
     * @return
     */
    public synchronized Boolean set_ped_LG_neg(Integer md, Integer dbside, Integer feb, Integer val) {
        String fs;

        if (PPrLib.getDebugPPr()) {
            fs = "FEB.set_ped_LG_neg -> MD: %d DB side: %d FEB: %d";
            System.out.println(String.format(fs, md, dbside, feb));
        }

        Integer command = DBReg.CMD_BIT_E_MASK
                | feb_to_FPGA(feb)
                | feb_to_card(feb)
                | ((DBReg.CMD_SET_PED_LG_NEG << DBReg.CMD_OFFSET) & DBReg.CMD_MASK)
                | (val & DBReg.CMD_DATA_MASK);

        return send_asyncFEcommand(md, dbside, command);
    }

    /**
     *
     * @param md Input minidrawer (0:broadcast; 1 to 4 for selected minidrawer).
     * @param dbside
     * @param feb
     * @return
     */
    public synchronized Boolean readback_switches(Integer md, Integer dbside, Integer feb) {
        Integer command;
        Boolean ret;
        String fs;

        if (PPrLib.getDebugPPr()) {
            fs = "PPR.readback_switches -> MD: %d DB side: %d FEB: %d";
            System.out.println(String.format(fs, md, dbside, feb));
        }

        command = DBReg.CMD_BIT_E_MASK
                | feb_to_FPGA(feb)
                | feb_to_card(feb)
                | ((DBReg.CMD_GET_SWITCHES << DBReg.CMD_OFFSET) & DBReg.CMD_MASK);

        ret = send_asyncFEcommand(md, dbside, command);
        if (!ret) {
            return false;
        }

        command = DBReg.CMD_BIT_B_MASK
                | feb_to_FPGA(feb)
                | feb_to_card(feb)
                | ((DBReg.CMD_GET_SWITCHES << DBReg.CMD_OFFSET) & DBReg.CMD_MASK);

        ret = send_asyncFEcommand(md, dbside, command);

        return ret;
    }

    /**
     *
     * @param md Input minidrawer (0:broadcast; 1 to 4 for selected minidrawer).
     * @param dbside
     * @param feb
     * @return
     */
    public synchronized Boolean readback_CIS_DAC(Integer md, Integer dbside, Integer feb) {
        Integer command;
        Boolean ret;
        String fs;

        if (PPrLib.getDebugPPr()) {
            fs = "PPR.feb_readback_CIS_DAC -> MD: %d DB side: %d FEB: %d";
            System.out.println(String.format(fs, md, dbside, feb));
        }

        command = DBReg.CMD_BIT_E_MASK
                | feb_to_FPGA(feb)
                | feb_to_card(feb)
                | ((DBReg.CMD_GET_CIS_DAC << DBReg.CMD_OFFSET) & DBReg.CMD_MASK);

        ret = send_asyncFEcommand(md, dbside, command);
        if (!ret) {
            return false;
        }

        command = DBReg.CMD_BIT_B_MASK
                | feb_to_FPGA(feb)
                | feb_to_card(feb)
                | ((DBReg.CMD_GET_CIS_DAC << DBReg.CMD_OFFSET) & DBReg.CMD_MASK);

        ret = send_asyncFEcommand(md, dbside, command);

        return ret;
    }

    /**
     *
     * @param md Input minidrawer (0:broadcast; 1 to 4 for selected minidrawer).
     * @param dbside
     * @param feb
     * @return
     */
    public synchronized Boolean readback_ped_HG_pos(Integer md, Integer dbside, Integer feb) {
        Integer command;
        Boolean ret;
        String fs;

        if (PPrLib.getDebugPPr()) {
            fs = "PPR.feb_readback_ped_HG_pos -> MD: %d DB side: %d FEB: %d";
            System.out.println(String.format(fs, md, dbside, feb));
        }

        command = DBReg.CMD_BIT_E_MASK
                | feb_to_FPGA(feb)
                | feb_to_card(feb)
                | ((DBReg.CMD_GET_PED_HG_POS << DBReg.CMD_OFFSET) & DBReg.CMD_MASK);

        ret = send_asyncFEcommand(md, dbside, command);
        if (!ret) {
            return false;
        }

        command = DBReg.CMD_BIT_B_MASK
                | feb_to_FPGA(feb)
                | feb_to_card(feb)
                | ((DBReg.CMD_GET_PED_HG_POS << DBReg.CMD_OFFSET) & DBReg.CMD_MASK);

        ret = send_asyncFEcommand(md, dbside, command);

        return ret;
    }

    /**
     * Sends first a PPr async FE command (ped HG negative) with bit E=1 and
     * then with bit B=1. The FE command is then readback from the mainboard to
     * the DB.
     *
     * @param md Input minidrawer (0:broadcast; 1 to 4 for selected minidrawer).
     * @param dbside
     * @param feb
     * @return
     */
    public synchronized Boolean readback_ped_HG_neg(Integer md, Integer dbside, Integer feb) {
        Integer command;
        Boolean ret;
        String fs;

        if (PPrLib.getDebugPPr()) {
            fs = "PPR.feb_readback_ped_HG_neg -> MD: %d DB side: %d FEB: %d";
            System.out.println(String.format(fs, md, dbside, feb));
        }

        command = DBReg.CMD_BIT_E_MASK
                | feb_to_FPGA(feb)
                | feb_to_card(feb)
                | ((DBReg.CMD_GET_PED_HG_NEG << DBReg.CMD_OFFSET) & DBReg.CMD_MASK);

        ret = send_asyncFEcommand(md, dbside, command);
        if (!ret) {
            return false;
        }

        command = DBReg.CMD_BIT_B_MASK
                | feb_to_FPGA(feb)
                | feb_to_card(feb)
                | ((DBReg.CMD_GET_PED_HG_NEG << DBReg.CMD_OFFSET) & DBReg.CMD_MASK);

        ret = send_asyncFEcommand(md, dbside, command);

        return ret;
    }

    /**
     *
     * @param md Input minidrawer (0:broadcast; 1 to 4 for selected minidrawer).
     * @param dbside
     * @param feb
     * @return
     */
    public synchronized Boolean readback_ped_LG_pos(Integer md, Integer dbside, Integer feb) {
        Integer command;
        Boolean ret;
        String fs;

        if (PPrLib.getDebugPPr()) {
            fs = "PPR.feb_readback_ped_LG_pos -> MD: %d DB side: %d FEB: %d";
            System.out.println(String.format(fs, md, dbside, feb));
        }

        command = DBReg.CMD_BIT_E_MASK
                | feb_to_FPGA(feb)
                | feb_to_card(feb)
                | ((DBReg.CMD_GET_PED_LG_POS << DBReg.CMD_OFFSET) & DBReg.CMD_MASK);

        ret = send_asyncFEcommand(md, dbside, command);
        if (!ret) {
            return false;
        }

        command = DBReg.CMD_BIT_B_MASK
                | feb_to_FPGA(feb)
                | feb_to_card(feb)
                | ((DBReg.CMD_GET_PED_LG_POS << DBReg.CMD_OFFSET) & DBReg.CMD_MASK);

        ret = send_asyncFEcommand(md, dbside, command);

        return ret;
    }

    /**
     *
     * @param md Input minidrawer (0:broadcast; 1 to 4 for selected minidrawer)
     * @param dbside
     * @param feb
     * @return
     */
    public synchronized Boolean readback_ped_LG_neg(Integer md, Integer dbside, Integer feb) {
        Integer command;
        Boolean ret;
        String fs;

        if (PPrLib.getDebugPPr()) {
            fs = "PPR.feb_readback_ped_LG_neg -> MD: %d DB side: %d FEB: %d";
            System.out.println(String.format(fs, md, dbside, feb));
        }

        command = DBReg.CMD_BIT_E_MASK
                | feb_to_FPGA(feb)
                | feb_to_card(feb)
                | ((DBReg.CMD_GET_PED_LG_NEG << DBReg.CMD_OFFSET) & DBReg.CMD_MASK);

        ret = send_asyncFEcommand(md, dbside, command);
        if (!ret) {
            return false;
        }

        command = DBReg.CMD_BIT_B_MASK
                | feb_to_FPGA(feb)
                | feb_to_card(feb)
                | ((DBReg.CMD_GET_PED_LG_NEG << DBReg.CMD_OFFSET) & DBReg.CMD_MASK);

        ret = send_asyncFEcommand(md, dbside, command);

        return ret;
    }

    /**
     *
     * @param md Input minidrawer (0 for broadcast. Otherwise, 1 to 4).
     * @param dbside Input DB FPGA side (0: broadcast, 1: side A, 2: side B).
     * @param feb
     * @return
     */
    public synchronized Boolean load_ped_HG(Integer md, Integer dbside, Integer feb) {
        String fs;

        if (PPrLib.getDebugPPr()) {
            fs = "FEB.load_ped_HG -> MD: %d DB side: %d FEB: %d";
            System.out.println(String.format(fs, md, dbside, feb));
        }

        Integer command = DBReg.CMD_BIT_E_MASK
                | feb_to_FPGA(feb)
                | feb_to_card(feb)
                | (DBReg.CMD_LOAD_ADC_DAC_HG << DBReg.CMD_OFFSET) & DBReg.CMD_MASK;

        return send_asyncFEcommand(md, dbside, command);
    }

    /**
     *
     * @param md
     * @param dbside
     * @param feb
     * @return
     */
    public synchronized Boolean load_ped_LG(Integer md, Integer dbside, Integer feb) {
        String fs;

        if (PPrLib.getDebugPPr()) {
            fs = "FEB.load_ped_LG -> MD: %d DB side: %d FEB: %d";
            System.out.println(String.format(fs, md, dbside, feb));
        }

        Integer command = DBReg.CMD_BIT_E_MASK
                | feb_to_FPGA(feb)
                | feb_to_card(feb)
                | (DBReg.CMD_LOAD_ADC_DAC_LG << DBReg.CMD_OFFSET) & DBReg.CMD_MASK;

        return send_asyncFEcommand(md, dbside, command);
    }

    /**
     *
     * @param BCID
     * @return
     */
    public synchronized Boolean send_L1A(Integer BCID, Integer arg) {
        Integer cmd = 0;
        Boolean ret;

        // Resets memory (reset bit set to 1).
        cmd = ByteTools.setBit(0, 1, cmd);
        ret = io.write(PPrReg.RO_SYNC_CMD, cmd);
        if (!ret) {
            return false;
        }

        // Allows to write in memory (reset bit set to 0).
        cmd = ByteTools.setBit(0, 0, cmd);
        ret = io.write(PPrReg.RO_SYNC_CMD, cmd);
        if (!ret) {
            return false;
        }

        // Sends L1A trigger through a synchronous PPr command.
        ret = send_syncPPrcommand(BCID & 0xFFF, arg);
        if (!ret) {
            return false;
        }

        // Enables trigger sequence.
        cmd = ByteTools.setBit(1, 1, cmd);
        ret = io.write(PPrReg.RO_SYNC_CMD, cmd);
        if (!ret) {
            return false;
        }

        cmd = ByteTools.setBit(1, 0, cmd);
        ret = io.write(PPrReg.RO_SYNC_CMD, cmd);
        if (!ret) {
            return false;
        }

        // Resets memory again.
        cmd = ByteTools.setBit(0, 1, cmd);
        ret = io.write(PPrReg.RO_SYNC_CMD, cmd);
        if (!ret) {
            return false;
        }

        return ret;
    }

    /**
     * Builds a mainboard CIS FEB configuration and sends it through a PPr async
     * command.
     *
     * @param md Input minidrawer (0:broadcast; 1 to 4 for selected minidrawer).
     * @param dbside Input DB FPGA side (0: broadcast; 1: side A; 2: side B).
     * @param BCID_charge Input BCID to charge capacitor.
     * @param BCID_discharge Input BCID to discharge capacitor.
     * @param gain Input gain (0: low gain; 1: high gain).
     * @return the IPbus transaction status (Boolean).
     */
    public synchronized Boolean set_CIS_BCID_settings(
            Integer md,
            Integer dbside,
            Integer BCID_charge,
            Integer BCID_discharge,
            Integer gain) {
        String fs;

        if (PPrLib.getDebugPPr()) {
            fs = "FEB.set_CIS_BCID_settings() -> MD: %d DB side: %d";
            System.out.println(String.format(fs, md, dbside));
        }

        Integer command = (BCID_discharge << 14) | (BCID_charge << 2) | (gain << 1) | 0x1;

        return send_asyncCIScommand(md, dbside, command);
    }

    /**
     * Converts pedestal DAC bias offsets to pedestal ADC counts.
     *
     * @param VpDACs
     * @param VnDACs
     * @return
     */
    public synchronized Integer convert_ped_DACs_to_ADC(Integer VpDACs, Integer VnDACs) {
        Integer ADCcounts;

        Integer DACrange = 2261 - 1278;
        Integer countBase = VpDACs - VnDACs + DACrange;

        ADCcounts = (int) Math.round(Math.ceil((4096 * countBase) / (2 * DACrange)));

        return ADCcounts;
    }

    /**
     * Converts pedestal ADC counts to pedestal DAC bias offsets.
     *
     * @param ADCcounts
     * @return
     */
    public synchronized ArrayList<Integer> convert_ped_ADC_to_DACs(Integer ADCcounts) {
        ArrayList<Integer> VinDACs = new ArrayList<>();
        Integer diffDAC, DACrange;

        VinDACs.clear();

        DACrange = 2261 - 1278;

        diffDAC = (int) Math.round(Math.floor(DACrange * ADCcounts / 4096));
        VinDACs.add(1278 + diffDAC);

        diffDAC = (int) Math.round(Math.floor((DACrange * ADCcounts / 4096)) + 0.5);
        VinDACs.add(2261 - diffDAC);

        return VinDACs;
    }
}
