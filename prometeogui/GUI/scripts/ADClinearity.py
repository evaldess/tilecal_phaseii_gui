#
# Migrated from Demo scripts
# Fernando Carrio Argos
# University of Valencia
# fernando.carrio@cern.ch
#
import numpy as np
from scipy import stats
import sys
sys.path.append('./common')
from ipbus import ipbus

def ADClinearity(iterations):
    ppr = ipbus()
    
    ###################
    bcid_l1a = 3200
    charge = 0xa0
    ################################
    nsteps=iterations
    
    ###########  CHANGE HERE THE NUMBER OF MINIDRAWERS TO TEST  ########################
    nMD=4# Number of Minidrawers to be tested
    firstMD=0# .. starting from this MD ( MD goes from 0 to 3)
    ####################################################################################
    
    stableP=1650
    stableM=2530
    #### Range is 1 V (+/- 500 mV if pedestal at 0). StepDAC = 1000/nsteps
    StepDAC= 1000/nsteps
    valDACList = [i*(int(StepDAC)) for i in range(nsteps)]#55 steps of 16 DAC ~= steps of 10mV (i*16 range(55))
    nDAC = len(valDACList)
    valDAC = [0]*nDAC

    for i in range(nDAC):
        valDAC[i]=valDACList[i]
    #Disable TTC

    ppr.ipbus.write("cfg1", 0x0)
    #Disable Deadtime 
    ppr.ipbus.write("cfg2",0x0)
    ppr.ResetCounter
    
    #############
    samplesLG = np.zeros((4,12,nsteps))
    samplesHG = np.zeros((4,12,nsteps)) 
    for i in range(nDAC):
        chargeP=stableP+valDAC[i]
        chargeM=stableM-valDAC[i]
        
        for md in range(firstMD,firstMD+nMD):
            for adc in range(12):
                FPGA = ((adc/6)<<1)+ (adc%2)
                card = (adc/2)%3
                
                ppr.AsyncWrite((md+1),0x1,0x8000000)
                ppr.AsyncWrite((md+1),0x1, (1<<22)+(FPGA<<18)+(card<<16)+0x6000+int(chargeP))
                ppr.AsyncWrite((md+1),0x1,0x8000000)
                
                ppr.AsyncWrite((md+1),0x1,0x8000000)
                ppr.AsyncWrite((md+1),0x1, (1<<22)+(FPGA<<18)+(card<<16)+0x7000+int(chargeM))
                ppr.AsyncWrite((md+1),0x1,0x8000000)
    
                ppr.AsyncWrite((md+1),0x1,0x8000000)
                ppr.AsyncWrite((md+1),0x1, (1<<22)+(FPGA<<18)+(card<<16)+0x4000+int(chargeP))
                ppr.AsyncWrite((md+1),0x1,0x8000000)
    
                ppr.AsyncWrite((md+1),0x1,0x8000000)
                ppr.AsyncWrite((md+1),0x1, (1<<22)+(FPGA<<18)+(card<<16)+0x5000+int(chargeM))
                ppr.AsyncWrite((md+1),0x1,0x8000000)
    
                ppr.AsyncWrite((md+1),0x1,0x8000000)
                ppr.AsyncWrite((md+1),0x1,(1<<22)+(FPGA<<18)+(card<<16)+0xc000+int(charge)) #loadLG
                ppr.AsyncWrite((md+1),0x1,0x80000000)
                ppr.AsyncWrite((md+1),0x1,(1<<22)+(FPGA<<18)+(card<<16)+0xd000+int(charge)) #loadHG
                ppr.AsyncWrite((md+1),0x1,0x80000000)
    
        ppr.SyncClear
        ppr.SyncRest
        ppr.SyncWrite(0,0,bcid_l1a, 1)
        ppr.SyncLoop(0)
        ppr.SyncRest
        ppr.SyncClear
#    
        for md in range(firstMD, firstMD+nMD):
            for adc in range(12):
                hg = ppr.ReadPipelines(md, adc, 1)
                lg = ppr.ReadPipelines(md, adc, 0)

                sumhg=0
                sumlg=0
                for a in range(len(hg)):
                    if (hg[a]&0x1000 == 0x0):
                        sumhg+=(hg[a]&0xFFF)
                    else:
                        sumhg+=0
                    if (lg[a]&0x1000 == 0x0 ):
                        sumlg+=(lg[a]&0xFFF)
                    else:
                        sumlg+=0
                samplesLG[md][adc][i]=sumlg/16
                samplesHG[md][adc][i]=sumhg/16
    ###Finally Read the L1ID to release the busy 
        BCID = ppr.ipbus.read("LastEventBCID")
        L1ID = ppr.ipbus.read("LastEventL1ID")
        print " L1ID = ", L1ID , "BCID=", BCID
    return (samplesLG, samplesHG)
#print (samplesLG[0][1:10])