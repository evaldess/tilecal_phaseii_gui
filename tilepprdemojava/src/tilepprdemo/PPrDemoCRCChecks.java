/* 
 *  TilePPrDemoJava Project
 *  2019 (c) IFIC-CERN
 */
package tilepprdemo;

import java.awt.Color;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.swing.JSlider;
import javax.swing.JTextField;

import ipbusjavalibrary.utilities.ByteTools;
import tilepprjavalibrary.lib.PPrLib;

/**
 *
 * @author Juan Valls {@literal (valls@cern.ch)}
 * @author Fernando Carrió {@literal (fernando.carrio@cern.ch)}
 * @author Alberto Valero {@literal (alberto.valero@cern.ch)}
 */
public class PPrDemoCRCChecks extends javax.swing.JPanel {

    /**
     * Creates new form PPrDemoCRCChecks
     *
     * @param pprlib
     */
    public PPrDemoCRCChecks(PPrLib pprlib) {
        System.out.println("PPrDemoCRCChecks -> constructor");

        // NetBeans components settings.
        initComponents();

        // Set PPrLib class object as given by constructor argument.
        this.pprlib = pprlib;

        crc_check_info = new String[]{
            "Nb of Frames", "CRC Frame Errors", "Fraction per Million", "Bit Error Rate",
            "Effective Errors", "Downlink Status", "Latency"};

        link_strings = new String[]{"A0", "A1", "B0", "B1"};

        side_strings = new String[]{"A", "B"};

        selected_MD = new ArrayList<>();

        TextFieldMD1A0 = new JTextField[]{
            TextFieldMD1A0s1, TextFieldMD1A0s2, TextFieldMD1A0s3,
            TextFieldMD1A0s4, TextFieldMD1A0s5, TextFieldMD1A0s6,
            TextFieldMD1A0s7};

        TextFieldMD1A1 = new JTextField[]{
            TextFieldMD1A1s1, TextFieldMD1A1s2, TextFieldMD1A1s3,
            TextFieldMD1A1s4, TextFieldMD1A1s5, TextFieldMD1A1s6,
            TextFieldMD1A1s7};

        TextFieldMD1B0 = new JTextField[]{
            TextFieldMD1B0s1, TextFieldMD1B0s2, TextFieldMD1B0s3,
            TextFieldMD1B0s4, TextFieldMD1B0s5, TextFieldMD1B0s6,
            TextFieldMD1B0s7};

        TextFieldMD1B1 = new JTextField[]{
            TextFieldMD1B1s1, TextFieldMD1B1s2, TextFieldMD1B1s3,
            TextFieldMD1B1s4, TextFieldMD1B1s5, TextFieldMD1B1s6,
            TextFieldMD1B1s7};

        TextFieldMD2A0 = new JTextField[]{
            TextFieldMD2A0s1, TextFieldMD2A0s2, TextFieldMD2A0s3,
            TextFieldMD2A0s4, TextFieldMD2A0s5, TextFieldMD2A0s6,
            TextFieldMD2A0s7};

        TextFieldMD2A1 = new JTextField[]{
            TextFieldMD2A1s1, TextFieldMD2A1s2, TextFieldMD2A1s3,
            TextFieldMD2A1s4, TextFieldMD2A1s5, TextFieldMD2A1s6,
            TextFieldMD2A1s7};

        TextFieldMD2B0 = new JTextField[]{
            TextFieldMD2B0s1, TextFieldMD2B0s2, TextFieldMD2B0s3,
            TextFieldMD2B0s4, TextFieldMD2B0s5, TextFieldMD2B0s6,
            TextFieldMD2B0s7};

        TextFieldMD2B1 = new JTextField[]{
            TextFieldMD2B1s1, TextFieldMD2B1s2, TextFieldMD2B1s3,
            TextFieldMD2B1s4, TextFieldMD2B1s5, TextFieldMD2B1s6,
            TextFieldMD2B1s7};

        TextFieldMD3A0 = new JTextField[]{
            TextFieldMD3A0s1, TextFieldMD3A0s2, TextFieldMD3A0s3,
            TextFieldMD3A0s4, TextFieldMD3A0s5, TextFieldMD3A0s6,
            TextFieldMD3A0s7};

        TextFieldMD3A1 = new JTextField[]{
            TextFieldMD3A1s1, TextFieldMD3A1s2, TextFieldMD3A1s3,
            TextFieldMD3A1s4, TextFieldMD3A1s5, TextFieldMD3A1s6,
            TextFieldMD3A1s7};

        TextFieldMD3B0 = new JTextField[]{
            TextFieldMD3B0s1, TextFieldMD3B0s2, TextFieldMD3B0s3,
            TextFieldMD3B0s4, TextFieldMD3B0s5, TextFieldMD3B0s6,
            TextFieldMD3B0s7};

        TextFieldMD3B1 = new JTextField[]{
            TextFieldMD3B1s1, TextFieldMD3B1s2, TextFieldMD3B1s3,
            TextFieldMD3B1s4, TextFieldMD3B1s5, TextFieldMD3B1s6,
            TextFieldMD3B1s7};

        TextFieldMD4A0 = new JTextField[]{
            TextFieldMD4A0s1, TextFieldMD4A0s2, TextFieldMD4A0s3,
            TextFieldMD4A0s4, TextFieldMD4A0s5, TextFieldMD4A0s6,
            TextFieldMD4A0s7};

        TextFieldMD4A1 = new JTextField[]{
            TextFieldMD4A1s1, TextFieldMD4A1s2, TextFieldMD4A1s3,
            TextFieldMD4A1s4, TextFieldMD4A1s5, TextFieldMD4A1s6,
            TextFieldMD4A1s7};

        TextFieldMD4B0 = new JTextField[]{
            TextFieldMD4B0s1, TextFieldMD4B0s2, TextFieldMD4B0s3,
            TextFieldMD4B0s4, TextFieldMD4B0s5, TextFieldMD4B0s6,
            TextFieldMD4B0s7};

        TextFieldMD4B1 = new JTextField[]{
            TextFieldMD4B1s1, TextFieldMD4B1s2, TextFieldMD4B1s3,
            TextFieldMD4B1s4, TextFieldMD4B1s5, TextFieldMD4B1s6,
            TextFieldMD4B1s7};

        // First dimension is minidrawer. Second is link (A0,A1,B0,B1) and third is data.
        TextFieldMD = new JTextField[4][4][7];
        for (int i = 0; i < 7; i++) {
            TextFieldMD[0][0][i] = TextFieldMD1A0[i];
            TextFieldMD[0][1][i] = TextFieldMD1A1[i];
            TextFieldMD[0][2][i] = TextFieldMD1B0[i];
            TextFieldMD[0][3][i] = TextFieldMD1B1[i];

            TextFieldMD[1][0][i] = TextFieldMD2A0[i];
            TextFieldMD[1][1][i] = TextFieldMD2A1[i];
            TextFieldMD[1][2][i] = TextFieldMD2B0[i];
            TextFieldMD[1][3][i] = TextFieldMD2B1[i];

            TextFieldMD[2][0][i] = TextFieldMD3A0[i];
            TextFieldMD[2][1][i] = TextFieldMD3A1[i];
            TextFieldMD[2][2][i] = TextFieldMD3B0[i];
            TextFieldMD[2][3][i] = TextFieldMD3B1[i];

            TextFieldMD[3][0][i] = TextFieldMD4A0[i];
            TextFieldMD[3][1][i] = TextFieldMD4A1[i];
            TextFieldMD[3][2][i] = TextFieldMD4B0[i];
            TextFieldMD[3][3][i] = TextFieldMD4B1[i];
        }

        latencies = new ArrayList<>();

        // Defines Runnable ArrayList for QSFP Resets.
        RunnablesResetQSFP = new ArrayList<>();

        // Define Runnables through lambda expressions.
        initDefineRunnables();

        // Defines instance of ThreadPoolExecutor class to create a thread pool.
        // A thread pool manages a collection of Runnable threads.   
        executor = new ThreadPoolExecutor(
                5,
                5,
                0L,
                TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>(),
                new TBThreadFactory("PPrDemoCRCChecks"
                        + ""));

        // Defines an instance of the ScheduledThreadPoolExecutor class, a subclass of the ThreadPoolExecutor 
        // that can additionally schedule commands to run after a given delay, 
        // or to execute periodically.
        executorScheduled = new ScheduledThreadPoolExecutor(
                5,
                new TBThreadFactory("PPrDemoCRCChecksMonitor"));
    }

    /**
     * Defines Runnables for threads. A Runnable object is an interface designed
     * to provide a common protocol for objects that wish to execute code while
     * they are active
     */
    private void initDefineRunnables() {
        RunnableReadCRCChecks = () -> {
            readCRCChecks();
        };

        RunnableUpdateScheduler = () -> {
            read_update_scheduler();
        };

        for (int i = 0; i < 4; i++) {
            final int iFinal = i;
            RunnablesResetQSFP.add((Runnable) () -> {
                resetQSFP(iFinal);
            });
        }
    }

    /**
     * Gets minidrawers to process.
     */
    private synchronized void get_selected_MDs() {
        selected_MD.clear();
        selected_MD.add(CheckBoxMD1.isSelected() ? 1 : 0);
        selected_MD.add(CheckBoxMD2.isSelected() ? 1 : 0);
        selected_MD.add(CheckBoxMD3.isSelected() ? 1 : 0);
        selected_MD.add(CheckBoxMD4.isSelected() ? 1 : 0);
    }

    /**
     * Resets selected PPrDemo QSFP transceiver.
     *
     * @param qsfp Input QSFP transceiver to be reset (0 to 3).
     */
    private synchronized void resetQSFP(Integer qsfp) {
        Boolean ret = pprlib.ppr.reset_QSFP_Link(qsfp);
        String fs = "PPrDemoLinksStatus.resetQSFP(): reset QSFP %d -> %s";
        Console.print_cr(String.format(fs, qsfp + 1, (ret != null ? "Ok" : "Fail")));
    }

    /**
     *
     */
    private synchronized void readCRCChecks() {
        Integer val;
        Integer valA, valB;
        String fs;
        Long lval;
        Double dval;

        get_selected_MDs();

        fs = "%s %d %d %d %d";
        Console.print_cr(String.format(fs, "PPrDemoLinksStatus.readCRCChecks() -> minidrawers: ",
                selected_MD.get(0), selected_MD.get(1), selected_MD.get(2), selected_MD.get(3)));

        // Loop on active minidrawers.
        for (int md = 0; md < 4; md++) {
            if (selected_MD.get(md) == 1) {

                fs = "  MD%d:";
                Console.print_cr(String.format(fs, md + 1));

                lval = pprlib.ppr.get_frames(md);

                valA = pprlib.ppr.get_CRC_tot_errors(md, "A");
                valB = pprlib.ppr.get_CRC_tot_errors(md, "B");

                if (latencies != null) {
                    latencies.clear();
                }

                latencies = pprlib.ppr.get_latencies(md);

                // Loop on links for each minidrawer
                for (int i = 0; i < 4; i++) {

                    // Get Nb of frames.
                    fs = "  Link %s frames read status: %s";
                    Console.print_cr(String.format(fs, link_strings[i], (lval != null ? "Ok" : "Fail")));
                    if (lval == null) {
                        TextFieldMD[md][i][0].setText("Error");
                        TextFieldMD[md][i][0].setForeground(Color.RED);
                    } else {
                        fs = "  Link %s nb of frames: 0x%016X (%d)";
                        Console.print_cr(String.format(fs, link_strings[i], lval, lval));
                        TextFieldMD[md][i][0].setText(String.format("%d", lval));
                        TextFieldMD[md][i][0].setForeground(Color.black);
                    }

                    // Get CRC frame errors.
                    val = pprlib.ppr.get_CRC_errors(md, link_strings[i]);
                    fs = "  Link %s CRC errors read status: %s";
                    Console.print_cr(String.format(fs, link_strings[i], (val != null ? "Ok" : "Fail")));
                    if (val == null) {
                        TextFieldMD[md][i][1].setText("Error");
                        TextFieldMD[md][i][1].setForeground(Color.RED);
                    } else {
                        fs = "  Link %s CRC errors: 0x%08X (%d) -> %s";
                        Console.print_cr(String.format(fs, link_strings[i], val, val, ByteTools.intToString(val, 4)));
                        if (val <= -1) {
                            val = Integer.MAX_VALUE;
                        }
                        TextFieldMD[md][i][1].setText(String.format("%d", val));
                        TextFieldMD[md][i][1].setForeground(Color.black);
                    }

                    // Get CRC fraction per million errors.
                    if (val == null || lval == null) {
                        TextFieldMD[md][i][2].setText("Error");
                        TextFieldMD[md][i][2].setForeground(Color.RED);
                    } else {
                        dval = pprlib.ppr.get_CRC_fraction_per_million(val, lval);
                        fs = "  Link %s fraction per million: %f";
                        Console.print_cr(String.format(fs, link_strings[i], dval));
                        TextFieldMD[md][i][2].setText(String.format("%.1f", dval));
                        TextFieldMD[md][i][2].setForeground(Color.black);
                    }

                    // Get CRC bit error rate.
                    if (val == null || lval == null) {
                        TextFieldMD[md][i][3].setText("Error");
                        TextFieldMD[md][i][3].setForeground(Color.RED);
                    } else {
                        dval = pprlib.ppr.get_CRC_BER(val, lval);
                        fs = "  Link %s bit error rate: %f";
                        Console.print_cr(String.format(fs, link_strings[i], dval));
                        TextFieldMD[md][i][3].setText(String.format("%.4e", dval));
                        TextFieldMD[md][i][3].setForeground(Color.black);
                    }

                    // Get Effective (total) errors.
                    if (i == 0 || i == 1) {
                        if (valA == null) {
                            TextFieldMD[md][i][4].setText("Error");
                            TextFieldMD[md][i][4].setForeground(Color.RED);
                        } else {
                            fs = "  Link %s total effective errors: 0x%08X (%d)";
                            Console.print_cr(String.format(fs, link_strings[i], valA, valA));
                            if (valA <= -1) {
                                valA = Integer.MAX_VALUE;
                            }
                            TextFieldMD[md][i][4].setText(String.format("%d", valA));
                            TextFieldMD[md][i][4].setForeground(Color.black);
                        }
                    } else {
                        if (valB == null) {
                            TextFieldMD[md][i][4].setText("Error");
                            TextFieldMD[md][i][4].setForeground(Color.RED);
                        } else {
                            fs = "  Link %s total effective errors: 0x%08X (%d)";
                            Console.print_cr(String.format(fs, link_strings[i], valB, valB));
                            if (valB <= -1) {
                                valB = Integer.MAX_VALUE;
                            }
                            TextFieldMD[md][i][4].setText(String.format("%d", valB));
                            TextFieldMD[md][i][4].setForeground(Color.black);
                        }
                    }

                    // Get latencies.
                    if (i == 0 || i == 1) {
                        if (latencies == null) {
                            TextFieldMD[md][i][6].setText("Error");
                            TextFieldMD[md][i][6].setForeground(Color.RED);
                        } else {
                            fs = "  Link %s latency: %d";
                            Console.print_cr(String.format(fs, link_strings[i], latencies.get(0)));
                            TextFieldMD[md][i][6].setText(String.format("%d", latencies.get(0)));
                            TextFieldMD[md][i][6].setForeground(Color.black);
                        }
                    } else {
                        if (latencies == null) {
                            TextFieldMD[md][i][6].setText("Error");
                            TextFieldMD[md][i][6].setForeground(Color.RED);
                        } else {
                            fs = "  Link %s latency: %d";
                            Console.print_cr(String.format(fs, link_strings[i], latencies.get(1)));
                            TextFieldMD[md][i][6].setText(String.format("%d", latencies.get(1)));
                            TextFieldMD[md][i][6].setForeground(Color.black);
                        }
                    }
                }
            }
        }
    }

    /**
     * Runnable task used by the ScheduledThreadPoolExecutor. It reads all V6533
     * VME HV registers. Then set corresponding fields and updates panel with
     * field values.
     */
    private synchronized void read_update_scheduler() {
        if (!IPbusPanel.getConnectFlag()) {
            // Attempts to cancel execution of this task. This attempt will fail
            // if the task has already completed, has already been cancelled,
            // or could not be cancelled for some other reason.
            Boolean ret = scheduledFuture.cancel(true);

            String fs = "%s %s";
            Console.print_cr(String.format(fs, "PPrDemoCRCChecks -> scheduler cancelled (no IPbus connection)... ",
                    (ret ? "Ok." : "Failed.")));

            return;
        }

        String sl = String.format("read_update_scheduler() -> refresh interval %d sec",
                SliderReadUpdate.getValue());
        Console.print_cr(sl);

        Console.print_cr("read_update_scheduler() -> elapsed time: "
                + LocalDateTime.now());

        readCRCChecks();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    private void initComponents() {//GEN-BEGIN:initComponents
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jPanel34 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        TextFieldMD1A0s1 = new javax.swing.JTextField();
        TextFieldMD1A0s2 = new javax.swing.JTextField();
        TextFieldMD1A0s3 = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        TextFieldMD1A1s1 = new javax.swing.JTextField();
        TextFieldMD1B0s1 = new javax.swing.JTextField();
        TextFieldMD1B1s1 = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        TextFieldMD1A0s4 = new javax.swing.JTextField();
        TextFieldMD1A0s5 = new javax.swing.JTextField();
        TextFieldMD1A0s6 = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        TextFieldMD1A0s7 = new javax.swing.JTextField();
        TextFieldMD1A1s2 = new javax.swing.JTextField();
        TextFieldMD1B0s2 = new javax.swing.JTextField();
        TextFieldMD1B1s2 = new javax.swing.JTextField();
        TextFieldMD1A1s3 = new javax.swing.JTextField();
        TextFieldMD1B0s3 = new javax.swing.JTextField();
        TextFieldMD1B1s3 = new javax.swing.JTextField();
        TextFieldMD1A1s4 = new javax.swing.JTextField();
        TextFieldMD1B0s5 = new javax.swing.JTextField();
        TextFieldMD1B1s6 = new javax.swing.JTextField();
        TextFieldMD1A1s6 = new javax.swing.JTextField();
        TextFieldMD1A1s5 = new javax.swing.JTextField();
        TextFieldMD1A1s7 = new javax.swing.JTextField();
        TextFieldMD1B0s7 = new javax.swing.JTextField();
        TextFieldMD1B1s5 = new javax.swing.JTextField();
        TextFieldMD1B0s4 = new javax.swing.JTextField();
        TextFieldMD1B1s4 = new javax.swing.JTextField();
        TextFieldMD1B0s6 = new javax.swing.JTextField();
        TextFieldMD1B1s7 = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jPanel36 = new javax.swing.JPanel();
        jLabel39 = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        TextFieldMD2A0s1 = new javax.swing.JTextField();
        TextFieldMD2A0s2 = new javax.swing.JTextField();
        TextFieldMD2A0s3 = new javax.swing.JTextField();
        jLabel43 = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        TextFieldMD2A1s1 = new javax.swing.JTextField();
        TextFieldMD2B0s1 = new javax.swing.JTextField();
        TextFieldMD2B1s1 = new javax.swing.JTextField();
        jLabel46 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        jLabel48 = new javax.swing.JLabel();
        TextFieldMD2A0s4 = new javax.swing.JTextField();
        TextFieldMD2A0s5 = new javax.swing.JTextField();
        TextFieldMD2A0s6 = new javax.swing.JTextField();
        jLabel49 = new javax.swing.JLabel();
        TextFieldMD2A0s7 = new javax.swing.JTextField();
        TextFieldMD2A1s2 = new javax.swing.JTextField();
        TextFieldMD2B0s2 = new javax.swing.JTextField();
        TextFieldMD2B1s2 = new javax.swing.JTextField();
        TextFieldMD2A1s3 = new javax.swing.JTextField();
        TextFieldMD2B0s3 = new javax.swing.JTextField();
        TextFieldMD2B1s3 = new javax.swing.JTextField();
        TextFieldMD2A1s4 = new javax.swing.JTextField();
        TextFieldMD2B0s5 = new javax.swing.JTextField();
        TextFieldMD2B1s6 = new javax.swing.JTextField();
        TextFieldMD2A1s6 = new javax.swing.JTextField();
        TextFieldMD2A1s5 = new javax.swing.JTextField();
        TextFieldMD2A1s7 = new javax.swing.JTextField();
        TextFieldMD2B0s7 = new javax.swing.JTextField();
        TextFieldMD2B1s5 = new javax.swing.JTextField();
        TextFieldMD2B0s4 = new javax.swing.JTextField();
        TextFieldMD2B1s4 = new javax.swing.JTextField();
        TextFieldMD2B0s6 = new javax.swing.JTextField();
        TextFieldMD2B1s7 = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jPanel37 = new javax.swing.JPanel();
        jLabel50 = new javax.swing.JLabel();
        jLabel51 = new javax.swing.JLabel();
        jLabel52 = new javax.swing.JLabel();
        jLabel53 = new javax.swing.JLabel();
        TextFieldMD3A0s1 = new javax.swing.JTextField();
        TextFieldMD3A0s2 = new javax.swing.JTextField();
        TextFieldMD3A0s3 = new javax.swing.JTextField();
        jLabel54 = new javax.swing.JLabel();
        jLabel55 = new javax.swing.JLabel();
        jLabel56 = new javax.swing.JLabel();
        TextFieldMD3A1s1 = new javax.swing.JTextField();
        TextFieldMD3B0s1 = new javax.swing.JTextField();
        TextFieldMD3B1s1 = new javax.swing.JTextField();
        jLabel57 = new javax.swing.JLabel();
        jLabel58 = new javax.swing.JLabel();
        jLabel59 = new javax.swing.JLabel();
        TextFieldMD3A0s4 = new javax.swing.JTextField();
        TextFieldMD3A0s5 = new javax.swing.JTextField();
        TextFieldMD3A0s6 = new javax.swing.JTextField();
        jLabel60 = new javax.swing.JLabel();
        TextFieldMD3A0s7 = new javax.swing.JTextField();
        TextFieldMD3A1s2 = new javax.swing.JTextField();
        TextFieldMD3B0s2 = new javax.swing.JTextField();
        TextFieldMD3B1s2 = new javax.swing.JTextField();
        TextFieldMD3A1s3 = new javax.swing.JTextField();
        TextFieldMD3B0s3 = new javax.swing.JTextField();
        TextFieldMD3B1s3 = new javax.swing.JTextField();
        TextFieldMD3A1s4 = new javax.swing.JTextField();
        TextFieldMD3B0s5 = new javax.swing.JTextField();
        TextFieldMD3B1s6 = new javax.swing.JTextField();
        TextFieldMD3A1s6 = new javax.swing.JTextField();
        TextFieldMD3A1s5 = new javax.swing.JTextField();
        TextFieldMD3A1s7 = new javax.swing.JTextField();
        TextFieldMD3B0s7 = new javax.swing.JTextField();
        TextFieldMD3B1s5 = new javax.swing.JTextField();
        TextFieldMD3B0s4 = new javax.swing.JTextField();
        TextFieldMD3B1s4 = new javax.swing.JTextField();
        TextFieldMD3B0s6 = new javax.swing.JTextField();
        TextFieldMD3B1s7 = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jPanel38 = new javax.swing.JPanel();
        jLabel61 = new javax.swing.JLabel();
        jLabel62 = new javax.swing.JLabel();
        jLabel63 = new javax.swing.JLabel();
        jLabel64 = new javax.swing.JLabel();
        TextFieldMD4A0s1 = new javax.swing.JTextField();
        TextFieldMD4A0s2 = new javax.swing.JTextField();
        TextFieldMD4A0s3 = new javax.swing.JTextField();
        jLabel65 = new javax.swing.JLabel();
        jLabel66 = new javax.swing.JLabel();
        jLabel67 = new javax.swing.JLabel();
        TextFieldMD4A1s1 = new javax.swing.JTextField();
        TextFieldMD4B0s1 = new javax.swing.JTextField();
        TextFieldMD4B1s1 = new javax.swing.JTextField();
        jLabel68 = new javax.swing.JLabel();
        jLabel69 = new javax.swing.JLabel();
        jLabel70 = new javax.swing.JLabel();
        TextFieldMD4A0s4 = new javax.swing.JTextField();
        TextFieldMD4A0s5 = new javax.swing.JTextField();
        TextFieldMD4A0s6 = new javax.swing.JTextField();
        jLabel71 = new javax.swing.JLabel();
        TextFieldMD4A0s7 = new javax.swing.JTextField();
        TextFieldMD4A1s2 = new javax.swing.JTextField();
        TextFieldMD4B0s2 = new javax.swing.JTextField();
        TextFieldMD4B1s2 = new javax.swing.JTextField();
        TextFieldMD4A1s3 = new javax.swing.JTextField();
        TextFieldMD4B0s3 = new javax.swing.JTextField();
        TextFieldMD4B1s3 = new javax.swing.JTextField();
        TextFieldMD4A1s4 = new javax.swing.JTextField();
        TextFieldMD4B0s5 = new javax.swing.JTextField();
        TextFieldMD4B1s6 = new javax.swing.JTextField();
        TextFieldMD4A1s6 = new javax.swing.JTextField();
        TextFieldMD4A1s5 = new javax.swing.JTextField();
        TextFieldMD4A1s7 = new javax.swing.JTextField();
        TextFieldMD4B0s7 = new javax.swing.JTextField();
        TextFieldMD4B1s5 = new javax.swing.JTextField();
        TextFieldMD4B0s4 = new javax.swing.JTextField();
        TextFieldMD4B1s4 = new javax.swing.JTextField();
        TextFieldMD4B0s6 = new javax.swing.JTextField();
        TextFieldMD4B1s7 = new javax.swing.JTextField();
        jPanel15 = new javax.swing.JPanel();
        jPanel11 = new javax.swing.JPanel();
        jPanel12 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        CheckBoxReadUpdate = new javax.swing.JCheckBox();
        SliderReadUpdate = new javax.swing.JSlider();
        ButtonRead = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        CheckBoxMD1 = new javax.swing.JCheckBox();
        CheckBoxMD2 = new javax.swing.JCheckBox();
        CheckBoxMD3 = new javax.swing.JCheckBox();
        CheckBoxMD4 = new javax.swing.JCheckBox();
        jPanel13 = new javax.swing.JPanel();
        jPanel14 = new javax.swing.JPanel();
        ButtonResetQ1 = new javax.swing.JButton();
        ButtonResetQ2 = new javax.swing.JButton();
        ButtonResetQ3 = new javax.swing.JButton();
        ButtonResetQ4 = new javax.swing.JButton();

        java.awt.GridBagLayout jPanel1Layout = new java.awt.GridBagLayout();
        jPanel1Layout.columnWidths = new int[] {0};
        jPanel1Layout.rowHeights = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0};
        jPanel1.setLayout(jPanel1Layout);

        jPanel9.setLayout(new java.awt.GridBagLayout());

        jPanel34.setBorder(javax.swing.BorderFactory.createTitledBorder("MD1"));
        java.awt.GridBagLayout jPanel34Layout = new java.awt.GridBagLayout();
        jPanel34Layout.columnWidths = new int[] {0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0};
        jPanel34Layout.rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0};
        jPanel34.setLayout(jPanel34Layout);

        jLabel17.setText("Nb of Frames");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel34.add(jLabel17, gridBagConstraints);

        jLabel18.setText("CRC Frame Errors");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel34.add(jLabel18, gridBagConstraints);

        jLabel19.setText("Fraction per Million");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 0;
        jPanel34.add(jLabel19, gridBagConstraints);

        jLabel20.setText("Side A0");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel34.add(jLabel20, gridBagConstraints);

        TextFieldMD1A0s1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD1A0s1.setText("0");
        TextFieldMD1A0s1.setPreferredSize(new java.awt.Dimension(150, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 2;
        jPanel34.add(TextFieldMD1A0s1, gridBagConstraints);

        TextFieldMD1A0s2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD1A0s2.setText("0");
        TextFieldMD1A0s2.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 2;
        jPanel34.add(TextFieldMD1A0s2, gridBagConstraints);

        TextFieldMD1A0s3.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD1A0s3.setText("0");
        TextFieldMD1A0s3.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 2;
        jPanel34.add(TextFieldMD1A0s3, gridBagConstraints);

        jLabel21.setText("Side A1");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel34.add(jLabel21, gridBagConstraints);

        jLabel22.setText("Side B0");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel34.add(jLabel22, gridBagConstraints);

        jLabel23.setText("Side B1");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel34.add(jLabel23, gridBagConstraints);

        TextFieldMD1A1s1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD1A1s1.setText("0");
        TextFieldMD1A1s1.setPreferredSize(new java.awt.Dimension(150, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 4;
        jPanel34.add(TextFieldMD1A1s1, gridBagConstraints);

        TextFieldMD1B0s1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD1B0s1.setText("0");
        TextFieldMD1B0s1.setPreferredSize(new java.awt.Dimension(150, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 6;
        jPanel34.add(TextFieldMD1B0s1, gridBagConstraints);

        TextFieldMD1B1s1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD1B1s1.setText("0");
        TextFieldMD1B1s1.setPreferredSize(new java.awt.Dimension(150, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 8;
        jPanel34.add(TextFieldMD1B1s1, gridBagConstraints);

        jLabel24.setText("Bit Error Rate");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 0;
        jPanel34.add(jLabel24, gridBagConstraints);

        jLabel25.setText("Effective Errors");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 0;
        jPanel34.add(jLabel25, gridBagConstraints);

        jLabel26.setText("Downlink Status");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 0;
        jPanel34.add(jLabel26, gridBagConstraints);

        TextFieldMD1A0s4.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD1A0s4.setText("0");
        TextFieldMD1A0s4.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 2;
        jPanel34.add(TextFieldMD1A0s4, gridBagConstraints);

        TextFieldMD1A0s5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD1A0s5.setText("0");
        TextFieldMD1A0s5.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 2;
        jPanel34.add(TextFieldMD1A0s5, gridBagConstraints);

        TextFieldMD1A0s6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD1A0s6.setText("0");
        TextFieldMD1A0s6.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 2;
        jPanel34.add(TextFieldMD1A0s6, gridBagConstraints);

        jLabel27.setText("Latency");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 0;
        jPanel34.add(jLabel27, gridBagConstraints);

        TextFieldMD1A0s7.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD1A0s7.setText("0");
        TextFieldMD1A0s7.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 2;
        jPanel34.add(TextFieldMD1A0s7, gridBagConstraints);

        TextFieldMD1A1s2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD1A1s2.setText("0");
        TextFieldMD1A1s2.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 4;
        jPanel34.add(TextFieldMD1A1s2, gridBagConstraints);

        TextFieldMD1B0s2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD1B0s2.setText("0");
        TextFieldMD1B0s2.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 6;
        jPanel34.add(TextFieldMD1B0s2, gridBagConstraints);

        TextFieldMD1B1s2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD1B1s2.setText("0");
        TextFieldMD1B1s2.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 8;
        jPanel34.add(TextFieldMD1B1s2, gridBagConstraints);

        TextFieldMD1A1s3.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD1A1s3.setText("0");
        TextFieldMD1A1s3.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 4;
        jPanel34.add(TextFieldMD1A1s3, gridBagConstraints);

        TextFieldMD1B0s3.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD1B0s3.setText("0");
        TextFieldMD1B0s3.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 6;
        jPanel34.add(TextFieldMD1B0s3, gridBagConstraints);

        TextFieldMD1B1s3.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD1B1s3.setText("0");
        TextFieldMD1B1s3.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 8;
        jPanel34.add(TextFieldMD1B1s3, gridBagConstraints);

        TextFieldMD1A1s4.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD1A1s4.setText("0");
        TextFieldMD1A1s4.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 4;
        jPanel34.add(TextFieldMD1A1s4, gridBagConstraints);

        TextFieldMD1B0s5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD1B0s5.setText("0");
        TextFieldMD1B0s5.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 6;
        jPanel34.add(TextFieldMD1B0s5, gridBagConstraints);

        TextFieldMD1B1s6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD1B1s6.setText("0");
        TextFieldMD1B1s6.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 8;
        jPanel34.add(TextFieldMD1B1s6, gridBagConstraints);

        TextFieldMD1A1s6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD1A1s6.setText("0");
        TextFieldMD1A1s6.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 4;
        jPanel34.add(TextFieldMD1A1s6, gridBagConstraints);

        TextFieldMD1A1s5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD1A1s5.setText("0");
        TextFieldMD1A1s5.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 4;
        jPanel34.add(TextFieldMD1A1s5, gridBagConstraints);

        TextFieldMD1A1s7.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD1A1s7.setText("0");
        TextFieldMD1A1s7.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 4;
        jPanel34.add(TextFieldMD1A1s7, gridBagConstraints);

        TextFieldMD1B0s7.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD1B0s7.setText("0");
        TextFieldMD1B0s7.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 6;
        jPanel34.add(TextFieldMD1B0s7, gridBagConstraints);

        TextFieldMD1B1s5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD1B1s5.setText("0");
        TextFieldMD1B1s5.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 8;
        jPanel34.add(TextFieldMD1B1s5, gridBagConstraints);

        TextFieldMD1B0s4.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD1B0s4.setText("0");
        TextFieldMD1B0s4.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 6;
        jPanel34.add(TextFieldMD1B0s4, gridBagConstraints);

        TextFieldMD1B1s4.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD1B1s4.setText("0");
        TextFieldMD1B1s4.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 8;
        jPanel34.add(TextFieldMD1B1s4, gridBagConstraints);

        TextFieldMD1B0s6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD1B0s6.setText("0");
        TextFieldMD1B0s6.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 6;
        jPanel34.add(TextFieldMD1B0s6, gridBagConstraints);

        TextFieldMD1B1s7.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD1B1s7.setText("0");
        TextFieldMD1B1s7.setToolTipText("");
        TextFieldMD1B1s7.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 8;
        jPanel34.add(TextFieldMD1B1s7, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel9.add(jPanel34, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel1.add(jPanel9, gridBagConstraints);

        jPanel2.setLayout(new java.awt.GridBagLayout());

        jPanel36.setBorder(javax.swing.BorderFactory.createTitledBorder("MD2"));
        java.awt.GridBagLayout jPanel36Layout = new java.awt.GridBagLayout();
        jPanel36Layout.columnWidths = new int[] {0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0};
        jPanel36Layout.rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0};
        jPanel36.setLayout(jPanel36Layout);

        jLabel39.setText("Nb of Frames");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel36.add(jLabel39, gridBagConstraints);

        jLabel40.setText("CRC Frame Errors");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel36.add(jLabel40, gridBagConstraints);

        jLabel41.setText("Fraction per Million");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 0;
        jPanel36.add(jLabel41, gridBagConstraints);

        jLabel42.setText("Side A0");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel36.add(jLabel42, gridBagConstraints);

        TextFieldMD2A0s1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD2A0s1.setText("0");
        TextFieldMD2A0s1.setPreferredSize(new java.awt.Dimension(150, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 2;
        jPanel36.add(TextFieldMD2A0s1, gridBagConstraints);

        TextFieldMD2A0s2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD2A0s2.setText("0");
        TextFieldMD2A0s2.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 2;
        jPanel36.add(TextFieldMD2A0s2, gridBagConstraints);

        TextFieldMD2A0s3.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD2A0s3.setText("0");
        TextFieldMD2A0s3.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 2;
        jPanel36.add(TextFieldMD2A0s3, gridBagConstraints);

        jLabel43.setText("Side A1");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel36.add(jLabel43, gridBagConstraints);

        jLabel44.setText("Side B0");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel36.add(jLabel44, gridBagConstraints);

        jLabel45.setText("Side B1");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel36.add(jLabel45, gridBagConstraints);

        TextFieldMD2A1s1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD2A1s1.setText("0");
        TextFieldMD2A1s1.setPreferredSize(new java.awt.Dimension(150, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 4;
        jPanel36.add(TextFieldMD2A1s1, gridBagConstraints);

        TextFieldMD2B0s1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD2B0s1.setText("0");
        TextFieldMD2B0s1.setPreferredSize(new java.awt.Dimension(150, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 6;
        jPanel36.add(TextFieldMD2B0s1, gridBagConstraints);

        TextFieldMD2B1s1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD2B1s1.setText("0");
        TextFieldMD2B1s1.setPreferredSize(new java.awt.Dimension(150, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 8;
        jPanel36.add(TextFieldMD2B1s1, gridBagConstraints);

        jLabel46.setText("Bit Error Rate");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 0;
        jPanel36.add(jLabel46, gridBagConstraints);

        jLabel47.setText("Effective Errors");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 0;
        jPanel36.add(jLabel47, gridBagConstraints);

        jLabel48.setText("Downlink Status");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 0;
        jPanel36.add(jLabel48, gridBagConstraints);

        TextFieldMD2A0s4.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD2A0s4.setText("0");
        TextFieldMD2A0s4.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 2;
        jPanel36.add(TextFieldMD2A0s4, gridBagConstraints);

        TextFieldMD2A0s5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD2A0s5.setText("0");
        TextFieldMD2A0s5.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 2;
        jPanel36.add(TextFieldMD2A0s5, gridBagConstraints);

        TextFieldMD2A0s6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD2A0s6.setText("0");
        TextFieldMD2A0s6.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 2;
        jPanel36.add(TextFieldMD2A0s6, gridBagConstraints);

        jLabel49.setText("Latency");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 0;
        jPanel36.add(jLabel49, gridBagConstraints);

        TextFieldMD2A0s7.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD2A0s7.setText("0");
        TextFieldMD2A0s7.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 2;
        jPanel36.add(TextFieldMD2A0s7, gridBagConstraints);

        TextFieldMD2A1s2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD2A1s2.setText("0");
        TextFieldMD2A1s2.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 4;
        jPanel36.add(TextFieldMD2A1s2, gridBagConstraints);

        TextFieldMD2B0s2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD2B0s2.setText("0");
        TextFieldMD2B0s2.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 6;
        jPanel36.add(TextFieldMD2B0s2, gridBagConstraints);

        TextFieldMD2B1s2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD2B1s2.setText("0");
        TextFieldMD2B1s2.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 8;
        jPanel36.add(TextFieldMD2B1s2, gridBagConstraints);

        TextFieldMD2A1s3.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD2A1s3.setText("0");
        TextFieldMD2A1s3.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 4;
        jPanel36.add(TextFieldMD2A1s3, gridBagConstraints);

        TextFieldMD2B0s3.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD2B0s3.setText("0");
        TextFieldMD2B0s3.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 6;
        jPanel36.add(TextFieldMD2B0s3, gridBagConstraints);

        TextFieldMD2B1s3.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD2B1s3.setText("0");
        TextFieldMD2B1s3.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 8;
        jPanel36.add(TextFieldMD2B1s3, gridBagConstraints);

        TextFieldMD2A1s4.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD2A1s4.setText("0");
        TextFieldMD2A1s4.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 4;
        jPanel36.add(TextFieldMD2A1s4, gridBagConstraints);

        TextFieldMD2B0s5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD2B0s5.setText("0");
        TextFieldMD2B0s5.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 6;
        jPanel36.add(TextFieldMD2B0s5, gridBagConstraints);

        TextFieldMD2B1s6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD2B1s6.setText("0");
        TextFieldMD2B1s6.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 8;
        jPanel36.add(TextFieldMD2B1s6, gridBagConstraints);

        TextFieldMD2A1s6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD2A1s6.setText("0");
        TextFieldMD2A1s6.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 4;
        jPanel36.add(TextFieldMD2A1s6, gridBagConstraints);

        TextFieldMD2A1s5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD2A1s5.setText("0");
        TextFieldMD2A1s5.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 4;
        jPanel36.add(TextFieldMD2A1s5, gridBagConstraints);

        TextFieldMD2A1s7.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD2A1s7.setText("0");
        TextFieldMD2A1s7.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 4;
        jPanel36.add(TextFieldMD2A1s7, gridBagConstraints);

        TextFieldMD2B0s7.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD2B0s7.setText("0");
        TextFieldMD2B0s7.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 6;
        jPanel36.add(TextFieldMD2B0s7, gridBagConstraints);

        TextFieldMD2B1s5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD2B1s5.setText("0");
        TextFieldMD2B1s5.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 8;
        jPanel36.add(TextFieldMD2B1s5, gridBagConstraints);

        TextFieldMD2B0s4.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD2B0s4.setText("0");
        TextFieldMD2B0s4.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 6;
        jPanel36.add(TextFieldMD2B0s4, gridBagConstraints);

        TextFieldMD2B1s4.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD2B1s4.setText("0");
        TextFieldMD2B1s4.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 8;
        jPanel36.add(TextFieldMD2B1s4, gridBagConstraints);

        TextFieldMD2B0s6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD2B0s6.setText("0");
        TextFieldMD2B0s6.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 6;
        jPanel36.add(TextFieldMD2B0s6, gridBagConstraints);

        TextFieldMD2B1s7.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD2B1s7.setText("0");
        TextFieldMD2B1s7.setToolTipText("");
        TextFieldMD2B1s7.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 8;
        jPanel36.add(TextFieldMD2B1s7, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel2.add(jPanel36, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel1.add(jPanel2, gridBagConstraints);

        jPanel3.setLayout(new java.awt.GridBagLayout());

        jPanel37.setBorder(javax.swing.BorderFactory.createTitledBorder("MD3"));
        java.awt.GridBagLayout jPanel37Layout = new java.awt.GridBagLayout();
        jPanel37Layout.columnWidths = new int[] {0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0};
        jPanel37Layout.rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0};
        jPanel37.setLayout(jPanel37Layout);

        jLabel50.setText("Nb of Frames");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel37.add(jLabel50, gridBagConstraints);

        jLabel51.setText("CRC Frame Errors");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel37.add(jLabel51, gridBagConstraints);

        jLabel52.setText("Fraction per Million");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 0;
        jPanel37.add(jLabel52, gridBagConstraints);

        jLabel53.setText("Side A0");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel37.add(jLabel53, gridBagConstraints);

        TextFieldMD3A0s1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD3A0s1.setText("0");
        TextFieldMD3A0s1.setPreferredSize(new java.awt.Dimension(150, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 2;
        jPanel37.add(TextFieldMD3A0s1, gridBagConstraints);

        TextFieldMD3A0s2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD3A0s2.setText("0");
        TextFieldMD3A0s2.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 2;
        jPanel37.add(TextFieldMD3A0s2, gridBagConstraints);

        TextFieldMD3A0s3.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD3A0s3.setText("0");
        TextFieldMD3A0s3.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 2;
        jPanel37.add(TextFieldMD3A0s3, gridBagConstraints);

        jLabel54.setText("Side A1");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel37.add(jLabel54, gridBagConstraints);

        jLabel55.setText("Side B0");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel37.add(jLabel55, gridBagConstraints);

        jLabel56.setText("Side B1");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel37.add(jLabel56, gridBagConstraints);

        TextFieldMD3A1s1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD3A1s1.setText("0");
        TextFieldMD3A1s1.setPreferredSize(new java.awt.Dimension(150, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 4;
        jPanel37.add(TextFieldMD3A1s1, gridBagConstraints);

        TextFieldMD3B0s1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD3B0s1.setText("0");
        TextFieldMD3B0s1.setPreferredSize(new java.awt.Dimension(150, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 6;
        jPanel37.add(TextFieldMD3B0s1, gridBagConstraints);

        TextFieldMD3B1s1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD3B1s1.setText("0");
        TextFieldMD3B1s1.setPreferredSize(new java.awt.Dimension(150, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 8;
        jPanel37.add(TextFieldMD3B1s1, gridBagConstraints);

        jLabel57.setText("Bit Error Rate");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 0;
        jPanel37.add(jLabel57, gridBagConstraints);

        jLabel58.setText("Effective Errors");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 0;
        jPanel37.add(jLabel58, gridBagConstraints);

        jLabel59.setText("Downlink Status");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 0;
        jPanel37.add(jLabel59, gridBagConstraints);

        TextFieldMD3A0s4.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD3A0s4.setText("0");
        TextFieldMD3A0s4.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 2;
        jPanel37.add(TextFieldMD3A0s4, gridBagConstraints);

        TextFieldMD3A0s5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD3A0s5.setText("0");
        TextFieldMD3A0s5.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 2;
        jPanel37.add(TextFieldMD3A0s5, gridBagConstraints);

        TextFieldMD3A0s6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD3A0s6.setText("0");
        TextFieldMD3A0s6.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 2;
        jPanel37.add(TextFieldMD3A0s6, gridBagConstraints);

        jLabel60.setText("Latency");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 0;
        jPanel37.add(jLabel60, gridBagConstraints);

        TextFieldMD3A0s7.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD3A0s7.setText("0");
        TextFieldMD3A0s7.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 2;
        jPanel37.add(TextFieldMD3A0s7, gridBagConstraints);

        TextFieldMD3A1s2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD3A1s2.setText("0");
        TextFieldMD3A1s2.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 4;
        jPanel37.add(TextFieldMD3A1s2, gridBagConstraints);

        TextFieldMD3B0s2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD3B0s2.setText("0");
        TextFieldMD3B0s2.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 6;
        jPanel37.add(TextFieldMD3B0s2, gridBagConstraints);

        TextFieldMD3B1s2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD3B1s2.setText("0");
        TextFieldMD3B1s2.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 8;
        jPanel37.add(TextFieldMD3B1s2, gridBagConstraints);

        TextFieldMD3A1s3.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD3A1s3.setText("0");
        TextFieldMD3A1s3.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 4;
        jPanel37.add(TextFieldMD3A1s3, gridBagConstraints);

        TextFieldMD3B0s3.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD3B0s3.setText("0");
        TextFieldMD3B0s3.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 6;
        jPanel37.add(TextFieldMD3B0s3, gridBagConstraints);

        TextFieldMD3B1s3.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD3B1s3.setText("0");
        TextFieldMD3B1s3.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 8;
        jPanel37.add(TextFieldMD3B1s3, gridBagConstraints);

        TextFieldMD3A1s4.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD3A1s4.setText("0");
        TextFieldMD3A1s4.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 4;
        jPanel37.add(TextFieldMD3A1s4, gridBagConstraints);

        TextFieldMD3B0s5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD3B0s5.setText("0");
        TextFieldMD3B0s5.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 6;
        jPanel37.add(TextFieldMD3B0s5, gridBagConstraints);

        TextFieldMD3B1s6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD3B1s6.setText("0");
        TextFieldMD3B1s6.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 8;
        jPanel37.add(TextFieldMD3B1s6, gridBagConstraints);

        TextFieldMD3A1s6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD3A1s6.setText("0");
        TextFieldMD3A1s6.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 4;
        jPanel37.add(TextFieldMD3A1s6, gridBagConstraints);

        TextFieldMD3A1s5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD3A1s5.setText("0");
        TextFieldMD3A1s5.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 4;
        jPanel37.add(TextFieldMD3A1s5, gridBagConstraints);

        TextFieldMD3A1s7.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD3A1s7.setText("0");
        TextFieldMD3A1s7.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 4;
        jPanel37.add(TextFieldMD3A1s7, gridBagConstraints);

        TextFieldMD3B0s7.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD3B0s7.setText("0");
        TextFieldMD3B0s7.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 6;
        jPanel37.add(TextFieldMD3B0s7, gridBagConstraints);

        TextFieldMD3B1s5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD3B1s5.setText("0");
        TextFieldMD3B1s5.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 8;
        jPanel37.add(TextFieldMD3B1s5, gridBagConstraints);

        TextFieldMD3B0s4.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD3B0s4.setText("0");
        TextFieldMD3B0s4.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 6;
        jPanel37.add(TextFieldMD3B0s4, gridBagConstraints);

        TextFieldMD3B1s4.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD3B1s4.setText("0");
        TextFieldMD3B1s4.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 8;
        jPanel37.add(TextFieldMD3B1s4, gridBagConstraints);

        TextFieldMD3B0s6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD3B0s6.setText("0");
        TextFieldMD3B0s6.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 6;
        jPanel37.add(TextFieldMD3B0s6, gridBagConstraints);

        TextFieldMD3B1s7.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD3B1s7.setText("0");
        TextFieldMD3B1s7.setToolTipText("");
        TextFieldMD3B1s7.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 8;
        jPanel37.add(TextFieldMD3B1s7, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel3.add(jPanel37, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        jPanel1.add(jPanel3, gridBagConstraints);

        jPanel4.setLayout(new java.awt.GridBagLayout());

        jPanel38.setBorder(javax.swing.BorderFactory.createTitledBorder("MD4"));
        java.awt.GridBagLayout jPanel38Layout = new java.awt.GridBagLayout();
        jPanel38Layout.columnWidths = new int[] {0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0, 25, 0};
        jPanel38Layout.rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0};
        jPanel38.setLayout(jPanel38Layout);

        jLabel61.setText("Nb of Frames");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel38.add(jLabel61, gridBagConstraints);

        jLabel62.setText("CRC Frame Errors");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel38.add(jLabel62, gridBagConstraints);

        jLabel63.setText("Fraction per Million");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 0;
        jPanel38.add(jLabel63, gridBagConstraints);

        jLabel64.setText("Side A0");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel38.add(jLabel64, gridBagConstraints);

        TextFieldMD4A0s1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD4A0s1.setText("0");
        TextFieldMD4A0s1.setPreferredSize(new java.awt.Dimension(150, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 2;
        jPanel38.add(TextFieldMD4A0s1, gridBagConstraints);

        TextFieldMD4A0s2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD4A0s2.setText("0");
        TextFieldMD4A0s2.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 2;
        jPanel38.add(TextFieldMD4A0s2, gridBagConstraints);

        TextFieldMD4A0s3.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD4A0s3.setText("0");
        TextFieldMD4A0s3.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 2;
        jPanel38.add(TextFieldMD4A0s3, gridBagConstraints);

        jLabel65.setText("Side A1");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel38.add(jLabel65, gridBagConstraints);

        jLabel66.setText("Side B0");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel38.add(jLabel66, gridBagConstraints);

        jLabel67.setText("Side B1");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel38.add(jLabel67, gridBagConstraints);

        TextFieldMD4A1s1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD4A1s1.setText("0");
        TextFieldMD4A1s1.setPreferredSize(new java.awt.Dimension(150, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 4;
        jPanel38.add(TextFieldMD4A1s1, gridBagConstraints);

        TextFieldMD4B0s1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD4B0s1.setText("0");
        TextFieldMD4B0s1.setPreferredSize(new java.awt.Dimension(150, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 6;
        jPanel38.add(TextFieldMD4B0s1, gridBagConstraints);

        TextFieldMD4B1s1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD4B1s1.setText("0");
        TextFieldMD4B1s1.setPreferredSize(new java.awt.Dimension(150, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 8;
        jPanel38.add(TextFieldMD4B1s1, gridBagConstraints);

        jLabel68.setText("Bit Error Rate");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 0;
        jPanel38.add(jLabel68, gridBagConstraints);

        jLabel69.setText("Effective Errors");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 0;
        jPanel38.add(jLabel69, gridBagConstraints);

        jLabel70.setText("Downlink Status");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 0;
        jPanel38.add(jLabel70, gridBagConstraints);

        TextFieldMD4A0s4.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD4A0s4.setText("0");
        TextFieldMD4A0s4.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 2;
        jPanel38.add(TextFieldMD4A0s4, gridBagConstraints);

        TextFieldMD4A0s5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD4A0s5.setText("0");
        TextFieldMD4A0s5.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 2;
        jPanel38.add(TextFieldMD4A0s5, gridBagConstraints);

        TextFieldMD4A0s6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD4A0s6.setText("0");
        TextFieldMD4A0s6.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 2;
        jPanel38.add(TextFieldMD4A0s6, gridBagConstraints);

        jLabel71.setText("Latency");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 0;
        jPanel38.add(jLabel71, gridBagConstraints);

        TextFieldMD4A0s7.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD4A0s7.setText("0");
        TextFieldMD4A0s7.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 2;
        jPanel38.add(TextFieldMD4A0s7, gridBagConstraints);

        TextFieldMD4A1s2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD4A1s2.setText("0");
        TextFieldMD4A1s2.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 4;
        jPanel38.add(TextFieldMD4A1s2, gridBagConstraints);

        TextFieldMD4B0s2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD4B0s2.setText("0");
        TextFieldMD4B0s2.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 6;
        jPanel38.add(TextFieldMD4B0s2, gridBagConstraints);

        TextFieldMD4B1s2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD4B1s2.setText("0");
        TextFieldMD4B1s2.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 8;
        jPanel38.add(TextFieldMD4B1s2, gridBagConstraints);

        TextFieldMD4A1s3.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD4A1s3.setText("0");
        TextFieldMD4A1s3.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 4;
        jPanel38.add(TextFieldMD4A1s3, gridBagConstraints);

        TextFieldMD4B0s3.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD4B0s3.setText("0");
        TextFieldMD4B0s3.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 6;
        jPanel38.add(TextFieldMD4B0s3, gridBagConstraints);

        TextFieldMD4B1s3.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD4B1s3.setText("0");
        TextFieldMD4B1s3.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 8;
        jPanel38.add(TextFieldMD4B1s3, gridBagConstraints);

        TextFieldMD4A1s4.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD4A1s4.setText("0");
        TextFieldMD4A1s4.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 4;
        jPanel38.add(TextFieldMD4A1s4, gridBagConstraints);

        TextFieldMD4B0s5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD4B0s5.setText("0");
        TextFieldMD4B0s5.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 6;
        jPanel38.add(TextFieldMD4B0s5, gridBagConstraints);

        TextFieldMD4B1s6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD4B1s6.setText("0");
        TextFieldMD4B1s6.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 8;
        jPanel38.add(TextFieldMD4B1s6, gridBagConstraints);

        TextFieldMD4A1s6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD4A1s6.setText("0");
        TextFieldMD4A1s6.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 4;
        jPanel38.add(TextFieldMD4A1s6, gridBagConstraints);

        TextFieldMD4A1s5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD4A1s5.setText("0");
        TextFieldMD4A1s5.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 4;
        jPanel38.add(TextFieldMD4A1s5, gridBagConstraints);

        TextFieldMD4A1s7.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD4A1s7.setText("0");
        TextFieldMD4A1s7.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 4;
        jPanel38.add(TextFieldMD4A1s7, gridBagConstraints);

        TextFieldMD4B0s7.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD4B0s7.setText("0");
        TextFieldMD4B0s7.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 6;
        jPanel38.add(TextFieldMD4B0s7, gridBagConstraints);

        TextFieldMD4B1s5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD4B1s5.setText("0");
        TextFieldMD4B1s5.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 8;
        jPanel38.add(TextFieldMD4B1s5, gridBagConstraints);

        TextFieldMD4B0s4.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD4B0s4.setText("0");
        TextFieldMD4B0s4.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 6;
        jPanel38.add(TextFieldMD4B0s4, gridBagConstraints);

        TextFieldMD4B1s4.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD4B1s4.setText("0");
        TextFieldMD4B1s4.setPreferredSize(new java.awt.Dimension(100, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 8;
        jPanel38.add(TextFieldMD4B1s4, gridBagConstraints);

        TextFieldMD4B0s6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldMD4B0s6.setText("0");
        TextFieldMD4B0s6.setPreferredSize(new java.awt.Dimension(35, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 6;
        jPanel38.add(TextFieldMD4B0s6, gridBagConstraints);

        TextFieldMD4B1s7.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD4B1s7.setText("0");
        TextFieldMD4B1s7.setToolTipText("");
        TextFieldMD4B1s7.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 8;
        jPanel38.add(TextFieldMD4B1s7, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel4.add(jPanel38, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        jPanel1.add(jPanel4, gridBagConstraints);

        jPanel15.setLayout(new java.awt.GridBagLayout());

        java.awt.GridBagLayout jPanel11Layout = new java.awt.GridBagLayout();
        jPanel11Layout.columnWidths = new int[] {0, 15, 0, 15, 0};
        jPanel11Layout.rowHeights = new int[] {0};
        jPanel11.setLayout(jPanel11Layout);

        jPanel12.setLayout(new java.awt.GridBagLayout());

        java.awt.GridBagLayout jPanel6Layout = new java.awt.GridBagLayout();
        jPanel6Layout.columnWidths = new int[] {0, 10, 0, 10, 0, 10, 0};
        jPanel6Layout.rowHeights = new int[] {0, 5, 0, 5, 0};
        jPanel6.setLayout(jPanel6Layout);

        jPanel7.setLayout(new java.awt.GridBagLayout());

        CheckBoxReadUpdate.setText("Read Update (seconds)");
        CheckBoxReadUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckBoxReadUpdateActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel7.add(CheckBoxReadUpdate, gridBagConstraints);

        SliderReadUpdate.setMajorTickSpacing(4);
        SliderReadUpdate.setMaximum(30);
        SliderReadUpdate.setMinimum(1);
        SliderReadUpdate.setMinorTickSpacing(1);
        SliderReadUpdate.setPaintLabels(true);
        SliderReadUpdate.setPaintTicks(true);
        SliderReadUpdate.setSnapToTicks(true);
        SliderReadUpdate.setValue(2);
        SliderReadUpdate.setPreferredSize(new java.awt.Dimension(230, 52));
        SliderReadUpdate.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                SliderReadUpdateStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        jPanel7.add(SliderReadUpdate, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 4;
        jPanel6.add(jPanel7, gridBagConstraints);

        ButtonRead.setText("Read");
        ButtonRead.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonReadActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        jPanel6.add(ButtonRead, gridBagConstraints);

        jPanel8.setLayout(new java.awt.GridBagLayout());

        java.awt.GridBagLayout jPanel10Layout = new java.awt.GridBagLayout();
        jPanel10Layout.columnWidths = new int[] {0, 10, 0, 10, 0, 10, 0};
        jPanel10Layout.rowHeights = new int[] {0};
        jPanel10.setLayout(jPanel10Layout);

        CheckBoxMD1.setSelected(true);
        CheckBoxMD1.setText("MD1");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel10.add(CheckBoxMD1, gridBagConstraints);

        CheckBoxMD2.setSelected(true);
        CheckBoxMD2.setText("MD2");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel10.add(CheckBoxMD2, gridBagConstraints);

        CheckBoxMD3.setSelected(true);
        CheckBoxMD3.setText("MD3");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel10.add(CheckBoxMD3, gridBagConstraints);

        CheckBoxMD4.setSelected(true);
        CheckBoxMD4.setText("MD4");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel10.add(CheckBoxMD4, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel8.add(jPanel10, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel6.add(jPanel8, gridBagConstraints);

        jPanel12.add(jPanel6, new java.awt.GridBagConstraints());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel11.add(jPanel12, gridBagConstraints);

        jPanel13.setLayout(new java.awt.GridBagLayout());

        jPanel14.setLayout(new java.awt.GridBagLayout());

        ButtonResetQ1.setText("Reset Q1");
        ButtonResetQ1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonResetQ1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel14.add(ButtonResetQ1, gridBagConstraints);

        ButtonResetQ2.setText("Reset Q2");
        ButtonResetQ2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonResetQ2ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel14.add(ButtonResetQ2, gridBagConstraints);

        ButtonResetQ3.setText("Reset Q3");
        ButtonResetQ3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonResetQ3ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        jPanel14.add(ButtonResetQ3, gridBagConstraints);

        ButtonResetQ4.setText("Reset Q4");
        ButtonResetQ4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonResetQ4ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        jPanel14.add(ButtonResetQ4, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        jPanel13.add(jPanel14, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel11.add(jPanel13, gridBagConstraints);

        jPanel15.add(jPanel11, new java.awt.GridBagConstraints());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        jPanel1.add(jPanel15, gridBagConstraints);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(66, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(132, Short.MAX_VALUE))
        );
    }//GEN-END:initComponents

    /**
     * Starts an executor scheduling Runnable task through a
     * ScheduledThreadPoolExecutor. Only if connected and if the automatic
     * update checkbox is set. Otherwise do nothing.
     *
     * @param evt Input ActionEvent object.
     */
    private void CheckBoxReadUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckBoxReadUpdateActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            return;
        }

        if (CheckBoxReadUpdate.isSelected()) {
            Console.print_cr("PPrDemoCRCChecks -> read continuously each "
                    + SliderReadUpdate.getValue() + " seconds...");

            // Changes label CRC checks in MainFrame.
            MainFrame.LabelCRCChecksMainFrame.setText("running");
            MainFrame.LabelCRCChecksMainFrame.setForeground(Color.GREEN.darker());

            // Creates and executes a periodic action that becomes enabled first
            // after the given initial delay, and subsequently with the given
            // period;
            scheduledFuture = executorScheduled.scheduleAtFixedRate(
                    RunnableUpdateScheduler,
                    0,
                    SliderReadUpdate.getValue(),
                    TimeUnit.SECONDS);
        } else {
            // Attempts to cancel execution of this task. This attempt will fail
            // if the task has already completed, has already been cancelled,
            // or could not be cancelled for some other reason.
            Boolean ret = scheduledFuture.cancel(true);

            Console.print_cr("PPrDemoCRCChecks -> scheduler cancel status: "
                    + (ret ? "Ok" : "Failed"));

            // Changes label CRC checks in MainFrame.
            MainFrame.LabelCRCChecksMainFrame.setText("not running");
            MainFrame.LabelCRCChecksMainFrame.setForeground(Color.RED);
        }
    }//GEN-LAST:event_CheckBoxReadUpdateActionPerformed

    /**
     * Action to be performed on changing the Slider update time. The present
     * scheduled executor is cancelled and a new one is executed with the new
     * update time. Works only if automatic update check box is active and there
     * is an IPbus connection.
     *
     * @param evt Input ActionEvent object.
     */
    private void SliderReadUpdateStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_SliderReadUpdateStateChanged
        // Needed in order not to call this method several times
        // upon a change of a state of the slider
        JSlider source = (JSlider) evt.getSource();
        if (!source.getValueIsAdjusting()) {

            // Perform action only if connected and in the automatic
            // read update box is set. Otherwise do nothing.
            if ((CheckBoxReadUpdate.isSelected())
                    && IPbusPanel.getConnectFlag()) {

                // Attempts to cancel execution of this task. This attempt will fail
                // if the task has already completed, has already been cancelled,
                // or could not be cancelled for some other reason.
                Boolean ret = scheduledFuture.cancel(true);

                Console.print_cr("PPrDemoCRCChecks -> scheduler cancelled: "
                        + (ret ? "Ok." : "Failed.") + " New update time: "
                        + SliderReadUpdate.getValue() + " seconds..."
                );

                scheduledFuture = executorScheduled.scheduleAtFixedRate(
                        RunnableUpdateScheduler,
                        0,
                        SliderReadUpdate.getValue(),
                        TimeUnit.SECONDS);
            }
        }
    }//GEN-LAST:event_SliderReadUpdateStateChanged

    private void ButtonReadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonReadActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoCRCChecks -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnableReadCRCChecks);
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonReadActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonReadActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonReadActionPerformed

    /**
     * Sends a global reset to PPr QSFP transceiver 1.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonResetQ1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonResetQ1ActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksCtrl -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetQSFP.get(0));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonResetQ1ActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonResetQ1ActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonResetQ1ActionPerformed

    /**
     * Sends a global reset to PPr QSFP transceiver 2.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonResetQ2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonResetQ2ActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksCtrl -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetQSFP.get(1));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonResetQ2ActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonResetQ2ActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonResetQ2ActionPerformed

    /**
     * Sends a global reset to PPr QSFP transceiver 3.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonResetQ3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonResetQ3ActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksCtrl -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetQSFP.get(2));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonResetQ2ActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonResetQ2ActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonResetQ3ActionPerformed

    /**
     * Sends a global reset to PPr QSFP transceiver 4.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonResetQ4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonResetQ4ActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksCtrl -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetQSFP.get(3));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonResetQ2ActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonResetQ2ActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonResetQ4ActionPerformed

    /**
     * Declares a PPr class object to communicate with PPr IPbus.
     */
    private PPrLib pprlib;

    /**
     * Declares a ThreadPoolExecutor object. It executes each submitted task
     * using one of possibly several pooled threads, normally configured using
     * Executors factory methods.
     */
    public static ThreadPoolExecutor executor;

    /**
     * Declares a ScheduledThreadPoolExecutor object.
     */
    public static ScheduledThreadPoolExecutor executorScheduled;

    // Declares ScheduleFuture<V> interface (extends Future<V>). This is
    // a return from the ScheduledThreadPoolExecutor object method
    // scheduleAtFixedRate().
    private ScheduledFuture<?> scheduledFuture;

    /**
     * Declares Runnable for readCRCChecks() method.
     */
    private Runnable RunnableReadCRCChecks;

    /**
     * Declares Runnable ArrayList for QSFP Resets.
     */
    private final ArrayList<Runnable> RunnablesResetQSFP;

    /**
     *
     */
    private Runnable RunnableUpdateScheduler;

    /**
     * ArrayList with selected minidrawers to be filled with data.
     */
    private final ArrayList<Integer> selected_MD;

    String[] crc_check_info;

    String[] link_strings;

    String[] side_strings;

    private ArrayList<Integer> latencies;

    private static javax.swing.JTextField[] TextFieldMD1A0;
    private static javax.swing.JTextField[] TextFieldMD1A1;
    private static javax.swing.JTextField[] TextFieldMD1B0;
    private static javax.swing.JTextField[] TextFieldMD1B1;

    private static javax.swing.JTextField[] TextFieldMD2A0;
    private static javax.swing.JTextField[] TextFieldMD2A1;
    private static javax.swing.JTextField[] TextFieldMD2B0;
    private static javax.swing.JTextField[] TextFieldMD2B1;

    private static javax.swing.JTextField[] TextFieldMD3A0;
    private static javax.swing.JTextField[] TextFieldMD3A1;
    private static javax.swing.JTextField[] TextFieldMD3B0;
    private static javax.swing.JTextField[] TextFieldMD3B1;

    private static javax.swing.JTextField[] TextFieldMD4A0;
    private static javax.swing.JTextField[] TextFieldMD4A1;
    private static javax.swing.JTextField[] TextFieldMD4B0;
    private static javax.swing.JTextField[] TextFieldMD4B1;

    private static javax.swing.JTextField[][][] TextFieldMD;

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ButtonRead;
    private javax.swing.JButton ButtonResetQ1;
    private javax.swing.JButton ButtonResetQ2;
    private javax.swing.JButton ButtonResetQ3;
    private javax.swing.JButton ButtonResetQ4;
    private javax.swing.JCheckBox CheckBoxMD1;
    private javax.swing.JCheckBox CheckBoxMD2;
    private javax.swing.JCheckBox CheckBoxMD3;
    private javax.swing.JCheckBox CheckBoxMD4;
    public static javax.swing.JCheckBox CheckBoxReadUpdate;
    private javax.swing.JSlider SliderReadUpdate;
    private javax.swing.JTextField TextFieldMD1A0s1;
    private javax.swing.JTextField TextFieldMD1A0s2;
    private javax.swing.JTextField TextFieldMD1A0s3;
    private javax.swing.JTextField TextFieldMD1A0s4;
    private javax.swing.JTextField TextFieldMD1A0s5;
    private javax.swing.JTextField TextFieldMD1A0s6;
    private javax.swing.JTextField TextFieldMD1A0s7;
    private javax.swing.JTextField TextFieldMD1A1s1;
    private javax.swing.JTextField TextFieldMD1A1s2;
    private javax.swing.JTextField TextFieldMD1A1s3;
    private javax.swing.JTextField TextFieldMD1A1s4;
    private javax.swing.JTextField TextFieldMD1A1s5;
    private javax.swing.JTextField TextFieldMD1A1s6;
    private javax.swing.JTextField TextFieldMD1A1s7;
    private javax.swing.JTextField TextFieldMD1B0s1;
    private javax.swing.JTextField TextFieldMD1B0s2;
    private javax.swing.JTextField TextFieldMD1B0s3;
    private javax.swing.JTextField TextFieldMD1B0s4;
    private javax.swing.JTextField TextFieldMD1B0s5;
    private javax.swing.JTextField TextFieldMD1B0s6;
    private javax.swing.JTextField TextFieldMD1B0s7;
    private javax.swing.JTextField TextFieldMD1B1s1;
    private javax.swing.JTextField TextFieldMD1B1s2;
    private javax.swing.JTextField TextFieldMD1B1s3;
    private javax.swing.JTextField TextFieldMD1B1s4;
    private javax.swing.JTextField TextFieldMD1B1s5;
    private javax.swing.JTextField TextFieldMD1B1s6;
    private javax.swing.JTextField TextFieldMD1B1s7;
    private javax.swing.JTextField TextFieldMD2A0s1;
    private javax.swing.JTextField TextFieldMD2A0s2;
    private javax.swing.JTextField TextFieldMD2A0s3;
    private javax.swing.JTextField TextFieldMD2A0s4;
    private javax.swing.JTextField TextFieldMD2A0s5;
    private javax.swing.JTextField TextFieldMD2A0s6;
    private javax.swing.JTextField TextFieldMD2A0s7;
    private javax.swing.JTextField TextFieldMD2A1s1;
    private javax.swing.JTextField TextFieldMD2A1s2;
    private javax.swing.JTextField TextFieldMD2A1s3;
    private javax.swing.JTextField TextFieldMD2A1s4;
    private javax.swing.JTextField TextFieldMD2A1s5;
    private javax.swing.JTextField TextFieldMD2A1s6;
    private javax.swing.JTextField TextFieldMD2A1s7;
    private javax.swing.JTextField TextFieldMD2B0s1;
    private javax.swing.JTextField TextFieldMD2B0s2;
    private javax.swing.JTextField TextFieldMD2B0s3;
    private javax.swing.JTextField TextFieldMD2B0s4;
    private javax.swing.JTextField TextFieldMD2B0s5;
    private javax.swing.JTextField TextFieldMD2B0s6;
    private javax.swing.JTextField TextFieldMD2B0s7;
    private javax.swing.JTextField TextFieldMD2B1s1;
    private javax.swing.JTextField TextFieldMD2B1s2;
    private javax.swing.JTextField TextFieldMD2B1s3;
    private javax.swing.JTextField TextFieldMD2B1s4;
    private javax.swing.JTextField TextFieldMD2B1s5;
    private javax.swing.JTextField TextFieldMD2B1s6;
    private javax.swing.JTextField TextFieldMD2B1s7;
    private javax.swing.JTextField TextFieldMD3A0s1;
    private javax.swing.JTextField TextFieldMD3A0s2;
    private javax.swing.JTextField TextFieldMD3A0s3;
    private javax.swing.JTextField TextFieldMD3A0s4;
    private javax.swing.JTextField TextFieldMD3A0s5;
    private javax.swing.JTextField TextFieldMD3A0s6;
    private javax.swing.JTextField TextFieldMD3A0s7;
    private javax.swing.JTextField TextFieldMD3A1s1;
    private javax.swing.JTextField TextFieldMD3A1s2;
    private javax.swing.JTextField TextFieldMD3A1s3;
    private javax.swing.JTextField TextFieldMD3A1s4;
    private javax.swing.JTextField TextFieldMD3A1s5;
    private javax.swing.JTextField TextFieldMD3A1s6;
    private javax.swing.JTextField TextFieldMD3A1s7;
    private javax.swing.JTextField TextFieldMD3B0s1;
    private javax.swing.JTextField TextFieldMD3B0s2;
    private javax.swing.JTextField TextFieldMD3B0s3;
    private javax.swing.JTextField TextFieldMD3B0s4;
    private javax.swing.JTextField TextFieldMD3B0s5;
    private javax.swing.JTextField TextFieldMD3B0s6;
    private javax.swing.JTextField TextFieldMD3B0s7;
    private javax.swing.JTextField TextFieldMD3B1s1;
    private javax.swing.JTextField TextFieldMD3B1s2;
    private javax.swing.JTextField TextFieldMD3B1s3;
    private javax.swing.JTextField TextFieldMD3B1s4;
    private javax.swing.JTextField TextFieldMD3B1s5;
    private javax.swing.JTextField TextFieldMD3B1s6;
    private javax.swing.JTextField TextFieldMD3B1s7;
    private javax.swing.JTextField TextFieldMD4A0s1;
    private javax.swing.JTextField TextFieldMD4A0s2;
    private javax.swing.JTextField TextFieldMD4A0s3;
    private javax.swing.JTextField TextFieldMD4A0s4;
    private javax.swing.JTextField TextFieldMD4A0s5;
    private javax.swing.JTextField TextFieldMD4A0s6;
    private javax.swing.JTextField TextFieldMD4A0s7;
    private javax.swing.JTextField TextFieldMD4A1s1;
    private javax.swing.JTextField TextFieldMD4A1s2;
    private javax.swing.JTextField TextFieldMD4A1s3;
    private javax.swing.JTextField TextFieldMD4A1s4;
    private javax.swing.JTextField TextFieldMD4A1s5;
    private javax.swing.JTextField TextFieldMD4A1s6;
    private javax.swing.JTextField TextFieldMD4A1s7;
    private javax.swing.JTextField TextFieldMD4B0s1;
    private javax.swing.JTextField TextFieldMD4B0s2;
    private javax.swing.JTextField TextFieldMD4B0s3;
    private javax.swing.JTextField TextFieldMD4B0s4;
    private javax.swing.JTextField TextFieldMD4B0s5;
    private javax.swing.JTextField TextFieldMD4B0s6;
    private javax.swing.JTextField TextFieldMD4B0s7;
    private javax.swing.JTextField TextFieldMD4B1s1;
    private javax.swing.JTextField TextFieldMD4B1s2;
    private javax.swing.JTextField TextFieldMD4B1s3;
    private javax.swing.JTextField TextFieldMD4B1s4;
    private javax.swing.JTextField TextFieldMD4B1s5;
    private javax.swing.JTextField TextFieldMD4B1s6;
    private javax.swing.JTextField TextFieldMD4B1s7;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel62;
    private javax.swing.JLabel jLabel63;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel65;
    private javax.swing.JLabel jLabel66;
    private javax.swing.JLabel jLabel67;
    private javax.swing.JLabel jLabel68;
    private javax.swing.JLabel jLabel69;
    private javax.swing.JLabel jLabel70;
    private javax.swing.JLabel jLabel71;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel34;
    private javax.swing.JPanel jPanel36;
    private javax.swing.JPanel jPanel37;
    private javax.swing.JPanel jPanel38;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    // End of variables declaration//GEN-END:variables
}
