#
# Sergio Landete
# Fernando Carrio Argos
# University of Valencia
# fernando.carrio@cern.ch
#
import Tkinter as tk
from matplotlib import style
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import numpy as np
import sys
sys.path.append('./scripts')
from CISshape import CISshape
sys.path.append('./common')
from UpdatePlot import UpdatePlot
 
class CISshapeTab(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self,parent)
        self.configure(background="snow3")
        botonFrame=tk.Frame(self)
        botonFrame.configure(background="snow3")
        botonFrame.pack(side=tk.TOP)
        self.nsamp=16
        
        self.md=tk.IntVar()
        self.stateHGLG=tk.IntVar()
        self.showsamp=tk.IntVar()

        buttonMD1 = tk.Radiobutton(botonFrame, text="MD1", indicatoron = 0, width = 2, padx = 10, variable=self.md, command=lambda:self.animate(), value=0)
        buttonMD2 = tk.Radiobutton(botonFrame, text="MD2", indicatoron = 0, width = 2, padx = 10, variable=self.md, command=lambda:self.animate(), value=1)
        buttonMD3 = tk.Radiobutton(botonFrame, text="MD3", indicatoron = 0, width = 2, padx = 10, variable=self.md, command=lambda:self.animate(), value=2)
        buttonMD4 = tk.Radiobutton(botonFrame, text="MD4", indicatoron = 0, width = 2, padx = 10, variable=self.md, command=lambda:self.animate(), value=3)
        buttonMD1.pack(padx=20, pady=10, ipadx=50, side=tk.LEFT)
        buttonMD2.pack(padx=20, pady=10, ipadx=50, side=tk.LEFT)
        buttonMD3.pack(padx=20, pady=10, ipadx=50, side=tk.LEFT)
        buttonMD4.pack(padx=20, pady=10, ipadx=50, side=tk.LEFT)

        buttonHG = tk.Radiobutton(botonFrame, text="HG", indicatoron = 0, width = 2, padx = 10, variable=self.stateHGLG, command=lambda:self.animate(), value=1)
        buttonLG = tk.Radiobutton(botonFrame, text="LG", indicatoron = 0, width = 2, padx = 10, variable=self.stateHGLG, command=lambda:self.animate(), value=0)
        buttonHG.config(activebackground='dark green', bg='dark green', fg='black')
        buttonLG.config(activebackground='dark red', bg='dark red', fg='black')
        buttonHG.pack(padx=5, pady=10, ipadx=10, side=tk.LEFT)
        buttonLG.pack(padx=5, pady=10, ipadx=10, side=tk.LEFT)

        buttonSeven = tk.Radiobutton(botonFrame, text="7", indicatoron = 0, width = 2, padx = 10, variable=self.showsamp, command=lambda:self.animate(), value=7)
        buttonSixteen = tk.Radiobutton(botonFrame, text="16", indicatoron = 0, width = 2, padx = 10, variable=self.showsamp, command=lambda:self.animate(), value=16)
        buttonSeven.config(activebackground='grey', bg='grey', fg='black')
        buttonSixteen.config(activebackground='grey', bg='grey', fg='black')
        buttonSeven.pack(padx=20, pady=10, ipadx=10, side=tk.LEFT)
        buttonSixteen.pack(padx=0, pady=10, ipadx=10, side=tk.LEFT)
        self.showsamp.set(16)
        buttonRun=tk.Button(botonFrame, text="Run", command=lambda:self.runtest())
        buttonRun.pack(padx=20, pady=10, ipadx=20, side=tk.LEFT)    
        
        style.use("ggplot")
        self.figure=Figure(figsize=(5,5), dpi=100)
        self.samplesLG = np.zeros((4,12,16))
        self.samplesHG = np.zeros((4,12,16)) 
        self.ax1 = 12*[0]
        xaxes= [0, self.nsamp]                                
        yaxes= [0, 4095]
        for i in range (12):
            self.ax1[i] = self.figure.add_subplot(2, 6, i+1)
            self.ax1[i].xaxis.set_ticks_position("bottom")
            self.ax1[i].set_xlabel('Samples',fontsize=10)
            self.ax1[i].xaxis.label.set_color('black')

            self.ax1[i].set_xlim(xaxes)
            self.ax1[i].set_ylim(yaxes)
            for label in self.ax1[i].xaxis.get_ticklabels():
                label.set_fontsize(8)
            for label in self.ax1[i].yaxis.get_ticklabels():
                label.set_fontsize(8)
        self.ax1[0].set_ylabel('ADC counts',fontsize=10)                               
        self.ax1[0].yaxis.label.set_color('black')
        self.ax1[6].set_ylabel('ADC counts',fontsize=10)                               
        self.ax1[6].yaxis.label.set_color('black')

        self.figure.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.4, hspace=0.4)
        self.canvas=FigureCanvasTkAgg(self.figure, self)
        self.canvas.show()
        self.canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH ,expand=True)
        self.canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=True)
        self.animate()

    def runtest(self):
        self.samplesLG, self.samplesHG=CISshape(self.stateHGLG.get())
        self.animate()
            
    def animate(self):
        samplesaux = np.zeros((12,self.nsamp))
        md = self.md.get()

        if self.stateHGLG.get() == 1:
                samplesaux = self.samplesHG[md]
                color = 'g'
                if self.showsamp.get() == 7:
                        nsamp_init = 4
                        max_sample = 7                        
                else:
                        nsamp_init = 0
                        max_sample = 16
        else:
                samplesaux = self.samplesLG[md]
                color = 'r'
                if self.showsamp.get() == 7:
                        nsamp_init = 4
                        max_sample = 7
                else:
                        nsamp_init = 0
                        max_sample = 16                                
        self.figure.clear()
        self.figure.suptitle('MD'+str(md+1))
        
        for i in range (12):
                self.ax1[i] = self.figure.add_subplot(2, 6, i+1)
                self.ax1[i].cla()
                self.ax1[i].xaxis.set_ticks_position("bottom")
                self.ax1[i].clear()
                self.ax1[i].plot(range(max_sample),samplesaux[i][range(nsamp_init, max_sample+nsamp_init)],color, linewidth=2, marker='o')
                if np.argmax(samplesaux[i][range(nsamp_init, max_sample+nsamp_init)]) == (7-nsamp_init):
                        self.ax1[i].set_xlabel('Max in: '+ str(1 + np.argmax(samplesaux[i][range(nsamp_init, max_sample+nsamp_init)])),fontsize=10)
                        self.ax1[i].xaxis.label.set_color('green')
                else:
                        self.ax1[i].set_xlabel('Max in: '+ str(1 + np.argmax(samplesaux[i][range(nsamp_init, max_sample+nsamp_init)])),fontsize=10)                          
                        self.ax1[i].xaxis.label.set_color('red')
                if self.stateHGLG.get() == 1:
                        self.ax1[i].set_title('PMT HG ' + str(i + md*12 + 1),fontsize=10)
                else:
                        self.ax1[i].set_title('PMT LG ' + str(i + md*12 + 1),fontsize=10)
                        self.ax1[i].xaxis.tick_bottom()
                self.ax1[0].set_ylabel('ADC counts',fontsize=10)                               
                self.ax1[0].yaxis.label.set_color('black')
                self.ax1[6].set_ylabel('ADC counts',fontsize=10)                               
                self.ax1[6].yaxis.label.set_color('black')                
                min_value = np.min(samplesaux[i][range(nsamp_init, max_sample+nsamp_init)])
                max_value = np.max(samplesaux[i][range(nsamp_init, max_sample+nsamp_init)])
                if (max_value-min_value)< 5:
                        yaxes= [0, 250]
                        self.ax1[i].set_xlabel('No pulse',fontsize=10)                               
                        self.ax1[i].xaxis.label.set_color('yellow')
                else:
                        yaxes= [min_value -5, max_value + 10]
                        self.ax1[i].set_ylim(yaxes)

                for label in self.ax1[i].xaxis.get_ticklabels():
                        label.set_fontsize(8)
                for label in self.ax1[i].yaxis.get_ticklabels():
                        label.set_fontsize(8)
        del samplesaux
        self.figure.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.4, hspace=0.4)
        self.canvas.draw()       