#
# Sergio Landete
# Fernando Carrio Argos
# University of Valencia
# fernando.carrio@cern.ch
#
import Tkinter as tk
from matplotlib import style
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import numpy as np
import sys
sys.path.append('./scripts')
from ADClinearity import ADClinearity
sys.path.append('./common')
from UpdatePlot import UpdatePlot 

class ADClinTab(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self,parent)
        self.configure(background="snow3")
        botonFrame=tk.Frame(self)
        botonFrame.configure(background="snow3")
        botonFrame.pack(side=tk.TOP)
        self.nsamp=10
        self.md=tk.IntVar()
        self.stateHGLG=tk.IntVar()

        buttonMD1 = tk.Radiobutton(botonFrame, text="MD1", indicatoron = 0, width = 2, padx = 10, variable=self.md, command=lambda:self.animate(), value=0)
        buttonMD2 = tk.Radiobutton(botonFrame, text="MD2", indicatoron = 0, width = 2, padx = 10, variable=self.md, command=lambda:self.animate(), value=1)
        buttonMD3 = tk.Radiobutton(botonFrame, text="MD3", indicatoron = 0, width = 2, padx = 10, variable=self.md, command=lambda:self.animate(), value=2)
        buttonMD4 = tk.Radiobutton(botonFrame, text="MD4", indicatoron = 0, width = 2, padx = 10, variable=self.md, command=lambda:self.animate(), value=3)
        buttonMD1.pack(padx=20, pady=10, ipadx=50, side=tk.LEFT)
        buttonMD2.pack(padx=20, pady=10, ipadx=50, side=tk.LEFT)
        buttonMD3.pack(padx=20, pady=10, ipadx=50, side=tk.LEFT)
        buttonMD4.pack(padx=20, pady=10, ipadx=50, side=tk.LEFT)

        buttonHG = tk.Radiobutton(botonFrame, text="HG", indicatoron = 0, width = 2, padx = 10, variable=self.stateHGLG, command=lambda:self.animate(), value=1)
        buttonLG = tk.Radiobutton(botonFrame, text="LG", indicatoron = 0, width = 2, padx = 10, variable=self.stateHGLG, command=lambda:self.animate(), value=0)
        buttonHG.config(activebackground='dark green', bg='dark green', fg='black')
        buttonLG.config(activebackground='dark red', bg='dark red', fg='black')
        buttonHG.pack(padx=5, pady=10, ipadx=10, side=tk.LEFT)
        buttonLG.pack(padx=5, pady=10, ipadx=10, side=tk.LEFT)

        buttonRun=tk.Button(botonFrame, text="Run", command=lambda:self.runtest())
        buttonRun.pack(padx=20, pady=10, ipadx=20, side=tk.LEFT)
        
        style.use("ggplot")
        self.figure=Figure(figsize=(5,5), dpi=100)                
        self.samplesLG = np.zeros((4,12,self.nsamp))
        self.samplesHG = np.zeros((4,12,self.nsamp)) 
        self.ax1 = 12*[0]
        xaxes= [0, self.nsamp]                                
        yaxes= [0, 4095]
        for i in range (12):
            self.ax1[i] = self.figure.add_subplot(2, 6, i+1)
            self.ax1[i].xaxis.set_ticks_position("bottom")
            self.ax1[i].set_xlabel('Iteration',fontsize=10)
            self.ax1[i].xaxis.label.set_color('black')

            self.ax1[i].set_xlim(xaxes)
            self.ax1[i].set_ylim(yaxes)
            for label in self.ax1[i].xaxis.get_ticklabels():
                label.set_fontsize(8)
            for label in self.ax1[i].yaxis.get_ticklabels():
                label.set_fontsize(8)
        self.ax1[0].set_ylabel('ADC counts',fontsize=10)                               
        self.ax1[0].yaxis.label.set_color('black')
        self.ax1[6].set_ylabel('ADC counts',fontsize=10)                               
        self.ax1[6].yaxis.label.set_color('black')

        self.figure.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.4, hspace=0.4)
        self.canvas=FigureCanvasTkAgg(self.figure, self)
        self.canvas.show()
        self.canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH ,expand=True)
        self.canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=True)
        self.animate()
    ###########---------------------------------------------###############
    def runtest(self):
        print ("Test is running")
        self.samplesLG, self.samplesHG=ADClinearity(self.nsamp)
        self.animate()
    
    def animate(self):
            md = self.md.get()
            xaxes= [0, self.nsamp-1]                                
            yaxes= [0, 4095]
            for i in range (12):
                self.ax1[i].clear()
                self.ax1[i].xaxis.set_ticks_position("bottom")
                                               
                self.ax1[i].yaxis.label.set_color('black')
                self.ax1[i].set_xlabel('Iteration',fontsize=10)
                self.ax1[i].xaxis.label.set_color('black')

                self.ax1[i].set_xlim(xaxes)
                self.ax1[i].set_ylim(yaxes)
                for label in self.ax1[i].xaxis.get_ticklabels():
                    label.set_fontsize(8)
                for label in self.ax1[i].yaxis.get_ticklabels():
                    label.set_fontsize(8)                    
            self.ax1[0].set_ylabel('ADC counts',fontsize=10)
            self.ax1[6].set_ylabel('ADC counts',fontsize=10)
    
            samplesaux = np.zeros((12,self.nsamp))
    
            if self.stateHGLG.get() == 1:
                    samplesaux = self.samplesHG[md]
                    color = 'g'
            else:
                    samplesaux = self.samplesLG[md]
                    color = 'r'
    
            self.figure.suptitle('MD'+str(md+1))
    
            for i in range (12):
                    self.ax1[i] = self.figure.add_subplot(2, 6, i+1)
                    self.ax1[i].xaxis.set_ticks_position("bottom")
                    self.ax1[i].plot(range(self.nsamp),samplesaux[i],color, linewidth=2, marker='o')
                    if self.stateHGLG.get() == 1:
                            self.ax1[i].set_title('PMT HG ' + str(i + md*12 + 1),fontsize=10)
                    else:
                            self.ax1[i].set_title('PMT LG ' + str(i + md*12 + 1),fontsize=10)
                            self.ax1[i].xaxis.tick_bottom()
            del samplesaux
            self.figure.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.4, hspace=0.4)
            self.canvas.draw()