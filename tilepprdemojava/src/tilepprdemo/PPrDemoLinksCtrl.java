/* 
 *  PPrDemo Test Bench Java Client
 *  2019 (c) IFIC-CERN
 */
package tilepprdemo;

import java.awt.Color;
import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.swing.JComboBox;

import ipbusjavalibrary.utilities.ByteTools;

import tilepprjavalibrary.lib.PPrLib;

/**
 *
 * @author Juan Valls {@literal (valls@cern.ch)}
 * @author Fernando Carrió {@literal (fernando.carrio@cern.ch)}
 * @author Alberto Valero {@literal (alberto.valero@cern.ch)}
 */
public class PPrDemoLinksCtrl extends javax.swing.JPanel {

    /**
     * Creates new form PPrDemoLinks class.
     *
     * @param pprlib
     */
    public PPrDemoLinksCtrl(PPrLib pprlib) {

        this.pprlib = pprlib;

        // NetBeans components settings.
        initComponents();

        ComboBoxMD1 = new JComboBox[]{ComboBoxLSMD1A, ComboBoxLSMD1B};
        ComboBoxMD2 = new JComboBox[]{ComboBoxLSMD2A, ComboBoxLSMD2B};
        ComboBoxMD3 = new JComboBox[]{ComboBoxLSMD3A, ComboBoxLSMD3B};
        ComboBoxMD4 = new JComboBox[]{ComboBoxLSMD4A, ComboBoxLSMD4B};

        // First dimension is minidrawer. Second is Side (A,B).
        ComboBoxMD = new JComboBox[4][2];
        for (int i = 0; i < 2; i++) {
            ComboBoxMD[0][i] = ComboBoxMD1[i];
            ComboBoxMD[1][i] = ComboBoxMD2[i];
            ComboBoxMD[2][i] = ComboBoxMD3[i];
            ComboBoxMD[3][i] = ComboBoxMD4[i];
        }

        // Defines instance of ThreadPoolExecutor class to create a thread pool.
        // A thread pool manages a collection of Runnable threads.   
        executor = new ThreadPoolExecutor(
                5,
                5,
                0L,
                TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>(),
                new TBThreadFactory("PPrDemoLinksCtrl"));

        // Defines Runnable ArrayList for QSFP Resets.
        RunnablesResetQSFP = new ArrayList<>();

        // Defines Runnable ArrayList for MD DB Resets.
        RunnablesResetDB = new ArrayList<>();

        // Define Runnables through lambda expressions.
        initDefineRunnables();

        LabelReadLS.setText("");
    }

    /**
     * Defines Runnables for threads. A Runnable object is an interface designed
     * to provide a common protocol for objects that wish to execute code while
     * they are active
     */
    private void initDefineRunnables() {
        RunnableReadLinkSelect = () -> {
            readLinkSelect();
        };

        RunnableWriteLinkSelect = () -> {
            writeLinkSelect();
        };

        for (int i = 0; i < 4; i++) {
            final int iQSFP = i;
            RunnablesResetQSFP.add((Runnable) () -> {
                resetQSFP(iQSFP);
            });
        }

        for (int i = 0; i < 4; i++) {
            final int iMD = i;
            for (int j = 0; j < 2; j++) {
                final int jSide = j;
                RunnablesResetDB.add((Runnable) () -> {
                    resetDB(iMD, jSide);
                });
            }
        }
    }

    /**
     * Reads PPr Link Control register. Then parses read value to get the link
     * control select values for each minidrawer and each side (A or B).
     */
    private synchronized void readLinkSelect() {
        Integer retval;
        String fs;
        String[] strside = {"A", "B"};
        String[] str = {"Automatic", "Side A", "Side B", "Error"};

        // Reads PPr Link Control and Global Resets register.
        Integer val = pprlib.ppr.get_links_control_global_resets();
        fs = "%s %s";
        Console.print_cr(String.format(fs, "PPrDemoGlobal.readLinkSelect -> read status: ",
                (val != null ? "Ok" : "Fail")));

        if (val == null) {
            LabelReadLS.setText("Error");
            LabelReadLS.setForeground(Color.RED);
            return;
        } else {
            LabelReadLS.setText(String.format("0x%08X", val));
            LabelReadLS.setForeground(Color.blue);
        }

        fs = "%s 0x%08X (%d) -> %s";
        Console.print_cr(String.format(fs, "  value read: ",
                val, val, ByteTools.intToString(val, 4)));

        // Loop on minidrawers.
        for (int md = 0; md < 4; md++) {
            // Loop on sides.
            for (int i = 0; i < 2; i++) {
                retval = pprlib.ppr.get_links_control_select(val, md, strside[i]);
                switch (retval) {
                    case 0:
                        ComboBoxMD[md][i].setSelectedIndex(0);
                        break;
                    case 1:
                        ComboBoxMD[md][i].setSelectedIndex(1);
                        break;
                    case 2:
                        ComboBoxMD[md][i].setSelectedIndex(2);
                        break;
                    default:
                        retval = 3;
                }

                fs = "  Link MD%d%s is set to: %d -> %s";
                Console.print_cr(String.format(fs, md + 1, strside[i], retval, str[retval]));
            }
        }
    }

    /**
     * Writes PPr Link Control register for each minidrawer and each side from
     * selected values from ComboBoxes.
     */
    private synchronized void writeLinkSelect() {
        String fs;
        String[] strside = {"A", "B"};
        Integer nval;

        Console.print_cr("PPrDemoLinksCtrl.writeLinkSelect() ->");

        // First reads the PPr Link Control and Global Resets register.
        Integer oval = pprlib.ppr.get_links_control_global_resets();
        fs = "  read status: %s";
        Console.print_cr(String.format(fs, (oval != null ? "Ok" : "Fail")));
        if (oval == null) {
            return;
        }
        fs = "  original register: 0x%08X -> %s";
        Console.print_cr(String.format(fs, oval, ByteTools.intToString(oval, 4)));

        nval = oval;

        // Loop on minidrawers.
        for (int md = 0; md < 4; md++) {
            // Loop on sides.
            for (int i = 0; i < 2; i++) {
                Integer idx = ComboBoxMD[md][i].getSelectedIndex();
                Object selected = ComboBoxMD[md][i].getSelectedItem();
                String mode = selected.toString();
                nval = pprlib.ppr.set_links_control_select(nval, md, strside[i], idx);
                fs = "  Change MD%d Side %s to: %s";
                Console.print_cr(String.format(fs, md + 1, strside[i], mode));
            }
        }

        fs = "  new register: 0x%08X -> %s";
        Console.print_cr(String.format(fs, nval, ByteTools.intToString(nval, 4)));

        // Now writes the PPr Link Control and Global Resets register.
        Boolean ret = pprlib.ppr.set_links_control_global_resets(nval);
        fs = "  Write status: %s";
        Console.print_cr(String.format(fs, (ret ? "Ok" : "Fail")));
    }

    /**
     * Resets selected PPrDemo QSFP transceiver.
     *
     * @param qsfp Input QSFP transceiver to be reset (0 to 3).
     */
    private synchronized void resetQSFP(Integer qsfp) {
        Boolean ret = pprlib.ppr.reset_QSFP_Link(qsfp);
        String fs = "PPrDemoLinksCtrl.resetQSFP(): reset QSFP %d -> %s";
        Console.print_cr(String.format(fs, qsfp + 1, (ret != null ? "Ok" : "Fail")));
    }

    /**
     * Resets selected minidrawer DB Side.
     *
     * @param md Input minidrawer (0 to 3).
     * @param side Input DB side (0 for Side A, 1 for side B).
     */
    private synchronized void resetDB(Integer md, Integer side) {
        String[] strside = {"A", "B"};

        Boolean ret = pprlib.ppr.reset_DB_FPGA(md, side);
        String fs = "PPrDemoLinksCtrl.resetDB(): reset MD %d DB Side %s -> %s";
        Console.print_cr(String.format(fs, md + 1, strside[side], (ret != null ? "Ok" : "Fail")));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    private void initComponents() {//GEN-BEGIN:initComponents
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        ComboBoxLSMD1A = new javax.swing.JComboBox<>();
        jPanel7 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        ComboBoxLSMD1B = new javax.swing.JComboBox<>();
        jPanel8 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        ComboBoxLSMD2A = new javax.swing.JComboBox<>();
        jPanel27 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        ComboBoxLSMD2B = new javax.swing.JComboBox<>();
        jPanel28 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        ComboBoxLSMD3A = new javax.swing.JComboBox<>();
        jPanel29 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        ComboBoxLSMD3B = new javax.swing.JComboBox<>();
        jPanel30 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        ComboBoxLSMD4A = new javax.swing.JComboBox<>();
        jPanel31 = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        ComboBoxLSMD4B = new javax.swing.JComboBox<>();
        jPanel32 = new javax.swing.JPanel();
        ButtonWriteLinkSelect = new javax.swing.JButton();
        ButtonReadLinkSelect = new javax.swing.JButton();
        LabelReadLS = new javax.swing.JLabel();
        jPanel35 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        jPanel12 = new javax.swing.JPanel();
        ButtonResetQSFP1 = new javax.swing.JButton();
        ButtonResetQSFP2 = new javax.swing.JButton();
        ButtonResetQSFP3 = new javax.swing.JButton();
        ButtonResetQSFP4 = new javax.swing.JButton();
        jPanel33 = new javax.swing.JPanel();
        jPanel13 = new javax.swing.JPanel();
        jPanel14 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jPanel15 = new javax.swing.JPanel();
        jPanel18 = new javax.swing.JPanel();
        jPanel20 = new javax.swing.JPanel();
        jPanel21 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jPanel23 = new javax.swing.JPanel();
        ButtonResetMD1DBA = new javax.swing.JButton();
        ButtonResetMD1DBB = new javax.swing.JButton();
        jPanel16 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jPanel17 = new javax.swing.JPanel();
        ButtonResetMD2DBA = new javax.swing.JButton();
        ButtonResetMD2DBB = new javax.swing.JButton();
        jPanel19 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jPanel24 = new javax.swing.JPanel();
        ButtonResetMD3DBA = new javax.swing.JButton();
        ButtonResetMD3DBB = new javax.swing.JButton();
        jPanel25 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jPanel26 = new javax.swing.JPanel();
        ButtonResetMD4DBA = new javax.swing.JButton();
        ButtonResetMD4DBB = new javax.swing.JButton();

        java.awt.GridBagLayout jPanel1Layout = new java.awt.GridBagLayout();
        jPanel1Layout.columnWidths = new int[] {0};
        jPanel1Layout.rowHeights = new int[] {0, 12, 0, 12, 0, 12, 0, 12, 0};
        jPanel1.setLayout(jPanel1Layout);

        java.awt.GridBagLayout jPanel2Layout = new java.awt.GridBagLayout();
        jPanel2Layout.columnWidths = new int[] {0};
        jPanel2Layout.rowHeights = new int[] {0, 10, 0, 10, 0};
        jPanel2.setLayout(jPanel2Layout);

        jPanel3.setLayout(new java.awt.GridBagLayout());

        jLabel1.setText("<html><b>Link Select</b></html>");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel3.add(jLabel1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel2.add(jPanel3, gridBagConstraints);

        jPanel4.setLayout(new java.awt.GridBagLayout());

        java.awt.GridBagLayout jPanel5Layout = new java.awt.GridBagLayout();
        jPanel5Layout.columnWidths = new int[] {0, 14, 0, 14, 0, 14, 0, 14, 0, 14, 0, 14, 0, 14, 0};
        jPanel5Layout.rowHeights = new int[] {0};
        jPanel5.setLayout(jPanel5Layout);

        java.awt.GridBagLayout jPanel6Layout = new java.awt.GridBagLayout();
        jPanel6Layout.columnWidths = new int[] {0};
        jPanel6Layout.rowHeights = new int[] {0, 5, 0};
        jPanel6.setLayout(jPanel6Layout);

        jLabel2.setText("MD1 Side A");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel6.add(jLabel2, gridBagConstraints);

        ComboBoxLSMD1A.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Automatic", "Side A", "Side B" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel6.add(ComboBoxLSMD1A, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel5.add(jPanel6, gridBagConstraints);

        java.awt.GridBagLayout jPanel7Layout = new java.awt.GridBagLayout();
        jPanel7Layout.columnWidths = new int[] {0};
        jPanel7Layout.rowHeights = new int[] {0, 5, 0};
        jPanel7.setLayout(jPanel7Layout);

        jLabel3.setText("MD1 Side B");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel7.add(jLabel3, gridBagConstraints);

        ComboBoxLSMD1B.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Automatic", "Side A", "Side B" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel7.add(ComboBoxLSMD1B, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel5.add(jPanel7, gridBagConstraints);

        java.awt.GridBagLayout jPanel8Layout = new java.awt.GridBagLayout();
        jPanel8Layout.columnWidths = new int[] {0};
        jPanel8Layout.rowHeights = new int[] {0, 5, 0};
        jPanel8.setLayout(jPanel8Layout);

        jLabel4.setText("MD2 Side A");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel8.add(jLabel4, gridBagConstraints);

        ComboBoxLSMD2A.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Automatic", "Side A", "Side B" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel8.add(ComboBoxLSMD2A, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel5.add(jPanel8, gridBagConstraints);

        java.awt.GridBagLayout jPanel27Layout = new java.awt.GridBagLayout();
        jPanel27Layout.columnWidths = new int[] {0};
        jPanel27Layout.rowHeights = new int[] {0, 5, 0};
        jPanel27.setLayout(jPanel27Layout);

        jLabel10.setText("MD2 Side B");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel27.add(jLabel10, gridBagConstraints);

        ComboBoxLSMD2B.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Automatic", "Side A", "Side B" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel27.add(ComboBoxLSMD2B, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel5.add(jPanel27, gridBagConstraints);

        java.awt.GridBagLayout jPanel28Layout = new java.awt.GridBagLayout();
        jPanel28Layout.columnWidths = new int[] {0};
        jPanel28Layout.rowHeights = new int[] {0, 5, 0};
        jPanel28.setLayout(jPanel28Layout);

        jLabel12.setText("MD3 Side A");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel28.add(jLabel12, gridBagConstraints);

        ComboBoxLSMD3A.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Automatic", "Side A", "Side B" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel28.add(ComboBoxLSMD3A, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel5.add(jPanel28, gridBagConstraints);

        java.awt.GridBagLayout jPanel29Layout = new java.awt.GridBagLayout();
        jPanel29Layout.columnWidths = new int[] {0};
        jPanel29Layout.rowHeights = new int[] {0, 5, 0};
        jPanel29.setLayout(jPanel29Layout);

        jLabel13.setText("MD3 Side B");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel29.add(jLabel13, gridBagConstraints);

        ComboBoxLSMD3B.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Automatic", "Side A", "Side B" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel29.add(ComboBoxLSMD3B, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 0;
        jPanel5.add(jPanel29, gridBagConstraints);

        java.awt.GridBagLayout jPanel30Layout = new java.awt.GridBagLayout();
        jPanel30Layout.columnWidths = new int[] {0};
        jPanel30Layout.rowHeights = new int[] {0, 5, 0};
        jPanel30.setLayout(jPanel30Layout);

        jLabel14.setText("MD4 Side A");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel30.add(jLabel14, gridBagConstraints);

        ComboBoxLSMD4A.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Automatic", "Side A", "Side B" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel30.add(ComboBoxLSMD4A, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 0;
        jPanel5.add(jPanel30, gridBagConstraints);

        java.awt.GridBagLayout jPanel31Layout = new java.awt.GridBagLayout();
        jPanel31Layout.columnWidths = new int[] {0};
        jPanel31Layout.rowHeights = new int[] {0, 5, 0};
        jPanel31.setLayout(jPanel31Layout);

        jLabel15.setText("MD4 Side B");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel31.add(jLabel15, gridBagConstraints);

        ComboBoxLSMD4B.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Automatic", "Side A", "Side B" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel31.add(ComboBoxLSMD4B, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 0;
        jPanel5.add(jPanel31, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel4.add(jPanel5, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel2.add(jPanel4, gridBagConstraints);

        java.awt.GridBagLayout jPanel32Layout = new java.awt.GridBagLayout();
        jPanel32Layout.columnWidths = new int[] {0, 15, 0, 15, 0};
        jPanel32Layout.rowHeights = new int[] {0};
        jPanel32.setLayout(jPanel32Layout);

        ButtonWriteLinkSelect.setText("Write");
        ButtonWriteLinkSelect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonWriteLinkSelectActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel32.add(ButtonWriteLinkSelect, gridBagConstraints);

        ButtonReadLinkSelect.setText("Read");
        ButtonReadLinkSelect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonReadLinkSelectActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel32.add(ButtonReadLinkSelect, gridBagConstraints);

        LabelReadLS.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelReadLS.setText("status");
        LabelReadLS.setPreferredSize(new java.awt.Dimension(90, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel32.add(LabelReadLS, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel2.add(jPanel32, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel1.add(jPanel2, gridBagConstraints);

        java.awt.GridBagLayout jPanel35Layout = new java.awt.GridBagLayout();
        jPanel35Layout.columnWidths = new int[] {0};
        jPanel35Layout.rowHeights = new int[] {0};
        jPanel35.setLayout(jPanel35Layout);

        java.awt.GridBagLayout jPanel9Layout = new java.awt.GridBagLayout();
        jPanel9Layout.columnWidths = new int[] {0};
        jPanel9Layout.rowHeights = new int[] {0, 5, 0, 5, 0};
        jPanel9.setLayout(jPanel9Layout);

        jPanel10.setLayout(new java.awt.GridBagLayout());

        jLabel5.setText("<html><b>Link QSFP Global Reset</b></html>");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel10.add(jLabel5, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel9.add(jPanel10, gridBagConstraints);

        jPanel11.setLayout(new java.awt.GridBagLayout());

        java.awt.GridBagLayout jPanel12Layout = new java.awt.GridBagLayout();
        jPanel12Layout.columnWidths = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0};
        jPanel12Layout.rowHeights = new int[] {0};
        jPanel12.setLayout(jPanel12Layout);

        ButtonResetQSFP1.setText("Reset Q1");
        ButtonResetQSFP1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonResetQSFP1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel12.add(ButtonResetQSFP1, gridBagConstraints);

        ButtonResetQSFP2.setText("Reset Q2");
        ButtonResetQSFP2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonResetQSFP2ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel12.add(ButtonResetQSFP2, gridBagConstraints);

        ButtonResetQSFP3.setText("Reset Q3");
        ButtonResetQSFP3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonResetQSFP3ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel12.add(ButtonResetQSFP3, gridBagConstraints);

        ButtonResetQSFP4.setText("Reset Q4");
        ButtonResetQSFP4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonResetQSFP4ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 0;
        jPanel12.add(ButtonResetQSFP4, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel11.add(jPanel12, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel9.add(jPanel11, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel35.add(jPanel9, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel1.add(jPanel35, gridBagConstraints);

        jPanel33.setLayout(new java.awt.GridBagLayout());

        java.awt.GridBagLayout jPanel13Layout = new java.awt.GridBagLayout();
        jPanel13Layout.columnWidths = new int[] {0};
        jPanel13Layout.rowHeights = new int[] {0, 5, 0, 5, 0};
        jPanel13.setLayout(jPanel13Layout);

        jPanel14.setLayout(new java.awt.GridBagLayout());

        jLabel6.setText("<html><b>DB FPGA Link Reset</b></html>");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel14.add(jLabel6, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel13.add(jPanel14, gridBagConstraints);

        jPanel15.setLayout(new java.awt.GridBagLayout());

        java.awt.GridBagLayout jPanel18Layout = new java.awt.GridBagLayout();
        jPanel18Layout.columnWidths = new int[] {0, 14, 0, 14, 0, 14, 0};
        jPanel18Layout.rowHeights = new int[] {0};
        jPanel18.setLayout(jPanel18Layout);

        jPanel20.setLayout(new java.awt.GridBagLayout());

        java.awt.GridBagLayout jPanel21Layout = new java.awt.GridBagLayout();
        jPanel21Layout.columnWidths = new int[] {0};
        jPanel21Layout.rowHeights = new int[] {0, 5, 0};
        jPanel21.setLayout(jPanel21Layout);

        jLabel11.setText("MD1 Reset");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel21.add(jLabel11, gridBagConstraints);

        jPanel23.setLayout(new java.awt.GridBagLayout());

        ButtonResetMD1DBA.setText("Side A");
        ButtonResetMD1DBA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonResetMD1DBAActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel23.add(ButtonResetMD1DBA, gridBagConstraints);

        ButtonResetMD1DBB.setText("Side B");
        ButtonResetMD1DBB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonResetMD1DBBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        jPanel23.add(ButtonResetMD1DBB, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel21.add(jPanel23, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel20.add(jPanel21, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel18.add(jPanel20, gridBagConstraints);

        java.awt.GridBagLayout jPanel16Layout = new java.awt.GridBagLayout();
        jPanel16Layout.columnWidths = new int[] {0};
        jPanel16Layout.rowHeights = new int[] {0, 5, 0};
        jPanel16.setLayout(jPanel16Layout);

        jLabel7.setText("MD2 Reset");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel16.add(jLabel7, gridBagConstraints);

        jPanel17.setLayout(new java.awt.GridBagLayout());

        ButtonResetMD2DBA.setText("Side A");
        ButtonResetMD2DBA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonResetMD2DBAActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel17.add(ButtonResetMD2DBA, gridBagConstraints);

        ButtonResetMD2DBB.setText("Side B");
        ButtonResetMD2DBB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonResetMD2DBBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        jPanel17.add(ButtonResetMD2DBB, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel16.add(jPanel17, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel18.add(jPanel16, gridBagConstraints);

        java.awt.GridBagLayout jPanel19Layout = new java.awt.GridBagLayout();
        jPanel19Layout.columnWidths = new int[] {0};
        jPanel19Layout.rowHeights = new int[] {0, 5, 0};
        jPanel19.setLayout(jPanel19Layout);

        jLabel8.setText("MD3 Reset");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel19.add(jLabel8, gridBagConstraints);

        jPanel24.setLayout(new java.awt.GridBagLayout());

        ButtonResetMD3DBA.setText("Side A");
        ButtonResetMD3DBA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonResetMD3DBAActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel24.add(ButtonResetMD3DBA, gridBagConstraints);

        ButtonResetMD3DBB.setText("Side B");
        ButtonResetMD3DBB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonResetMD3DBBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        jPanel24.add(ButtonResetMD3DBB, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel19.add(jPanel24, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel18.add(jPanel19, gridBagConstraints);

        java.awt.GridBagLayout jPanel25Layout = new java.awt.GridBagLayout();
        jPanel25Layout.columnWidths = new int[] {0};
        jPanel25Layout.rowHeights = new int[] {0, 5, 0};
        jPanel25.setLayout(jPanel25Layout);

        jLabel9.setText("MD4 Reset");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel25.add(jLabel9, gridBagConstraints);

        jPanel26.setLayout(new java.awt.GridBagLayout());

        ButtonResetMD4DBA.setText("Side A");
        ButtonResetMD4DBA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonResetMD4DBAActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel26.add(ButtonResetMD4DBA, gridBagConstraints);

        ButtonResetMD4DBB.setText("Side B");
        ButtonResetMD4DBB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonResetMD4DBBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        jPanel26.add(ButtonResetMD4DBB, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel25.add(jPanel26, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel18.add(jPanel25, gridBagConstraints);

        jPanel15.add(jPanel18, new java.awt.GridBagConstraints());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel13.add(jPanel15, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel33.add(jPanel13, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel1.add(jPanel33, gridBagConstraints);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(170, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(369, Short.MAX_VALUE))
        );
    }//GEN-END:initComponents

    /**
     * Reads PPr Link Control Select register.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonReadLinkSelectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonReadLinkSelectActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksCtrl -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnableReadLinkSelect);
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonReadLinkSelectActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonReadLinkSelectActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonReadLinkSelectActionPerformed

    /**
     * Sends a global reset to PPr QSFP transceiver 1.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonResetQSFP1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonResetQSFP1ActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksCtrl -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetQSFP.get(0));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonResetQ1ActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonResetQ1ActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonResetQSFP1ActionPerformed

    /**
     * Sends a global reset to PPr QSFP transceiver 2.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonResetQSFP2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonResetQSFP2ActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksCtrl -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetQSFP.get(1));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonResetQ2ActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonResetQ2ActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonResetQSFP2ActionPerformed

    /**
     * Sends a global reset to PPr QSFP transceiver 3.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonResetQSFP3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonResetQSFP3ActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksCtrl -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetQSFP.get(2));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonResetQ3ActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonResetQ3ActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonResetQSFP3ActionPerformed

    /**
     * Sends a global reset to PPr QSFP transceiver 4.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonResetQSFP4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonResetQSFP4ActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksCtrl -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetQSFP.get(3));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonResetQ4ActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonResetQ4ActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonResetQSFP4ActionPerformed

    /**
     * Writes PPr Link Control Select register.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonWriteLinkSelectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonWriteLinkSelectActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksCtrl -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnableWriteLinkSelect);
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonWriteLinkSelectActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonWriteLinkSelectActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonWriteLinkSelectActionPerformed

    /**
     * Resets DB FPGA Side A for MD 1.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonResetMD1DBAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonResetMD1DBAActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksCtrl -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetDB.get(0));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonResetMD1DBAActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonResetMD1DBAActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonResetMD1DBAActionPerformed

    /**
     * Resets DB FPGA Side B for MD 1.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonResetMD1DBBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonResetMD1DBBActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksCtrl -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetDB.get(1));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonResetMD1DBBActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonResetMD1DBBActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonResetMD1DBBActionPerformed

    /**
     * Resets DB FPGA Side A for MD 2.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonResetMD2DBAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonResetMD2DBAActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksCtrl -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetDB.get(2));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonResetMD2DBAActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonResetMD2DBAActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonResetMD2DBAActionPerformed

    /**
     * Resets DB FPGA Side B for MD 2.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonResetMD2DBBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonResetMD2DBBActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksCtrl -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetDB.get(3));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonResetMD2DBBActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonResetMD2DBBActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonResetMD2DBBActionPerformed

    /**
     * Resets DB FPGA Side A for MD 3.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonResetMD3DBAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonResetMD3DBAActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksCtrl -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetDB.get(4));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonResetMD3DBAActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonResetMD3DBAActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonResetMD3DBAActionPerformed

    /**
     * Resets DB FPGA Side B for MD 3.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonResetMD3DBBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonResetMD3DBBActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksCtrl -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetDB.get(5));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonResetMD3DBBActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonResetMD3DBBActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonResetMD3DBBActionPerformed

    /**
     * Resets DB FPGA Side A for MD 4.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonResetMD4DBAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonResetMD4DBAActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksCtrl -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetDB.get(6));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonResetMD4DBAActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonResetMD4DBAActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonResetMD4DBAActionPerformed

    /**
     * Resets DB FPGA Side B for MD 4.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonResetMD4DBBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonResetMD4DBBActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoLinksCtrl -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnablesResetDB.get(7));
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonResetMD4DBBActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonResetMD4DBBActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonResetMD4DBBActionPerformed

    /**
     * Declares a PPr class object to communicate with PPr IPbus server.
     */
    private PPrLib pprlib;

    /**
     * Declares a ThreadPoolExecutor object. It executes each submitted task
     * using one of possibly several pooled threads, normally configured using
     * Executors factory methods.
     */
    public static ThreadPoolExecutor executor;

    /**
     * Declares Runnable for readLinkSelect() method.
     */
    Runnable RunnableReadLinkSelect;

    /**
     * Declares Runnable for writeLinkSelect() method.
     */
    Runnable RunnableWriteLinkSelect;

    /**
     * Declares Runnable ArrayList for QSFP Resets.
     */
    private final ArrayList<Runnable> RunnablesResetQSFP;

    /**
     * Declares Runnable ArrayList for MD DB Resets.
     */
    private final ArrayList<Runnable> RunnablesResetDB;

    private static javax.swing.JComboBox[] ComboBoxMD1;
    private static javax.swing.JComboBox[] ComboBoxMD2;
    private static javax.swing.JComboBox[] ComboBoxMD3;
    private static javax.swing.JComboBox[] ComboBoxMD4;

    private static javax.swing.JComboBox[][] ComboBoxMD;

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ButtonReadLinkSelect;
    private javax.swing.JButton ButtonResetMD1DBA;
    private javax.swing.JButton ButtonResetMD1DBB;
    private javax.swing.JButton ButtonResetMD2DBA;
    private javax.swing.JButton ButtonResetMD2DBB;
    private javax.swing.JButton ButtonResetMD3DBA;
    private javax.swing.JButton ButtonResetMD3DBB;
    private javax.swing.JButton ButtonResetMD4DBA;
    private javax.swing.JButton ButtonResetMD4DBB;
    private javax.swing.JButton ButtonResetQSFP1;
    private javax.swing.JButton ButtonResetQSFP2;
    private javax.swing.JButton ButtonResetQSFP3;
    private javax.swing.JButton ButtonResetQSFP4;
    private javax.swing.JButton ButtonWriteLinkSelect;
    private javax.swing.JComboBox<String> ComboBoxLSMD1A;
    private javax.swing.JComboBox<String> ComboBoxLSMD1B;
    private javax.swing.JComboBox<String> ComboBoxLSMD2A;
    private javax.swing.JComboBox<String> ComboBoxLSMD2B;
    private javax.swing.JComboBox<String> ComboBoxLSMD3A;
    private javax.swing.JComboBox<String> ComboBoxLSMD3B;
    private javax.swing.JComboBox<String> ComboBoxLSMD4A;
    private javax.swing.JComboBox<String> ComboBoxLSMD4B;
    private javax.swing.JLabel LabelReadLS;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel23;
    private javax.swing.JPanel jPanel24;
    private javax.swing.JPanel jPanel25;
    private javax.swing.JPanel jPanel26;
    private javax.swing.JPanel jPanel27;
    private javax.swing.JPanel jPanel28;
    private javax.swing.JPanel jPanel29;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel30;
    private javax.swing.JPanel jPanel31;
    private javax.swing.JPanel jPanel32;
    private javax.swing.JPanel jPanel33;
    private javax.swing.JPanel jPanel35;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    // End of variables declaration//GEN-END:variables
}
