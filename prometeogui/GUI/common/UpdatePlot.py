def UpdatePlot(frame, md, hg,xlabel,ylabel):
    for i in range(12):
            frame.ax[i].xaxis.set_ticks_position("bottom")
            frame.ax[i].yaxis.label.set_color('black')
            frame.ax[i].set_xlabel(xlabel,fontsize=10)
            frame.ax[i].xaxis.label.set_color('black')
            for label in frame.ax[i].xaxis.get_ticklabels():
                    label.set_fontsize(8)
            for label in frame.ax[i].yaxis.get_ticklabels():
                    label.set_fontsize(8)                    
    frame.ax[6].set_ylabel(ylabel,fontsize=10)
    frame.ax[0].set_ylabel(ylabel,fontsize=10)


    frame.figure.suptitle('MD'+str(md+1))
    for i in range (12):
        frame.ax[i] = frame.figure.add_subplot(2, 6, i+1)
        frame.ax[i].xaxis.set_ticks_position("bottom")
        if hg == 1:
            frame.ax[i].set_title('PMT HG ' + str(i + md*12 + 1),fontsize=10)
            frame.ax[i].xaxis.tick_bottom()                                
        else:
            frame.ax[i].set_title('PMT LG ' + str(i + md*12 + 1),fontsize=10)
    frame.figure.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.4, hspace=0.4)
    frame.canvas.draw()
    return